SHELL = /bin/bash
MAKE = /usr/bin/make
RM = rm -f
BATS = ./test/modules/bats-core/bin/bats
SHELLCHECK = shellcheck
INSTALL = install
INSTALL_PROGRAM = $(INSTALL)

PARALLEL := $(shell command -v parallel 2> /dev/null)
ifdef PARALLEL
BATS_JOBS = -j 10
endif
BATS_FLAGS = $(BATS_JOBS) -r
BATS_TIMED = -T

.PHONY: test

SHARE_DIR = ./share
LIB_DIR = $(SHARE_DIR)/macbrowz/lib
LAUNCHER_DIR = $(SHARE_DIR)/macbrowz/launcher
SUPPORT_DIR = ./support

RUN_SHELLCHECK := $(SHELLCHECK) -x -P $(LIB_DIR)
TEST_DIRS = test/lib test/launcher_script test/macbrowz test/new_release

prefix = /usr/local
install_bindir = $(prefix)/bin
install_sharedir = $(prefix)/share


build: application

all: clean build

application: launcher

launcher:
	$(MAKE) -C $(LAUNCHER_DIR)

release: build lint test do_release

do_release:
	$(SUPPORT_DIR)/new_release.sh

clean: clean_build

clean_build:
	$(MAKE) -C $(LAUNCHER_DIR) clean

check: lint test

lint: lint-lib lint-launcher-script lint-macbrowz lint-new-release

lint-lib:
	$(RUN_SHELLCHECK) $(LIB_DIR)/*.sh

lint-launcher-script:
	$(RUN_SHELLCHECK) $(LAUNCHER_DIR)/*.sh

lint-macbrowz:
	$(RUN_SHELLCHECK) bin/macbrowz

lint-new-release:
	$(RUN_SHELLCHECK) $(SUPPORT_DIR)/new_release.sh

# run a filtered set of tests
# eg, make f="test name pattern" ftest
ftest:
	$(BATS) -f "$(f)" $(BATS_FLAGS) $(TEST_DIRS)

test:
	$(BATS) $(BATS_TIMED) $(BATS_FLAGS) $(TEST_DIRS)

test-lib:
	$(BATS) $(BATS_FLAGS) test/lib

test-launcher-script:
	$(BATS) $(BATS_FLAGS) test/launcher_script

test-macbrowz:
	$(BATS) $(BATS_FLAGS) test/macbrowz

test-new-release:
	$(BATS) $(BATS_FLAGS) test/new_release


install:
	$(INSTALL_PROGRAM) -dm 755 bin $(DESTDIR)$(install_bindir)
	$(INSTALL_PROGRAM) bin/macbrowz $(DESTDIR)$(install_bindir)/macbrowz
	cd "$(SHARE_DIR)" && find . -type d -exec $(INSTALL_PROGRAM) -dm 755 "{}" "$(DESTDIR)$(install_sharedir)/{}" \;
	cd "$(SHARE_DIR)" && find . -type f -exec $(INSTALL_PROGRAM) -p "{}" "$(DESTDIR)$(install_sharedir)/{}" \;

uninstall:
	$(RM) $(DESTDIR)$(install_bindir)/macbrowz
	$(RM) -r $(DESTDIR)$(install_sharedir)/macbrowz
