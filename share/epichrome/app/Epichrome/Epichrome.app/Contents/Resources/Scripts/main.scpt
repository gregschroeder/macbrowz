JsOsaDAS1.001.00bplist00�Vscripto � / / 
 / / 
 / /     m a i n . j s :   A   J a v a S c r i p t   G U I   f o r   c r e a t i n g   E p i c h r o m e   a p p s . 
 / / 
 / /     C o p y r i g h t   ( C )   2 0 2 1     D a v i d   M a r m o r 
 / / 
 / /     h t t p s : / / g i t h u b . c o m / d m a r m o r / e p i c h r o m e 
 / / 
 / /     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y 
 / /     i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y 
 / /     t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r 
 / /     ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n . 
 / / 
 / /     T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l , 
 / /     b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f 
 / /     M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e 
 / /     G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s . 
 / / 
 / /     Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e 
 / /     a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . 
 
 
 / /   V E R S I O N 
 c o n s t   k V e r s i o n   =   " 2 . 4 . 2 6 [ 0 0 1 ] " ; 
 
 
 / /   J X A   S E T U P 
 c o n s t   k A p p   =   A p p l i c a t i o n . c u r r e n t A p p l i c a t i o n ( ) ; 
 k A p p . i n c l u d e S t a n d a r d A d d i t i o n s   =   t r u e ; 
 c o n s t   k S y s E v e n t s   =   A p p l i c a t i o n ( ' S y s t e m   E v e n t s ' ) ; 
 c o n s t   k F i n d e r   =   A p p l i c a t i o n ( ' F i n d e r ' ) ; 
 
 
 / /   - - -   C O N S T A N T S   - - - 
 
 / /   a p p   I D   i n f o   ( m a x   l e n g t h   f o r   C F B u n d l e I d e n t i f i e r   i s   3 0   c h a r a c t e r s ) 
 c o n s t   k B u n d l e I D B a s e   =   ' o r g . e p i c h r o m e . a p p . ' ; 
 c o n s t   k A p p I D M a x L e n g t h   =   3 0   -   k B u n d l e I D B a s e . l e n g t h ; 
 c o n s t   k A p p I D M i n L e n g t h   =   3 ; 
 c o n s t   k A p p I D I l l e g a l C h a r s R e   =   / [ ^ - a - z A - Z 0 - 9 _ ] / g ; 
 
 / /   s t a t u s   d o t s 
 c o n s t   k D o t S e l e c t e d   =   '%�� ' ;   / /'G� 
 c o n s t   k D o t U n s e l e c t e d   =   '%�� ' ; 
 c o n s t   k D o t A d v a n c e d   =   '&f� ' ; 
 c o n s t   k D o t C u r r e n t   =   '�=�9 ' ; 
 c o n s t   k D o t C h a n g e d   =   '�=�8 ' ; 
 c o n s t   k D o t N e e d s U p d a t e   =   '�=�: ' ; 
 c o n s t   k D o t S u c c e s s   =   '' ' ;   / /  '� 
 c o n s t   k D o t E r r o r   =   '�=ޫ ' ; 
 c o n s t   k D o t S k i p   =   ''� ' ;   / /  %�� 
 c o n s t   k D o t W a r n i n g   =   '&�� ' ; 
 c o n s t   k D o t G e a r   =   '&�� ' ; 
 c o n s t   k D o t I n f o   =   '!9� ' ; 
 
 / /   a p p   i n f o   a n d   t h e i r   s u m m a r y   h e a d e r s 
 c o n s t   k A p p I n f o K e y s   =   { 
         p a t h :   ' P a t h ' , 
         v e r s i o n :   ' V e r s i o n ' , 
         d i s p l a y N a m e :   ' A p p   N a m e ' , 
         s h o r t N a m e :   ' S h o r t   N a m e ' , 
         w i n d o w S t y l e :   ' S t y l e ' , 
         u r l s :   [ ' U R L ' ,   ' T a b s ' ] , 
         r e g i s t e r B r o w s e r :   ' R e g i s t e r   a s   B r o w s e r ' , 
         i c o n :   ' I c o n ' , 
         e n g i n e :   ' A p p   E n g i n e ' , 
         i d :   k D o t G e a r   +   '   A p p   I D   /   D a t a   D i r e c t o r y ' , 
         u p d a t e A c t i o n :   k D o t G e a r   +   '   U p d a t e   A c t i o n ' , 
         d o D a t a B a c k u p :   k D o t G e a r   +   '   B a c k u p   A p p   D a t a ' , 
         s k i p W e l c o m e :   k D o t G e a r   +   '   S k i p   W e l c o m e   P a g e   A f t e r   M i n o r   U p d a t e   o r   E d i t ' 
 } ; 
 
 / /   a p p   d e f a u l t s   &   s e t t i n g s 
 c o n s t   k W i n S t y l e A p p   =   ' A p p   W i n d o w ' ; 
 c o n s t   k W i n S t y l e B r o w s e r   =   ' B r o w s e r   T a b s ' ; 
 c o n s t   k A p p D e f a u l t U R L   =   " h t t p s : / / w w w . g o o g l e . c o m / m a i l / " ; 
 
 c o n s t   k I c o n D E F A U L T   =   0 ; 
 c o n s t   k I c o n C U S T O M     =   1 ; 
 c o n s t   k I c o n A U T O         =   2 ; 
 
 c o n s t   k A d v a n c e d D e f a u l t s   =   { 
         i d :   n u l l , 
         u p d a t e A c t i o n :   ' p r o m p t ' , 
         d o D a t a B a c k u p :   f a l s e , 
         s k i p W e l c o m e :   f a l s e 
 } ; 
 
 c o n s t   k B r o w s e r I n f o   =   { 
         ' c o m . m i c r o s o f t . e d g e m a c ' :   { 
                 s h o r t N a m e :   ' E d g e ' , 
                 d i s p l a y N a m e :   ' M i c r o s o f t   E d g e ' 
         } , 
         ' c o m . v i v a l d i . V i v a l d i ' :   { 
                 s h o r t N a m e :   ' V i v a l d i ' , 
                 d i s p l a y N a m e :   ' V i v a l d i ' 
         } , 
         ' c o m . o p e r a s o f t w a r e . O p e r a ' :   { 
                 s h o r t N a m e :   ' O p e r a ' , 
                 d i s p l a y N a m e :   ' O p e r a ' 
         } , 
         ' c o m . b r a v e . B r o w s e r ' :   { 
                 s h o r t N a m e :   ' B r a v e ' , 
                 d i s p l a y N a m e :   ' B r a v e   B r o w s e r ' 
         } , 
         ' o r g . c h r o m i u m . C h r o m i u m ' :   { 
                 s h o r t N a m e :   ' C h r o m i u m ' , 
                 d i s p l a y N a m e :   ' C h r o m i u m ' 
         } , 
         ' c o m . g o o g l e . C h r o m e ' :   { 
                 s h o r t N a m e :   ' C h r o m e ' , 
                 d i s p l a y N a m e :   ' G o o g l e   C h r o m e ' 
         } 
 } ; 
 c o n s t   k E n g i n e s   =   [ 
         { 
                 t y p e :   ' i n t e r n a l ' , 
                 i d :   ' c o m . b r a v e . B r o w s e r ' 
         } , 
         { 
                 t y p e :   ' e x t e r n a l ' , 
                 i d :   ' c o m . g o o g l e . C h r o m e ' 
         } 
 ] ; 
 c o n s t   k U p d a t e A c t i o n s   =   { 
         p r o m p t :   { 
                 b u t t o n :   ' D i s p l a y   P r o m p t ' , 
                 m e s s a g e :   ' d i s p l a y   t h e   d e f a u l t   u p d a t e   p r o m p t   d i a l o g ' , 
                 v a l u e :   ' ' 
         } , 
         a u t o :   { 
                 b u t t o n :   ' U p d a t e   A u t o m a t i c a l l y ' , 
                 m e s s a g e :   ' b e   a u t o m a t i c a l l y   u p d a t e d   w i t h   n o   p r o m p t ' , 
                 v a l u e :   ' A u t o ' 
         } , 
         n e v e r :   { 
                 b u t t o n :   ' N e v e r   U p d a t e ' , 
                 m e s s a g e :   ' n e v e r   a t t e m p t   t o   u p d a t e   i t s e l f ' , 
                 v a l u e :   ' N e v e r ' 
         } 
 } ; 
 
 / /   l e g a l   i c o n   c o m p   s i z e s   &   b a c k g r o u n d s 
 c o n s t   k E p i I c o n C o m p S t y l e s   =   { 
         s i z e :   [ ' m e d i u m ' ,   ' l a r g e ' ,   ' s m a l l ' ] , 
         b g :   [ ' w h i t e ' ,   ' g r a y ' ,   ' b l a c k ' ] 
 } ; 
 
 / /   s c r i p t   p a t h s 
 c o n s t   k E p i c h r o m e S c r i p t   =   k A p p . p a t h T o R e s o u r c e ( " e p i c h r o m e . s h " ,   { 
         i n D i r e c t o r y : " S c r i p t s "   } ) . t o S t r i n g ( ) ; 
 
 / /   a p p   r e s o u r c e s 
 c o n s t   k E p i c h r o m e I c o n   =   k A p p . p a t h T o R e s o u r c e ( " d r o p l e t . i c n s " ) ; 
 c o n s t   k E p i A p p I c o n   =   k A p p . p a t h T o R e s o u r c e ( ' R u n t i m e / C o n t e n t s / R e s o u r c e s / a p p . i c n s ' ) ; 
 c o n s t   k E p i S c a n I c o n   =   k A p p . p a t h T o R e s o u r c e ( " s c a n . i c n s " ) ; 
 
 / /   g e n e r a l   u t i l i t y 
 c o n s t   k D a y   =   2 4   *   6 0   *   6 0   *   1 0 0 0 ; 
 c o n s t   k I n d e n t   =   '   ' . r e p e a t ( 3 ) ; 
 
 / /   a c t i o n s 
 c o n s t   k A c t i o n C R E A T E   =   1 ; 
 c o n s t   k A c t i o n E D I T   =   2 ; 
 c o n s t   k A c t i o n U P D A T E   =   3 ; 
 
 / /   s t e p   r e s u l t s 
 c o n s t   k S t e p R e s u l t S U C C E S S   =   1 ; 
 c o n s t   k S t e p R e s u l t E R R O R   =   2 ; 
 c o n s t   k S t e p R e s u l t R E P O R T E R R O R   =   3 ; 
 c o n s t   k S t e p R e s u l t Q U I T   =   4 ; 
 c o n s t   k S t e p R e s u l t S K I P   =   5 ; 
 
 
 / /   - - -   G L O B A L   V A R I A B L E S   - - - 
 
 / /   l a s t   r u n   v e r s i o n   o f   E p i c h r o m e 
 l e t   g E p i L a s t V e r s i o n   =   ' ' ; 
 
 / /   E p i c h r o m e   s t a t e 
 l e t   g E p i L a s t D i r   =   { 
         c r e a t e :   n u l l , 
         e d i t :   n u l l , 
         i c o n :   n u l l 
 } 
 
 / /   s t a t e   o f   E p i c h r o m e L o g i n   i n s t a l l a t i o n 
 l e t   g E p i L o g i n S c a n E n a b l e d   =   ' u n s e t ' ; 
 
 / /   l i s t   o f   E p i c h r o m e   l o c a t i o n s   w h e r e   a   d e f a u l t   a p p   d i r e c t o r y   h a s   b e e n   c r e a t e d 
 l e t   g E p i D e f a u l t A p p D i r C r e a t e d   =   [ ] ; 
 
 / /   t h e   l a s t   e r r o r   e n c o u n t e r e d   c h e c k i n g   G i t H u b 
 l e t   g E p i G i t h u b F a t a l E r r o r   =   ' ' ; 
 
 / /   i n f o   f r o m   c o r e . s h 
 l e t   g C o r e I n f o   =   n u l l ; 
 
 / /   i c o n   c o m p o s i t i n g   s e t t i n g s 
 l e t   g I c o n S e t t i n g s   =   { 
         c r o p :   f a l s e , 
         c o m p B i g S u r :   f a l s e , 
         c o m p S i z e :   k E p i I c o n C o m p S t y l e s . s i z e [ 0 ] , 
         c o m p B G :   k E p i I c o n C o m p S t y l e s . b g [ 0 ] 
 } 
 
 / /   n e w   a p p   d e f a u l t s 
 l e t   g A p p I n f o D e f a u l t   =   { 
         d i s p l a y N a m e :   ' M y   E p i c h r o m e   A p p ' , 
         s h o r t N a m e :   f a l s e , 
         w i n d o w S t y l e :   k W i n S t y l e A p p , 
         u r l s :   [ ] , 
         r e g i s t e r B r o w s e r :   f a l s e , 
         i c o n :   k I c o n A U T O , 
         e n g i n e :   k E n g i n e s [ 0 ] , 
         u p d a t e A c t i o n :   k A d v a n c e d D e f a u l t s . u p d a t e A c t i o n , 
         d o D a t a B a c k u p :   k A d v a n c e d D e f a u l t s . d o D a t a B a c k u p , 
         s k i p W e l c o m e :   k A d v a n c e d D e f a u l t s . s k i p W e l c o m e 
 } ; 
 
 l e t   g F i r s t D i a l o g O p t M s g   =   ' \ n \ n ( '   +   k D o t G e a r   +   '   T o   e d i t   E p i c h r o m e   p r e f e r e n c e s ,   h o l d   d o w n   O p t i o n   d u r i n g   l a u n c h . ) ' ; 
 
 
 / /   - - -   F U N C T I O N S   - - - 
 
 / /   - - -   T O P - L E V E L   H A N D L E R S   - - - 
 
 / /   R U N :   h a n d l e r   f o r   w h e n   a p p   i s   r u n   w i t h o u t   d r o p p e d   f i l e s 
 f u n c t i o n   r u n ( )   { 
         m a i n W r a p p e r ( ) ; 
 } 
 
 
 / /   O P E N D O C U M E N T S :   h a n d l e r   f u n c t i o n   f o r   f i l e s   d r o p p e d   o n   t h e   a p p 
 f u n c t i o n   o p e n D o c u m e n t s ( a A p p s )   { 
         m a i n W r a p p e r ( a A p p s ) ; 
 } 
 
 
 / /   - - -   M A I N   F U N C T I O N   - - - 
 
 f u n c t i o n   m a i n W r a p p e r ( a A p p s = [ ] )   { 
         
         l e t   m y E r r ; 
         
         / /   w r a p   e v e r y t h i n g   i n   a   t r y   t o   c a t c h   a l l   e r r o r s 
         t r y   { 
                 
                 / /   r u n   m a i n   a p p 
                 m a i n ( a A p p s ) ; 
                 
                 / /   w r i t e   o u t   p e r s i s t e n t   p r o p e r t i e s 
                 w r i t e P r o p e r t i e s ( ) ; 
                 
         }   c a t c h ( m y E r r )   { 
                 
                 / /   a l l   e r r o r s   c a u g h t   h e r e   s h o u l d   b e   r e p o r t e d 
                 [ u n d e f i n e d ,   m y E r r . m e s s a g e ]   =   e r r I s R e p o r t a b l e ( m y E r r . m e s s a g e ) ; 
                 e r r l o g ( m y E r r . m e s s a g e ,   ' F A T A L ' ) ; 
                 
                 i f   ( d i a l o g ( " F a t a l   e r r o r :   "   +   m y E r r . m e s s a g e ,   { 
                         w i t h T i t l e :   ' E r r o r ' , 
                         w i t h I c o n :   ' s t o p ' , 
                         / /   b u t t o n s :   [ ' R e p o r t   E r r o r   &   Q u i t ' ,   ' Q u i t ' ] ,     / /   $ $ $   D I S A B L E   R E P O R T I N G 
                         b u t t o n s :   [ ' Q u i t ' ] , 
                         / /   c a n c e l B u t t o n :   2 ,     / /   $ $ $   D I S A B L E   R E P O R T I N G 
                         d e f a u l t B u t t o n :   1 
                 } ) . b u t t o n I n d e x   = =   0 )   { 
                         / /   $ $ $   D I S A B L E   R E P O R T I N G 
                         / /   r e p o r t E r r o r ( ' E p i c h r o m e   '   +   k V e r s i o n   +   '   r e p o r t s   f a t a l   e r r o r :   " '   +   m y E r r . m e s s a g e   +   ' " ' ) ; 
                 } 
         } 
         
         d e b u g l o g ( ' E x i t i n g   E p i c h r o m e   '   +   k V e r s i o n   +   ' . ' ) ; 
 } 
 
 f u n c t i o n   m a i n ( a A p p s = [ ] )   { 
                 
         / /   c o l l e c t   m o d i f i e r   k e y s   f o r   b r i n g i n g   u p   p r e f e r e n c e s 
         / /   h t t p s : / / a p p l e . s t a c k e x c h a n g e . c o m / q u e s t i o n s / 3 5 2 2 3 6 / d e t e c t - i f - c t r l - k e y - i s - p r e s s e d 
         O b j C . i m p o r t ( ' C o c o a ' ) ; 
         l e t   i D o P r e f s   =   B o o l e a n ( $ . N S E v e n t . m o d i f i e r F l a g s   &   $ . N S E v e n t M o d i f i e r F l a g O p t i o n ) ; 
         
         / /   b r i n g   a p p   t o   t h e   f r o n t 
         k A p p . a c t i v a t e ( ) ; 
         
         
         / /   A P P   I N I T 
         
         / /   s e t   u p   d a t a   p a t h   &   s e t t i n g s   f i l e 
         g C o r e I n f o   =   { } ; 
         t r y   { 
                 O b j C . i m p o r t ( ' s t d l i b ' ) ; 
                 g C o r e I n f o . d a t a P a t h   =   $ . g e t e n v ( ' H O M E ' )   +   ' / L i b r a r y / A p p l i c a t i o n   S u p p o r t / E p i c h r o m e ' ; 
                 g C o r e I n f o . s e t t i n g s F i l e   =   g C o r e I n f o . d a t a P a t h   +   " / e p i c h r o m e . p l i s t " ; 
         }   c a t c h   ( m y E r r )   { 
                 g C o r e I n f o . d a t a P a t h   =   ' ' ; 
                 g C o r e I n f o . s e t t i n g s F i l e   =   ' ' ; 
         } 
         
         / /   r e a d   i n   p e r s i s t e n t   p r o p e r t i e s 
         r e a d P r o p e r t i e s ( ) ; 
         
         / /   i n i t i a l i z e   c o r e ,   o p t i o n a l l y   c h e c k   g i t h u b   f o r   u p d a t e s 
         l e t   m y I n i t I n f o   =   J S O N . p a r s e ( s h e l l ( 
                 ' c o r e D o I n i t = 1 ' , 
                 ' e p i A c t i o n = i n i t ' , 
                 ' e p i G i t h u b F a t a l E r r o r = '   +   ( g E p i G i t h u b F a t a l E r r o r   ?   ' 1 '   :   ' ' ) 
         ) ) ; 
         
         / /   c h e c k   f o r   c o r e   e r r o r s 
         i f   ( m y I n i t I n f o . c o r e . e r r o r )   { 
                 t h r o w   E r r o r ( m y I n i t I n f o . c o r e . e r r o r ) ; 
         } 
         
         / /   m a k e   s u r e   d a t a   p a t h   m a t c h e s 
         i f   ( m y I n i t I n f o . c o r e . d a t a P a t h   ! =   g C o r e I n f o . d a t a P a t h )   { 
                 t h r o w   E r r o r ( ' G o t   u n e x p e c t e d   E p i c h r o m e   d a t a   p a t h . ' ) ; 
         } 
         
         / /   r e p l a c e   o l d   c o r e   i n f o   w i t h   i n i t   i n f o 
         m y I n i t I n f o . c o r e . s e t t i n g s F i l e   =   g C o r e I n f o . s e t t i n g s F i l e ; 
         g C o r e I n f o   =   m y I n i t I n f o . c o r e ; 
         
         / /   s e t   u p   s e t   i c o n   p a t h s 
         g C o r e I n f o . p r e v i e w I c o n   =   g C o r e I n f o . d a t a P a t h   +   ' / p r e v i e w . i c n s ' ; 
         g C o r e I n f o . a u t o I c o n S o u r c e   =   g C o r e I n f o . d a t a P a t h   +   ' / a u t o i c o n . p n g ' ; 
         g C o r e I n f o . a u t o I c o n T e m p D i r   =   g C o r e I n f o . d a t a P a t h   +   ' / a u t o i c o n _ t m p ' ; 
         
         / /   i n i t   e n g i n e   l i s t 
         f o r   ( l e t   c u r E n g   o f   k E n g i n e s )   { 
                 c u r E n g . b u t t o n   =   e n g i n e N a m e ( c u r E n g ) ; 
         } 
         
         / /   h a n d l e   a n y   G i t H u b   u p d a t e s   f o u n d   d u r i n g   i n i t 
         i f   ( m y I n i t I n f o . g i t h u b )   { 
                 h a n d l e G i t h u b U p d a t e ( m y I n i t I n f o . g i t h u b ) ; 
         } 
         
         
         / /   O F F E R   T O   I N S T A L L   L O G I N   I T E M   I F   N O T   P R E V I O U S L Y   S E T 
         
         l e t   i L o g i n S c a n I s E n a b l e d   =   l o g i n S c a n G e t S t a t e ( ) ; 
         
         i f   ( ( g E p i L o g i n S c a n E n a b l e d   = = =   t r u e )   & &   ! i L o g i n S c a n I s E n a b l e d )   { 
                 l o g i n S c a n S e t S t a t e ( t r u e ) ; 
         }   e l s e   i f   ( g E p i L o g i n S c a n E n a b l e d   = =   ' u n s e t ' )   { 
                 i f   ( i L o g i n S c a n I s E n a b l e d )   { 
                         g E p i L o g i n S c a n E n a b l e d   =   t r u e ; 
                         d i a l o g ( ' T h e   E p i c h r o m e   l o g i n   s c a n   w a s   f o u n d   a l r e a d y   e n a b l e d . \ n \ n '   + 
                         ' I f   y o u   w i s h   t o   d i s a b l e   i t ,   y o u   c a n   d o   s o   b y   h o l d i n g   d o w n   t h e   O p t i o n   k e y   w h i l e   s t a r t i n g   E p i c h r o m e . ' ,   { 
                                 w i t h T i t l e :   ' L o g i n   S c a n   E n a b l e d ' , 
                                 w i t h I c o n :   k E p i S c a n I c o n , 
                                 b u t t o n s :   [ ' O K ' ] , 
                                 d e f a u l t B u t t o n :   1 
                         } ) ; 
                 }   e l s e   { 
                         w h i l e   ( t r u e )   { 
                                 i f   ( d i a l o g ( " B y   d e f a u l t ,   E p i c h r o m e   r u n s   a   b r i e f   s c a n   a t   l o g i n   t o   e n s u r e   y o u r   a p p s   a r e   r e a d y   t o   u s e .   I t   i s   s t r o n g l y   r e c o m m e n d e d   y o u   e n a b l e   t h i s . \ n \ n "   + 
                                 
                                 " Y o u   c a n   c h a n g e   y o u r   s e t t i n g   l a t e r   b y   h o l d i n g   d o w n   t h e   O p t i o n   k e y   w h i l e   s t a r t i n g   E p i c h r o m e . \ n \ n "   + 
                                 
                                 k D o t I n f o   +   "   E p i c h r o m e   2 . 4   a p p s   \ " h o t - s w a p \ "   t h e i r   e n g i n e   i n t o   t h e   a p p   a t   l a u n c h .   I n   m o s t   r e s p e c t s ,   t h i s   i s   a n   i m p r o v e m e n t   o v e r   2 . 3 ,   b u t   i f   y o u r   c o m p u t e r   c r a s h e s   w i t h   a p p s   r u n n i n g ,   t h e y   c a n   b e   l e f t   i n   a n   u n u s a b l e   s t a t e .   T h i s   s c a n   s i l e n t l y   f i x e s   a n y   a p p s   l e f t   i n   t h i s   s t a t e . " ,   { 
                                         w i t h T i t l e :   ' L o g i n   S c a n ' , 
                                         w i t h I c o n :   k E p i S c a n I c o n , 
                                         b u t t o n s :   [ ' E n a b l e ' ,   ' D i s a b l e ' ] , 
                                         d e f a u l t B u t t o n :   1 
                                 } ) . b u t t o n I n d e x   = =   0 )   { 
                                         
                                         / /   e n a b l e   l o g i n   i t e m 
                                         l o g i n S c a n S e t S t a t e ( t r u e ) ; 
                                         b r e a k ; 
                                 }   e l s e   i f   ( c o n f i r m D i s a b l e L o g i n S c a n ( ) )   { 
                                         g E p i L o g i n S c a n E n a b l e d   =   f a l s e ; 
                                         b r e a k ; 
                                 }   e l s e   { 
                                         c o n t i n u e ; 
                                 } 
                         } 
                 } 
         } 
         
         
         / /   R U N   P R E F S   I F   R E Q U E S T E D 
         i f   ( i D o P r e f s )   { 
                 e d i t P r e f e r e n c e s ( ) ; 
         } 
         
         
         / /   H A N D L E   R U N   B O T H   W I T H   A N D   W I T H O U T   D R O P P E D   A P P S 
         
         i f   ( a A p p s . l e n g t h   = =   0 )   { 
                 
                 w h i l e   ( t r u e )   { 
                         / /   n o   d r o p p e d   f i l e s ,   s o   a s k   u s e r   f o r   r u n   m o d e 
                         l e t   m y D l g R e s u l t   =   d i a l o g ( ' W o u l d   y o u   l i k e   t o   c r e a t e   a   n e w   a p p   o r   e d i t   e x i s t i n g   a p p s ? '   +   g F i r s t D i a l o g O p t M s g ,   { 
                                 w i t h T i t l e :   ' S e l e c t   A c t i o n   |   E p i c h r o m e   2 . 4 . 2 6 [ 0 0 1 ] ' , 
                                 w i t h I c o n :   k E p i c h r o m e I c o n , 
                                 b u t t o n s :   [ ' C r e a t e ' ,   ' E d i t ' ,   ' Q u i t ' ] , 
                                 d e f a u l t B u t t o n :   1 , 
                                 c a n c e l B u t t o n :   3 
                         } ) . b u t t o n I n d e x ; 
                         
                         / /   d o n ' t   s h o w   t h i s   a g a i n 
                         g F i r s t D i a l o g O p t M s g   =   ' ' ; 
                         
                         i f   ( m y D l g R e s u l t   = =   0 )   { 
                                 
                                 / /   C r e a t e   b u t t o n 
                                 
                                 r e t u r n   r u n C r e a t e ( ) ; 
                                 
                         }   e l s e   i f   ( m y D l g R e s u l t   = =   1 )   { 
                                 
                                 / /   E d i t / U p d a t e   b u t t o n 
                                 
                                 / /   s h o w   f i l e   s e l e c t i o n   d i a l o g 
                                 l e t   a A p p s   =   f i l e D i a l o g ( ' o p e n ' ,   g E p i L a s t D i r ,   ' e d i t ' ,   { 
                                         w i t h P r o m p t :   ' S e l e c t   a n y   a p p s   y o u   w a n t   t o   e d i t   o r   u p d a t e . ' , 
                                         o f T y p e :   [ " c o m . a p p l e . a p p l i c a t i o n " ] , 
                                         m u l t i p l e S e l e c t i o n s A l l o w e d :   t r u e , 
                                         i n v i s i b l e s :   f a l s e 
                                 } ) ; 
                                 i f   ( ! a A p p s )   { 
                                         / /   c a n c e l e d :   a s k   u s e r   t o   s e l e c t   a c t i o n   a g a i n 
                                         c o n t i n u e ; 
                                 } 
                                 
                                 / /   i f   w e   g o t   h e r e ,   t h e   u s e r   c h o s e   f i l e s 
                                 r e t u r n   r u n E d i t ( a A p p s ) ; 
                                 
                         }   e l s e   { 
                                 i f   ( c o n f i r m Q u i t ( ) )   {   r e t u r n ;   } 
                         } 
                 } 
         }   e l s e   { 
                 
                 / /   w e   h a v e   d r o p p e d   a p p s ,   s o   g o   s t r a i g h t   t o   e d i t 
                 r e t u r n   r u n E d i t ( a A p p s ) ; 
         } 
 } 
 
 
 / /   - - -   C R E A T E   A N D   E D I T   M O D E S   - - - 
 
 / /   R U N C R E A T E :   r u n   i n   c r e a t e   m o d e 
 f u n c t i o n   r u n C r e a t e ( )   { 
         
         / /   c r e a t e   d e f a u l t   a p p   d i r   i f   i t   h a s n ' t   a l r e a d y   b e e n   c r e a t e d 
         
         i f   ( ! g E p i D e f a u l t A p p D i r C r e a t e d . i n c l u d e s ( g C o r e I n f o . e p i P a t h ) )   { 
                 
                 / /   r u n   e p i c h r o m e . s h   t o   c r e a t e   d i r e c t o r y 
                 l e t   m y D e f a u l t A p p D i r   =   s h e l l ( ' e p i A c t i o n = d e f a u l t a p p d i r ' ) ; 
                 
                 / /   h a n d l e   d e f a u l t   a p p   d i r 
                 i f   ( m y D e f a u l t A p p D i r )   { 
                         
                         / /   w e   w o n ' t   t r y   i t   a g a i n   f o r   t h i s   l o c a t i o n   o f   E p i c h r o m e ,   e v e n   i f   t h e r e   w a s   a n   e r r o r 
                         g E p i D e f a u l t A p p D i r C r e a t e d . p u s h ( g C o r e I n f o . e p i P a t h ) ; 
                         
                         / /   s e t   u p   m e s s a g e s 
                         l e t   m y D e v i c e N o t e   =   [ ' B e c a u s e   E p i c h r o m e   i s   i n s t a l l e d   o n   a   d i f f e r e n t   d r i v e   f r o m   t h e   / A p p l i c a t i o n s   f o l d e r ,   y o u r   a p p s   w i l l   n o t   b e   a b l e   t o   u s e   e x t e n s i o n s   l i k e   1 P a s s w o r d   t h a t   r e q u i r e   a   v a l i d a t e d   b r o w s e r .   I f   y o u   w a n t   t o   c r e a t e   a p p s   f o r   u s e   w i t h   1 P a s s w o r d ,   y o u   s h o u l d   f i r s t   m o v e   E p i c h r o m e   t o   / A p p l i c a t i o n s . ' ] ; 
                         
                         i f   ( m y D e f a u l t A p p D i r . s t a r t s W i t h ( ' E R R O R | ' ) )   { 
                                 / /   w a r n   t h e   u s e r 
                                 l e t   m y D l g M e s s a g e   =   ' U n a b l e   t o   c r e a t e   A p p s   f o l d e r .   ( '   +   m y D e f a u l t A p p D i r . s l i c e ( 6 )   +   ' ) ' ; 
                                 
                                 / /   a d d   m e s s a g e s   b a s e d   o n   l o c a t i o n   o f   E p i c h r o m e 
                                 i f   ( g C o r e I n f o . e p i P a t h . s t a r t s W i t h ( ' / A p p l i c a t i o n s / E p i c h r o m e / ' ) )   { 
                                         / /   E p i c h r o m e . a p p   i s   i n   / A p p l i c a t i o n s / E p i c h r o m e 
                                         m y D l g M e s s a g e   + =   ' \ n \ n I t   i s   s t r o n g l y   r e c o m m e n d e d   y o u   c r e a t e   a   s u b f o l d e r   u n d e r   / A p p l i c a t i o n s / E p i c h r o m e   f o r   y o u r   a p p s . ' ; 
                                 }   e l s e   i f   ( ! g C o r e I n f o . w r o n g D e v i c e )   { 
                                         / /   E p i c h r o m e . a p p   i s   i n   / A p p l i c a t i o n s   o r   o n   s a m e   v o l u m e 
                                         m y D l g M e s s a g e   + =   ' \ n \ n I t   i s   s t r o n g l y   r e c o m m e n d e d   y o u   c r e a t e   a   s u b f o l d e r   u n d e r   / A p p l i c a t i o n s   f o r   y o u r   a p p s . ' ; 
                                 }   e l s e   { 
                                         / /   E p i c h r o m e . a p p   i s   o n   a   d i f f e r e n t   v o l u m e   f r o m   / A p p l i c a t i o n s 
                                         m y D l g M e s s a g e   + =   ' \ n \ n A n y   a p p s   y o u   c r e a t e   M U S T   b e   o n   t h e   s a m e   d r i v e   a s   E p i c h r o m e .   '   +   m y D e v i c e N o t e ; 
                                 } 
                                 
                                 / /   s h o w   w a r n i n g 
                                 d i a l o g ( m y D l g M e s s a g e ,   { 
                                         w i t h T i t l e :   ' W a r n i n g ' , 
                                         w i t h I c o n :   ' c a u t i o n ' , 
                                         b u t t o n s :   [ ' O K ' ] , 
                                         d e f a u l t B u t t o n :   1 
                                 } ) ; 
                         }   e l s e   { 
                                 
                                 / /   f l a g   i f   w e   f o u n d   a n   a l r e a d y - e x i s t i n g   a p p   d i r 
                                 l e t   m y D i r E x i s t s   =   f a l s e ; 
                                 i f   ( m y D e f a u l t A p p D i r . s t a r t s W i t h ( ' E X I S T S | ' ) )   { 
                                         m y D i r E x i s t s   =   t r u e ; 
                                         m y D e f a u l t A p p D i r   =   m y D e f a u l t A p p D i r . s l i c e ( 7 ) ; 
                                 } 
                                 
                                 / /   s e t   d e f a u l t   d i r e c t o r i e s 
                                 g E p i L a s t D i r . c r e a t e   =   m y D e f a u l t A p p D i r ; 
                                 i f   ( ! g E p i L a s t D i r . e d i t )   { 
                                         g E p i L a s t D i r . e d i t   =   m y D e f a u l t A p p D i r ; 
                                 } 
                                 
                                 / /   n o t i f y   u s e r   o f   a p p s   f o l d e r 
                                 l e t   m y D l g M e s s a g e ; 
                                 
                                 i f   ( m y D i r E x i s t s )   { 
                                         m y D l g M e s s a g e   =   ' A   f o l d e r   c a l l e d   " '   +   m y D e f a u l t A p p D i r . m a t c h ( ' [ ^ / ] * $ ' ) [ 0 ]   +   ' "   w a s   f o u n d   i n   t h e   l o c a t i o n   w h e r e   E p i c h r o m e   i s   i n s t a l l e d . ' ; 
                                 }   e l s e   { 
                                         m y D l g M e s s a g e   =   ' A   f o l d e r   c a l l e d   " '   +   m y D e f a u l t A p p D i r . m a t c h ( ' [ ^ / ] * $ ' ) [ 0 ]   +   ' "   h a s   b e e n   c r e a t e d   i n   t h e   l o c a t i o n   w h e r e   E p i c h r o m e   i s   i n s t a l l e d . ' ; 
                                 } 
                                 
                                 / /   a d d   w a r n i n g s   b a s e d   o n   l o c a t i o n   o f   E p i c h r o m e 
                                 i f   ( g C o r e I n f o . e p i P a t h . s t a r t s W i t h ( ' / A p p l i c a t i o n s / ' ) )   { 
                                         / /   E p i c h r o m e . a p p   i s   s o m e w h e r e   u n d e r   / A p p l i c a t i o n s 
                                         m y D l g M e s s a g e   + =   ' \ n \ n W h i l e   y o u   d o   n o t   h a v e   t o   k e e p   y o u r   a p p s   t h e r e ,   i t   i s   s t r o n g l y   r e c o m m e n d e d   y o u   d o . ' ; 
                                 }   e l s e   i f   ( ! g C o r e I n f o . w r o n g D e v i c e )   { 
                                         / /   E p i c h r o m e . a p p   i s   n o t   i n   / A p p l i c a t i o n s   b u t   o n   s a m e   v o l u m e 
                                         m y D l g M e s s a g e   + =   ' \ n \ n '   +   k D o t W a r n i n g   +   '   B e c a u s e   i t   i s   n o t   a   s u b f o l d e r   o f   / A p p l i c a t i o n s ,   a p p s   y o u   p u t   t h e r e   w i l l   n o t   b e   a b l e   t o   u s e   e x t e n s i o n s   l i k e   1 P a s s w o r d   t h a t   r e q u i r e   a   v a l i d a t e d   b r o w s e r .   I f   y o u   w a n t   t o   c r e a t e   a p p s   f o r   u s e   w i t h   1 P a s s w o r d ,   t h e y   m u s t   u s e   t h e   e x t e r n a l   e n g i n e   a n d   b e   p u t   i n   a   s u b f o l d e r   o f   / A p p l i c a t i o n s . ' ; 
                                 }   e l s e   { 
                                         / /   E p i c h r o m e . a p p   i s   o n   a   d i f f e r e n t   v o l u m e   f r o m   / A p p l i c a t i o n s 
                                         m y D l g M e s s a g e   + =   ' \ n \ n '   +   k D o t W a r n i n g   +   '   '   +   m y D e v i c e N o t e ; 
                                 } 
                                 
                                 / /   s h o w   w a r n i n g 
                                 d i a l o g ( m y D l g M e s s a g e ,   { 
                                         w i t h T i t l e :   ' A p p   F o l d e r   '   +   ( m y D i r E x i s t s   ?   ' F o u n d '   :   ' C r e a t e d ' ) , 
                                         w i t h I c o n :   k E p i c h r o m e I c o n , 
                                         b u t t o n s :   [ ' O K ' ] , 
                                         d e f a u l t B u t t o n :   1 
                                 } ) ; 
                         } 
                 } 
         } 
 
         / /   i n i t i a l i z e   a p p   i n f o   f r o m   d e f a u l t s 
         l e t   m y I n f o   =   { 
                 s t e p I n f o :   { 
                         a c t i o n :   k A c t i o n C R E A T E , 
                         t i t l e P r e f i x :   ' C r e a t e   A p p ' , 
                         d l g I c o n :   k E p i A p p I c o n , 
                         o r i g I c o n :   k E p i A p p I c o n , 
                         i s O n l y A p p :   t r u e , 
                 } , 
                 a p p I n f o :   g A p p I n f o D e f a u l t 
         } ; 
         u p d a t e A p p I n f o ( m y I n f o ,   O b j e c t . k e y s ( k A p p I n f o K e y s ) ) ; 
 
         / /   r u n   n e w   a p p   b u i l d   s t e p s 
         d o S t e p s ( [ 
                 s t e p C r e a t e D i s p l a y N a m e , 
                 s t e p S h o r t N a m e , 
                 s t e p W i n S t y l e , 
                 s t e p U R L s , 
                 s t e p B r o w s e r , 
                 s t e p I c o n , 
                 s t e p E n g i n e , 
                 s t e p B u i l d 
         ] ,   m y I n f o ) ; 
 } 
 
 
 / /   R U N E D I T :   r u n   i n   e d i t   m o d e 
 f u n c t i o n   r u n E d i t ( a A p p s )   { 
 
         l e t   m y D l g R e s u l t ; 
         l e t   m y A p p s   =   [ ] ; 
         l e t   m y E r r A p p s   =   [ ] ; 
         l e t   m y A p p L i s t   =   [ ] ; 
         l e t   m y E r r A p p L i s t   =   [ ] ; 
         l e t   m y H a s U p d a t e s   =   f a l s e ; 
 
         f o r   ( l e t   c u r A p p P a t h   o f   a A p p s )   { 
 
                 / /   g e t   p a t h   t o   a p p   a s   s t r i n g 
                 c u r A p p P a t h   =   c u r A p p P a t h . t o S t r i n g ( ) ; 
 
                 / /   b r e a k   d o w n   p a t h   i n t o   c o m p o n e n t s 
                 l e t   c u r A p p F i l e I n f o   =   g e t A p p P a t h I n f o ( c u r A p p P a t h ) ; 
 
                 / /   i n i t i a l i z e   a p p   o b j e c t 
                 l e t   c u r A p p   =   { 
                         r e s u l t :   k S t e p R e s u l t S K I P , 
                 } ; 
 
                 t r y   { 
 
                         / /   r u n   c o r e   s c r i p t   t o   i n i t i a l i z e 
                         c u r A p p . a p p I n f o   =   J S O N . p a r s e ( s h e l l ( 
                                 ' e p i A c t i o n = r e a d ' , 
                                 ' e p i A p p P a t h = '   +   c u r A p p P a t h 
                         ) ) ; 
 
                 }   c a t c h ( m y E r r )   { 
                         
                         / /   i f   t h i s   w a s   a   f a t a l   e r r o r   o r   b u g ,   t h r o w   i t 
                         i f   ( m y E r r . m e s s a g e . s t a r t s W i t h ( ' R E P O R T | ' ) )   { 
                                 t h r o w   m y E r r ; 
                         } 
                         
                         / /   s e e   i f   t h i s   i s   a n   e r r o r   t h a t   r e c o m m e n d s   a   r u n   o f   E p i c h r o m e   S c a n 
                         i f   ( m y E r r . m e s s a g e . s t a r t s W i t h ( ' S C A N | ' ) )   { 
                                 m y E r r . m e s s a g e   =   m y E r r . m e s s a g e . s l i c e ( 5 ) ; 
                                 c u r A p p . r e c o m m e n d S c a n   =   t r u e ; 
                         } 
                         
                         / /   r e c o r d   t h e   e r r o r   f o r   t h i s   a p p 
                         c u r A p p . e r r o r   =   m y E r r . m e s s a g e ; 
                         c u r A p p . a p p I n f o   =   { 
                                 f i l e :   c u r A p p F i l e I n f o 
                         } 
                         m y E r r A p p s . p u s h ( c u r A p p ) ; 
                         m y E r r A p p L i s t . p u s h ( k I n d e n t   +   ( c u r A p p . r e c o m m e n d S c a n   ?   k D o t W a r n i n g   :   k D o t E r r o r )   +   '   '   +   c u r A p p . a p p I n f o . f i l e . n a m e   +   ' :   '   +   c u r A p p . e r r o r ) ; 
                         c o n t i n u e ; 
                 } 
 
                 / /   a d d   i n   a p p   f i l e   i n f o 
                 c u r A p p . a p p I n f o . f i l e   =   c u r A p p F i l e I n f o ; 
 
                 / /   d e t e r m i n e   a p p   w i n d o w   s t y l e   a n d   g e t   U R L   l i s t 
                 i f   ( c u r A p p . a p p I n f o . c o m m a n d L i n e [ 0 ]   & & 
                         ( c u r A p p . a p p I n f o . c o m m a n d L i n e [ 0 ] . s t a r t s W i t h ( ' - - a p p = ' ) ) )   { 
                                 c u r A p p . a p p I n f o . w i n d o w S t y l e   =   k W i n S t y l e A p p ; 
                                 c u r A p p . a p p I n f o . u r l s   =   [   c u r A p p . a p p I n f o . c o m m a n d L i n e [ 0 ] . s l i c e ( 6 )   ] ; 
                 }   e l s e   { 
                         c u r A p p . a p p I n f o . w i n d o w S t y l e   =   k W i n S t y l e B r o w s e r ; 
                         c u r A p p . a p p I n f o . u r l s   =   c u r A p p . a p p I n f o . c o m m a n d L i n e ; 
                 } 
                 d e l e t e   c u r A p p . a p p I n f o . c o m m a n d L i n e ; 
 
                 / /   f i l l   o u t   e n g i n e   i n f o 
                 c u r A p p . a p p I n f o . e n g i n e . b u t t o n   =   e n g i n e N a m e ( c u r A p p . a p p I n f o . e n g i n e ) ; 
 
                 / /   s t a r t   s t e p I n f o 
                 l e t   m y D l g I c o n   =   s e t D l g I c o n ( c u r A p p . a p p I n f o ) ; 
                 c u r A p p . s t e p I n f o   =   { 
                         a c t i o n :   k A c t i o n E D I T , 
                         t i t l e P r e f i x :   ' E d i t i n g   " '   +   c u r A p p . a p p I n f o . d i s p l a y N a m e   +   ' " ' , 
                         d l g I c o n :   m y D l g I c o n , 
                         o r i g I c o n :   m y D l g I c o n , 
                         i s O n l y A p p :   f a l s e , 
                         i s L a s t A p p :   f a l s e , 
                 } ; 
 
                 / /   d e t e r m i n e   i f   t h e   a p p ' s   n a m e   m a t c h e s   t h e   d i s p l a y   n a m e 
                 c u r A p p . s t e p I n f o . i s O r i g F i l e n a m e   = 
                         ( c u r A p p . a p p I n f o . f i l e . b a s e . t o L o w e r C a s e ( )   = =   c u r A p p . a p p I n f o . d i s p l a y N a m e . t o L o w e r C a s e ( ) ) ; 
 
                 / /   c h e c k   i f   t h i s   a p p   n e e d s   t o   b e   u p d a t e d 
                 l e t   c u r A p p T e x t   =   c u r A p p . a p p I n f o . f i l e . n a m e ; 
                 i f   ( v c m p ( c u r A p p . a p p I n f o . v e r s i o n ,   k V e r s i o n )   <   0 )   { 
                         m y H a s U p d a t e s   =   t r u e ; 
                         c u r A p p . u p d a t e   =   t r u e ; 
                         c u r A p p T e x t   =   k I n d e n t   +   k D o t N e e d s U p d a t e   +   '   '   +   c u r A p p T e x t   +   '   ( '   +   c u r A p p . a p p I n f o . v e r s i o n   +   ' ) ' ; 
                 }   e l s e   { 
                         c u r A p p . u p d a t e   =   f a l s e ; 
                         c u r A p p T e x t   =   k I n d e n t   +   k D o t C u r r e n t   +   '   '   +   c u r A p p T e x t ; 
                 } 
 
                 / /   a d d   t o   l i s t   o f   a p p s   f o r   s u m m a r y 
                 m y A p p L i s t . p u s h ( c u r A p p T e x t ) ; 
 
                 / /   a d d   t o   l i s t   o f   a p p s   t o   p r o c e s s 
                 m y A p p s . p u s h ( c u r A p p ) ; 
         } 
 
         / /   m a r k   t h e   l a s t   a p p 
         i f   ( m y A p p s . l e n g t h   > =   1 )   { 
                 m y A p p s [ m y A p p s . l e n g t h   -   1 ] . s t e p I n f o . i s L a s t A p p   =   t r u e ; 
         } 
 
         / /   b y   d e f a u l t ,   e d i t   a p p s 
         l e t   m y D o E d i t   =   t r u e ; 
 
         / /   s e t   u p   d i a l o g   t e x t   &   b u t t o n s   d e p e n d i n g   i f   o n e   o r   m u l t i p l e   a p p s 
         l e t   m y D l g M e s s a g e ,   m y D l g T i t l e ,   m y B t n E d i t ,   m y B t n U p d a t e ; 
         l e t   m y D l g I c o n   =   k E p i A p p I c o n ; 
         l e t   m y D l g B u t t o n s   =   [ ] ; 
 
         / /   i f   a n y   a p p s   n e e d   u p d a t i n g ,   g i v e   o p t i o n   t o   o n l y   u p d a t e 
         i f   ( m y H a s U p d a t e s )   { 
 
                 / /   s e t   b a s e   t i t l e 
                 m y D l g T i t l e   =   ' C h o o s e   A c t i o n ' ; 
 
                 / /   s e t   b a s e   b u t t o n s 
                 m y B t n E d i t   =   ' E d i t ' ; 
                 m y B t n U p d a t e   =   ' U p d a t e ' ; 
 
                 i f   ( a A p p s . l e n g t h   = =   1 )   { 
                         m y D l g M e s s a g e   =   ' T h i s   a p p   w i l l   b e   u p d a t e d   f r o m   v e r s i o n   '   +   m y A p p s [ 0 ] . a p p I n f o . v e r s i o n   +   '   t o   '   +   k V e r s i o n   +   ' .   D o   y o u   w a n t   t o   e d i t   t h i s   a p p ,   o r   j u s t   u p d a t e   i t ? ' ; 
                         m y D l g I c o n   =   m y A p p s [ 0 ] . s t e p I n f o . d l g I c o n ; 
                 }   e l s e   { 
                         m y D l g M e s s a g e   =   " Y o u ' v e   s e l e c t e d   a t   l e a s t   o n e   a p p   w i t h   a n   o l d e r   v e r s i o n   ( m a r k e d   "   +   k D o t N e e d s U p d a t e   +   " ) .   E d i t   a n d   u p d a t e   a l l   s e l e c t e d   a p p s ,   o r   j u s t   u p d a t e   t h e   "   +   k D o t N e e d s U p d a t e   +   "   a p p s ? \ n \ n "   +   m y A p p L i s t . j o i n ( ' \ n ' ) ; 
                         m y B t n E d i t   + =   '   A l l ' ; 
                         m y B t n U p d a t e   + =   '   '   +   k D o t N e e d s U p d a t e ; 
                 } 
 
                 / /   s e t   b u t t o n   l i s t 
                 m y D l g B u t t o n s   =   [ m y B t n E d i t ,   m y B t n U p d a t e ] ; 
 
         }   e l s e   i f   ( m y A p p s . l e n g t h   >   0 )   { 
 
                 / /   n o   u p d a t e s ,   s o   o n l y   o f f e r   e d i t i n g 
 
                 / /   s e t   b a s e   t i t l e   a n d   b u t t o n 
                 m y D l g T i t l e   =   ' E d i t ' ; 
                 m y B t n E d i t   =   ' O K ' ; 
 
                 i f   ( a A p p s . l e n g t h   = =   1 )   { 
                         m y D l g M e s s a g e   =   ' C l i c k   O K   t o   b e g i n   e d i t i n g   " '   +   m y A p p s [ 0 ] . a p p I n f o . d i s p l a y N a m e   +   ' " .   A n y   c h a n g e s   w i l l   n o t   b e   a p p l i e d   u n t i l   y o u   c o m p l e t e   t h e   p r o c e s s . ' ; 
                         m y D l g T i t l e   + =   m y A p p s [ 0 ] . s t e p I n f o . t i t l e P r e f i x . r e p l a c e ( / ^ E d i t i n g / , ' ' ) ; 
                         m y D l g I c o n   =   m y A p p s [ 0 ] . s t e p I n f o . d l g I c o n ; 
                 }   e l s e   { 
                         m y D l g M e s s a g e   =   " C l i c k   O K   t o   b e g i n   e d i t i n g   t h e   s e l e c t e d   a p p s .   E a c h   a p p ' s   c h a n g e s   w i l l   b e   a p p l i e d   a s   s o o n   a s   y o u   c o m p l e t e   t h e   p r o c e s s   f o r   t h a t   a p p .   T h e   f o l l o w i n g   a p p s   w i l l   b e   e d i t e d : \ n \ n "   +   m y A p p L i s t . j o i n ( ' \ n ' ) ; 
                         m y D l g T i t l e   + =   '   A p p s ' ; 
                 } 
 
                 / /   s e t   b u t t o n   l i s t 
                 m y D l g B u t t o n s   =   [ m y B t n E d i t ] ; 
         }   e l s e   { 
 
                 / /   n o   a p p s   r e m a i n 
                 m y D l g M e s s a g e   =   ' T h e r e   a r e   n o   a p p s   t o   p r o c e s s . ' ; 
         } 
 
         / /   l i s t   a p p s   w i t h   e r r o r s 
         i f   ( m y E r r A p p s . l e n g t h   >   0 )   { 
                 
                 / /   f l a g   w h e t h e r   t o   r e c o m m e n d   a   s c a n 
                 l e t   m y R e c o m m e n d S c a n   =   m y E r r A p p s . r e d u c e ( ( a c c ,   c u r )   = >   B o o l e a n ( a c c   | |   c u r . r e c o m m e n d S c a n ) ,   f a l s e ) ; 
 
                 i f   ( m y A p p s . l e n g t h   = =   0 )   { 
                         m y D l g T i t l e   =   ' E r r o r ' ; 
                         m y D l g I c o n   =   ' s t o p ' ; 
                         i f   ( m y E r r A p p s . l e n g t h   = =   1 )   { 
                                 m y D l g M e s s a g e   =   ' E r r o r   r e a d i n g   '   +   m y E r r A p p s [ 0 ] . a p p I n f o . f i l e . n a m e   +   ' :   '   +   m y E r r A p p s [ 0 ] . e r r o r ; 
                                 i f   ( m y R e c o m m e n d S c a n )   { 
                                         m y D l g M e s s a g e   + =   ' \ n \ n '   +   k D o t I n f o   +   '   I f   t h e   a p p   i s   r u n n i n g ,   p l e a s e   q u i t   i t .   I f   i t   i s   n o t   r u n n i n g ,   r u n   E p i c h r o m e   S c a n . a p p   t o   d e a c t i v a t e   i t s   e n g i n e .   T h e n   t r y   a g a i n . ' ; 
                                 } 
                         } 
                 } 
 
                 i f   ( ( m y A p p s . l e n g t h   >   0 )   | |   ( m y E r r A p p s . l e n g t h   >   1 ) )   { 
                         m y D l g M e s s a g e   + =   ' \ n \ n '   +   k D o t W a r n i n g   +   '   ' ; 
                         i f   ( m y E r r A p p s . l e n g t h   = =   1 )   { 
                                 m y D l g M e s s a g e   + =   ' T h e r e   w a s   a n   e r r o r   r e a d i n g   t h e   f o l l o w i n g   a p p   a n d   i t   c a n n o t   b e   e d i t e d : \ n \ n ' ; 
                         }   e l s e   { 
                                 m y D l g M e s s a g e   + =   ' T h e r e   w e r e   e r r o r s   r e a d i n g   t h e   f o l l o w i n g   a p p s   a n d   t h e y   c a n n o t   b e   e d i t e d : \ n \ n ' ; 
                         } 
                         m y D l g M e s s a g e   + =   m y E r r A p p L i s t . j o i n ( ' \ n ' ) ; 
                         
                         / /   p o s s i b l y   r e c o m m e n d   s c a n 
                         i f   ( m y R e c o m m e n d S c a n )   { 
                                 m y D l g M e s s a g e   + =   ' \ n \ n '   +   k D o t I n f o   +   '   A t   l e a s t   o n e   o f   t h e s e   a p p s   m a y   b e   r u n n i n g .   I f   s o ,   p l e a s e   q u i t   t h e m .   I f   n o n e   o f   t h e m   a r e   r u n n i n g ,   r u n   E p i c h r o m e   S c a n . a p p   t o   r e s e t   t h e m .   T h e n   t r y   a g a i n . ' ; 
                         } 
                 } 
         } 
 
         / /   a d d   Q u i t   b u t t o n 
         m y D l g B u t t o n s . p u s h ( ' Q u i t ' ) ; 
 
         w h i l e   ( t r u e )   { 
                 / /   s e t   u p   d i a l o g   o p t i o n s 
                 l e t   m y D l g O p t i o n s   =   { 
                         w i t h T i t l e :   m y D l g T i t l e   +   '   |   E p i c h r o m e   2 . 4 . 2 6 [ 0 0 1 ] ' , 
                         w i t h I c o n :   m y D l g I c o n , 
                         b u t t o n s :   m y D l g B u t t o n s , 
                         d e f a u l t B u t t o n :   1 
                 } ; 
                 i f   ( m y D l g B u t t o n s . l e n g t h   >   1 )   {   m y D l g O p t i o n s . c a n c e l B u t t o n   =   m y D l g B u t t o n s . l e n g t h ;   } 
                 
                 / /   d i s p l a y   d i a l o g 
                 m y D l g R e s u l t   =   d i a l o g ( m y D l g M e s s a g e   +   g F i r s t D i a l o g O p t M s g ,   m y D l g O p t i o n s ) . b u t t o n R e t u r n e d ; 
                 
                 / /   o n l y   s h o w   p r e f e r e n c e s   n o t i f i c a t i o n   o n c e 
                 g F i r s t D i a l o g O p t M s g   =   ' ' ; 
                 
                 i f   ( m y D l g R e s u l t   = =   ' Q u i t ' )   { 
                         i f   ( ( m y D l g O p t i o n s . b u t t o n s . l e n g t h   = =   1 )   | |   c o n f i r m Q u i t ( ) )   {   r e t u r n ;   } 
                         c o n t i n u e ; 
                 }   e l s e   i f   ( m y D l g R e s u l t   = =   m y B t n U p d a t e )   { 
                         / /   u p d a t e s   o n l y ,   n o   a p p   e d i t i n g 
                         m y D o E d i t   =   f a l s e ; 
                 } 
 
                 b r e a k ; 
         } 
         
         / /   b u i l d   t e x t   t o   r e p r e s e n t   w i l l   b e   d o n e   ( e d i t i n g   a n d / o r   u p d a t i n g ) 
         l e t   m y A c t i o n T e x t   =   [ ] ; 
         i f   ( m y D o E d i t )   {   m y A c t i o n T e x t . p u s h ( ' e d i t e d ' ) ;   } 
         i f   ( m y H a s U p d a t e s )   {   m y A c t i o n T e x t . p u s h ( ' u p d a t e d ' ) ;   } 
         m y A c t i o n T e x t   =   m y A c t i o n T e x t . j o i n ( '   a n d   ' ) ; 
 
 
         / /   R U N   E D I T / U P D A T E   S T E P S 
 
         / /   m a r k   i f   t h i s   i s   t h e   o n l y   a p p   w e ' r e   p r o c e s s i n g 
         i f   ( ( a A p p s . l e n g t h   = =   1 )   & &   ( m y A p p s . l e n g t h   = =   1 ) )   { 
                 m y A p p s [ 0 ] . s t e p I n f o . i s O n l y A p p   =   t r u e ; 
                 m y A p p s [ 0 ] . s t e p I n f o . a c t i o n T e x t   =   m y A c t i o n T e x t ; 
         } 
 
         f o r   ( l e t   c u r A p p   o f   m y A p p s )   { 
 
                 i f   ( m y D o E d i t )   { 
 
                         / /   c h a n g e   a p p I n f o   t o   o l d A p p I n f o 
                         c u r A p p . o l d A p p I n f o   =   o b j C o p y ( c u r A p p . a p p I n f o ) ; 
                         
                         / /   c o n s i d e r   t h e   a p p ' s   c u r r e n t   I D   t o   b e   a n   a u t o I D 
                         c u r A p p . s t e p I n f o . a u t o I D   =   c u r A p p . a p p I n f o . i d 
                         
                         / /   i n i t i a l i z e   r e l e v a n t   i n f o 
                         u p d a t e A p p I n f o ( c u r A p p ,   O b j e c t . k e y s ( k A p p I n f o K e y s ) ) ; 
 
                         / /   r u n   e d i t   s t e p s 
                         c u r A p p . r e s u l t   =   d o S t e p s ( [ 
                                 s t e p E d i t D i s p l a y N a m e , 
                                 s t e p S h o r t N a m e , 
                                 s t e p W i n S t y l e , 
                                 s t e p U R L s , 
                                 s t e p B r o w s e r , 
                                 s t e p I c o n , 
                                 s t e p E n g i n e , 
                                 s t e p B u i l d 
                         ] ,   c u r A p p ) ; 
                 }   e l s e   { 
 
                         / /   r u n   u p d a t e - o n l y   s t e p 
                         i f   ( c u r A p p . u p d a t e )   { 
                                 c u r A p p . s t e p I n f o . a c t i o n   =   k A c t i o n U P D A T E ; 
                                 c u r A p p . r e s u l t   =   d o S t e p s ( [ s t e p B u i l d ] ,   c u r A p p ) ; 
                         } 
                 } 
 
                 / /   h a n d l e   r e s u l t 
                 i f   ( c u r A p p . r e s u l t   = =   k S t e p R e s u l t Q U I T )   {   b r e a k ;   } 
         } 
 
         / /   b u i l d   s u m m a r y 
         i f   ( a A p p s . l e n g t h   >   1 )   { 
 
                 l e t   m y S u c c e s s T e x t   =   [ ] ,   m y H a s S u c c e s s   =   f a l s e ; 
                 l e t   m y E r r o r T e x t   =   [ ] ,   m y H a s E r r o r   =   f a l s e ; 
                 l e t   m y R e p o r t E r r o r s   =   f a l s e ; 
                 l e t   m y A b o r t T e x t   =   [ ] ,   m y H a s A b o r t   =   f a l s e ; 
 
                 f o r   ( l e t   c u r A p p   o f   m y A p p s )   { 
                         i f   ( c u r A p p . r e s u l t   = =   k S t e p R e s u l t S U C C E S S )   { 
                                 m y S u c c e s s T e x t . p u s h ( k I n d e n t   +   k D o t S u c c e s s   +   '   '   +   c u r A p p . a p p I n f o . d i s p l a y N a m e ) ; 
                                 m y H a s S u c c e s s   =   t r u e ; 
                         }   e l s e   i f   ( c u r A p p . r e s u l t   = =   k S t e p R e s u l t E R R O R )   { 
                                 m y E r r o r T e x t . p u s h ( k I n d e n t   +   k D o t E r r o r   +   '   '   +   c u r A p p . a p p I n f o . d i s p l a y N a m e ) ; 
                                 m y H a s E r r o r   =   t r u e ; 
                         }   e l s e   i f   ( c u r A p p . r e s u l t   = =   k S t e p R e s u l t R E P O R T E R R O R )   { 
                                 m y E r r o r T e x t . p u s h ( k I n d e n t   +   k D o t E r r o r   +   '   '   +   c u r A p p . a p p I n f o . d i s p l a y N a m e ) ; 
                                 m y H a s E r r o r   =   t r u e ; 
                                 / / m y R e p o r t E r r o r s   =   t r u e ;     / /   $ $ $   D I S A B L E   R E P O R T I N G 
                         }   e l s e   i f   ( ( c u r A p p . r e s u l t   = =   k S t e p R e s u l t S K I P )   | |   ( c u r A p p . r e s u l t   = =   k S t e p R e s u l t Q U I T ) )   { 
                                 m y A b o r t T e x t . p u s h ( k I n d e n t   +   k D o t S k i p   +   '   '   +   c u r A p p . a p p I n f o . d i s p l a y N a m e ) ; 
                                 m y H a s A b o r t   =   t r u e ; 
                         } 
                 } 
 
                 l e t   m y D l g M e s s a g e ; 
                 l e t   m y D l g B u t t o n s   =   [ ' Q u i t ' ] ; 
                 l e t   m y D l g O p t i o n s   =   { 
                         w i t h T i t l e :   ' S u m m a r y ' , 
                         w i t h I c o n :   k E p i c h r o m e I c o n , 
                         b u t t o n s :   m y D l g B u t t o n s , 
                         d e f a u l t B u t t o n :   1 
                 } ; 
                 
                 i f   ( ! m y H a s E r r o r   & &   ! m y H a s A b o r t )   { 
                         i f   ( m y H a s S u c c e s s )   { 
                                 m y D l g M e s s a g e   =   ' A l l   a p p s   w e r e   '   +   m y A c t i o n T e x t   +   '   s u c c e s s f u l l y ! ' ; 
                         }   e l s e   { 
                                 m y D l g M e s s a g e   =   ' N o   a p p s   w e r e   p r o c e s s e d ! ' ; 
                         } 
                 }   e l s e   { 
                         
                         / /   i f   r e p o r t a b l e   e r r o r s   e n c o u n t e r e d ,   a d d   r e p o r t   e r r o r   b u t t o n 
                         i f   ( m y R e p o r t E r r o r s )   { 
                                 / /   t h i s   i s   n o w   d e f a u l t ,   &   Q u i t   b u t t o n   i s   c a n c e l 
                                 i f   ( m y E r r o r T e x t . l e n g t h   >   1 )   { 
                                         m y D l g B u t t o n s . u n s h i f t ( ' R e p o r t   E r r o r s   &   Q u i t ' ) ; 
                                 }   e l s e   { 
                                         m y D l g B u t t o n s . u n s h i f t ( ' R e p o r t   E r r o r   &   Q u i t ' ) ; 
                                 } 
                                 m y D l g O p t i o n s . c a n c e l B u t t o n   =   2 ; 
                         } 
                         
                         / /   b u i l d   s u m m a r y   m e s s a g e 
                         m y D l g M e s s a g e   =   [ ] ; 
                         i f   ( m y H a s S u c c e s s )   { 
                                 m y D l g M e s s a g e . p u s h ( ' T h e s e   a p p s   w e r e   '   +   m y A c t i o n T e x t   +   '   s u c c e s s f u l l y : \ n \ n '   +   m y S u c c e s s T e x t . j o i n ( ' \ n ' ) ) ; 
                         } 
                         i f   ( m y H a s E r r o r )   { 
                                 m y D l g M e s s a g e . p u s h ( ' T h e s e   a p p s   e n c o u n t e r e d   e r r o r s : \ n \ n '   +   m y E r r o r T e x t . j o i n ( ' \ n ' ) ) ; 
                         } 
                         i f   ( m y H a s A b o r t )   { 
                                 m y D l g M e s s a g e . p u s h ( ' T h e s e   a p p s   w e r e   s k i p p e d : \ n \ n '   +   m y A b o r t T e x t . j o i n ( ' \ n ' ) ) ; 
                         } 
                         m y D l g M e s s a g e   =   m y D l g M e s s a g e . j o i n ( ' \ n \ n ' ) ; 
                 } 
 
                 / /   s h o w   s u m m a r y 
                 i f   ( ( d i a l o g ( m y D l g M e s s a g e ,   m y D l g O p t i o n s ) . b u t t o n I n d e x   = =   0 )   & &   ( m y D l g B u t t o n s . l e n g t h   = =   2 ) )   { 
                         / /   r e p o r t   e r r o r s     $ $ $   D I S A B L E   R E P O R T I N G 
                         / /   r e p o r t E r r o r ( ' E p i c h r o m e   '   +   k V e r s i o n   +   '   r e p o r t s   e r r o r s   w h i l e   '   +   ( m y D o E d i t   ?   ' e d i t i n g '   :   ' u p d a t i n g ' )   +   '   m u l t i p l e   a p p s ' ) ; 
                 } 
         } 
 } 
 
 
 / /   - - -   S T A R T U P / S H U T D O W N   F U N C T I O N S   - - - 
 
 / /   R E A D P R O P E R T I E S :   r e a d   p r o p e r t i e s   u s e r   d a t a ,   o r   i n i t i a l i z e   a n y   n o t   f o u n d 
 f u n c t i o n   r e a d P r o p e r t i e s ( )   { 
 
         l e t   m y P r o p e r t i e s   =   n u l l ,   m y E r r ; 
         
 	 / /   r e a d   i n   t h e   p r o p e r t y   l i s t 
         i f   ( g C o r e I n f o . s e t t i n g s F i l e )   { 
                 t r y   { 
                         m y P r o p e r t i e s   =   k S y s E v e n t s . p r o p e r t y L i s t F i l e s . b y N a m e ( g C o r e I n f o . s e t t i n g s F i l e ) . c o n t e n t s . v a l u e ( ) ; 
                 }   c a t c h ( m y E r r )   { 
                         / /   i g n o r e 
                         e r r l o g ( ' U n a b l e   t o   r e a d   p r o p e r t i e s :   '   +   m y E r r . m e s s a g e ) ; 
                 } 
         } 
         
         / /   i f   n o   p r o p e r t i e s   r e a d   i n ,   w e ' r e   d o n e 
         i f   ( ! m y P r o p e r t i e s )   {   r e t u r n ;   } 
         
 
         / /   S E T   P R O P E R T I E S   F R O M   T H E   F I L E   A N D   I N I T I A L I Z E   A N Y   P R O P E R T I E S   N O T   F O U N D 
 
         / /   E P I C H R O M E   S E T T I N G S 
         
         / /   l a s t V e r s i o n 
         i f   ( t y p e o f   m y P r o p e r t i e s [ " l a s t V e r s i o n " ]   = = =   ' s t r i n g ' )   { 
                 g E p i L a s t V e r s i o n   =   m y P r o p e r t i e s [ " l a s t V e r s i o n " ] ; 
         } 
         
         / /   l a s t A p p C r e a t e P a t h   ( f a l l   b a c k   t o   o l d   l a s t A p p P a t h ) 
 	 i f   ( t y p e o f   m y P r o p e r t i e s [ " l a s t A p p C r e a t e P a t h " ]   = = =   ' s t r i n g ' )   { 
                 g E p i L a s t D i r . c r e a t e   =   m y P r o p e r t i e s [ " l a s t A p p C r e a t e P a t h " ] ; 
         }   e l s e   i f   ( t y p e o f   m y P r o p e r t i e s [ " l a s t A p p P a t h " ]   = = =   ' s t r i n g ' )   { 
                 g E p i L a s t D i r . c r e a t e   =   m y P r o p e r t i e s [ " l a s t A p p P a t h " ] ; 
         } 
 
         / /   l a s t E d i t A p p P a t h   ( f a l l   b a c k   t o   c r e a t e   p a t h ) 
 	 i f   ( t y p e o f   m y P r o p e r t i e s [ " l a s t A p p E d i t P a t h " ]   = = =   ' s t r i n g ' )   { 
                 g E p i L a s t D i r . e d i t   =   m y P r o p e r t i e s [ " l a s t A p p E d i t P a t h " ] ; 
         }   e l s e   i f   ( g E p i L a s t D i r . c r e a t e )   { 
                 g E p i L a s t D i r . e d i t   =   g E p i L a s t D i r . c r e a t e ; 
         } 
         
         / /   l a s t I c o n P a t h 
         i f   ( t y p e o f   m y P r o p e r t i e s [ " l a s t I c o n P a t h " ]   = = =   ' s t r i n g ' )   { 
                 g E p i L a s t D i r . i c o n   =   m y P r o p e r t i e s [ " l a s t I c o n P a t h " ] ; 
         } 
         
         / /   l o g i n S c a n E n a b l e d 
 	 i f   ( t y p e o f   m y P r o p e r t i e s [ " l o g i n S c a n E n a b l e d " ]   = = =   ' b o o l e a n ' )   { 
                 g E p i L o g i n S c a n E n a b l e d   =   m y P r o p e r t i e s [ " l o g i n S c a n E n a b l e d " ] ; 
         } 
         
         / /   d e f a u l t A p p D i r C r e a t e d 
 	 i f   ( m y P r o p e r t i e s [ " d e f a u l t A p p D i r C r e a t e d " ]   i n s t a n c e o f   A r r a y )   { 
                 g E p i D e f a u l t A p p D i r C r e a t e d   =   m y P r o p e r t i e s [ " d e f a u l t A p p D i r C r e a t e d " ] ; 
         } 
         
         / /   g i t h u b F a t a l E r r o r 
 	 i f   ( t y p e o f   m y P r o p e r t i e s [ " g i t h u b F a t a l E r r o r " ]   = = =   ' s t r i n g ' )   { 
                 g E p i G i t h u b F a t a l E r r o r   =   m y P r o p e r t i e s [ " g i t h u b F a t a l E r r o r " ] ; 
         } 
 
         / /   i c o n   c o m p o s i t t i n g   s e t t i n g 
         i f   ( t y p e o f   m y P r o p e r t i e s [ " i c o n C r o p " ]   = = =   ' b o o l e a n ' )   { 
                 g I c o n S e t t i n g s . c r o p   =   m y P r o p e r t i e s [ " i c o n C r o p " ] ; 
         } 
         i f   ( t y p e o f   m y P r o p e r t i e s [ " i c o n C o m p B i g S u r " ]   = = =   ' b o o l e a n ' )   { 
                 g I c o n S e t t i n g s . c o m p B i g S u r   =   m y P r o p e r t i e s [ " i c o n C o m p B i g S u r " ] ; 
         } 
         i f   ( t y p e o f   m y P r o p e r t i e s [ " i c o n C o m p S i z e " ]   = = =   ' s t r i n g ' )   { 
                 g I c o n S e t t i n g s . c o m p S i z e   =   m y P r o p e r t i e s [ " i c o n C o m p S i z e " ] ; 
         } 
         i f   ( t y p e o f   m y P r o p e r t i e s [ " i c o n B a c k g r o u n d " ]   = = =   ' s t r i n g ' )   { 
                 g I c o n S e t t i n g s . c o m p B G   =   m y P r o p e r t i e s [ " i c o n B a c k g r o u n d " ] ; 
         } 
 
 
         / /   A P P   P R E F E R E N C E S 
 
         / /   a p p S t y l e   ( D O N ' T   S T O R E   F O R   N O W ) 
         / /   i f   ( m y P r o p e r t i e s [ " a p p S t y l e " ] )   { 
         / /           g A p p I n f o D e f a u l t . w i n d o w S t y l e   =   m y P r o p e r t i e s [ " a p p S t y l e " ] ; 
         / /   } 
 
         l e t   m y Y e s N o R e g e x = / ^ ( Y e s | N o ) $ / ; 
 
 	 / /   d o R e g i s t e r B r o w s e r 
 	 i f   ( m y Y e s N o R e g e x . t e s t ( m y P r o p e r t i e s [ " d o R e g i s t e r B r o w s e r " ] ) )   { 
                 g A p p I n f o D e f a u l t . r e g i s t e r B r o w s e r   =   ( m y P r o p e r t i e s [ " d o R e g i s t e r B r o w s e r " ]   ! =   ' N o ' ) ; 
         } 
 
 	 / /   d o C u s t o m I c o n 
 	 i f   ( m y P r o p e r t i e s [ " d o C u s t o m I c o n " ]   = =   ' Y e s ' )   { 
                 g A p p I n f o D e f a u l t . i c o n   =   k I c o n C U S T O M ; 
         }   e l s e   i f   ( m y P r o p e r t i e s [ " d o C u s t o m I c o n " ]   = =   ' N o ' )   { 
                 g A p p I n f o D e f a u l t . i c o n   =   k I c o n D E F A U L T ; 
         }   e l s e   { 
                 g A p p I n f o D e f a u l t . i c o n   =   k I c o n A U T O ; 
         } 
         
         / /   u p d a t e A c t i o n 
         i f   ( ( t y p e o f   m y P r o p e r t i e s [ " u p d a t e A c t i o n " ]   = = =   ' s t r i n g ' )   & & 
                 k U p d a t e A c t i o n s . h a s O w n P r o p e r t y ( m y P r o p e r t i e s [ " u p d a t e A c t i o n " ] ) )   { 
                 g A p p I n f o D e f a u l t . u p d a t e A c t i o n   =   m y P r o p e r t i e s [ " u p d a t e A c t i o n " ] ; 
         } 
         
         / /   d o D a t a B a c k u p 
         i f   ( t y p e o f   m y P r o p e r t i e s [ " d o D a t a B a c k u p " ]   = = =   ' b o o l e a n ' )   { 
                 g A p p I n f o D e f a u l t . d o D a t a B a c k u p   =   m y P r o p e r t i e s [ " d o D a t a B a c k u p " ] ; 
         } 
 
         / /   s k i p W e l c o m e 
         i f   ( t y p e o f   m y P r o p e r t i e s [ " s k i p W e l c o m e " ]   = = =   ' b o o l e a n ' )   { 
                 g A p p I n f o D e f a u l t . s k i p W e l c o m e   =   m y P r o p e r t i e s [ " s k i p W e l c o m e " ] ; 
         } 
 } 
 
 
 / /   H A N D L E G I T H U B U P D A T E :   h a n d l e   a n y   G i t H u b   u p d a t e s   r e t u r n e d   f r o m   c o r e   i n i t 
 f u n c t i o n   h a n d l e G i t h u b U p d a t e ( a G i t h u b I n f o )   { 
         
         i f   ( a G i t h u b I n f o . h a s O w n P r o p e r t y ( ' v e r s i o n ' ) )   { 
                 
                 / /   N E W   V E R S I O N   F O U N D 
                 
                 / /   v a r i a b l e s   t o   p a s s   b a c k   f o r   w r i t i n g   i n f o   f i l e 
                 l e t   m y N e x t V e r s i o n   =   ' ' ,   m y G i t h u b D i a l o g E r r   =   ' ' ; 
                 
                 l e t   m y E r r ; 
                 
                 t r y   { 
                         
                         l e t   m y D l g R e s u l t ; 
                         
                         t r y   { 
                                 m y D l g R e s u l t   =   d i a l o g ( a G i t h u b I n f o . m e s s a g e ,   { 
                                         w i t h T i t l e :   ' U p d a t e   A v a i l a b l e ' , 
                                         w i t h I c o n :   k E p i c h r o m e I c o n , 
                                         b u t t o n s : [ ' D o w n l o a d ' ,   ' R e m i n d   M e   L a t e r ' ,   ' I g n o r e   T h i s   V e r s i o n ' ] , 
                                         d e f a u l t B u t t o n :   1 , 
                                         c a n c e l B u t t o n :   2 
                                 } ) . b u t t o n I n d e x ; 
                         }   c a t c h ( m y E r r )   { 
                                 t h r o w   E r r o r ( ' U n a b l e   t o   d i s p l a y   u p d a t e   d i a l o g . ' ) ; 
                         } 
                                 
                         i f   ( m y D l g R e s u l t   = =   0 )   { 
                                 
                                 / /   D O W N L O A D   b u t t o n 
                                 
                                 / /   o p e n   U R L s 
                                 t r y   { 
                                         f o r   ( l e t   c u r U r l   o f   a G i t h u b I n f o . u r l s )   { 
                                                 k A p p . o p e n L o c a t i o n ( c u r U r l ) ; 
                                         } 
                                 }   c a t c h ( m y E r r )   { 
                                         / /   U R L   o p e n   d i d n ' t   w o r k   - -   s t i l l   c o n s i d e r   t h i s   v e r s i o n   d o w n l o a d e d 
                                         e r r l o g ( ' U n a b l e   t o   o p e n   u p d a t e   U R L .   ( '   +   m y E r r . m e s s a g e   +   ' ) ' ) ; 
                                         t r y   { 
                                                 d i a l o g ( ' U n a b l e   t o   o p e n   u p d a t e   p a g e   o n   G i t H u b .   P l e a s e   t r y   d o w n l o a d i n g   t h i s   u p d a t e   y o u r s e l f   a t   t h e   f o l l o w i n g   U R L : \ n \ n '   +   a G i t h u b I n f o . u r l s [ 1 ] ,   { 
                                                         w i t h T i t l e :   ' U n a b l e   T o   D o w n l o a d ' , 
                                                         w i t h I c o n :   ' c a u t i o n ' , 
                                                         b u t t o n s :   [ ' O K ' ] , 
                                                         d e f a u l t B u t t o n :   1 
                                                 } ) ; 
                                         }   c a t c h ( m y E r r )   { 
                                                 / /   i g n o r e   e r r o r s   w i t h   t h i s   d i a l o g 
                                                 e r r l o g ( ' U n a b l e   t o   d i s p l a y   G i t H u b   e r r o r   d i a l o g :   '   +   m y E r r . m e s s a g e ) ; 
                                         } 
                                 } 
                                 
                                 / /   m a r k   t h i s   v e r s i o n   a s   d o w n l o a d e d 
                                 m y N e x t V e r s i o n   =   a G i t h u b I n f o . v e r s i o n ; 
                                 
                         }   e l s e   i f   ( m y D l g R e s u l t   = =   1 )   { 
                                 
                                 / /   L A T E R   b u t t o n   - -   j u s t   c l e a r   n e x t   v e r s i o n 
                         }   e l s e   { 
                                 
                                 / /   I G N O R E   b u t t o n   - -   m a r k   t h i s   v e r s i o n   a s   d o w n l o a d e d 
                                 m y N e x t V e r s i o n   =   a G i t h u b I n f o . v e r s i o n ; 
                         } 
                 }   c a t c h ( m y E r r )   { 
                         m y G i t h u b D i a l o g E r r   =   m y E r r . m e s s a g e ; 
                         m y N e x t V e r s i o n   =   a G i t h u b I n f o . p r e v G i t h u b V e r s i o n ; 
                         e r r l o g ( m y E r r . m e s s a g e ) ; 
                 } 
                 
                 / /   w r i t e   t o   G i t H u b   i n f o   f i l e 
                 l e t   m y W r i t e R e s u l t   =   s h e l l ( ' e p i A c t i o n = g i t h u b r e s u l t ' , 
                         ' e p i G i t h u b D i a l o g E r r = '   +   m y G i t h u b D i a l o g E r r , 
                         ' e p i C h e c k D a t e = '   +   a G i t h u b I n f o . c h e c k D a t e , 
                         ' e p i G i t h u b L a s t E r r o r = '   +   a G i t h u b I n f o . l a s t E r r o r , 
                         ' e p i N e x t V e r s i o n = '   +   m y N e x t V e r s i o n ) ; 
                 
                 / /   i f   a n y   e r r o r s   a r e   r e t u r n e d ,   h a n d l e   t h e m 
                 i f   ( m y W r i t e R e s u l t )   { 
                         a G i t h u b I n f o   =   J S O N . p a r s e ( m y W r i t e R e s u l t ) ; 
                 } 
         } 
         
         
         / /   H A N D L E   G I T H U B   U P D A T E   E R R O R S 
         
         i f   ( a G i t h u b I n f o . h a s O w n P r o p e r t y ( ' e r r o r ' ) )   { 
                 
                 / /   i f   t h e r e ' s   a   w a r n i n g   m e s s a g e ,   d i s p l a y   a l e r t 
                 i f   ( a G i t h u b I n f o . e r r o r )   { 
                         d i a l o g ( a G i t h u b I n f o . e r r o r ,   { 
                                 w i t h T i t l e :   ' C h e c k i n g   F o r   U p d a t e ' , 
                                 w i t h I c o n :   ' c a u t i o n ' , 
                                 b u t t o n s :   [ ' O K ' ] , 
                                 d e f a u l t B u t t o n :   1 
                         } ) ; 
                 } 
                 
                 / /   i f   t h i s   i s   a   f a t a l   e r r o r ,   s a v e   t h a t   i n f o 
                 g E p i G i t h u b F a t a l E r r o r   =   a G i t h u b I n f o . i s F a t a l ; 
                 
                 / /   f l a g   a n   e r r o r 
                 r e t u r n   f a l s e ; 
         } 
         
         / /   s u c c e s s 
         r e t u r n   t r u e ; 
 } 
 
 
 / /   W R I T E P R O P E R T I E S :   w r i t e   p r o p e r t i e s   b a c k   t o   p l i s t   f i l e 
 f u n c t i o n   w r i t e P r o p e r t i e s ( )   { 
         
         / /   i f   w e   d o n ' t   h a v e   a   s e t t i n g s   f i l e ,   w e ' r e   d o n e 
         i f   ( ! g C o r e I n f o . s e t t i n g s F i l e )   {   r e t u r n ;   } 
         
         l e t   m y P r o p e r t i e s ,   m y E r r ; 
         
         d e b u g l o g ( " W r i t i n g   p r e f e r e n c e s . " ) ; 
         
         t r y   { 
                 / /   c r e a t e   e m p t y   p l i s t   f i l e 
                 m y P r o p e r t i e s   =   k S y s E v e n t s . P r o p e r t y L i s t F i l e ( { 
                         n a m e :   g C o r e I n f o . s e t t i n g s F i l e 
                 } ) . m a k e ( ) ; 
                 
                 
                 / /   E P I C H R O M E   S T A T E 
                 
                 / /   o u r   v e r s i o n   o f   E p i c h r o m e   i s   n o w   t h e   l a s t   t o   r u n 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " l a s t V e r s i o n " , 
                                 v a l u e : k V e r s i o n 
                         } ) 
                 ) ; 
                 
                 / /   l a s t - u s e d   a p p   p a t h s 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " l a s t A p p C r e a t e P a t h " , 
                                 v a l u e : g E p i L a s t D i r . c r e a t e 
                         } ) 
                 ) ; 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " l a s t A p p E d i t P a t h " , 
                                 v a l u e : g E p i L a s t D i r . e d i t 
                         } ) 
                 ) ; 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d :   " s t r i n g " , 
                                 n a m e :   " l a s t I c o n P a t h " , 
                                 v a l u e :   g E p i L a s t D i r . i c o n 
                         } ) 
                 ) ; 
                 
                 i f   ( t y p e o f   g E p i L o g i n S c a n E n a b l e d   = = =   ' b o o l e a n ' )   { 
                         m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                                 k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                         k i n d :   " b o o l e a n " , 
                                         n a m e :   " l o g i n S c a n E n a b l e d " , 
                                         v a l u e :   g E p i L o g i n S c a n E n a b l e d 
                                 } ) 
                         ) ; 
                 } 
                 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d :   " l i s t " , 
                                 n a m e :   " d e f a u l t A p p D i r C r e a t e d " , 
                                 v a l u e :   g E p i D e f a u l t A p p D i r C r e a t e d 
                         } ) 
                 ) ; 
                 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " g i t h u b F a t a l E r r o r " , 
                                 v a l u e : g E p i G i t h u b F a t a l E r r o r 
                         } ) 
                 ) ; 
                 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " b o o l e a n " , 
                                 n a m e : " i c o n C r o p " , 
                                 v a l u e : g I c o n S e t t i n g s . c r o p 
                         } ) 
                 ) ; 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " b o o l e a n " , 
                                 n a m e : " i c o n C o m p B i g S u r " , 
                                 v a l u e : g I c o n S e t t i n g s . c o m p B i g S u r 
                         } ) 
                 ) ; 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " i c o n C o m p S i z e " , 
                                 v a l u e : ( g I c o n S e t t i n g s . c o m p S i z e   ?   g I c o n S e t t i n g s . c o m p S i z e   :   ' ' ) 
                         } ) 
                 ) ; 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " i c o n B a c k g r o u n d " , 
                                 v a l u e : g I c o n S e t t i n g s . c o m p B G 
                         } ) 
                 ) ; 
 
                 
                 / /   A P P   P R E F E R E N C E S 
                 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " a p p S t y l e " , 
                                 v a l u e : g A p p I n f o D e f a u l t . w i n d o w S t y l e 
                         } ) 
                 ) ; 
                 l e t   m y R e g i s t e r B r o w s e r   =   ( g A p p I n f o D e f a u l t . r e g i s t e r B r o w s e r   ?   ' Y e s '   :   ' N o ' ) ; 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d :   " s t r i n g " , 
                                 n a m e :   " d o R e g i s t e r B r o w s e r " , 
                                 v a l u e :   m y R e g i s t e r B r o w s e r 
                         } ) 
                 ) ; 
                 l e t   m y I c o n   =   g A p p I n f o D e f a u l t . i c o n ; 
                 i f   ( m y I c o n   = =   k I c o n D E F A U L T )   { 
                         m y I c o n   =   ' N o ' ; 
                 }   e l s e   i f   ( m y I c o n   = =   k I c o n C U S T O M )   { 
                         m y I c o n   =   ' Y e s ' ; 
                 }   e l s e   { 
                         m y I c o n   =   ' A u t o ' ; 
                 } 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " d o C u s t o m I c o n " , 
                                 v a l u e :   m y I c o n 
                         } ) 
                 ) ; 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " a p p E n g i n e T y p e " , 
                                 v a l u e : g A p p I n f o D e f a u l t . e n g i n e . t y p e 
                         } ) 
                 ) ; 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d : " s t r i n g " , 
                                 n a m e : " a p p E n g i n e I D " , 
                                 v a l u e : g A p p I n f o D e f a u l t . e n g i n e . i d 
                         } ) 
                 ) ; 
                 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d :   " s t r i n g " , 
                                 n a m e :   " u p d a t e A c t i o n " , 
                                 v a l u e :   g A p p I n f o D e f a u l t . u p d a t e A c t i o n 
                         } ) 
                 ) ; 
 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d :   " b o o l e a n " , 
                                 n a m e :   " d o D a t a B a c k u p " , 
                                 v a l u e :   g A p p I n f o D e f a u l t . d o D a t a B a c k u p 
                         } ) 
                 ) ; 
                 
                 m y P r o p e r t i e s . p r o p e r t y L i s t I t e m s . p u s h ( 
                         k S y s E v e n t s . P r o p e r t y L i s t I t e m ( { 
                                 k i n d :   " b o o l e a n " , 
                                 n a m e :   " s k i p W e l c o m e " , 
                                 v a l u e :   g A p p I n f o D e f a u l t . s k i p W e l c o m e 
                         } ) 
                 ) ; 
                 
         }   c a t c h ( m y E r r )   { 
                 / /   i g n o r e   e r r o r s ,   w e   j u s t   w o n ' t   h a v e   p e r s i s t e n t   p r o p e r t i e s 
                 e r r l o g ( ' U n a b l e   t o   w r i t e   p r o p e r t i e s :   '   +   m y E r r . m e s s a g e ) ; 
         } 
 } 
 
 
 / /   - - -   B U I L D   S T E P S   - - - 
 
 / /   D O S T E P S :   r u n   a   s e t   o f   b u i l d   o r   e d i t   s t e p s 
 f u n c t i o n   d o S t e p s ( a S t e p s ,   a I n f o ,   a O p t i o n s = { } )   { 
         / /   R U N   T H E   S T E P S   T O   B U I L D   T H E   A P P 
         
         / /   s t a r t   o n   s t e p   0 
         l e t   m y N e x t S t e p   =   0 ; 
         
         / /   a s s u m e   s u c c e s s 
         l e t   m y R e s u l t   =   k S t e p R e s u l t S U C C E S S ; 
         
         l e t   m y S t e p R e s u l t ; 
         
         / /   l e t   t h e   s t e p s   k n o w   h o w   m a n y   o f   t h e m   t h e r e   a r e 
         a I n f o . s t e p I n f o . n u m S t e p s   =   a S t e p s . l e n g t h ; 
         
         / /   i n i t i a l i z e   s t e p   n u m b e r s 
         a I n f o . s t e p I n f o . c u r S t e p N u m b e r   =   m y N e x t S t e p ; 
         a I n f o . s t e p I n f o . p r e v S t e p N u m b e r   =   - 1 ; 
         
         w h i l e   ( t r u e )   { 
 
                 / /   b a c k e d   o u t   o f   t h e   f i r s t   s t e p 
                 i f   ( m y N e x t S t e p   <   0 )   { 
                         
                         / /   i f   n o   c o n f i r m 
                         i f   ( a O p t i o n s . a b o r t S i l e n t )   { 
                                 r e t u r n   k S t e p R e s u l t S K I P ; 
                         } 
                         
                         / /   n o r m a l i z e 
                         m y N e x t S t e p   =   - 1 ; 
 
                         l e t   m y D l g B u t t o n s   =   [ ' N o ' ,   ' Y e s ' ] ; 
                         l e t   m y D l g M e s s a g e   =   ' A r e   y o u   s u r e   y o u   w a n t   t o   ' ; 
                         l e t   m y D l g T i t l e   =   ' C o n f i r m   ' ; 
                         l e t   m y R e s u l t   =   k S t e p R e s u l t Q U I T ; 
 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
                                 m y D l g M e s s a g e   =   ' T h e   a p p   h a s   n o t   b e e n   c r e a t e d .   '   +   m y D l g M e s s a g e   +   ' q u i t ? ' ; 
                                 m y D l g T i t l e   + =   ' Q u i t ' ; 
                         }   e l s e   { 
 
                                 i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                                         m y D l g M e s s a g e   =   ' Y o u   h a v e   n o t   f i n i s h e d   e d i t i n g   " '   +   a I n f o . a p p I n f o . d i s p l a y N a m e   +   ' " .   Y o u r   c h a n g e s   h a v e   n o t   b e e n   s a v e d '   +   ( a I n f o . u p d a t e   ?   '   a n d   t h e   a p p   w i l l   n o t   b e   u p d a t e d .   '   :   ' .   ' )   +   m y D l g M e s s a g e ; 
                                 }   e l s e   { 
                                         m y D l g M e s s a g e   =   ' Y o u   c a n c e l e d   t h e   u p d a t e   o f   " '   +   a I n f o . a p p I n f o . d i s p l a y N a m e   +   ' " .   T h e   a p p   h a s   n o t   b e e n   u p d a t e d .   '   +   m y D l g M e s s a g e ; 
                                 } 
 
                                 i f   ( a I n f o . s t e p I n f o . i s L a s t A p p )   { 
                                         m y D l g M e s s a g e   + =   ' q u i t ? ' ; 
                                         m y D l g T i t l e   + =   ' Q u i t ' ; 
                                 }   e l s e   { 
                                         m y D l g M e s s a g e   + =   ' s k i p   t h i s   a p p ? ' ; 
                                         m y D l g T i t l e   + =   ' S k i p ' ; 
                                         m y D l g B u t t o n s . p u s h ( ' Q u i t ' ) ; 
                                         m y R e s u l t   =   k S t e p R e s u l t S K I P ; 
                                 } 
                         } 
 
                         l e t   m y D l g R e s u l t ; 
 
                         / /   c o n f i r m   b a c k - o u t 
                         m y D l g R e s u l t   =   d i a l o g ( m y D l g M e s s a g e ,   { 
                                 w i t h T i t l e :   m y D l g T i t l e , 
                                 w i t h I c o n :   a I n f o . s t e p I n f o . d l g I c o n , 
                                 b u t t o n s :   m y D l g B u t t o n s , 
                                 d e f a u l t B u t t o n :   2 , 
                                 c a n c e l B u t t o n :   1 
                         } ) . b u t t o n I n d e x ; 
 
                         / /   r e t u r n   a p p r o p r i a t e   v a l u e 
                         i f   ( m y D l g R e s u l t   = =   0 )   { 
                                 / /   N o   b u t t o n   - -   c o n t i n u e   w i t h   s t e p s 
                                 m y N e x t S t e p   - =   m y S t e p R e s u l t ; 
                         }   e l s e   i f   ( m y D l g R e s u l t   = =   1 )   { 
                                 / /   Y e s   b u t t o n   - -   e n d   s t e p s 
                                 r e t u r n   m y R e s u l t ; 
                         }   e l s e   { 
                                 / /   Q u i t   b u t t o n   - -   s e n d   Q u i t   r e s u l t 
                                 r e t u r n   k S t e p R e s u l t Q U I T ; 
                         } 
                 }   e l s e   i f   ( m y N e x t S t e p   > =   a S t e p s . l e n g t h )   { 
                         
                         / /   p a s t   t h e   e n d   o f   o u r   s t e p s ,   s o   w e ' r e   d o n e 
                         b r e a k ; 
                 } 
                 
                 i f   ( m y N e x t S t e p   = =   0 )   { 
                         i f   ( a O p t i o n s . a b o r t B a c k B u t t o n )   { 
                                 a I n f o . s t e p I n f o . b a c k B u t t o n   =   a O p t i o n s . a b o r t B a c k B u t t o n ; 
                         }   e l s e   { 
                                 i f   ( a I n f o . s t e p I n f o . i s O n l y A p p )   { 
                                         a I n f o . s t e p I n f o . b a c k B u t t o n   =   ' Q u i t ' ; 
                                 }   e l s e   { 
                                         a I n f o . s t e p I n f o . b a c k B u t t o n   =   ' A b o r t ' ; 
                                 } 
                         } 
                 }   e l s e   { 
                         a I n f o . s t e p I n f o . b a c k B u t t o n   =   ' B a c k ' ; 
                 } 
                 
                 / /   s e t   s t e p   n u m b e r 
                 i f   ( a I n f o . s t e p I n f o . r e s t a r t S t e p )   { 
                         / /   s i m u l a t e   g o i n g   f o r w a r d 
                         a I n f o . s t e p I n f o . p r e v S t e p N u m b e r   =   m y N e x t S t e p   -   1 ; 
                         a I n f o . s t e p I n f o . r e s t a r t S t e p   =   f a l s e ; 
                 }   e l s e   { 
                         a I n f o . s t e p I n f o . p r e v S t e p N u m b e r   =   a I n f o . s t e p I n f o . c u r S t e p N u m b e r ; 
                 } 
                 a I n f o . s t e p I n f o . c u r S t e p N u m b e r   =   m y N e x t S t e p ; 
                 
                 i f   ( a I n f o . s t e p I n f o . a c t i o n   ! =   k A c t i o n U P D A T E )   { 
 
                         / /   u p d a t e   s t e p   n u m b e r   u n l e s s   t h e s e   a r e   s u b s t e p s 
                         i f   ( ! a O p t i o n s . i s S u b S t e p )   { 
                                 a I n f o . s t e p I n f o . n u m T e x t   =   ( a O p t i o n s . s t e p T i t l e   ?   a O p t i o n s . s t e p T i t l e   :   ' S t e p ' )   + 
                                 '   '   +   ( m y N e x t S t e p   +   1 ) . t o S t r i n g ( )   + 
                                 '   o f   '   +   a S t e p s . l e n g t h . t o S t r i n g ( ) ; 
                         } 
                         
                         / /   s e t   s t e p   d i a l o g   t i t l e 
                         a I n f o . s t e p I n f o . d l g T i t l e   =   a I n f o . s t e p I n f o . t i t l e P r e f i x   +   '   |   '   +   a I n f o . s t e p I n f o . n u m T e x t ; 
                         
                         / /   i n   e d i t   m o d e ,   a d d   s t a t u s   d o t 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                                 l e t   m y S t a t u s D o t   =   ( a I n f o . a p p I n f o S t a t u s . v e r s i o n . c h a n g e d   ?   k D o t N e e d s U p d a t e   :   k D o t C u r r e n t ) ; 
                                 i f   ( O b j e c t . k e y s ( a I n f o . a p p I n f o S t a t u s ) . f i l t e r ( x   = >   x   ! =   ' v e r s i o n ' ) . r e d u c e ( ( a , v )   = >   a   | |   a I n f o . a p p I n f o S t a t u s [ v ] . c h a n g e d ,   f a l s e ) )   { 
                                         m y S t a t u s D o t   =   k D o t C h a n g e d ; 
                                 } 
                                 a I n f o . s t e p I n f o . d l g T i t l e   =   m y S t a t u s D o t   +   '   '   +   a I n f o . s t e p I n f o . d l g T i t l e ; 
                         } 
                 } 
                 
                 / /   R U N   T H E   N E X T   S T E P 
 
                 m y S t e p R e s u l t   =   a S t e p s [ m y N e x t S t e p ] ( a I n f o ) ; 
                 
                 
                 / /   C H E C K   R E S U L T   O F   S T E P 
 
                 i f   ( t y p e o f ( m y S t e p R e s u l t )   = =   ' n u m b e r ' )   { 
 
                         / /   F O R W A R D   O R   B A C K :   M O V E   O N   T O   A N O T H E R   S T E P 
 
                         / /   m o v e   f o r w a r d ,   b a c k w a r d ,   o r   s t a y   o n   t h e   s a m e   s t e p 
                         m y N e x t S t e p   + =   m y S t e p R e s u l t ; 
 
                         c o n t i n u e ; 
 
                 }   e l s e   i f   ( m y S t e p R e s u l t   i n s t a n c e o f   O b j e c t )   { 
 
                         / /   S T E P   R E T U R N E D   E R R O R 
                         
                         / /   $ $ $   D I S A B L E   R E P O R T I N G 
                         m y S t e p R e s u l t . r e p o r t E r r o r   =   f a l s e ; 
                         
                         e r r l o g ( m y S t e p R e s u l t . m e s s a g e ,   m y S t e p R e s u l t . b a c k S t e p   ?   ' E R R O R '   :   ' F A T A L ' ) ; 
                         
                         m y R e s u l t   =   ( m y S t e p R e s u l t . r e p o r t E r r o r   ?   k S t e p R e s u l t R E P O R T E R R O R   :   k S t e p R e s u l t E R R O R ) ; 
                         
                         l e t   m y D l g B u t t o n s ,   m y D l g O p t i o n s ,   m y D l g R e s u l t ; 
                         
                         / /   s e t   u p   b u t t o n s 
                         i f   ( a I n f o . s t e p I n f o . i s O n l y A p p )   { 
                                 i f   ( m y S t e p R e s u l t . r e p o r t E r r o r )   { 
                                         m y D l g B u t t o n s   =   [ ' R e p o r t   E r r o r   &   Q u i t ' ,   ' Q u i t ' ] ; 
                                 }   e l s e   { 
                                         m y D l g B u t t o n s   =   [ ' Q u i t ' ] ; 
                                 } 
                         }   e l s e   { 
                                 m y D l g B u t t o n s   =   [ ' S k i p ' ] ; 
                         } 
                         
                         m y D l g O p t i o n s   =   { 
                                 w i t h T i t l e :   m y S t e p R e s u l t . t i t l e , 
                                 w i t h I c o n :   ' s t o p ' , 
                                 b u t t o n s :   m y D l g B u t t o n s , 
                                 d e f a u l t B u t t o n :   1 
                         } ; 
                         
                         / /   i f   w e ' r e   a l l o w e d   t o   b a c k s t e p   &   t h i s   i s n ' t   t h e   f i r s t   s t e p 
                         i f   ( ( m y S t e p R e s u l t . b a c k S t e p   ! = =   f a l s e )   & &   ( m y N e x t S t e p   ! =   0 ) )   { 
 
                                 / /   s h o w   B a c k   b u t t o n   t o o 
                                 m y D l g B u t t o n s . u n s h i f t ( ' B a c k ' ) ; 
 
                                 / /   d i a l o g   w i t h   B a c k   b u t t o n 
                                 m y D l g R e s u l t   =   d i a l o g ( m y S t e p R e s u l t . m e s s a g e ,   m y D l g O p t i o n s ) . b u t t o n I n d e x ; 
                                 
                                 / /   h a n d l e   d i a l o g   r e s u l t 
                                 i f   ( m y D l g R e s u l t   = =   0 )   { 
                                         / /   B a c k   b u t t o n 
                                         m y N e x t S t e p   + =   m y S t e p R e s u l t . b a c k S t e p ; 
                                         c o n t i n u e ; 
                                 }   e l s e   { 
                                         m y D l g R e s u l t - - ;     / /   c o n f o r m   w i t h   n o n - b a c k - b u t t o n   d i a l o g 
                                 } 
                         }   e l s e   { 
                                 
                                 / /   i f   Q u i t   b u t t o n   e x i s t s ,   m a k e   i t   c a n c e l 
                                 i f   ( m y D l g B u t t o n s . l e n g t h   = =   2 )   { 
                                         m y D l g O p t i o n s . c a n c e l B u t t o n   =   2 ; 
                                 } 
                                 
                                 / /   d i a l o g   w i t h   n o   B a c k   b u t t o n 
                                 m y D l g R e s u l t   =   d i a l o g ( m y S t e p R e s u l t . m e s s a g e ,   m y D l g O p t i o n s ) . b u t t o n I n d e x ; 
                         } 
                         
                         i f   ( a I n f o . s t e p I n f o . i s O n l y A p p   & &   m y S t e p R e s u l t . r e p o r t E r r o r   & &   ( m y D l g R e s u l t   = =   0 ) )   { 
                                 / /   u s e r   c l i c k e d   ' R e p o r t   E r r o r   &   Q u i t ' 
                                 / /   $ $ $   D I S A B L E   R E P O R T I N G 
                                 / /   r e p o r t E r r o r ( ' E p i c h r o m e   '   +   k V e r s i o n   +   '   r e p o r t s   a n   e r r o r   '   + 
                                 / /           ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   ?   ' c r e a t i n g '   : 
                                 / /                   ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   ?   ' e d i t i n g '   :   ' u p d a t i n g ' ) )   + 
                                 / /           '   a n   a p p :   " '   +   e r r I s R e p o r t a b l e ( m y S t e p R e s u l t . e r r o r . m e s s a g e ) [ 1 ]   +   ' " ' ) ; 
                         } 
                 } 
                 
                 / /   d o n e 
                 b r e a k ; 
         } 
 
         r e t u r n   m y R e s u l t ; 
 } 
 
 
 / /   S T E P C R E A T E D I S P L A Y N A M E :   s t e p   f u n c t i o n   t o   g e t   d i s p l a y   n a m e   a n d   a p p   p a t h   f o r   C R E A T E   a c t i o n 
 f u n c t i o n   s t e p C r e a t e D i s p l a y N a m e ( a I n f o )   { 
 
         / /   s t a t u s   v a r i a b l e 
 	 l e t   m y E r r ; 
         
         
         / /   C H O O S E   W H E R E   T O   S A V E   T H E   A P P 
 
         l e t   m y D l g O p t i o n s   =   { 
                 w i t h P r o m p t :   a I n f o . s t e p I n f o . n u m T e x t   +   ' :   S e l e c t   n a m e   a n d   l o c a t i o n   f o r   t h e   a p p . ' , 
                 d e f a u l t N a m e :   a I n f o . a p p I n f o . d i s p l a y N a m e 
         } ; 
 
         l e t   m y T r y A g a i n   =   t r u e ; 
         w h i l e   ( m y T r y A g a i n )   { 
 
                 m y T r y A g a i n   =   f a l s e ;   / /   a s s u m e   w e ' l l   s u c c e e d 
 
                 l e t   m y A p p P a t h ; 
                 
                 / /   s a v e   c u r r e n t   l a s t   d i r   i n   c a s e   o f   c e r t a i n   w a r n i n g s / e r r o r s 
                 l e t   m y L a s t D i r   =   g E p i L a s t D i r . c r e a t e ; 
 
                 / /   s h o w   f i l e   s e l e c t i o n   d i a l o g 
                 m y A p p P a t h   =   f i l e D i a l o g ( ' s a v e ' ,   g E p i L a s t D i r ,   ' c r e a t e ' ,   m y D l g O p t i o n s ) ; 
                 i f   ( ! m y A p p P a t h )   { 
                         / /   c a n c e l e d 
                         r e t u r n   - 1 ; 
                 } 
 
                 / /   b r e a k   d o w n   t h e   p a t h   &   c a n o n i c a l i z e   a p p   n a m e 
                 l e t   m y O l d F i l e   =   a I n f o . a p p I n f o . f i l e ; 
                 l e t   m y O l d S h o r t N a m e   =   a I n f o . a p p I n f o . s h o r t N a m e ; 
                 i f   ( ! g e t A p p P a t h I n f o ( m y A p p P a t h ,   a I n f o . a p p I n f o ) )   { 
                         r e t u r n   { 
                                 m e s s a g e :   " U n a b l e   t o   p a r s e   a p p   p a t h . " , 
                                 t i t l e :   " E r r o r " , 
                                 b a c k S t e p :   f a l s e 
                         } ; 
                 } 
                 
                 / /   i f   p a t h   h a s n ' t   c h a n g e d   &   w e   a l r e a d y   h a v e   a   s h o r t   n a m e ,   u s e   t h a t 
                 i f   ( m y O l d S h o r t N a m e   & &   m y O l d F i l e   & & 
                         ( m y O l d F i l e . p a t h   = =   a I n f o . a p p I n f o . f i l e . p a t h ) )   { 
                         a I n f o . a p p I n f o . s h o r t N a m e   =   m y O l d S h o r t N a m e ; 
                 } 
                 
                 / /   c h e c k   f o r   p r o b l e m s / w a r n i n g s   w i t h   t h i s   l o c a t i o n 
                 l e t   m y P a t h C h e c k   =   J S O N . p a r s e ( s h e l l ( 
                         ' e p i A c t i o n = c h e c k p a t h ' , 
                         ' a p p D i r = '   +   a I n f o . a p p I n f o . f i l e . d i r , 
                         ' a p p P a t h = '   +   a I n f o . a p p I n f o . f i l e . p a t h 
                 ) ) ; 
                 
                 / /   w a r n i n g   d i a l o g   i n f o 
                 l e t   m y W a r n M e s s a g e L i s t   =   [ ] ; 
                 l e t   m y W a r n M e s s a g e   =   ' ' ; 
                 l e t   m y W a r n O v e r r i d e   =   t r u e ; 
                 l e t   m y W a r n R e p l a c e   =   f a l s e ; 
                 l e t   m y W a r n O p t i o n s   =   { 
                         b u t t o n s :   [ ' O K ' ] , 
                         d e f a u l t B u t t o n :   1 
                 } ; 
                 
                 / /   s t a r t   b u i l d i n g   w a r n i n g   m e s s a g e 
                 i f   ( ! m y P a t h C h e c k . c a n W r i t e D i r )   { 
                         m y W a r n M e s s a g e L i s t . p u s h ( " Y o u   d o n ' t   h a v e   p e r m i s s i o n   t o   w r i t e   t o   t h a t   f o l d e r .   P l e a s e   c h o o s e   a n o t h e r   l o c a t i o n   f o r   y o u r   a p p . " ) ; 
                         m y W a r n O v e r r i d e   =   f a l s e ; 
                         
                         / /   b a d   l o c a t i o n ,   s o   r e s e t   l a s t D i r 
                         g E p i L a s t D i r . c r e a t e   =   m y L a s t D i r ; 
                         
                 }   e l s e   i f   ( m y P a t h C h e c k . a p p E x i s t s )   { 
                         m y W a r n M e s s a g e L i s t . p u s h ( ' A   f i l e   o r   f o l d e r   n a m e d   " '   +   a I n f o . a p p I n f o . f i l e . n a m e   +   ' "   a l r e a d y   e x i s t s   i n   t h a t   l o c a t i o n . ' ) ; 
                         i f   ( ! m y P a t h C h e c k . c a n W r i t e A p p )   { 
                                 m y W a r n M e s s a g e L i s t [ 0 ]   + =   ' P l e a s e   c h o o s e   a n o t h e r   l o c a t i o n   o r   n a m e   f o r   y o u r   a p p . ' ; 
                                 m y W a r n O v e r r i d e   =   f a l s e ; 
                         }   e l s e   i f   ( m y A p p P a t h . e x t A d d e d )   { 
                                 m y W a r n R e p l a c e   =   t r u e ; 
                                 i f   ( ! ( m y P a t h C h e c k . r i g h t D e v i c e   & &   m y P a t h C h e c k . i n A p p l i c a t i o n s F o l d e r ) )   { 
                                         m y W a r n M e s s a g e L i s t [ 0 ]   + =   '   I f   y o u   c o n t i n u e ,   i t   w i l l   b e   o v e r w r i t t e n . ' ; 
                                 } 
                         }   e l s e   { 
                                 
                                 / /   t h e   p a t h   r e t u r n e d   b y   t h e   d i a l o g   w a s   a l r e a d y   i d e n t i c a l   t o   t h e   e x i s t i n g 
                                 / /   a p p ,   s o   t h e   d i a l o g   a l r e a d y   w a r n e d   u s   a b o u t   r e p l a c i n g   i t 
                                 m y W a r n M e s s a g e L i s t . p o p ( ) ; 
                         } 
                 } 
                 
                 / /   i f   w e   d o n ' t   h a v e   a n   u n r e c o v e r a b l e   p r o b l e m ,   a d d   a n y   o t h e r   p r o b l e m s 
                 i f   ( m y W a r n O v e r r i d e )   { 
                         
                         / /   o n l y   a d d   t h e s e   l o c a t i o n   w a r n i n g s   i f   t h i s   i s   a   n e w   l o c a t i o n   f r o m   l a s t   t i m e 
                         i f   ( m y L a s t D i r   ! =   g E p i L a s t D i r . c r e a t e )   { 
 
                                 / /   l o c a t i o n   i s   o n   t h e   w r o n g   d e v i c e 
                                 i f   ( ! m y P a t h C h e c k . r i g h t D e v i c e )   { 
                                         m y W a r n M e s s a g e L i s t . p u s h ( ' T h a t   l o c a t i o n   i s   o n   a   d i f f e r e n t   d r i v e   t h a n   E p i c h r o m e .   I f   y o u   c r e a t e   t h e   a p p   t h e r e ,   i t   w i l l   N O T   w o r k   u n t i l   y o u   m o v e   i t   o n t o   t h e   s a m e   d r i v e   a s   E p i c h r o m e . ' ) ; 
                                 } 
                                 
                                 / /   l o c a t i o n   i s n ' t   i n   t h e   A p p l i c a t i o n s   f o l d e r 
                                 i f   ( ! m y P a t h C h e c k . i n A p p l i c a t i o n s F o l d e r )   { 
                                         m y W a r n M e s s a g e L i s t . p u s h ( ' T h a t   l o c a t i o n   i s   n o t   a   s u b f o l d e r   o f   / A p p l i c a t i o n s ,   s o   t h e   a p p   w i l l   n o t   b e   a b l e   t o   u s e   e x t e n s i o n s   l i k e   1 P a s s w o r d   t h a t   r e q u i r e   a   v a l i d a t e d   b r o w s e r . ' ) ; 
                                 } 
                         } 
                         
                         / /   a d d   o n   t h e   f i n a l   p r o m p t   &   f o r m a t   t h e   f i n a l   m e s s a g e 
                         i f   ( m y W a r n M e s s a g e L i s t . l e n g t h   >   0 )   { 
                                 i f   ( ( m y W a r n M e s s a g e L i s t . l e n g t h   = =   1 )   & &   m y W a r n R e p l a c e )   { 
                                         m y W a r n M e s s a g e   =   m y W a r n M e s s a g e L i s t [ 0 ]   +   '   D o   y o u   w a n t   t o   r e p l a c e   i t ? ' ; 
                                 }   e l s e   { 
                                         i f   ( m y W a r n M e s s a g e L i s t . l e n g t h   >   1 )   { 
                                                 m y W a r n M e s s a g e L i s t . u n s h i f t ( ' Y o u r   s e l e c t e d   l o c a t i o n   h a s   '   +   m y W a r n M e s s a g e L i s t . l e n g t h . t o S t r i n g ( )   +   '   w a r n i n g s : ' ) ; 
                                                 m y W a r n M e s s a g e   =   m y W a r n M e s s a g e L i s t . j o i n ( ' \ n \ n       '   +   k D o t S e l e c t e d   +   '   ' ) ; 
                                         }   e l s e   { 
                                                 m y W a r n M e s s a g e   =   m y W a r n M e s s a g e L i s t [ 0 ] ; 
                                         } 
                                         m y W a r n M e s s a g e   + =   ' \ n \ n D o   y o u   w a n t   t o   c o n t i n u e   a n y w a y ? ' ; 
                                 } 
                                 
                                 / /   s e t   u p   d i a l o g   o p t i o n s 
                                 m y W a r n O p t i o n s . w i t h T i t l e   =   ' W a r n i n g ' ; 
                                 m y W a r n O p t i o n s . w i t h I c o n   =   ' c a u t i o n ' ; 
                                 m y W a r n O p t i o n s . b u t t o n s . u n s h i f t ( ' C a n c e l ' ) ; 
                         } 
                 }   e l s e   { 
                         / /   s e t   u p   d i a l o g   o p t i o n s   f o r   u n c a n c e l a b l e   d i a l o g 
                         m y W a r n O p t i o n s . w i t h T i t l e   =   ' E r r o r ' ; 
                         m y W a r n O p t i o n s . w i t h I c o n   =   ' s t o p ' ; 
                 } 
                 
                 / /   i f   t h e r e   a r e   a n y   w a r n i n g s ,   d i s p l a y   d i a l o g 
                 i f   ( m y W a r n M e s s a g e L i s t . l e n g t h   >   0 )   { 
                         i f   ( d i a l o g ( m y W a r n M e s s a g e ,   m y W a r n O p t i o n s ) . b u t t o n I n d e x   = =   0 )   { 
                                 m y T r y A g a i n   =   t r u e ; 
                                 
                                 / /   i f   w e ' r e   c a n c e l l i n g   a n y t h i n g   o t h e r   t h a n   a   r e p l a c e ,   r e s e t   l a s t d i r 
                                 i f   ( ( m y W a r n M e s s a g e L i s t . l e n g t h   >   1 )   | |   ! m y W a r n R e p l a c e )   { 
                                         / /   b a d   l o c a t i o n ,   s o   r e s e t   l a s t D i r 
                                         g E p i L a s t D i r . c r e a t e   =   m y L a s t D i r ; 
                                 } 
                                 
                                 / /   t r y   a g a i n 
                                 c o n t i n u e ; 
                         } 
                 } 
         } 
         
         / /   i f   w e   g o t   h e r e ,   w e   h a v e   a   g o o d   p a t h   a n d   d i s p l a y   n a m e 
         u p d a t e A p p I n f o ( a I n f o ,   [ ' p a t h ' ,   ' d i s p l a y N a m e ' ] ) ; 
 
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P E D I T D I S P L A Y N A M E :   s t e p   f u n c t i o n   t o   g e t   d i s p l a y   n a m e   a n d   a p p   p a t h   f o r   C R E A T E   a c t i o n 
 f u n c t i o n   s t e p E d i t D i s p l a y N a m e ( a I n f o )   { 
 
         / /   s t a t u s   v a r i a b l e s 
         l e t   m y E r r ,   m y D l g R e s u l t ; 
 
         l e t   m y D l g M e s s a g e   =   ' E d i t   t h e   n a m e   o f   t h e   a p p . ' ; 
         
         m y D l g R e s u l t   =   d i a l o g ( m y D l g M e s s a g e   +   a I n f o . a p p I n f o S t a t u s . d i s p l a y N a m e . s t e p S u m m a r y ,   { 
                 w i t h T i t l e :   a I n f o . s t e p I n f o . d l g T i t l e , 
                 w i t h I c o n :   a I n f o . s t e p I n f o . d l g I c o n , 
                 d e f a u l t A n s w e r :   a I n f o . a p p I n f o . d i s p l a y N a m e , 
                 b u t t o n s :   [ ' O K ' ,   a I n f o . s t e p I n f o . b a c k B u t t o n ] , 
                 d e f a u l t B u t t o n :   1 , 
                 c a n c e l B u t t o n :   2 
         } ) ; 
         
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                 / /   B a c k   b u t t o n 
                 r e t u r n   - 1 ; 
         } 
         
         / /   w e   o n l y   n e e d   t h e   t e x t   n o w 
         m y D l g R e s u l t   =   m y D l g R e s u l t . t e x t R e t u r n e d ; 
         
         / /   i f   d i s p l a y   n a m e   c h a n g e d   a n d   t h i s   w o u l d   r e s u l t   i n   r e n a m i n g   t h e   a p p ,   c o n f i r m 
         i f   ( a I n f o . s t e p I n f o . i s O r i g F i l e n a m e   & & 
                 ( a I n f o . a p p I n f o . d i s p l a y N a m e   = =   a I n f o . o l d A p p I n f o . d i s p l a y N a m e )   & & 
                 ( m y D l g R e s u l t   ! =   a I n f o . o l d A p p I n f o . d i s p l a y N a m e ) )   { 
                 
                 i f   ( d i a l o g ( " A r e   y o u   s u r e   y o u   w a n t   t o   c h a n g e   t h e   a p p ' s   n a m e ?   T h i s   w i l l   r e n a m e   t h e   a p p   f i l e : \ n \ n "   +   k I n d e n t   +   k D o t U n s e l e c t e d   +   "   O l d   n a m e :   "   +   a I n f o . o l d A p p I n f o . f i l e . n a m e   +   " \ n "   +   k I n d e n t   +   k D o t S e l e c t e d   +   "   N e w   n a m e :   "   +   m y D l g R e s u l t   +   " . a p p " ,   { 
                         w i t h T i t l e :   ' C o n f i r m   R e n a m e   A p p ' , 
                         w i t h I c o n :   ' c a u t i o n ' , 
                         b u t t o n s :   [ ' C a n c e l ' ,   ' O K ' ] , 
                         d e f a u l t B u t t o n :   1 
                 } ) . c a n c e l e d )   { 
                         / /   r e p e a t   t h i s   s t e p 
                         r e t u r n   0 ; 
                 } 
                 
                 / /   u p d a t e   p a t h   f o r   n e w   d i s p l a y   n a m e 
                 a I n f o . a p p I n f o . f i l e . b a s e   =   m y D l g R e s u l t ; 
                 a I n f o . a p p I n f o . f i l e . n a m e   =   m y D l g R e s u l t   +   ' . a p p ' ; 
                 a I n f o . a p p I n f o . f i l e . p a t h   =   a I n f o . a p p I n f o . f i l e . d i r   +   ' / '   +   a I n f o . a p p I n f o . f i l e . n a m e ; 
                 u p d a t e A p p I n f o ( a I n f o ,   ' p a t h ' ) ; 
         } 
 
         / /   i f   w e   g o t   h e r e ,   s e t   t h e   d i s p l a y   n a m e 
         u p d a t e A p p I n f o ( a I n f o ,   ' d i s p l a y N a m e ' ,   m y D l g R e s u l t ) ; 
 
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P S H O R T N A M E :   s t e p   f u n c t i o n   f o r   s e t t i n g   a p p   s h o r t   n a m e 
 f u n c t i o n   s t e p S h o r t N a m e ( a I n f o )   { 
 
         / /   s t a t u s   v a r i a b l e s 
         l e t   m y T r y A g a i n ,   m y E r r ,   m y D l g R e s u l t ; 
 
         l e t   m y A p p S h o r t N a m e P r o m p t   =   ' t h e   s h o r t   a p p   n a m e   t h a t   w i l l   a p p e a r   i n   t h e   m e n u   b a r   ( 1 6   c h a r a c t e r s   o r   l e s s ) . ' ; 
 
         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
                 m y A p p S h o r t N a m e P r o m p t   =   ' E n t e r   '   +     m y A p p S h o r t N a m e P r o m p t ; 
         }   e l s e   { 
                 / /   e d i t   p r o m p t 
                 m y A p p S h o r t N a m e P r o m p t   =   ' E d i t   '   +     m y A p p S h o r t N a m e P r o m p t ; 
         } 
 
         m y T r y A g a i n   =   t r u e ; 
         w h i l e   ( m y T r y A g a i n )   { 
 
                 / /   a s s u m e   s u c c e s s 
                 m y T r y A g a i n   =   f a l s e ; 
                 
                 m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o , 
                         m y A p p S h o r t N a m e P r o m p t   +   a I n f o . a p p I n f o S t a t u s . s h o r t N a m e . s t e p S u m m a r y ,   { 
                                 d e f a u l t A n s w e r :   a I n f o . a p p I n f o . s h o r t N a m e , 
                                 b u t t o n s :   [ ' O K ' ] , 
                         } 
                 ) ; 
                 
                 i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   1 )   { 
                         / /   B a c k   b u t t o n 
                         r e t u r n   - 1 ; 
                 } 
                 
                 m y D l g R e s u l t   =   m y D l g R e s u l t . t e x t R e t u r n e d ; 
                 i f   ( m y D l g R e s u l t . l e n g t h   >   1 6 )   { 
                         m y T r y A g a i n   =   t r u e ; 
                         m y A p p S h o r t N a m e P r o m p t   =   k D o t W a r n i n g   +   "   T h a t   n a m e   i s   t o o   l o n g .   P l e a s e   l i m i t   t h e   n a m e   t o   1 6   c h a r a c t e r s   o r   l e s s . " ; 
                         a I n f o . a p p I n f o . s h o r t N a m e   =   m y D l g R e s u l t . s l i c e ( 0 ,   1 6 ) ; 
                 }   e l s e   i f   ( m y D l g R e s u l t . l e n g t h   = =   0 )   { 
                         m y T r y A g a i n   =   t r u e ; 
                         m y A p p S h o r t N a m e P r o m p t   =   k D o t W a r n i n g   +   "   N o   n a m e   e n t e r e d .   P l e a s e   t r y   a g a i n . " ; 
                 } 
         } 
         
         / /   i f   w e   g o t   h e r e ,   w e   h a v e   a   g o o d   n a m e 
         u p d a t e A p p I n f o ( a I n f o ,   ' s h o r t N a m e ' ,   m y D l g R e s u l t ) ; 
         
         / /   c r e a t e   a u t o m a t i c   I D   i f   n e e d e d 
         i f   ( ! a I n f o . a p p I n f o . i d )   { 
                 c r e a t e A p p I D ( a I n f o ) ; 
         } 
         
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P W I N S T Y L E :   s t e p   f u n c t i o n   f o r   s e t t i n g   a p p   w i n d o w   s t y l e 
 f u n c t i o n   s t e p W i n S t y l e ( a I n f o )   { 
 
         / /   s t a t u s   v a r i a b l e s 
 	 l e t   m y E r r ; 
 
         / /   s e t   u p   d i a l o g   m e s s a g e 
         l e t   m y D l g M e s s a g e   =   " A p p   S t y l e : \ n \ n A P P   W I N D O W   -   T h e   a p p   w i l l   d i s p l a y   a n   a p p - s t y l e   w i n d o w   w i t h   t h e   g i v e n   U R L .   ( T h i s   i s   o r d i n a r i l y   w h a t   y o u ' l l   w a n t . ) \ n \ n B R O W S E R   T A B S   -   T h e   a p p   w i l l   d i s p l a y   a   f u l l   b r o w s e r   w i n d o w   w i t h   t h e   g i v e n   t a b s . " ; 
 
         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
                 m y D l g M e s s a g e   =   ' C h o o s e   '   +     m y D l g M e s s a g e ; 
         }   e l s e   { 
                 m y D l g M e s s a g e   =   ' E d i t   '   +     m y D l g M e s s a g e ; 
         } 
 
         / /   d i s p l a y   d i a l o g 
         l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   m y D l g M e s s a g e ,   { 
                         k e y :   ' w i n d o w S t y l e ' , 
                         b u t t o n s :   [ k W i n S t y l e A p p ,   k W i n S t y l e B r o w s e r ] 
                 } ) ; 
         
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                         / /   B a c k   b u t t o n 
                         r e t u r n   - 1 ; 
         } 
         
         / /   u p d a t e   w i n d o w   s t y l e 
         u p d a t e A p p I n f o ( a I n f o ,   ' w i n d o w S t y l e ' ,   m y D l g R e s u l t . b u t t o n V a l u e ) ; 
         
         / /   u p d a t e   U R L s   t o o   f o r   n e x t   s t e p 
         u p d a t e A p p I n f o ( a I n f o ,   ' u r l s ' ) ; 
 
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P U R L S :   s t e p   f u n c t i o n   f o r   s e t t i n g   a p p   U R L s 
 f u n c t i o n   s t e p U R L s ( a I n f o )   { 
 
         / /   s t a t u s   v a r i a b l e s 
         l e t   m y E r r ,   m y D l g R e s u l t ; 
 
         i f   ( a I n f o . a p p I n f o . w i n d o w S t y l e   = =   k W i n S t y l e A p p )   { 
 
                 / /   A P P   W I N D O W   S T Y L E 
 
                 / /   s e t   u p   d i a l o g   p r o m p t 
 
                 l e t   m y D l g M e s s a g e   =   ' U R L : ' ; 
 
                 i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
 
                         / /   c r e a t e   p r o m p t 
                         m y D l g M e s s a g e   =   ' C h o o s e   '   +     m y D l g M e s s a g e ; 
 
                         / /   i n i t i a l i z e   U R L   l i s t 
                         i f   ( a I n f o . a p p I n f o . u r l s . l e n g t h   = =   0 )   { 
                                 a I n f o . a p p I n f o . u r l s . p u s h ( k A p p D e f a u l t U R L ) ; 
                         } 
 
                 }   e l s e   { 
 
                         / /   e d i t   p r o m p t 
                         m y D l g M e s s a g e   =   ' E d i t   '   +     m y D l g M e s s a g e ; 
                 } 
                 
                 m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   m y D l g M e s s a g e   +   a I n f o . a p p I n f o S t a t u s . u r l s . s t e p S u m m a r y ,   { 
                         d e f a u l t A n s w e r :   a I n f o . a p p I n f o . u r l s [ 0 ] , 
                         b u t t o n s :   [ ' O K ' ] 
                 } ) ; 
                 i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   0 )   { 
                         / /   O K   b u t t o n 
                         a I n f o . a p p I n f o . u r l s [ 0 ]   =   m y D l g R e s u l t . t e x t R e t u r n e d ; 
                 }   e l s e   { 
                         / /   B A C K   b u t t o n 
                         r e t u r n   - 1 ; 
                 } 
                 
                 / /   u p d a t e   s u m m a r i e s 
                 u p d a t e A p p I n f o ( a I n f o ,   ' u r l s ' ) ; 
                 
         }   e l s e   { 
 
                 / /   B R O W S E R   T A B S 
 
                 / /   T A B L I S T :   b u i l d   r e p r e s e n t a t i o n   o f   b r o w s e r   t a b s 
                 f u n c t i o n   t a b l i s t ( t a b s ,   t a b n u m )   { 
 
                         l e t   t t e x t ,   t i ; 
 
                         i f   ( t a b s . l e n g t h   = =   0 )   { 
                                 r e t u r n   k I n d e n t   +   k D o t S e l e c t e d   +   "   [ n o   t a b s   s p e c i f i e d ] \ n \ n C l i c k   \ " A d d \ "   t o   a d d   a   t a b .   I f   y o u   c l i c k   \ " D o n e   ( D o n ' t   A d d ) \ "   n o w ,   t h e   a p p   w i l l   d e t e r m i n e   w h i c h   t a b s   t o   o p e n   o n   s t a r t u p   u s i n g   i t s   p r e f e r e n c e s ,   j u s t   a s   C h r o m e   w o u l d . " 
                         }   e l s e   { 
                                 i f   ( t a b s . l e n g t h   = =   1 )   { 
                                         t t e x t   =   " O n e   t a b   s p e c i f i e d : " ; 
                                 }   e l s e   { 
                                         t t e x t   =   t a b s . l e n g t h . t o S t r i n g ( )   +   '   t a b s   s p e c i f i e d : ' ; 
                                 } 
 
                                 / /   a d d   t a b s   t h e m s e l v e s   t o   t h e   t e x t 
                                 t i   =   1 ; 
                                 f o r   ( c o n s t   t   o f   t a b s )   { 
                                         i f   ( t i   = =   t a b n u m )   { 
                                                 t t e x t   + =   ' \ n '   +   k I n d e n t   +   k D o t S e l e c t e d   +   '     [ t h e   t a b   y o u   a r e   e d i t i n g ] ' ; 
                                         }   e l s e   { 
                                                 t t e x t   + =   ' \ n '   +   k I n d e n t   +   k D o t U n s e l e c t e d   +   '     '   +   t ; 
                                         } 
                                         t i + + ; 
                                 } 
 
                                 i f   ( t i   = =   t a b n u m )   { 
                                         t t e x t   + =   ' \ n '   +   k I n d e n t   +   k D o t S e l e c t e d   +   '     [ n e w   t a b   w i l l   b e   a d d e d   h e r e ] ' ; 
                                 } 
 
                                 r e t u r n   t t e x t ; 
                         } 
                 } 
 
                 / /   s e t   u p   d i a l o g   p r o m p t 
 
                 l e t   m y C u r T a b   =   1 
                 
                 / /   s e t   u p   d i a l o g   o p t i o n s 
                 l e t   m y D l g O p t i o n s   =   { 
                         w i t h T i t l e :   a I n f o . s t e p I n f o . d l g T i t l e , 
                         w i t h I c o n :   a I n f o . s t e p I n f o . d l g I c o n 
                 } ; 
                 
                 / /   d e f a u l t   b u t t o n   f o r   t h e   A d d / D o n e   d i a l o g 
                 / /   s e t   d e f a u l t   b u t t o n 
                 l e t   m y D e f a u l t B u t t o n   =   2 ; 
                 / /   i f   ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   & &   ! a I n f o . s t e p I n f o . u r l s A d d e d O r R e m o v e d )   { 
                 / /           m y D e f a u l t B u t t o n   =   2 ; 
                 / /   } 
                 
                 w h i l e   ( t r u e )   { 
 
                         i f   ( m y C u r T a b   >   a I n f o . a p p I n f o . u r l s . l e n g t h )   { 
 
                                 / /   w e ' r e   p a s t   t h e   e n d   o f   t h e   e n t e r e d   U R L s ,   s o   s h o w   t h e   A d d / D o n e   d i a l o g 
                                 m y D l g O p t i o n s . d e f a u l t A n s w e r   =   k A p p D e f a u l t U R L ; 
                                 m y D l g O p t i o n s . b u t t o n s   =   [ ' A d d ' ,   " D o n e   ( D o n ' t   A d d ) " ] ; 
                                 m y D l g O p t i o n s . c a n c e l B u t t o n   =   3 ; 
                                 
                                 i f   ( m y C u r T a b   = =   1 )   { 
                                         
                                         / /   n o   U R L s   y e t 
 
                                         / /   u s e   B a c k   b u t t o n 
                                         m y D l g O p t i o n s . b u t t o n s . p u s h ( a I n f o . s t e p I n f o . b a c k B u t t o n ) ; 
 
                                         / /   i f   w e ' r e   i n   c r e a t e   m o d e ,   o r   i f   w e ' r e   e d i t i n g   a n d   h a v e   d e l e t e d 
                                         / /   t a b s   t h a t   h a d   b e e n   i n   t h e   a p p ,   d e f a u l t   t o   A d d 
                                         i f   ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   | | 
                                                 ( a I n f o . o l d A p p I n f o . u r l s . l e n g t h   >   0 ) )   { 
                                                 m y D e f a u l t B u t t o n   =   1 ; 
                                         } 
                                 }   e l s e   { 
                                         
                                         / /   U R L s   s e l e c t e d 
                                         
                                         / /   u s e   P r e v i o u s   b u t t o n 
                                         m y D l g O p t i o n s . b u t t o n s . p u s h ( ' P r e v i o u s ' ) ; 
                                 } 
                                 
                                 / /   s e t   d e f a u l t   b u t t o n 
                                 m y D l g O p t i o n s . d e f a u l t B u t t o n   =   m y D e f a u l t B u t t o n ; 
                                 
                                 / /   s h o w   d i a l o g 
                                 m y D l g R e s u l t   =   d i a l o g ( 
                                         t a b l i s t ( a I n f o . a p p I n f o . u r l s ,   m y C u r T a b )   +   a I n f o . a p p I n f o S t a t u s . u r l s . s t e p S u m m a r y , 
                                         m y D l g O p t i o n s ) ; 
                                 i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   0 )   { 
                                         
                                         / /   A D D   b u t t o n 
 
                                         / /   w e ' v e   j u s t   a d d e d   a   U R L ,   s o   d e f a u l t   t o   t h e   s a m e   f o r   n e x t   U R L 
                                         m y D e f a u l t B u t t o n   =   1 ; 
                                         
                                         / /   a d d   t h e   c u r r e n t   t e x t   t o   t h e   e n d   o f   t h e   l i s t   o f   U R L s 
                                         a I n f o . a p p I n f o . u r l s . p u s h ( m y D l g R e s u l t . t e x t R e t u r n e d ) ; 
                                         u p d a t e A p p I n f o ( a I n f o ,   ' u r l s ' ) ; 
                                         m y C u r T a b + + ; 
                                         
                                 }   e l s e   { 
                                         
                                         / /   a n y t h i n g   o t h e r   t h a n   A d d ,   d e f a u l t   t o   D o n ' t   A d d   n e x t   t i m e 
                                         m y D e f a u l t B u t t o n   =   2 ; 
                                         
                                         i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   1 )   { 
                                                 
                                                 / /   D O N E   ( D O N ' T   A D D )   b u t t o n 
                                                 
                                                 / /   w e ' r e   d o n e ,   d o n ' t   a d d   t h e   c u r r e n t   t e x t   t o   t h e   l i s t 
                                                 b r e a k ; 
                                                 
                                         }   e l s e   { 
                                                 
                                                 i f   ( m y C u r T a b   = =   1 )   { 
                                                         / /   B A C K   b u t t o n 
                                                         m y C u r T a b   =   0 ; 
                                                         b r e a k ; 
                                                 }   e l s e   { 
                                                         / /   P R E V I O U S   b u t t o n 
                                                         m y C u r T a b - - ; 
                                                 } 
                                         } 
                                 } 
                         }   e l s e   { 
                                 
                                 / /   w e ' r e   i n   t h e   m i d d l e   o f   e x i s t i n g   U R L s ,   s o   s h o w   t h e   N e x t / R e m o v e   d i a l o g 
                                 m y D l g O p t i o n s . d e f a u l t A n s w e r   =   a I n f o . a p p I n f o . u r l s [ m y C u r T a b - 1 ] ; 
                                 m y D l g O p t i o n s . b u t t o n s   =   [ ' N e x t ' ,   ' R e m o v e ' ] ; 
                                 m y D l g O p t i o n s . d e f a u l t B u t t o n   =   1 ; 
 
                                 / /   i f   t h i s   i s   t h e   f i r s t   U R L ,   u s e   B a c k   b u t t o n ,   o t h e r w i s e   u s e   P r e v i o u s 
                                 i f   ( m y C u r T a b   = =   1 )   { 
                                         m y D l g O p t i o n s . b u t t o n s . p u s h ( a I n f o . s t e p I n f o . b a c k B u t t o n ) ; 
                                         m y D l g O p t i o n s . c a n c e l B u t t o n   =   3 ; 
                                 }   e l s e   { 
                                         m y D l g O p t i o n s . b u t t o n s . p u s h ( ' P r e v i o u s ' ) ; 
                                         d e l e t e   m y D l g O p t i o n s . c a n c e l B u t t o n ; 
                                 } 
                                 
                                 / /   s h o w   d i a l o g 
                                 m y D l g R e s u l t   =   d i a l o g ( 
                                         t a b l i s t ( a I n f o . a p p I n f o . u r l s ,   m y C u r T a b )   +   a I n f o . a p p I n f o S t a t u s . u r l s . s t e p S u m m a r y , 
                                         m y D l g O p t i o n s ) ; 
 
                                 i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   0 )   { 
 
                                         / /   N E X T   b u t t o n 
                                         
                                         a I n f o . a p p I n f o . u r l s [ m y C u r T a b   -   1 ]   =   m y D l g R e s u l t . t e x t R e t u r n e d ; 
                                         m y C u r T a b + + ; 
 
                                 }   e l s e   i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   1 )   { 
 
                                         / /   R E M O V E   b u t t o n 
                                         
                                         / /   m a r k   t h a t   w e ' v e   r e m o v e d   a   U R L 
                                         a I n f o . s t e p I n f o . u r l s A d d e d O r R e m o v e d   =   t r u e ; 
                                                                                 
                                         i f   ( m y C u r T a b   = =   1 )   { 
                                                 a I n f o . a p p I n f o . u r l s . s h i f t ( ) ; 
                                         }   e l s e   i f   ( m y C u r T a b   = =   a I n f o . a p p I n f o . u r l s . l e n g t h )   { 
                                                 a I n f o . a p p I n f o . u r l s . p o p ( ) ; 
                                                 m y C u r T a b - - ; 
                                         }   e l s e   { 
                                                 a I n f o . a p p I n f o . u r l s . s p l i c e ( m y C u r T a b - 1 ,   1 ) ; 
                                         } 
                                 }   e l s e   { 
 
                                         i f   ( m y C u r T a b   = =   1 )   { 
                                                 / /   B A C K   b u t t o n 
                                                 m y C u r T a b   =   0 ; 
                                                 b r e a k ; 
                                         }   e l s e   { 
                                                 / /   P R E V I O U S   b u t t o n 
                                                 a I n f o . a p p I n f o . u r l s [ m y C u r T a b   -   1 ]   =   m y D l g R e s u l t . t e x t R e t u r n e d ; 
                                                 m y C u r T a b - - ; 
                                         } 
                                 } 
                                 
                                 / /   u p d a t e   s t e p   s u m m a r y 
                                 u p d a t e A p p I n f o ( a I n f o ,   ' u r l s ' ) ; 
                         } 
                 } 
 
                 i f   ( m y C u r T a b   = =   0 )   { 
                         / /   w e   h i t   t h e   b a c k   b u t t o n 
                         r e t u r n   - 1 ; 
                 } 
         } 
 
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P B R O W S E R :   s t e p   t o   d e t e r m i n e   i f   a p p   s h o u l d   r e g i s t e r   a s   a   b r o w s e r 
 f u n c t i o n   s t e p B r o w s e r ( a I n f o )   { 
 
         / /   s t a t u s   v a r i a b l e s 
 	 l e t   m y E r r ; 
 
         / /   d i s p l a y   d i a l o g 
         l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   ' R e g i s t e r   a p p   a s   a   b r o w s e r ? ' ,   { 
                 k e y :   ' r e g i s t e r B r o w s e r ' , 
                 b u t t o n s :   { 
                         ' Y e s ' :   t r u e , 
                         ' N o ' :   f a l s e 
                 } 
         } ) ; 
         
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                         / /   B a c k   b u t t o n 
                         r e t u r n   - 1 ; 
         } 
         
         / /   u p d a t e   b r o w s e r   s e l e c t i o n 
         u p d a t e A p p I n f o ( a I n f o ,   ' r e g i s t e r B r o w s e r ' ,   m y D l g R e s u l t . b u t t o n V a l u e ) ; 
         
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P I C O N :   s t e p   f u n c t i o n   t o   d e t e r m i n e   c u s t o m   i c o n 
 f u n c t i o n   s t e p I c o n ( a I n f o )   { 
         
         / /   s e t   u p   s u b s t e p s 
         l e t   m y I c o n S t e p s   =   [ 
                 s t e p I c o n C h e c k E d i t , 
                 s t e p I c o n C h e c k A u t o , 
                 s t e p I c o n S e l e c t A u t o , 
                 s t e p I c o n C h e c k C u s t o m , 
                 s t e p I c o n C h o o s e T y p e , 
                 s t e p I c o n S e l e c t C u s t o m , 
                 s t e p I c o n C h e c k S t y l e , 
                 s t e p I c o n S e t C o m p , 
                 s t e p I c o n S e t C r o p 
         ] ; 
         
         / /   l e t   t h e   s t e p s   k n o w   h o w   m a n y   U R L s   w e   h a v e 
         a I n f o . s t e p I n f o . n u m U r l s   =   ( ( a I n f o . a p p I n f o . w i n d o w S t y l e   = =   k W i n S t y l e A p p )   ? 
                 1   : 
                 a I n f o . a p p I n f o . u r l s . l e n g t h ) ; 
         
         / /   r u n   s t e p s 
         l e t   m y I c o n S t e p s R e s u l t   =   d o S t e p s ( m y I c o n S t e p s ,   a I n f o ,   { 
                         i s S u b S t e p :   t r u e , 
                         a b o r t S i l e n t :   t r u e , 
                         a b o r t B a c k B u t t o n :   ' B a c k ' 
                 } 
         ) ; 
         
         / /   g e t   r i d   o f   t h i s   i t e m 
         d e l e t e   a I n f o . s t e p I n f o . n u m U r l s ; 
         
         i f   ( m y I c o n S t e p s R e s u l t   = =   k S t e p R e s u l t S K I P )   { 
                 / /   w e   b a c k e d   o u t 
                 r e t u r n   - 1 ; 
         } 
         
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P I C O N C H E C K E D I T :   s t e p   f u n c t i o n   t o   d e c i d e   w h e t h e r   t o   c h a n g e   a n   a p p ' s   e x i s t i n g   i c o n 
 f u n c t i o n   s t e p I c o n C h e c k E d i t ( a I n f o )   { 
         
         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                 
                 / /   s e t   u p   m a p   o b j e c t 
                 l e t   m y M a p O b j e c t   =   { 
                         s t e p I n f o :   k A c t i o n E D I T , 
                         a p p I n f o :   { 
                                 d e f a u l t T o Y e s :   ( a I n f o . o l d A p p I n f o . i c o n   ! =   a I n f o . a p p I n f o . i c o n ) 
                         } , 
                         o l d A p p I n f o :   { 
                                 d e f a u l t T o Y e s :   f a l s e 
                         } 
                 } ; 
                 
                 l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   " D o   y o u   w a n t   t o   c h a n g e   t h i s   a p p ' s   i c o n ? " ,   { 
                         m a p O b j e c t :   m y M a p O b j e c t , 
                         k e y :   ' d e f a u l t T o Y e s ' , 
                         b u t t o n s :   { 
                                 ' Y e s ' :   t r u e , 
                                 ' N o ' :     f a l s e 
                         } 
                 } ) ; 
                 
                 i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                         / /   B a c k   b u t t o n 
                         r e t u r n   - 1 ; 
                 } 
                 
                 i f   ( m y D l g R e s u l t . b u t t o n I n d e x   ! =   0 )   { 
                         / /   N o   - -   m a k e   s u r e   i c o n   m a t c h e s   o l d   i c o n 
                         a I n f o . a p p I n f o . i c o n   =   a I n f o . o l d A p p I n f o . i c o n ; 
                         
                         / /   u p d a t e   d i a l o g   i c o n 
                         a I n f o . s t e p I n f o . d l g I c o n   =   s e t D l g I c o n ( a I n f o . o l d A p p I n f o ) ; 
                         
                         / /   s k i p   t o   t h e   e n d 
                         r e t u r n   a I n f o . s t e p I n f o . n u m S t e p s ; 
                 } 
         }   e l s e   { 
                 
                 / /   t h i s   s t e p   w a s   s i l e n t ,   s o   i f   w e ' r e   b a c k i n g   o u t ,   k e e p   g o i n g 
                 i f   ( a I n f o . s t e p I n f o . p r e v S t e p N u m b e r   >   a I n f o . s t e p I n f o . c u r S t e p N u m b e r )   { 
                         r e t u r n   - 1 ; 
                 } 
         } 
         
         / /   w e ' r e   g o i n g   t o   e d i t   t h e   i c o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P I C O N C H E C K A U T O :   s t e p   f u n c t i o n   t o   d e t e r m i n e   w h e t h e r   t o   c h a n g e   a n   a u t o - i c o n 
 f u n c t i o n   s t e p I c o n C h e c k A u t o ( a I n f o )   { 
         
         i f   ( a I n f o . a p p I n f o . i c o n . a u t o I c o n U r l )   { 
 
                 l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   ' T h i s   a p p   i s   s e t   t o   u s e   a n   i c o n   f o u n d   a u t o m a t i c a l l y   a t   '   + 
                         a I n f o . a p p I n f o . i c o n . a u t o I c o n U r l   + 
                         ' .   D o   y o u   w a n t   t o   c o n t i n u e   w i t h   t h i s   i c o n   o r   c h a n g e   i t ? ' ,   { 
                                 b u t t o n s :   [ k D o t S e l e c t e d   +   '   C o n t i n u e ' ,   ' C h a n g e ' ] 
                         } 
                 ) ; 
                 
                 i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                         / /   B a c k   b u t t o n 
                         r e t u r n   - 1 ; 
                 } 
                 
                 i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   0 )   { 
                         / /   c o n t i n u e   u s i n g   t h i s   a u t o i c o n ,   m o v e   o n   t o   s e t t i n g   s t y l e 
                         r e t u r n   5 ; 
                 } 
         }   e l s e   { 
                 
                 / /   t h i s   s t e p   w a s   s i l e n t ,   s o   i f   w e ' r e   b a c k i n g   o u t ,   k e e p   g o i n g 
                 i f   ( a I n f o . s t e p I n f o . p r e v S t e p N u m b e r   >   a I n f o . s t e p I n f o . c u r S t e p N u m b e r )   { 
                         r e t u r n   - 1 ; 
                 } 
         } 
         
         / /   o t h e r w i s e ,   w e ' r e   g o i n g   t o   c h a n g e   t h e   i c o n 
         r e t u r n   1 ; 
 } 
 
 / /   S T E P I C O N S E L E C T A U T O :   s t e p   f u n c t i o n   t o   s e l e c t   t y p e   o f   i c o n   t o   u s e 
 f u n c t i o n   s t e p I c o n S e l e c t A u t o ( a I n f o )   { 
         
         l e t   m y A u t o U r l ; 
         
         / /   s e t   u p   m a p   o b j e c t 
         l e t   m y M a p O b j e c t   =   { 
                 d e f a u l t T o A u t o :   ( g e t I c o n T y p e ( a I n f o . a p p I n f o . i c o n )   = =   k I c o n A U T O ) 
         } ; 
         
         m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o , 
                 " D o   y o u   w a n t   t o   t r y   t o   a u t o m a t i c a l l y   f i n d   a n   i c o n   a t   "   + 
                 ( ( a I n f o . s t e p I n f o . n u m U r l s   >   1 )   ?   " o n e   o f   t h e   a p p ' s   U R L s "   : 
                         ( ( a I n f o . s t e p I n f o . n u m U r l s   >   1 )   ?   " t h e   a p p ' s   U R L "   :   ' a   U R L   y o u   c h o o s e ' ) )   + 
                 ' ,   o r   s e l e c t   y o u r   o w n   i c o n ? \ n \ n '   + 
                 k D o t I n f o   +   '   T h e   a u t o m a t i c   c h e c k   m a y   t a k e   s e v e r a l   s e c o n d s . ' ,   { 
                         m a p O b j e c t :   m y M a p O b j e c t , 
                         k e y :   ' d e f a u l t T o A u t o ' , 
                         b u t t o n s :   { 
                                 ' A u t o ' :   t r u e , 
                                 ' S e l e c t ' :   f a l s e 
                         } 
                 } 
         ) ; 
         
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                 / /   B a c k   b u t t o n 
                 r e t u r n   - 1 ; 
         } 
         
         i f   ( m y D l g R e s u l t . b u t t o n V a l u e )   { 
                 
                 i f   ( a I n f o . s t e p I n f o . n u m U r l s   >   1 )   { 
                         
                         / /   w e   h a v e   m u l t i p l e   U R L s ,   s o   a s k   u s e r   t o   c h o o s e 
                         m y A u t o U r l   =   k A p p . c h o o s e F r o m L i s t ( a I n f o . a p p I n f o . u r l s ,   { 
                                 w i t h T i t l e :   ' S e l e c t   U R L ' , 
                                 w i t h P r o m p t :   ' P l e a s e   s e l e c t   t h e   U R L   y o u   w i s h   t o   c h e c k   f o r   a n   i c o n . ' , 
                                 d e f a u l t I t e m s :   [ ( a I n f o . a p p I n f o . u r l s . i n c l u d e s ( a I n f o . a p p I n f o . a u t o I c o n U r l )   ? 
                                         a I n f o . a p p I n f o . a u t o I c o n U r l   :   a I n f o . a p p I n f o . u r l s [ 0 ] ) ] , 
                                 o k B u t t o n N a m e :   ' S e l e c t ' , 
                                 m u l t i p l e S e l e c t i o n s A l l o w e d :   f a l s e , 
                                 e m p t y S e l e c t i o n A l l o w e d :   f a l s e 
                         } ) ; 
                         
                         / /   h a n d l e   C a n c e l   b u t t o n 
                         i f   ( ! m y A u t o U r l )   { 
                                 r e t u r n   0 ; 
                         } 
                         
                         / /   g e t   s t r i n g 
                         m y A u t o U r l   =   m y A u t o U r l [ 0 ] ; 
                         
                 }   e l s e   i f   ( a I n f o . s t e p I n f o . n u m U r l s   = =   1 )   { 
                         
                         / /   w e   o n l y   h a v e   o n e   U R L 
                         m y A u t o U r l   =   a I n f o . a p p I n f o . u r l s [ 0 ] ; 
                         
                 }   e l s e   { 
                         
                         / /   w e   h a v e   n o   U R L s   s o   s e l e c t   a n   a r b i t r a r y   o n e 
                         w h i l e   ( t r u e )   { 
                                 
                                 / /   s h o w   d i a l o g 
                                 m y A u t o U r l   =   d i a l o g ( ' P l e a s e   e n t e r   a   U R L   t o   s e a r c h : ' ,   { 
                                         w i t h T i t l e :   ' C h o o s e   I c o n   U R L ' , 
                                         d e f a u l t A n s w e r :   k A p p D e f a u l t U R L , 
                                         b u t t o n s :   [ ' O K ' ,   ' C a n c e l ' ] , 
                                         d e f a u l t B u t t o n :   1 , 
                                         c a n c e l B u t t o n :   2 
                                 } ) ; 
                                 i f   ( m y A u t o U r l . c a n c e l e d )   { 
                                         / /   C a n c e l   b u t t o n 
                                         r e t u r n   0 ; 
                                 } 
                                 
                                 m y A u t o U r l   =   m y A u t o U r l . t e x t R e t u r n e d 
                                 i f   ( ! m y A u t o U r l )   { 
                                         / /   e m p t y 
                                         c o n t i n u e ; 
                                 } 
                                 
                                 / /   i f   w e   g o t   h e r e   w e   h a v e   a   U R L 
                                 b r e a k ; 
                         } 
                 } 
                 
                 / /   c h e c k   f o r   a n   a u t o   i c o n 
                 l e t   i E r r ; 
                 t r y   { 
                         i c o n P r e v i e w ( a I n f o . a p p I n f o . i c o n ,   n u l l ,   m y A u t o U r l ) ; 
                         
                 }   c a t c h ( i E r r )   { 
                         
                         i f   ( e r r I s R e p o r t a b l e ( i E r r . m e s s a g e ) [ 0 ] )   { 
                                 
                                 / /   f a t a l   e r r o r   i n   a u t o - i c o n   c o d e 
                                 r e t u r n   { 
                                         m e s s a g e :   e r r I s R e p o r t a b l e ( i E r r . m e s s a g e ) [ 1 ] , 
                                         t i t l e :   ' I c o n   C r e a t i o n   E r r o r ' , 
                                         e r r o r :   i E r r , 
                                         r e p o r t E r r o r :   t r u e , 
                                         b a c k S t e p :   0 
                                 } 
                         } 
                         
                         i f   ( i E r r . m e s s a g e . s t a r t s W i t h ( ' C A N C E L ' ) )   { 
                                 r e t u r n   0 ; 
                         } 
                         
                         / /   s h o w   e r r o r   &   t r y   a   n e w   i m a g e   f i l e 
                         d i a l o g ( i E r r . m e s s a g e   +   '   Y o u   w i l l   n e e d   t o   c r e a t e   a n   i c o n   m a n u a l l y . ' ,   { 
                                 w i t h T i t l e :   ' N o   A u t o m a t i c   I c o n ' , 
                                 w i t h I c o n :   ' c a u t i o n ' , 
                                 b u t t o n s :   [ ' O K ' ] , 
                                 d e f a u l t B u t t o n :   1 
                         } ) ; 
                         
                         / /   i f   w e   h a v e n ' t   y e t   s e t   a n   i c o n ,   m a k e   s u r e   d e f a u l t   i s   n o   l o n g e r   a u t o 
                         i f   ( a I n f o . a p p I n f o . i c o n   = =   k I c o n A U T O )   { 
                                 a I n f o . a p p I n f o . i c o n   =   k I c o n C U S T O M ; 
                         }   e l s e   i f   ( a I n f o . a p p I n f o . i c o n . a u t o I c o n U r l )   { 
                                 / /   w e   w e r e   t r y i n g   t o   c h a n g e   f r o m   a n   e x i s t i n g   a u t o - i c o n ,   s o   g o   b a c k   a n d   a s k   a g a i n 
                                 r e t u r n   - 1 ; 
                         } 
                         
                         / /   a l l   o t h e r   c a s e s ,   m o v e   o n   t o   c u s t o m / d e f a u l t   d i a l o g 
                         r e t u r n   1 ; 
                 } 
                 
                 / /   u p d a t e   a p p   i c o n   i n f o 
                 a I n f o . s t e p I n f o . p r e v I c o n   =   a I n f o . a p p I n f o . i c o n ; 
                 a I n f o . a p p I n f o . i c o n   =   { 
                         a u t o I c o n U r l :   m y A u t o U r l 
                 } 
                 
                 / /   u p d a t e   s u m m a r i e s 
                 u p d a t e A p p I n f o ( a I n f o ,   ' i c o n ' ) ; 
                 
                 / /   m o v e   o n   t o   s t y l e 
                 r e t u r n   4 ; 
         } 
         
         / /   i f   w e   g o t   h e r e ,   w e ' r e   n o t   d o i n g   a n   a u t o - i c o n ,   s o   m o v e   o n   t o   s e l e c t i n g   c u s t o m   i c o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P I C O N S E L E C T A U T O :   s t e p   f u n c t i o n   t o   s e l e c t   t y p e   o f   i c o n   t o   u s e 
 f u n c t i o n   s t e p I c o n C h e c k C u s t o m ( a I n f o )   { 
         
         i f   ( ( a I n f o . a p p I n f o . i c o n   i n s t a n c e o f   O b j e c t )   & & 
                 ( ! a I n f o . a p p I n f o . i c o n . a u t o I c o n U r l ) )   { 
                 
                 l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o , 
                         ' C o n t i n u e   u s i n g   t h e   c u r r e n t l y - s e l e c t e d   c u s t o m   i c o n ? ' ,   { 
                                 b u t t o n s :   [   k D o t S e l e c t e d   +   '   C o n t i n u e ' ,   ' C h a n g e '   ] 
                         } 
                 ) ; 
                 
                 i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                         / /   B a c k   b u t t o n 
                         r e t u r n   - 1 ; 
                 } 
                 
                 i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   0 )   { 
                         / /   C o n t i n u e 
                         r e t u r n   3 ; 
                 } 
         }   e l s e   { 
                 
                 / /   t h i s   s t e p   w a s   s i l e n t ,   s o   i f   w e ' r e   b a c k i n g   o u t ,   k e e p   g o i n g 
                 i f   ( a I n f o . s t e p I n f o . p r e v S t e p N u m b e r   >   a I n f o . s t e p I n f o . c u r S t e p N u m b e r )   { 
                         r e t u r n   - 1 ; 
                 } 
         } 
         
         / /   i f   w e   g o t   h e r e   w e ' l l   c h o o s e   a   n e w   i c o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P I C O N C H O O S E T Y P E :   s t e p   f u n c t i o n   t o   c h o o s e   b e t w e e n   c u s t o m   &   d e f a u l t   i c o n 
 f u n c t i o n   s t e p I c o n C h o o s e T y p e ( a I n f o )   { 
         
         / /   o b j e c t   t o   l e t   d i a l o g   k n o w   w h a t ' s   c u r r e n t l y   s e l e c t e d 
         l e t   m y M a p O b j e c t   =   { 
                 i c o n :   ( ( a I n f o . a p p I n f o . i c o n   = =   k I c o n D E F A U L T )   ?   f a l s e   :   t r u e ) 
         } ; 
                 
         m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o , 
                 ' D o   y o u   w a n t   t o   p r o v i d e   a   c u s t o m   i c o n   o r   u s e   t h e   d e f a u l t   E p i c h r o m e   i c o n ? ' ,   { 
                         m a p O b j e c t :   m y M a p O b j e c t , 
                         k e y :   ' i c o n ' , 
                         b u t t o n s :   { 
                                 ' C u s t o m ' :   t r u e , 
                                 ' D e f a u l t ' :     f a l s e 
                         } 
                 } 
         ) ; 
         
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                 / /   B a c k   b u t t o n 
                 r e t u r n   - 1 ; 
         } 
         
         / /   i f   e d i t i n g   &   i c o n   c h o i c e   c h a n g e d   f r o m   c u s t o m   t o   d e f a u l t ,   c o n f i r m   w h e t h e r   t o   r e m o v e   i c o n 
         i f   ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   & & 
                 ( ( a I n f o . a p p I n f o . i c o n   ! =   k I c o n D E F A U L T )   & &   ( a I n f o . o l d A p p I n f o . i c o n   ! =   k I c o n D E F A U L T ) )   & & 
                 ! m y D l g R e s u l t . b u t t o n V a l u e )   { 
                 
                 l e t   m y C o n f i r m R e s u l t   =   d i a l o g ( " A r e   y o u   s u r e   y o u   w a n t   t o   r e m o v e   t h i s   a p p ' s   c u s t o m   i c o n   a n d   r e p l a c e   i t   w i t h   t h e   d e f a u l t   E p i c h r o m e   i c o n ? " ,   { 
                         w i t h T i t l e :   ' C o n f i r m   I c o n   C h a n g e ' , 
                         w i t h I c o n :   ' c a u t i o n ' , 
                         b u t t o n s :   [ ' C a n c e l ' ,   ' O K ' ] , 
                         d e f a u l t B u t t o n :   1 
                 } ) . b u t t o n R e t u r n e d ; 
                 
                 i f   ( m y C o n f i r m R e s u l t   ! =   ' O K ' )   { 
                         / /   r e p e a t   t h i s   s t e p 
                         r e t u r n   0 ; 
                 } 
         } 
         
         i f   ( ! m y D l g R e s u l t . b u t t o n V a l u e )   { 
                 
                 / /   s e t   d e f a u l t   i c o n 
                 a I n f o . a p p I n f o . i c o n   =   k I c o n D E F A U L T ; 
                 a I n f o . s t e p I n f o . d l g I c o n   =   k E p i A p p I c o n ; 
                 
                 / /   u p d a t e   s u m m a r i e s 
                 u p d a t e A p p I n f o ( a I n f o ,   ' i c o n ' ) ; 
                 
                 / /   s k i p   a l l   r e m a i n i n g   s t e p s 
                 r e t u r n   a I n f o . s t e p I n f o . n u m S t e p s ; 
         } 
 
         / /   c u s t o m   i c o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P I C O N S E L E C T C U S T O M :   s t e p   f u n c t i o n   t o   s e l e c t   a   c u s t o m   i c o n 
 f u n c t i o n   s t e p I c o n S e l e c t C u s t o m ( a I n f o )   { 
         
         l e t   m y I c o n S o u r c e P a t h ; 
                 
         / /   s e t   u p   f i l e   s e l e c t i o n   d i a l o g   o p t i o n s 
         l e t   m y D l g O p t i o n s   =   { 
                 w i t h P r o m p t :   a I n f o . s t e p I n f o . n u m T e x t   +   ' :   S e l e c t   a n   i m a g e   t o   u s e   a s   a n   i c o n . ' , 
                 o f T y p e :   [ 
                         ' c o m . a d o b e . p d f ' , 
                         ' c o m . a d o b e . p h o t o s h o p - i m a g e ' , 
                         ' c o m . a d o b e . r a w - i m a g e ' , 
                         ' c o m . a p p l e . a t x ' , 
                         ' c o m . a p p l e . i c n s ' , 
                         ' c o m . a p p l e . p i c t ' , 
                         ' c o m . c a n o n . c r 2 - r a w - i m a g e ' , 
                         ' c o m . c a n o n . c r 3 - r a w - i m a g e ' , 
                         ' c o m . c a n o n . c r w - r a w - i m a g e ' , 
                         ' c o m . c a n o n . t i f - r a w - i m a g e ' , 
                         ' c o m . c o m p u s e r v e . g i f ' , 
                         ' c o m . d x o . r a w - i m a g e ' , 
                         ' c o m . e p s o n . r a w - i m a g e ' , 
                         ' c o m . f u j i . r a w - i m a g e ' , 
                         ' c o m . h a s s e l b l a d . 3 f r - r a w - i m a g e ' , 
                         ' c o m . h a s s e l b l a d . f f f - r a w - i m a g e ' , 
                         ' c o m . i l m . o p e n e x r - i m a g e ' , 
                         ' c o m . k o d a k . r a w - i m a g e ' , 
                         ' c o m . k o n i c a m i n o l t a . r a w - i m a g e ' , 
                         ' c o m . l e a f a m e r i c a . r a w - i m a g e ' , 
                         ' c o m . l e i c a . r a w - i m a g e ' , 
                         ' c o m . l e i c a . r w l - r a w - i m a g e ' , 
                         ' c o m . m i c r o s o f t . b m p ' , 
                         ' c o m . m i c r o s o f t . c u r ' , 
                         ' c o m . m i c r o s o f t . d d s ' , 
                         ' c o m . m i c r o s o f t . i c o ' , 
                         ' c o m . n i k o n . n r w - r a w - i m a g e ' , 
                         ' c o m . n i k o n . r a w - i m a g e ' , 
                         ' c o m . o l y m p u s . o r - r a w - i m a g e ' , 
                         ' c o m . o l y m p u s . r a w - i m a g e ' , 
                         ' c o m . o l y m p u s . s r - r a w - i m a g e ' , 
                         ' c o m . p a n a s o n i c . r a w - i m a g e ' , 
                         ' c o m . p a n a s o n i c . r w 2 - r a w - i m a g e ' , 
                         ' c o m . p e n t a x . r a w - i m a g e ' , 
                         ' c o m . p h a s e o n e . r a w - i m a g e ' , 
                         ' c o m . s a m s u n g . r a w - i m a g e ' , 
                         ' c o m . s g i . s g i - i m a g e ' , 
                         ' c o m . s o n y . a r w - r a w - i m a g e ' , 
                         ' c o m . s o n y . r a w - i m a g e ' , 
                         ' c o m . s o n y . s r 2 - r a w - i m a g e ' , 
                         ' c o m . t r u e v i s i o n . t g a - i m a g e ' , 
                         ' o r g . k h r o n o s . a s t c ' , 
                         ' o r g . k h r o n o s . k t x ' , 
                         ' p u b l i c . a v c i ' , 
                         ' p u b l i c . h e i c ' , 
                         ' p u b l i c . h e i c s ' , 
                         ' p u b l i c . h e i f ' , 
                         ' p u b l i c . j p e g ' , 
                         ' p u b l i c . j p e g - 2 0 0 0 ' , 
                         ' p u b l i c . m p o - i m a g e ' , 
                         ' p u b l i c . p b m ' , 
                         ' p u b l i c . p n g ' , 
                         ' p u b l i c . p v r ' , 
                         ' p u b l i c . r a d i a n c e ' , 
                         ' p u b l i c . t i f f ' , 
                         ' o r g . w e b m p r o j e c t . w e b p ' 
                 ] , 
                 i n v i s i b l e s :   f a l s e 
         } ; 
         
         / /   s h o w   f i l e   s e l e c t i o n   d i a l o g 
         m y I c o n S o u r c e P a t h   =   f i l e D i a l o g ( ' o p e n ' ,   g E p i L a s t D i r ,   ' i c o n ' ,   m y D l g O p t i o n s ) ; 
         i f   ( ! m y I c o n S o u r c e P a t h )   { 
                 / /   c a n c e l e d :   a s k   a b o u t   a   c u s t o m   i c o n   a g a i n 
                 r e t u r n   - 1 ; 
         } 
         
         / /   u p d a t e   c u s t o m   i c o n   i n f o 
         a I n f o . s t e p I n f o . p r e v I c o n   =   a I n f o . a p p I n f o . i c o n ; 
         a I n f o . a p p I n f o . i c o n   =   m y I c o n S o u r c e P a t h ; 
         
         / /   u p d a t e   s u m m a r i e s 
         u p d a t e A p p I n f o ( a I n f o ,   ' i c o n ' ) ; 
         
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P I C O N C H E C K S T Y L E :   s t e p   f u n c t i o n   t o   c o n f i r m   i c o n   s t y l e 
 f u n c t i o n   s t e p I c o n C h e c k S t y l e ( a I n f o )   { 
         
         / /   w h e r e   t o   b a c k s t e p   t o 
         l e t   m y B a c k S t e p   =   ( a I n f o . a p p I n f o . i c o n . a u t o I c o n U r l   ?   - 5   :   - 3 ) ; 
         
         / /   s t o r e   s o u r c e   i m a g e   d i m e n s i o n s 
         l e t   i S o u r c e S i z e   =   [ 0 , 0 ] ; 
         
         l e t   i E r r ; 
         t r y   { 
                 / /   g e t   p r e v i e w   i c o n   &   s o u r c e   i m a g e   d i m e n s i o n s 
                 a I n f o . s t e p I n f o . d l g I c o n   =   i c o n P r e v i e w ( a I n f o . a p p I n f o . i c o n ,   i S o u r c e S i z e ) ; 
                 
         }   c a t c h   ( i E r r )   { 
                 
                 / /   r o l l   b a c k   i c o n   t o   p r e v i o u s   s t a t e 
                 a I n f o . a p p I n f o . i c o n   =   a I n f o . s t e p I n f o . p r e v I c o n ; 
                 u p d a t e A p p I n f o ( a I n f o ,   ' i c o n ' ) ; 
                 
                 i f   ( e r r I s R e p o r t a b l e ( i E r r . m e s s a g e ) [ 0 ] )   { 
                         
                         / /   f a t a l   e r r o r   i n   i c o n   c r e a t i o n   c o d e 
                         a I n f o . s t e p I n f o . r e s t a r t S t e p   =   t r u e ; 
                         r e t u r n   { 
                                 m e s s a g e :   e r r I s R e p o r t a b l e ( i E r r . m e s s a g e ) [ 1 ] , 
                                 t i t l e :   ' I c o n   C r e a t i o n   E r r o r ' , 
                                 e r r o r :   i E r r , 
                                 r e p o r t E r r o r :   t r u e , 
                                 b a c k S t e p :   m y B a c k S t e p 
                         } 
                 } 
                 
                 i f   ( i E r r . m e s s a g e . s t a r t s W i t h ( ' C A N C E L ' ) )   { 
                         a I n f o . s t e p I n f o . r e s t a r t S t e p   =   t r u e ; 
                         r e t u r n   m y B a c k S t e p ; 
                 } 
                 
                 / /   s h o w   e r r o r   &   t r y   a   n e w   i m a g e   f i l e 
                 d i a l o g ( i E r r . m e s s a g e   +   '   P l e a s e   t r y   a   d i f f e r e n t   f i l e . ' ,   { 
                         w i t h T i t l e :   ' I c o n   C r e a t i o n   E r r o r ' , 
                         w i t h I c o n :   ' c a u t i o n ' , 
                         b u t t o n s :   [ ' O K ' ] , 
                         d e f a u l t B u t t o n :   1 
                 } ) ; 
                 
                 a I n f o . s t e p I n f o . r e s t a r t S t e p   =   t r u e ; 
                 r e t u r n   m y B a c k S t e p ; 
         } 
         
         / /   u p d a t e   i c o n   s o u r c e   d i m e n s i o n s 
         a I n f o . a p p I n f o . i c o n . s o u r c e W i d t h   =   i S o u r c e S i z e [ 0 ] ; 
         a I n f o . a p p I n f o . i c o n . s o u r c e H e i g h t   =   i S o u r c e S i z e [ 1 ] ; 
         a I n f o . a p p I n f o . i c o n . s o u r c e I s S q u a r e   =   ( ( a I n f o . a p p I n f o . i c o n . s o u r c e W i d t h   >   0 )   & & 
                 ( a I n f o . a p p I n f o . i c o n . s o u r c e W i d t h   = =   a I n f o . a p p I n f o . i c o n . s o u r c e H e i g h t ) ) ; 
         
         
         / /   S H O W   C O M P   S E T T I N G S   D I A L O G 
         
         / /   c r e a t e   m e s s a g e   w i t h   c u r r e n t   c o m p   s e t t i n g s 
         l e t   m y I c o n S t y l e M s g   =   ' \ n \ n '   +   k D o t S e l e c t e d   +   '   ' ; 
         i f   ( g I c o n S e t t i n g s . c o m p B i g S u r )   { 
                 m y I c o n S t y l e M s g   + =   ' B i g   S u r   S t y l e \ n '   +   k D o t S e l e c t e d   +   '   ' ; 
                 
                 i f   ( a I n f o . a p p I n f o . i c o n . s o u r c e I s S q u a r e )   { 
                         m y I c o n S t y l e M s g   + =   g I c o n S e t t i n g s . c o m p S i z e . c a p i t a l i z e d ( )   +   '   I m a g e   o n   ' ; 
                 }   e l s e   { 
                         m y I c o n S t y l e M s g   + =   ( g I c o n S e t t i n g s . c r o p   ?   ' C r o p '   :   ' F i t ' )   +   '   I m a g e   t o   '   + 
                         g I c o n S e t t i n g s . c o m p S i z e . c a p i t a l i z e d ( )   + 
                         '   S q u a r e   o n   ' ; 
                 } 
                 m y I c o n S t y l e M s g   + =   g I c o n S e t t i n g s . c o m p B G . c a p i t a l i z e d ( )   +   '   B G ' ; 
                 
         }   e l s e   { 
                 m y I c o n S t y l e M s g   + =   ' I m a g e   O n l y ' ; 
                 
                 i f   ( ! a I n f o . a p p I n f o . i c o n . s o u r c e I s S q u a r e )   { 
                         m y I c o n S t y l e M s g   + =   ' \ n '   +   k D o t S e l e c t e d   +   '   '   + 
                                 ( g I c o n S e t t i n g s . c r o p   ?   ' C r o p '   :   ' F i t ' )   +   '   I m a g e   t o   S q u a r e ' ; 
                 } 
         } 
         
         / /   s h o w   d i a l o g 
         l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   ' T h e   i m a g e   a t   l e f t   s h o w s   h o w   y o u r   i c o n   w i l l   l o o k   w i t h   t h e   c u r r e n t l y - s e l e c t e d   c o n v e r s i o n   o p t i o n s : '   +   m y I c o n S t y l e M s g ,   { 
                 b u t t o n s :   [ ' C o n f i r m ' ,   ' C h a n g e ' ] , 
                 d e f a u l t B u t t o n :   1 
         } ) ; 
         
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                 / /   B a c k   b u t t o n 
                 r e t u r n   m y B a c k S t e p ; 
         } 
         
         i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   1 )   { 
                 / /   c h a n g e   i c o n   s t y l e 
                 r e t u r n   1 ; 
                 
         } 
         
         / /   i f   w e   g o t   h e r e ,   i t ' s   t h e   C o n f i r m   b u t t o n ,   s o   w e ' r e   d o n e 
         r e t u r n   3 ; 
 } 
 
 
 / /   S T E P I C O N S E T C O M P :   s t e p   f u n c t i o n   t o   d e t e r m i n e   c u s t o m   i c o n   c o m p o s i t i n g   s t y l e 
 f u n c t i o n   s t e p I c o n S e t C o m p ( a I n f o )   { 
         
         / /   s h o w   s t e p   d i a l o g 
         l e t   i D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   ' P l e a s e   s e l e c t   w h e t h e r   t o   u s e   o n l y   y o u r   i m a g e   t o   c r e a t e   t h e   i c o n ,   o r   t o   c r e a t e   a   B i g   S u r - s t y l e   i c o n . ' ,   { 
                 m a p O b j e c t :   g I c o n S e t t i n g s , 
                 k e y :   ' c o m p B i g S u r ' , 
                 b u t t o n s :   { 
                         ' I m a g e   O n l y ' :   f a l s e , 
                         ' B i g   S u r ' :     t r u e 
                 } , 
                 w i t h I c o n :   i c o n P r e v i e w ( a I n f o . a p p I n f o . i c o n ) 
         } ) ; 
         
         / /   h a n d l e   B a c k   b u t t o n 
         i f   ( i D l g R e s u l t . c a n c e l e d )   { 
                 r e t u r n   - 1 ; 
         } 
         
         / /   u p d a t e   i c o n   c o m p   s e t t i n g s 
         g I c o n S e t t i n g s . c o m p B i g S u r   =   i D l g R e s u l t . b u t t o n V a l u e ; 
         
         / /   i f   B i g   S u r - s t y l e ,   s e t   s i z e   &   b a c k g r o u n d   c o l o r 
         i f   ( g I c o n S e t t i n g s . c o m p B i g S u r )   { 
                 
                 / /   b u i l d   a n d   c a c h e   s e l e c t i o n   l i s t 
                 i f   ( ! k E p i I c o n C o m p S t y l e s . m a p )   { 
                         
                         / /   i n i t i a l i z e   m a p s 
                         k E p i I c o n C o m p S t y l e s . m a p   =   { } ; 
                         k E p i I c o n C o m p S t y l e s . r e v e r s e M a p   =   { } ; 
                         
                         f o r   ( c o n s t   i S i z e   o f   k E p i I c o n C o m p S t y l e s . s i z e )   { 
                                 f o r   ( c o n s t   i B g   o f   k E p i I c o n C o m p S t y l e s . b g )   { 
                                         
                                         / /   t e x t   t o   d i s p l a y   i n   s e l e c t i o n   l i s t 
                                         l e t   i T e x t   =   i S i z e . c a p i t a l i z e d ( )   +   '   I m a g e   o n   '   +   i B g . c a p i t a l i z e d ( )   +   '   B a c k g r o u n d ' ; 
                                         
                                         / /   c r e a t e   m a p   a n d   r e v e r s e   m a p 
                                         k E p i I c o n C o m p S t y l e s . m a p [ i S i z e   +   ' , '   +   i B g ]   =   i T e x t ; 
                                         k E p i I c o n C o m p S t y l e s . r e v e r s e M a p [ i T e x t ]   =   { 
                                                 s i z e :   i S i z e , 
                                                 b g :   i B g 
                                         } 
                                 } 
                         } 
                 } 
                 
                 / /   d i s p l a y   s e l e c t i o n   l i s t 
                 l e t   i S e l e c t e d S t y l e   =   k A p p . c h o o s e F r o m L i s t ( O b j e c t . v a l u e s ( k E p i I c o n C o m p S t y l e s . m a p ) ,   { 
                         w i t h T i t l e :   ' B i g   S u r   I c o n   S e t t i n g s ' , 
                         w i t h P r o m p t :   ' P l e a s e   s e l e c t   t h e   s i z e   o f   y o u r   i m a g e   a n d   t h e   b a c k g r o u n d   c o l o r . ' , 
                         d e f a u l t I t e m s :   [ k E p i I c o n C o m p S t y l e s . m a p [ g I c o n S e t t i n g s . c o m p S i z e   +   ' , '   +   g I c o n S e t t i n g s . c o m p B G ] ] , 
                         o k B u t t o n N a m e :   ' S e l e c t ' , 
                         m u l t i p l e S e l e c t i o n s A l l o w e d :   f a l s e , 
                         e m p t y S e l e c t i o n A l l o w e d :   f a l s e 
                 } ) ; 
                 
                 / /   h a n d l e   C a n c e l   b u t t o n 
                 i f   ( ! i S e l e c t e d S t y l e )   {   r e t u r n   0 ;   } 
                 
                 / /   m a p   s e l e c t i o n   t o   v a l u e s 
                 i S e l e c t e d S t y l e   =   k E p i I c o n C o m p S t y l e s . r e v e r s e M a p [ i S e l e c t e d S t y l e ] ; 
                 g I c o n S e t t i n g s . c o m p S i z e   =   i S e l e c t e d S t y l e . s i z e ; 
                 g I c o n S e t t i n g s . c o m p B G   =   i S e l e c t e d S t y l e . b g ; 
         } 
         
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P I C O N C R O P :   s t e p   f u n c t i o n   t o   d e t e r m i n e   c u s t o m   i c o n   c r o p p i n g 
 f u n c t i o n   s t e p I c o n S e t C r o p ( a I n f o )   { 
         
         i f   ( ! a I n f o . a p p I n f o . i c o n . s o u r c e I s S q u a r e )   { 
                 
                 / /   s h o w   s t e p   d i a l o g 
                 l e t   i D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   ' H o w   t o   m a k e   n o n - s q u a r e   i m a g e   s q u a r e ? ' ,   { 
                         m a p O b j e c t :   g I c o n S e t t i n g s , 
                         k e y :   ' c r o p ' , 
                         b u t t o n s :   { 
                                 ' F i t ' :   f a l s e , 
                                 ' C r o p ' :     t r u e 
                         } , 
                         w i t h I c o n :   i c o n P r e v i e w ( a I n f o . a p p I n f o . i c o n ) 
                 } ) ; 
                 
                 / /   h a n d l e   B a c k   b u t t o n 
                 i f   ( i D l g R e s u l t . c a n c e l e d )   { 
                         r e t u r n   - 1 ; 
                 } 
                 
                 / /   u p d a t e   i c o n   c r o p   s e t t i n g 
                 g I c o n S e t t i n g s . c r o p   =   i D l g R e s u l t . b u t t o n V a l u e ; 
         }   e l s e   { 
                 
                 / /   t h i s   s t e p   w a s   s i l e n t ,   s o   i f   w e ' r e   b a c k i n g   o u t ,   k e e p   g o i n g 
                 i f   ( a I n f o . s t e p I n f o . p r e v S t e p N u m b e r   >   a I n f o . s t e p I n f o . c u r S t e p N u m b e r )   { 
                         r e t u r n   - 1 ; 
                 } 
         } 
         
         / /   r e t u r n   t o   c o n f i r m a t i o n 
         a I n f o . s t e p I n f o . r e s t a r t S t e p   =   t r u e ; 
         r e t u r n   - 2 ; 
 } 
 
 
 / /   S T E P E N G I N E :   s t e p   f u n c t i o n   t o   s e t   a p p   e n g i n e 
 f u n c t i o n   s t e p E n g i n e ( a I n f o )   { 
 
         / /   d i a l o g   m e s s a g e 
         l e t   m y D l g M e s s a g e ; 
         
         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
 
                 m y D l g M e s s a g e   =   " U s e   b u i l t - i n   a p p   e n g i n e ,   o r   e x t e r n a l   b r o w s e r   e n g i n e ? \ n \ n " ; 
 
                 m y D l g M e s s a g e   + =   k D o t W a r n i n g   +   "   N O T E :   I f   y o u   d o n ' t   k n o w   w h a t   t h i s   q u e s t i o n   m e a n s ,   c h o o s e   B u i l t - I n . \ n \ n " ; 
 
                 m y D l g M e s s a g e   + =   " I n   a l m o s t   a l l   c a s e s ,   u s i n g   t h e   b u i l t - i n   e n g i n e   w i l l   r e s u l t   i n   a   m o r e   f u n c t i o n a l   a p p .   U s i n g   a n   e x t e r n a l   b r o w s e r   e n g i n e   h a s   s e v e r a l   d i s a d v a n t a g e s ,   i n c l u d i n g   u n r e l i a b l e   l i n k   r o u t i n g ,   p o s s i b l e   l o s s   o f   c u s t o m   i c o n / a p p   n a m e ,   i n a b i l i t y   t o   g i v e   e a c h   a p p   i n d i v i d u a l   a c c e s s   t o   t h e   c a m e r a   a n d   m i c r o p h o n e ,   a n d   d i f f i c u l t y   r e l i a b l y   u s i n g   A p p l e S c r i p t   o r   K e y b o a r d   M a e s t r o   w i t h   t h e   a p p . \ n \ n " ; 
 
                 m y D l g M e s s a g e   + =   " T h e   m a i n   r e a s o n   t o   c h o o s e   t h e   e x t e r n a l   b r o w s e r   e n g i n e   i s   i f   y o u r   a p p   m u s t   r u n   o n   a   s i g n e d   b r o w s e r   ( f o r   t h i n g s   l i k e   t h e   1 P a s s w o r d   d e s k t o p   e x t e n s i o n - - i t   i s   N O T   n e e d e d   f o r   t h e   1 P a s s w o r d X   e x t e n s i o n ) . " ; 
         }   e l s e   { 
 
                 / /   e d i t   m e s s a g e 
                 m y D l g M e s s a g e   =   ' E d i t   a p p   e n g i n e   c h o i c e . \ n \ n ' ; 
 
                 m y D l g M e s s a g e   + =   k D o t W a r n i n g   +   "   N O T E :   I f   y o u   d o n ' t   k n o w   w h a t   t h i s   q u e s t i o n   m e a n s ,   c h o o s e   K e e p . \ n \ n " ; 
 
                 m y D l g M e s s a g e   + =   " S w i t c h i n g   a n   e x i s t i n g   a p p ' s   e n g i n e   w i l l   l o g   y o u   o u t   o f   a n y   e x i s t i n g   s e s s i o n s   i n   t h e   a p p ,   y o u   w i l l   l o s e   a n y   s a v e d   p a s s w o r d s ,   a n d   y o u   w i l l   n e e d   t o   r e i n s t a l l   a l l   y o u r   e x t e n s i o n s .   ( T h e   f i r s t   t i m e   y o u   r u n   t h e   u p d a t e d   a p p ,   i t   w i l l   o p e n   t h e   C h r o m e   W e b   S t o r e   p a g e   f o r   e a c h   e x t e n s i o n   y o u   h a d   i n s t a l l e d   t o   g i v e   y o u   a   c h a n c e   t o   r e i n s t a l l   t h e m .   O n c e   r e i n s t a l l e d ,   a n y   e x t e n s i o n   s e t t i n g s   s h o u l d   r e a p p e a r . ) \ n \ n " ; 
 
                 m y D l g M e s s a g e   + =   " T h e   b u i l t - i n   e n g i n e   h a s   m a n y   a d v a n t a g e s ,   i n c l u d i n g   m o r e   r e l i a b l e   l i n k   r o u t i n g ,   p r e v e n t i n g   i n t e r m i t t e n t   l o s s   o f   c u s t o m   i c o n / a p p   n a m e ,   a b i l i t y   t o   g i v e   t h e   a p p   i n d i v i d u a l   a c c e s s   t o   c a m e r a   a n d   m i c r o p h o n e ,   a n d   m o r e   r e l i a b l e   i n t e r a c t i o n   w i t h   A p p l e S c r i p t   a n d   K e y b o a r d   M a e s t r o . \ n \ n " ; 
 
                 m y D l g M e s s a g e   + =   " T h e   m a i n   a d v a n t a g e   o f   t h e   e x t e r n a l   e n g i n e   i s   i f   y o u r   a p p   m u s t   r u n   o n   a   s i g n e d   b r o w s e r   ( m a i n l y   n e e d e d   f o r   e x t e n s i o n s   l i k e   t h e   1 P a s s w o r d   d e s k t o p   e x t e n s i o n - - i t   i s   n o t   n e e d e d   f o r   t h e   1 P a s s w o r d X   e x t e n s i o n ) . " ; 
         } 
 
         l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   m y D l g M e s s a g e ,   { 
                 k e y :   ' e n g i n e ' , 
                 b u t t o n s :   k E n g i n e s . r e d u c e ( f u n c t i o n ( a c c ,   c u r )   {   a c c [ c u r . b u t t o n ]   =   c u r ;   r e t u r n   a c c ;   } ,   { } ) 
         } ) ; 
         
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                 / /   B a c k   b u t t o n 
                 r e t u r n   - 1 ; 
         } 
         
         / /   i f   e n g i n e   c h a n g e   c h o s e n   d u r i n g   e d i t i n g ,   c o n f i r m 
         i f   ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   & & 
                 o b j E q u a l s ( a I n f o . a p p I n f o . e n g i n e ,   a I n f o . o l d A p p I n f o . e n g i n e )   & & 
                 ! o b j E q u a l s ( m y D l g R e s u l t . b u t t o n V a l u e ,   a I n f o . o l d A p p I n f o . e n g i n e ) )   { 
                 
                 i f   ( d i a l o g ( " A r e   y o u   s u r e   y o u   w a n t   t o   s w i t c h   e n g i n e s ? \ n \ n I M P O R T A N T :   Y o u   w i l l   b e   l o g g e d   o u t   o f   a l l   e x i s t i n g   s e s s i o n s ,   l o s e   a l l   s a v e d   p a s s w o r d s ,   a n d   w i l l   n e e d   t o   r e i n s t a l l   a l l   e x t e n s i o n s .   I f   y o u   w a n t   t o   s a v e   o r   e x p o r t   a n y t h i n g ,   y o u   m u s t   d o   i t   B E F O R E   c o n t i n u i n g   w i t h   t h i s   c h a n g e . " ,   { 
                                 w i t h T i t l e :   ' C o n f i r m   E n g i n e   C h a n g e ' , 
                                 w i t h I c o n :   ' c a u t i o n ' , 
                                 b u t t o n s :   [ ' C a n c e l ' ,   ' O K ' ] , 
                                 d e f a u l t B u t t o n :   1 
                         } ) . c a n c e l e d )   { 
                         / /   r e p e a t   t h i s   s t e p 
                         r e t u r n   0 ; 
                 } 
         } 
         
         / /   s e t   a p p   e n g i n e 
         u p d a t e A p p I n f o ( a I n f o ,   ' e n g i n e ' ,   m y D l g R e s u l t . b u t t o n V a l u e ) ; 
 
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   A D V S T E P I D :   s t e p   f u n c t i o n   f o r   s e t t i n g   a p p   I D 
 f u n c t i o n   a d v S t e p I D ( a I n f o )   { 
 
         / /   s t a t u s   v a r i a b l e s 
         l e t   m y T r y A g a i n ,   m y E r r ,   m y D l g R e s u l t ; 
         l e t   m y D l g M e s s a g e ,   m y D l g O p t i o n s ; 
 
 
         / /   S E T   U P   D I A L O G 
 
         / /   I D   w a r n i n g s   &   l i m i t s 
         c o n s t   m y I D W a r n i n g   =   " I t   i s   S T R O N G L Y   r e c o m m e n d e d   n o t   t o   h a v e   m u l t i p l e   a p p s   w i t h   t h e   s a m e   I D .   T h e y   w i l l   i n t e r f e r e   w i t h   e a c h   o t h e r ' s   e n g i n e   a n d   d a t a   d i r e c t o r i e s ,   r e s u l t i n g   i n   p o s s i b l e   d a t a   c o r r u p t i o n   a n d   i n a b i l i t y   t o   r u n . " ; 
         c o n s t   m y A c t i o n W a r n i n g G e n e r a l   =   ' U n l e s s   y o u   a r e   S U R E   y o u   k n o w   w h a t   y o u   a r e   d o i n g ,   y o u   s h o u l d   n o t   c o n t i n u e . ' ; 
 
         c o n s t   m y I D L i m i t s   =   ' 1 2   c h a r a c t e r s   o r   l e s s   w i t h   o n l y   u n a c c e n t e d   l e t t e r s ,   n u m b e r s ,   a n d   t h e   s y m b o l s   -   a n d   _ ' ; 
         
         / /   s t a r t   b u i l d i n g   d i a l o g   m e s s a g e   &   b u t t o n s 
         m y D l g M e s s a g e   =   '   a   c u s t o m   a p p   I D   ( '   +   m y I D L i m i t s   +   ' ) ' ; 
         m y D l g O p t i o n s   =   {   d e f a u l t A n s w e r :   a I n f o . a p p I n f o . i d ,   b u t t o n s :   [ ' O K ' ]   } ; 
         
         / /   c u s t o m i z e   d i a l o g   m e s s a g e   &   b u t t o n s 
         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
                 i f   ( a I n f o . s t e p I n f o . i s C u s t o m I D )   { 
                         m y D l g O p t i o n s . b u t t o n s . p u s h ( ' U s e   D e f a u l t   ( '   +   a I n f o . s t e p I n f o . a u t o I D   +   ' ) ' ) ; 
                 }   e l s e   { 
                         m y D l g O p t i o n s . b u t t o n s . p u s h ( ' K e e p   D e f a u l t   ( '   +   a I n f o . s t e p I n f o . a u t o I D   +   ' ) ' ) ; 
                         m y D l g O p t i o n s . d e f a u l t B u t t o n   =   2 ; 
                 } 
                 m y D l g M e s s a g e   =   ' E n t e r '   +   m y D l g M e s s a g e   +   ' . ' ; 
         }   e l s e   i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                 i f   ( a I n f o . s t e p I n f o . a u t o I D )   { 
                         i f   ( a I n f o . s t e p I n f o . i s C u s t o m I D )   { 
                                 m y D l g O p t i o n s . b u t t o n s . p u s h ( ' U s e   D e f a u l t   ( '   +   a I n f o . s t e p I n f o . a u t o I D   +   ' ) ' ) ; 
                         }   e l s e   { 
                                 m y D l g O p t i o n s . b u t t o n s . p u s h ( ' K e e p   D e f a u l t   ( '   +   a I n f o . s t e p I n f o . a u t o I D   +   ' ) ' ) ; 
                                 m y D l g O p t i o n s . d e f a u l t B u t t o n   =   2 ; 
                         } 
                 }   e l s e   { 
                         m y D l g O p t i o n s . b u t t o n s . p u s h ( ' C r e a t e   D e f a u l t ' ) ; 
                 } 
                 i f   ( a I n f o . a p p I n f o S t a t u s . s h o r t N a m e . c h a n g e d   & &   ( ! a I n f o . a p p I n f o S t a t u s . i d . c h a n g e d ) )   { 
                         m y D l g M e s s a g e   =   k D o t W a r n i n g   +   "   T h e   a p p ' s   s h o r t   n a m e   h a s   c h a n g e d ,   b u t   i t s   I D   h a s   n o t . \ n \ n Y o u   m a y   e n t e r "   +   m y D l g M e s s a g e ; 
                 }   e l s e   { 
                         m y D l g M e s s a g e   =   ' E n t e r '   +   m y D l g M e s s a g e ; 
                 } 
                 m y D l g M e s s a g e   + =   ' ,   o r   c l i c k   " '   +   m y D l g O p t i o n s . b u t t o n s [ 1 ]   +   ' "   t o   l e t   E p i c h r o m e   c r e a t e   o n e . ' ; 
         } 
 
 
         / /   L O O P   T I L L   W E   H A V E   A N   A C C E P T A B L E   I D 
         
         w h i l e   ( t r u e )   { 
                 
                 / /   d i s p l a y   d i a l o g 
                 m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   m y D l g M e s s a g e   +   a I n f o . a p p I n f o S t a t u s . i d . s t e p S u m m a r y ,   m y D l g O p t i o n s ) ; 
                 
                 i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   0 )   { 
 
                         / /   N E W   C U S T O M   I D   C H O S E N 
 
                         / /   e r r o r - c h e c k   n e w   I D 
                         l e t   m y H a s E r r o r s   =   f a l s e ; 
                         m y D l g M e s s a g e   =   [ ] ; 
                         l e t   m y I D D e f a u l t   =   m y D l g R e s u l t . t e x t R e t u r n e d ; 
                         i f   ( m y D l g R e s u l t . t e x t R e t u r n e d . l e n g t h   <   1 )   { 
                                 m y H a s E r r o r s   =   t r u e ; 
                                 m y D l g M e s s a g e . p u s h ( ' i s   e m p t y ' ) ; 
                                 m y I D D e f a u l t   =   a I n f o . a p p I n f o . i d ; 
                         }   e l s e   { 
                                 i f   ( k A p p I D I l l e g a l C h a r s R e . t e s t ( m y D l g R e s u l t . t e x t R e t u r n e d ) )   { 
                                         m y H a s E r r o r s   =   t r u e ; 
                                         m y D l g M e s s a g e . p u s h ( ' c o n t a i n s   i l l e g a l   c h a r a c t e r s ' ) ; 
                                         m y I D D e f a u l t   =   m y I D D e f a u l t . r e p l a c e ( k A p p I D I l l e g a l C h a r s R e ,   ' ' ) ; 
                                 } 
                                 i f   ( m y D l g R e s u l t . t e x t R e t u r n e d . l e n g t h   >   k A p p I D M a x L e n g t h )   { 
                                         m y H a s E r r o r s   =   t r u e ; 
                                         m y D l g M e s s a g e . u n s h i f t ( ' i s   t o o   l o n g ' ) ; 
                                         m y I D D e f a u l t   =   m y I D D e f a u l t . s l i c e ( 0 ,   k A p p I D M a x L e n g t h ) ; 
                                 } 
                         } 
 
                         / /   b u i l d   e r r o r   m e s s a g e   a n d   t r y   a g a i n 
                         i f   ( m y H a s E r r o r s )   { 
                                 m y D l g M e s s a g e   =   k D o t W a r n i n g   +   '   T h e   e n t e r e d   I D   '   + 
                                         m y D l g M e s s a g e . j o i n ( '   a n d   ' )   + 
                                         ' . \ n \ n P l e a s e   e n t e r   a   n e w   I D   u s i n g   '   +   m y I D L i m i t s   +   ' . ' ; 
                                 m y D l g O p t i o n s . d e f a u l t A n s w e r   =   m y I D D e f a u l t ; 
                                 c o n t i n u e ; 
                         } 
 
                         / /   i f   w e   g o t   h e r e ,   w e   h a v e   a   l e g a l   I D 
 
                         / /   i f   w e   a l r e a d y   k n o w   t h i s   I D ,   w e ' r e   d o n e 
                         i f   ( ( m y D l g R e s u l t . t e x t R e t u r n e d   = =   a I n f o . a p p I n f o . i d )   | | 
                                 ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   & & 
                                         ( m y D l g R e s u l t . t e x t R e t u r n e d   = =   a I n f o . o l d A p p I n f o . i d ) ) )   { 
                                 b r e a k ; 
                         } 
 
                         / /   t h i s   i s   a   n e w   I D ,   s o   s e p a r a t e l y   c h e c k   f o r   a   u n i q u e   a p p   &   d a t a   d i r e c t o r y 
                         l e t   m y A p p E x i s t s   =   a p p I D A p p E x i s t s ( m y D l g R e s u l t . t e x t R e t u r n e d ) ; 
                         l e t   m y D a t a D i r E x i s t s   =   a p p I D D a t a D i r E x i s t s ( m y D l g R e s u l t . t e x t R e t u r n e d ) ; 
                         
                         / /   i f   n o t h i n g   f o u n d ,   w e   a r e   a l l   s e t 
                         i f   ( ( m y A p p E x i s t s   = = =   f a l s e )   & &   ( m y D a t a D i r E x i s t s   = = =   f a l s e ) )   {   b r e a k ;   } 
                         
                         l e t   m y C o n f i r m M e s s a g e   =   ' ' ; 
                         l e t   m y A c t i o n W a r n i n g   =   m y A c t i o n W a r n i n g G e n e r a l ; 
                         
                         / /   a n o t h e r   a p p   f o u n d ,   o r   e r r o r   c h e c k i n g 
                         i f   ( m y A p p E x i s t s   = = =   t r u e )   { 
                                 
                                 / /   i f   a n   a p p   e x i s t s   w i t h   t h i s   I D ,   w e   o n l y   n e e d   t h i s   c o n f i r m a t i o n   m e s s a g e 
                                 m y C o n f i r m M e s s a g e   =   ' T H A T   I D   I S   N O T   U N I Q U E .   T h e r e   i s   a l r e a d y   a n   a p p   w i t h   I D   " '   + 
                                         m y D l g R e s u l t . t e x t R e t u r n e d   +   ' "   o n   t h e   s y s t e m . \ n \ n '   + 
                                         m y I D W a r n i n g ; 
                                 
                         }   e l s e   i f   ( m y A p p E x i s t s   i n s t a n c e o f   E r r o r )   { 
                                 
                                 / /   w e   g o t   a n   e r r o r   c h e c k i n g   f o r   a n   a p p   a l r e a d y   o n   t h e   s y s t e m 
                                 m y C o n f i r m M e s s a g e   =   ' T H A T   I D   M A Y   N O T   B E   U N I Q U E .   T h e r e   w a s   a n   e r r o r   c h e c k i n g   t h e   s y s t e m   f o r   a p p s   w i t h   t h e   s a m e   I D .   ( '   + 
                                         m y A p p E x i s t s . m e s s a g e   +   ' ) \ n \ n '   +   m y I D W a r n i n g ; 
                                 
                                 / /   i f   n o   d a t a   d i r   f o u n d ,   o v e r r i d e   g e n e r a l   a c t i o n   w a r n i n g 
                                 i f   ( m y D a t a D i r E x i s t s   = = =   f a l s e )   { 
                                         m y A c t i o n W a r n i n g   =   ' U n l e s s   y o u   a r e   S U R E   t h i s   I D   i s   i n   f a c t   u n i q u e ,   y o u   s h o u l d   s e l e c t   a n o t h e r . ' ; 
                                 } 
                         } 
                         
                         / /   d a t a   d i r e c t o r y   f o u n d ,   o r   e r r o r   c h e c k i n g 
                         i f   ( m y D a t a D i r E x i s t s   ! = =   f a l s e )   { 
                                 i f   ( m y D a t a D i r E x i s t s   = = =   t r u e )   { 
                                         
                                         m y C o n f i r m M e s s a g e   + =   ( m y C o n f i r m M e s s a g e   ?   ' \ n \ n T h e r e   i s   a l s o   '   :   ' T h e r e   i s   ' )   + 
                                                 ' a l r e a d y   a   d a t a   d i r e c t o r y   f o r   '   + 
                                                 ( m y C o n f i r m M e s s a g e   ?   ' t h i s   I D '   :   ' I D   " '   +   m y D l g R e s u l t . t e x t R e t u r n e d   +   ' " ' )   + 
                                                 ' . \ n \ n I f   y o u   u s e   t h i s   I D ,   ' ; 
                                         
                                 }   e l s e   i f   ( m y D a t a D i r E x i s t s   i n s t a n c e o f   E r r o r )   { 
                                         
                                         m y C o n f i r m M e s s a g e   + =   ( m y C o n f i r m M e s s a g e   ?   ' \ n \ n T h e r e   w a s   a l s o   '   :   ' T h e r e   w a s   ' )   + 
                                                 ' a n   e r r o r   c h e c k i n g   f o r   a   d a t a   d i r e c t o r y   f o r   '   + 
                                                 ( m y C o n f i r m M e s s a g e   ?   ' t h i s   I D '   :   ' I D   " '   +   m y D l g R e s u l t . t e x t R e t u r n e d   +   ' " ' )   + 
                                                 ' .   ( '   +   m y D a t a D i r E x i s t s . m e s s a g e   + 
                                                 ' ) \ n \ n I f   y o u   u s e   t h i s   I D   a n d   t h e   d i r e c t o r y   a l r e a d y   e x i s t s ,   ' ; 
                                 } 
                                 m y C o n f i r m M e s s a g e   + =   ' t h e   a p p   w i l l   s w i t c h   t o   u s i n g   t h a t   d i r e c t o r y ,   a n d   w i l l   N O T   m i g r a t e   i t s   e x i s t i n g   d a t a . ' ; 
                         } 
                         
                         / /   f i n i s h   c o n f i r m   m e s s a g e 
                         m y C o n f i r m M e s s a g e   + =   ' \ n \ n '   +   m y A c t i o n W a r n i n g   +   ' \ n \ n D o   y o u   w a n t   t o   u s e   t h i s   I D   a n y w a y ? ' ; 
                         
                         / /   d i s p l a y   c o n f i r m   d i a l o g 
                         i f   ( d i a l o g ( m y C o n f i r m M e s s a g e ,   { 
                                 w i t h T i t l e :   ' D u p l i c a t e   I D ' , 
                                 w i t h I c o n :   ' c a u t i o n ' , 
                                 b u t t o n s :   [ ' C a n c e l ' ,   ' O K ' ] , 
                                 d e f a u l t B u t t o n :   1 , 
                                 c a n c e l B u t t o n :   1 
                         } ) . b u t t o n I n d e x   = =   0 )   { 
                                 / /   C a n c e l   - -   t r y   a g a i n 
                                 m y D l g M e s s a g e   =   ' E n t e r   a   n e w   a p p   I D   ( '   +   m y I D L i m i t s   +   ' ) ' ; 
                                 m y D l g O p t i o n s . d e f a u l t A n s w e r   =   m y D l g R e s u l t . t e x t R e t u r n e d ; 
                                 c o n t i n u e ; 
                         }   e l s e   { 
                                 / /   O K 
                                 b r e a k ; 
                         } 
                 
                 }   e l s e   i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   1 )   { 
                         / /   D E F A U L T   I D   b u t t o n 
                         c r e a t e A p p I D ( a I n f o ) ; 
                         r e t u r n   1 ; 
                 }   e l s e   { 
                         / /   B a c k 
                         r e t u r n   - 1 ; 
                 } 
                 
                 / /   w e   s h o u l d   n e v e r   g e t   h e r e 
                 r e t u r n   0 ; 
         } 
         
         / /   i f   w e   g o t   h e r e ,   w e   h a v e   c h o s e n   a n   I D 
         u p d a t e A p p I n f o ( a I n f o ,   ' i d ' ,   m y D l g R e s u l t . t e x t R e t u r n e d ) ; 
         
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   A D V S T E P U P D A T E :   s t e p   f u n c t i o n   t o   s e t   u p d a t i n g   o p t i o n s 
 f u n c t i o n   a d v S t e p U p d a t e ( a I n f o )   { 
         
         / /   s e t   u p   d i a l o g   m e s s a g e 
         l e t   m y C u r A c t i o n   =   k U p d a t e A c t i o n s [ a I n f o . a p p I n f o . u p d a t e A c t i o n ] ; 
         l e t   m y D l g M e s s a g e   =   ' H o w   s h o u l d   t h i s   a p p   h a n d l e   u p d a t e s   w h e n   a   n e w   v e r s i o n   o f   E p i c h r o m e   i s   i n s t a l l e d ? ' ; 
         l e t   m y D l g B u t t o n s   =   { } ; 
         m y D l g B u t t o n s [ m y C u r A c t i o n . b u t t o n ]   =   a I n f o . a p p I n f o . u p d a t e A c t i o n ; 
         l e t   m y D l g O t h e r O p t i o n s   =   O b j e c t . k e y s ( k U p d a t e A c t i o n s ) . f i l t e r ( x   = >   x   ! =   a I n f o . a p p I n f o . u p d a t e A c t i o n ) 
         m y D l g B u t t o n s [ ' O t h e r   O p t i o n s ' ]   =   m y D l g O t h e r O p t i o n s ; 
         
         / /   d i s p l a y   d e f a u l t   c h o i c e   d i a l o g 
         l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   m y D l g M e s s a g e   +   a I n f o . a p p I n f o S t a t u s . u p d a t e A c t i o n . s t e p S u m m a r y [ 0 ] ,   { 
                 k e y :   ' u p d a t e A c t i o n ' , 
                 b u t t o n s :   m y D l g B u t t o n s 
         } ) ; 
         
         / /   p r o c e s s   d i a l o g   r e s u l t 
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                 / /   B A C K   b u t t o n 
                 r e t u r n   - 1 ; 
         }   e l s e   i f   ( m y D l g R e s u l t . b u t t o n I n d e x   = =   0 )   { 
                 / /   < C U R R E N T   C H O I C E >   b u t t o n 
                 u p d a t e A p p I n f o ( a I n f o ,   ' u p d a t e A c t i o n ' ,   m y D l g R e s u l t . b u t t o n V a l u e ) ; 
 
         }   e l s e   { 
 
                 / /   O T H E R   O P T I O N S   b u t t o n 
                 
                 / /   b u i l d   d i a l o g   m e s s a g e   a n d   b u t t o n   m a p 
                 m y D l g M e s s a g e   =   ' S h o u l d   t h i s   a p p   '   +   k U p d a t e A c t i o n s [ m y D l g O t h e r O p t i o n s [ 0 ] ] . m e s s a g e   + 
                         '   o r   '   +   k U p d a t e A c t i o n s [ m y D l g O t h e r O p t i o n s [ 1 ] ] . m e s s a g e   +   ' ? ' ; 
                 m y D l g B u t t o n s   =   m y D l g O t h e r O p t i o n s . r e d u c e ( f u n c t i o n ( o b j ,   v a l )   { 
                         o b j [ k U p d a t e A c t i o n s [ v a l ] . b u t t o n ]   =   v a l ;   r e t u r n   o b j ; 
                 } ,   { } ) ; 
                 l e t   m y B u t t o n M a p   =   d i a l o g B u t t o n M a p ( a I n f o ,   ' u p d a t e A c t i o n ' ,   m y D l g B u t t o n s ) ; 
                 
                 / /   b u i l d   f i n a l   b u t t o n   l i s t 
                 m y D l g B u t t o n s   =   O b j e c t . k e y s ( m y B u t t o n M a p . m a p ) ; 
                 m y D l g B u t t o n s . p u s h ( ' C a n c e l ' ) ; 
                 
                 / /   d i s p l a y   d i a l o g 
                 m y D l g R e s u l t   =   d i a l o g ( m y D l g M e s s a g e   +   a I n f o . a p p I n f o S t a t u s . u p d a t e A c t i o n . s t e p S u m m a r y [ 1 ] ,   { 
                         w i t h T i t l e :   ' A d v a n c e d   U p d a t e   O p t i o n s ' , 
                         w i t h I c o n :   a I n f o . s t e p I n f o . d l g I c o n , 
                         b u t t o n s :   m y D l g B u t t o n s , 
                         d e f a u l t B u t t o n :   m y B u t t o n M a p . d e f a u l t B u t t o n , 
                         c a n c e l B u t t o n :   3 
                 } ,   m y B u t t o n M a p . m a p ) ; 
                 
                 / /   p r o c e s s   d i a l o g   r e s u l t 
                 i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                         / /   C A N C E L   b u t t o n 
                         r e t u r n   0 ; 
                 }   e l s e   { 
                         / /   o t h e r   c h o i c e   s e l e c t e d 
                         u p d a t e A p p I n f o ( a I n f o ,   ' u p d a t e A c t i o n ' ,   m y D l g R e s u l t . b u t t o n V a l u e ) ; 
                 } 
         } 
         
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   A D V S T E P D A T A B A C K U P :   s t e p   f u n c t i o n   t o   s e t   w h e t h e r   t o   b a c k u p   a p p   d a t a 
 f u n c t i o n   a d v S t e p D a t a B a c k u p ( a I n f o )   { 
         
         / /   d i s p l a y   d i a l o g 
         l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   " B a c k   u p   a p p   d a t a   w h e n   t h i s   a p p   i s   u p d a t e d   o r   e d i t e d ? " ,   { 
                 k e y :   ' d o D a t a B a c k u p ' , 
                 b u t t o n s :   { 
                         ' N o ' :   f a l s e , 
                         ' Y e s ' :   t r u e 
                 } 
         } ) ; 
         
         / /   p r o c e s s   d i a l o g   r e s u l t 
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                 / /   B A C K   b u t t o n 
                 r e t u r n   - 1 ; 
         } 
         
         / /   i f   c h a n g i n g   f r o m   n o t   b a c k i n g   u p   d a t a   t o   b a c k i n g   u p ,   g e t   c o n f i r m a t i o n 
         i f   ( ( m y D l g R e s u l t . b u t t o n I n d e x   = =   1 )   & &   ( ! a I n f o . a p p I n f o . d o D a t a B a c k u p )   & & 
                 ( ( ! a I n f o . o l d A p p I n f o )   | |   ! a I n f o . o l d A p p I n f o . d o D a t a B a c k u p ) )   { 
                 
                 l e t   m y C o n f i r m R e s u l t   =   d i a l o g ( " A p p   d a t a   c a n   g e t   v e r y   l a r g e .   B a c k i n g   i t   u p   m a y   s l o w   d o w n   a p p   u p d a t e s   a   l o t .   A r e   y o u   s u r e   y o u   w a n t   t o   d o   t h i s ? " ,   { 
                         w i t h T i t l e :   ' C o n f i r m   B a c k u p   A p p   D a t a ' , 
                         w i t h I c o n :   ' c a u t i o n ' , 
                         b u t t o n s :   [ ' C a n c e l ' ,   ' O K ' ] , 
                         d e f a u l t B u t t o n :   1 
                 } ) . b u t t o n R e t u r n e d ; 
                 
                 i f   ( m y C o n f i r m R e s u l t   ! =   ' O K ' )   { 
                         / /   r e p e a t   t h i s   s t e p 
                         r e t u r n   0 ; 
                 } 
         } 
         
         / /   u p d a t e   a p p   i n f o 
         u p d a t e A p p I n f o ( a I n f o ,   ' d o D a t a B a c k u p ' ,   m y D l g R e s u l t . b u t t o n V a l u e ) ; 
         
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   A D V S T E P S K I P W E L C O M E :   s t e p   f u n c t i o n   t o   s e t   w h e t h e r   t o   s k i p   t h e   w e l c o m e   s c r e e n 
 f u n c t i o n   a d v S t e p S k i p W e l c o m e ( a I n f o )   { 
         
         / /   d i s p l a y   d i a l o g 
         l e t   m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   " S k i p   t h e   w e l c o m e   p a g e   a f t e r   m i n o r   u p d a t e s   a n d   a p p   e d i t s ? \ n \ n "   +   k D o t W a r n i n g   +   '   N o t e :   I t   w i l l   s t i l l   b e   s h o w n   w h e n   t h e   a p p   i s   r u n   f o r   t h e   f i r s t   t i m e ,   a l l   s e t t i n g s   a r e   r e s e t ,   o r   a   m a j o r   u p d a t e   o c c u r s ,   a n d   t h e   l o g   o f   c h a n g e s   w i l l   s t i l l   b e   a v a i l a b l e   i n   t h e   b o o k m a r k   f o l d e r . ' ,   { 
                 k e y :   ' s k i p W e l c o m e ' , 
                 b u t t o n s :   { 
                         ' N o ' :   f a l s e , 
                         ' Y e s ' :   t r u e 
                 } 
         } ) ; 
         
         / /   p r o c e s s   d i a l o g   r e s u l t 
         i f   ( m y D l g R e s u l t . c a n c e l e d )   { 
                 / /   B A C K   b u t t o n 
                 r e t u r n   - 1 ; 
         } 
         
         / /   u p d a t e   a p p   i n f o 
         u p d a t e A p p I n f o ( a I n f o ,   ' s k i p W e l c o m e ' ,   m y D l g R e s u l t . b u t t o n V a l u e ) ; 
         
         / /   m o v e   o n 
         r e t u r n   1 ; 
 } 
 
 
 / /   S T E P B U I L D :   s t e p   f u n c t i o n   t o   b u i l d   a p p 
 f u n c t i o n   s t e p B u i l d ( a I n f o )   { 
 
         / /   s t a t u s   v a r i a b l e s 
         l e t   m y E r r ; 
 
         l e t   m y S c r i p t A c t i o n ; 
         
         i f   ( a I n f o . s t e p I n f o . a c t i o n   ! =   k A c t i o n U P D A T E )   { 
 
                 l e t   m y D l g R e s u l t ,   m y A p p S u m m a r y ,   m y A c t i o n B u t t o n ; 
                 l e t   m y D o B u i l d   =   t r u e ; 
 
                 / /   g e t   o r d e r e d   a r r a y   o f   a l l   i t e m s   w i t h   s u m m a r i e s 
                 l e t   m y S u m m a r y I t e m s   =   O b j e c t . e n t r i e s ( a I n f o . a p p I n f o S t a t u s ) . f i l t e r ( x   = >   B o o l e a n ( x [ 1 ] . b u i l d S u m m a r y ) ) . m a p ( x   = >   x [ 1 ] ) ; 
                 
                 i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
 
                         m y S c r i p t A c t i o n   =   ' b u i l d ' ; 
 
                         / /   c r e a t e   s u m m a r y   o f   t h e   a p p 
                         m y A p p S u m m a r y   =   ' R e a d y   t o   c r e a t e ! \ n \ n '   + 
                                 m y S u m m a r y I t e m s . m a p ( x   = >   x . b u i l d S u m m a r y ) . j o i n ( ' \ n \ n ' ) ; 
                         m y A c t i o n B u t t o n   =   ' C r e a t e ' ; 
                 }   e l s e   { 
 
                         m y S c r i p t A c t i o n   =   ' e d i t ' ; 
 
                         / /   e d i t   s u m m a r y   o f   a p p   &   l o o k   f o r   c h a n g e s 
                         l e t   m y C h a n g e d S u m m a r y   =   m y S u m m a r y I t e m s . f i l t e r ( x   = >   x . c h a n g e d ) . m a p ( x   = >   x . b u i l d S u m m a r y ) ; 
                         l e t   m y U n c h a n g e d S u m m a r y   =   m y S u m m a r y I t e m s . f i l t e r ( x   = >   ! x . c h a n g e d ) . m a p ( x   = >   x . b u i l d S u m m a r y ) ; 
                         
                         / /   s e t   u p   d i a l o g   m e s s a g e   a n d   a c t i o n 
                         i f   ( m y C h a n g e d S u m m a r y . l e n g t h   = =   0 )   { 
                                 m y D o B u i l d   =   f a l s e ; 
                                 m y A p p S u m m a r y   =   ' N o   c h a n g e s   h a v e   b e e n   m a d e   t o   t h i s   a p p . ' ; 
                                 m y A c t i o n B u t t o n   =   ( a I n f o . s t e p I n f o . i s O n l y A p p   ?   ' Q u i t '   :   ' S k i p ' ) ; 
                         }   e l s e   { 
                                 i f   ( a I n f o . a p p I n f o S t a t u s . v e r s i o n . c h a n g e d   & &   ( m y C h a n g e d S u m m a r y . l e n g t h   = =   1 ) )   { 
 
                                         / /   s e t   m e s s a g e / b u t t o n   f o r   u p d a t e   o n l y 
                                         m y A p p S u m m a r y   =   ' T h i s   a p p   w i l l   b e   u p d a t e d   t o   v e r s i o n   '   +   k V e r s i o n   + 
                                                 ' .   N o   o t h e r   c h a n g e s   h a v e   b e e n   m a d e . ' ; 
                                         m y A c t i o n B u t t o n   =   ' U p d a t e ' ; 
 
                                         / /   c h a n g e   s c r i p t   a c t i o n   t o   u p d a t e 
                                         m y S c r i p t A c t i o n   =   ' u p d a t e ' ; 
 
                                 }   e l s e   { 
 
                                         l e t   m y H a s U n c h a n g e d   =   ( m y U n c h a n g e d S u m m a r y . l e n g t h   >   0 ) ; 
 
                                         m y A p p S u m m a r y   =   ' R e a d y   t o   s a v e   c h a n g e s ! \ n \ n '   + 
                                                 ( m y H a s U n c h a n g e d   ?   ' C H A N G E D : \ n \ n '   :   ' ' )   + 
                                                 i n d e n t ( m y C h a n g e d S u m m a r y . j o i n ( ' \ n \ n ' ) ,   m y H a s U n c h a n g e d   ?   1   :   0 ) ; 
                                         i f   ( m y H a s U n c h a n g e d )   { 
                                                 m y A p p S u m m a r y   + =   ' \ n \ n U N C H A N G E D : \ n \ n '   + 
                                                         i n d e n t ( m y U n c h a n g e d S u m m a r y . j o i n ( ' \ n \ n ' ) ) ; 
                                         } 
                                         m y A c t i o n B u t t o n   =   ' S a v e   C h a n g e s ' ; 
                                         i f   ( a I n f o . a p p I n f o S t a t u s . v e r s i o n . c h a n g e d )   { 
                                                 m y A c t i o n B u t t o n   + =   '   &   U p d a t e ' ; 
                                         } 
                                 } 
                         } 
                 } 
 
                 / /   d i s p l a y   s u m m a r y 
                 m y D l g R e s u l t   =   s t e p D i a l o g ( a I n f o ,   m y A p p S u m m a r y ,   { b u t t o n s :   [ m y A c t i o n B u t t o n ,   k D o t G e a r   +   '   A d v a n c e d   S e t t i n g s ' ] } ) . b u t t o n I n d e x ; 
                 i f   ( m y D l g R e s u l t   = =   1 )   { 
                         / /   A D V A N C E D   b u t t o n 
                         a I n f o . s t e p I n f o . s h o w A d v a n c e d   =   t r u e ; 
                         d o S t e p s ( [ 
                                 a d v S t e p I D , 
                                 a d v S t e p U p d a t e , 
                                 a d v S t e p D a t a B a c k u p , 
                                 a d v S t e p S k i p W e l c o m e 
                         ] ,   a I n f o ,   { 
                                 a b o r t S i l e n t :   t r u e , 
                                 a b o r t B a c k B u t t o n :   ' B a c k ' , 
                                 s t e p T i t l e :   ' A d v a n c e d   S e t t i n g ' 
                         } ) ; 
                         r e t u r n   0 ; 
                 }   e l s e   i f   ( m y D l g R e s u l t   = =   2 )   { 
                         / /   B A C K   b u t t o n 
                         r e t u r n   - 1 ; 
                 } 
                 
                 / /   i f   w e   g o t   h e r e ,   u s e r   c l i c k e d   t h e   B U I L D   b u t t o n 
                 
                 / /   i f   t h e r e   a r e   n o   c h a n g e s ,   q u i t 
                 i f   ( ! m y D o B u i l d )   { 
                         / /   g o   t o   s t e p   - 1   t o   t r i g g e r   q u i t   d i a l o g 
                         r e t u r n   - a I n f o . s t e p I n f o . c u r S t e p N u m b e r   -   1 ; 
                 } 
         }   e l s e   { 
                 / /   u p d a t e   a c t i o n 
                 m y S c r i p t A c t i o n   =   ' u p d a t e ' ; 
         } 
 
 
         / /   B U I L D   S C R I P T   A R G U M E N T S 
         
         l e t   m y I c o n T y p e   =   g e t I c o n T y p e ( a I n f o . a p p I n f o . i c o n ) ; 
         l e t   m y S c r i p t A r g s   =   [ 
                 ' e p i A c t i o n = '   +   m y S c r i p t A c t i o n , 
                 ' C F B u n d l e D i s p l a y N a m e = '   +   a I n f o . a p p I n f o . d i s p l a y N a m e , 
                 ' C F B u n d l e N a m e = '   +   a I n f o . a p p I n f o . s h o r t N a m e , 
                 ' S S B I d e n t i f i e r = '   +   a I n f o . a p p I n f o . i d , 
                 ' S S B C u s t o m I c o n = '   +   ( ( m y I c o n T y p e   = =   k I c o n A U T O )   ?   ' A u t o '   :   ( ( m y I c o n T y p e   = =   k I c o n C U S T O M )   ?   ' Y e s '   :   ' N o ' ) ) , 
                 ' S S B R e g i s t e r B r o w s e r = '   +   ( a I n f o . a p p I n f o . r e g i s t e r B r o w s e r   ?   ' Y e s '   :   ' N o ' ) , 
                 ' S S B E n g i n e T y p e = '   +   a I n f o . a p p I n f o . e n g i n e . t y p e   +   ' | '   +   a I n f o . a p p I n f o . e n g i n e . i d , 
                 ' S S B U p d a t e A c t i o n = '   +   k U p d a t e A c t i o n s [ a I n f o . a p p I n f o . u p d a t e A c t i o n ] . v a l u e , 
                 ' S S B B a c k u p D a t a = '   +   ( a I n f o . a p p I n f o . d o D a t a B a c k u p   ?   ' Y e s '   :   ' N o ' ) , 
                 ' S S B S k i p W e l c o m e = '   +   ( a I n f o . a p p I n f o . s k i p W e l c o m e   ?   ' Y e s '   :   ' N o ' ) , 
         ] ; 
         
         / /   a d d   a p p   c o m m a n d   l i n e 
         m y S c r i p t A r g s . p u s h ( ' S S B C o m m a n d L i n e = ( ' ) ; 
         i f   ( a I n f o . a p p I n f o . w i n d o w S t y l e   = =   k W i n S t y l e A p p )   { 
                 m y S c r i p t A r g s . p u s h ( ' - - a p p = '   +   a I n f o . a p p I n f o . u r l s [ 0 ] ) ; 
         }   e l s e   i f   ( a I n f o . a p p I n f o . u r l s . l e n g t h   >   0 )   { 
                 m y S c r i p t A r g s . p u s h . a p p l y ( m y S c r i p t A r g s ,   a I n f o . a p p I n f o . u r l s ) ; 
         } 
         m y S c r i p t A r g s . p u s h ( ' ) ' ) ; 
 
         / /   a d d   i c o n   s o u r c e   i f   n e c e s s a r y 
         i f   ( a I n f o . a p p I n f o . i c o n   i n s t a n c e o f   O b j e c t )   { 
                 
                 / /   g e t   p a t h   t o   i c o n   s o u r c e   i m a g e 
                 m y S c r i p t A r g s . p u s h ( ' e p i I c o n S o u r c e = '   +   g e t I c o n S o u r c e P a t h ( a I n f o . a p p I n f o . i c o n ) ) ; 
                 
                 / /   a d d   i c o n   c o m p o s i t i n g   o p t i o n s 
                 m y S c r i p t A r g s . p u s h ( ' e p i I c o n C r o p = '   +   ( g I c o n S e t t i n g s . c r o p   ?   ' 1 '   :   ' ' ) ) ; 
                 i f   ( g I c o n S e t t i n g s . c o m p B i g S u r )   { 
                         m y S c r i p t A r g s . p u s h ( 
                                 ' e p i I c o n C o m p S i z e = '   +   g I c o n S e t t i n g s . c o m p S i z e , 
                                 ' e p i I c o n C o m p B G = '   +   g I c o n S e t t i n g s . c o m p B G ) ; 
                 }   e l s e   { 
                         m y S c r i p t A r g s . p u s h ( ' e p i I c o n C o m p S i z e = ' ) ; 
                 } 
         } 
         
         / /   a c t i o n - s p e c i f i c   a r g u m e n t s 
         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
                 
                 / /   C R E A T E - s p e c i f i c   a r g u m e n t s 
                 m y S c r i p t A r g s . p u s h ( ' e p i A p p P a t h = '   +   a I n f o . a p p I n f o . f i l e . p a t h ) ; 
                 
         }   e l s e   { 
                 / /   E D I T - / U P D A T E - s p e c i f i c   a r g u m e n t s 
 
                 / /   a d d   v e r s i o n   a r g u m e n t   f o r   b o t h   e d i t   &   u p d a t e 
                 m y S c r i p t A r g s . p u s h ( ' S S B V e r s i o n = '   +   a I n f o . a p p I n f o . v e r s i o n ) ; 
                 
                 i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                         / /   o l d   I D 
                         i f   ( a I n f o . a p p I n f o S t a t u s . i d . c h a n g e d )   { 
                                 m y S c r i p t A r g s . p u s h ( ' e p i O l d I d e n t i f i e r = '   +   a I n f o . o l d A p p I n f o . i d ) ; 
                         } 
                         / /   o l d   e n g i n e 
                         i f   ( a I n f o . a p p I n f o S t a t u s . e n g i n e . c h a n g e d )   { 
                                 m y S c r i p t A r g s . p u s h ( ' e p i O l d E n g i n e = '   +   a I n f o . o l d A p p I n f o . e n g i n e . t y p e   +   ' | '   +   a I n f o . o l d A p p I n f o . e n g i n e . i d ) ; 
                         } 
 
                         / /   a p p   p a t h 
                         m y S c r i p t A r g s . p u s h ( ' e p i A p p P a t h = '   +   a I n f o . o l d A p p I n f o . f i l e . p a t h ) ; 
                         i f   ( a I n f o . s t e p I n f o . i s O r i g F i l e n a m e   & &   a I n f o . a p p I n f o S t a t u s . d i s p l a y N a m e . c h a n g e d )   { 
                                 m y S c r i p t A r g s . p u s h ( ' e p i N e w A p p P a t h = '   +   a I n f o . a p p I n f o . f i l e . p a t h ) ; 
                         } 
                 }   e l s e   { 
                         / /   a p p   p a t h   f o r   u p d a t e 
                         m y S c r i p t A r g s . p u s h ( ' e p i A p p P a t h = '   +   a I n f o . a p p I n f o . f i l e . p a t h ) ; 
                 } 
         } 
 
 
         / /   C R E A T E / U P D A T E   T H E   A P P 
 
         l e t   m y B u i l d M e s s a g e ; 
         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
                 m y B u i l d M e s s a g e   =   [ ' C r e a t i n g ' ,   ' C r e a t i o n ' ,   ' C r e a t e d ' ] ; 
         }   e l s e   i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                 m y B u i l d M e s s a g e   =   [ ' S a v i n g   c h a n g e s   t o ' ,   ' S a v e ' ,   ' S a v e d ' ] ; 
         }   e l s e   { 
                 m y B u i l d M e s s a g e   =   [ ' U p d a t i n g ' ,   ' U p d a t e ' ,   ' U p d a t e d ' ] ; 
         } 
         
         l e t   m y D o R e p o r t   =   t r u e ; 
         t r y   { 
                 m y S c r i p t A r g s . p u s h ( ' e p i U p d a t e M e s s a g e = '   +   m y B u i l d M e s s a g e [ 0 ]   +   '   " '   +   a I n f o . a p p I n f o . d i s p l a y N a m e   +   ' . a p p " ' ) ; 
                 
                 / /   r u n   t h e   b u i l d / u p d a t e   s c r i p t ,   a n d   d o n ' t   a u t o m a t i c a l l y   r e p o r t   a n y   e r r o r s   i t   r e t u r n s 
                 m y D o R e p o r t   =   f a l s e ; 
                 s h e l l . a p p l y ( n u l l ,   m y S c r i p t A r g s ) ; 
                 m y D o R e p o r t   =   t r u e ; 
                 
                 / /   b r i n g   E p i c h r o m e   t o   t h e   f r o n t 
                 k A p p . a c t i v a t e ( ) ; 
                 
                 l e t   m y S u b E r r ; 
                 t r y   { 
                         r e g i s t e r A p p ( a I n f o . a p p I n f o . f i l e . p a t h ) ; 
                 }   c a t c h ( m y S u b E r r )   { 
                         e r r l o g ( ' U n a b l e   t o   r e g i s t e r   a p p   " '   +   a I n f o . a p p I n f o . f i l e . n a m e   +   ' " .   ( '   +   m y S u b E r r . m e s s a g e   +   ' )   I t   m a y   n o t   l a u n c h   i m m e d i a t e l y . ' ) ; 
                         / /   m y S u b E r r . m e s s a g e   =   ' W A R N \ r U n a b l e   t o   r e g i s t e r   a p p .   ( '   +   m y S u b E r r . m e s s a g e   +   ' )   I t   m a y   n o t   l a u n c h   i m m e d i a t e l y . ' 
                         / /   t h r o w ( m y S u b E r r ) ; 
                 } 
         }   c a t c h ( m y E r r )   { 
                 
                 / /   b r i n g   a p p   t o   t h e   f r o n t 
                 k A p p . a c t i v a t e ( ) ; 
                 
                 i f   ( m y E r r . m e s s a g e   = =   ' C A N C E L ' )   { 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n U P D A T E )   { 
                                 r e t u r n   - 1 ; 
                         }   e l s e   { 
                                 r e t u r n   0 ; 
                         } 
                 } 
                 
                 i f   ( m y E r r . m e s s a g e . s t a r t s W i t h ( ' W A R N \ r ' ) )   { 
                         
                         / /   c o l l a t e   a l l   w a r n i n g s 
                         l e t   m y W a r n i n g s   =   m y E r r . m e s s a g e . s p l i t ( ' \ r ' ) ; 
                         m y W a r n i n g s . s h i f t ( ) ; 
                         
                         l e t   m y W a r n i n g M s g   =   m y B u i l d M e s s a g e [ 1 ]   +   '   s u c c e e d e d ,   b u t   w i t h   t h e   f o l l o w i n g   w a r n i n g ' ; 
                         i f   ( m y W a r n i n g s . l e n g t h   = =   1 )   { 
                                 m y W a r n i n g M s g   + =   ' : \ n \ n '   +   m y W a r n i n g s [ 0 ] ; 
                         }   e l s e   { 
                                 m y W a r n i n g M s g   + =   ' s : \ n \ n '   +   k I n d e n t   +   k D o t S e l e c t e d   +   '   '   + 
                                         m y W a r n i n g s . j o i n ( ' \ n '   +   k I n d e n t   +   k D o t S e l e c t e d   +   '   ' ) ; 
                         } 
                         d i a l o g ( m y W a r n i n g M s g ,   { 
                                 w i t h T i t l e :   ' W a r n i n g ' , 
                                 w i t h I c o n :   ' c a u t i o n ' , 
                                 b u t t o n s :   [ ' O K ' ] , 
                                 d e f a u l t B u t t o n :   1 
                         } ) ; 
                 }   e l s e   { 
                         
                         / /   d e t e r m i n e   i f   e r r o r   i s   r e p o r t a b l e 
                         i f   ( ! m y D o R e p o r t )   { 
                                 [ m y D o R e p o r t ,   m y E r r . m e s s a g e ]   =   e r r I s R e p o r t a b l e ( m y E r r . m e s s a g e ) ; 
                         } 
                         
                         / /   s h o w   e r r o r   d i a l o g   &   q u i t   o r   g o   b a c k 
                         r e t u r n   { 
                                 m e s s a g e :   m y B u i l d M e s s a g e [ 1 ]   +   ( a I n f o . s t e p I n f o . i s O n l y A p p   ?   ' '   :   '   o f   a p p   " '   +   a I n f o . a p p I n f o . d i s p l a y N a m e   +   ' " ' )   +   '   f a i l e d :   '   +   m y E r r . m e s s a g e , 
                                 e r r o r :   m y E r r , 
                                 r e p o r t E r r o r :   m y D o R e p o r t , 
                                 t i t l e :   ' A p p l i c a t i o n   N o t   '   +   m y B u i l d M e s s a g e [ 2 ] , 
                                 b a c k S t e p :   0 
                         } ; 
                 } 
         } 
 
 
         / /   S U C C E S S !   G I V E   O P T I O N   T O   R E V E A L   O R   L A U N C H 
 
         i f   ( a I n f o . s t e p I n f o . i s O n l y A p p )   { 
 
                 l e t   m y D l g M e s s a g e   =   ' a p p   " '   +   a I n f o . a p p I n f o . d i s p l a y N a m e   +   ' " . ' ; 
                 i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
                         m y D l g M e s s a g e   =   ' C r e a t e d   E p i c h r o m e   '   +   m y D l g M e s s a g e   +   ' \ n \ n I M P O R T A N T   N O T E :   T h e   f i r s t   t i m e   y o u   r u n ,   a   w e l c o m e   p a g e   w i l l   o p e n .   P l e a s e   r e a d   i t   c a r e f u l l y !   I t   w i l l   s h o w   y o u   h o w   t o   e n a b l e   t h e   c o m p a n i o n   e x t e n s i o n   a n d   p e r f o r m   o t h e r   i m p o r t a n t   s e t u p . ' ; 
                 }   e l s e   i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                         m y D l g M e s s a g e   =   ' S a v e d   c h a n g e s   t o   '   +   m y D l g M e s s a g e ; 
                 }   e l s e   { 
                         m y D l g M e s s a g e   =   ' U p d a t e d   '   +   m y D l g M e s s a g e ; 
                 } 
 
                 / /   r e s e t   d i a l o g   i c o n   a s   a p p   m a y   h a v e   m o v e d 
                 a I n f o . a p p I n f o . i c o n P a t h   =   ' C o n t e n t s / R e s o u r c e s / a p p . i c n s ' ; 
                 l e t   m y D l g I c o n   =   s e t D l g I c o n ( a I n f o . a p p I n f o ) ; 
                 
                 m y D l g R e s u l t   =   d i a l o g ( m y D l g M e s s a g e ,   { 
                         w i t h T i t l e :   " S u c c e s s ! " , 
                         w i t h I c o n :   m y D l g I c o n , 
                         b u t t o n s :   [ " L a u n c h   N o w " ,   " R e v e a l   i n   F i n d e r " ,   " Q u i t " ] , 
                         d e f a u l t B u t t o n :   1 , 
                         c a n c e l B u t t o n :   3 
                 } ) . b u t t o n I n d e x ; 
                 
                 / /   L a u n c h   N o w 
                 i f   ( m y D l g R e s u l t   = =   0 )   { 
                         d e l a y ( 1 ) ; 
                         t r y   { 
                                 l a u n c h A p p ( a I n f o . a p p I n f o . f i l e . p a t h ,   [ ] ,   [ ] ,   { r e g i s t e r F i r s t :   t r u e } ) ; 
                         }   c a t c h ( m y E r r )   { 
                                 d i a l o g ( m y E r r . m e s s a g e   +   '   P l e a s e   t r y   l a u n c h i n g   f r o m   t h e   F i n d e r . ' ,   { 
                                         w i t h T i t l e :   ' U n a b l e   t o   L a u n c h ' , 
                                         w i t h I c o n :   ' c a u t i o n ' , 
                                         b u t t o n s :   [ ' O K ' ] , 
                                         d e f a u l t B u t t o n :   1 
                                 } ) ; 
                                 m y D l g R e s u l t   =   1 ; 
                         } 
                 } 
                 
                 / /   R e v e a l   i n   F i n d e r 
                 i f   ( m y D l g R e s u l t   = =   1 )   { 
                         l e t   m y A c t i v a t e F i n d e r   =   t r u e ; 
                         t r y   { 
                                 k F i n d e r . s e l e c t ( P a t h ( a I n f o . a p p I n f o . f i l e . p a t h ) ) ; 
                         }   c a t c h ( m y E r r )   { 
                                 
                                 l e t   m y F i n d e r E r r M s g   =   ' U n a b l e   t o   r e v e a l   " '   +   a I n f o . a p p I n f o . f i l e . n a m e   +   ' "   i n   t h e   F i n d e r :   ' ; 
                                 
                                 / /   b a d   p a t h 
                                 i f   ( m y E r r . e r r o r N u m b e r   = =   - 1 0 0 1 0 )   {   m y E r r . m e s s a g e   =   ' N o t   f o u n d . ' ;   } 
                                 
                                 m y F i n d e r E r r M s g   + =   m y E r r . m e s s a g e ; 
                                 
                                 e r r l o g ( m y F i n d e r E r r M s g ) ; 
                                 
                                 t r y   { 
                                         k F i n d e r . s e l e c t ( P a t h ( a I n f o . a p p I n f o . f i l e . d i r ) ) ; 
                                 }   c a t c h ( m y E r r )   { 
                                         
                                         m y F i n d e r E r r M s g   + =   '   A l s o   u n a b l e   t o   r e v e a l   e n c l o s i n g   f o l d e r :   ' ; 
                                         
                                         / /   b a d   p a t h 
                                         i f   ( m y E r r . e r r o r N u m b e r   = =   - 1 0 0 1 0 )   {   m y E r r . m e s s a g e   =   ' N o t   f o u n d . ' ;   } 
                                         
                                         m y F i n d e r E r r M s g   + =   m y E r r . m e s s a g e ; 
                                         
                                         e r r l o g ( m y F i n d e r E r r M s g ) ; 
                                         
                                         / /   i f   n o   e n c l o s i n g   f o l d e r ,   d o n ' t   b o t h e r   a c t i v a t i n g   t h e   F i n d e r 
                                         m y A c t i v a t e F i n d e r   =   f a l s e ; 
                                 } 
                                 
                                 / /   s h o w   a n   e r r o r   d i a l o g 
                                 d i a l o g ( m y F i n d e r E r r M s g ,   { 
                                         w i t h T i t l e :   ' U n a b l e   t o   R e v e a l   i n   F i n d e r ' , 
                                         w i t h I c o n :   ' c a u t i o n ' , 
                                         b u t t o n s :   [ ' O K ' ] , 
                                         d e f a u l t B u t t o n :   1 
                                 } ) ; 
                         } 
                         
                         / /   a c t i v a t e   F i n d e r   i f   w e   o p e n e d   a   w i n d o w 
                         i f   ( m y A c t i v a t e F i n d e r )   {   k F i n d e r . a c t i v a t e ( ) ;   } 
                 } 
         } 
         
         / /   w e ' r e   f i n i s h e d !   q u i t 
         r e t u r n   f a l s e ; 
 } 
 
 
 / /   - - -   P R E F E R E N C E S   - - - 
 
 / /   E D I T P R E F E R E N C E S :   e d i t   E p i c h r o m e   p r e f e r e n c e s 
 f u n c t i o n   e d i t P r e f e r e n c e s ( )   { 
         w h i l e   ( t r u e )   { 
                 l e t   i D l g R e s u l t   =   d i a l o g ( '   E p i c h r o m e   l o g i n   s c a n   i s   '   +   ( g E p i L o g i n S c a n E n a b l e d   ?   ' e n a b l e d '   :   ' d i s a b l e d ' )   +   ' . ' ,   { 
                         w i t h T i t l e :   ' L o g i n   S c a n   |   E p i c h r o m e   '   +   k V e r s i o n , 
                         w i t h I c o n :   k E p i S c a n I c o n , 
                         b u t t o n s :   [ ( g E p i L o g i n S c a n E n a b l e d   ?   ' D i s a b l e '   :   ' E n a b l e ' ) ,   ' D o n e ' ] , 
                         d e f a u l t B u t t o n :   2 
                 } ) . b u t t o n I n d e x ; 
                 i f   ( i D l g R e s u l t   = =   0 )   { 
                         / /   i f   d i s a b l i n g ,   c o n f i r m   f i r s t 
                         i f   ( g E p i L o g i n S c a n E n a b l e d   & &   ! c o n f i r m D i s a b l e L o g i n S c a n ( ) )   { 
                                 c o n t i n u e ; 
                         } 
                         
                         / /   c h a n g e   s t a t e 
                         l o g i n S c a n S e t S t a t e ( ! g E p i L o g i n S c a n E n a b l e d ) ; 
                         b r e a k ; 
                 }   e l s e   { 
                         b r e a k ; 
                 } 
         } 
 } 
 
 
 / /   C O N F I R M D I S A B L E L O G I N S C A N :   c o n f i r m   d i a l o g   f o r   d i s a b l i n g   l o g i n   s c a n 
 f u n c t i o n   c o n f i r m D i s a b l e L o g i n S c a n ( )   { 
         r e t u r n   ( d i a l o g ( " A r e   y o u   s u r e   y o u   w a n t   t o   d i s a b l e   t h e   l o g i n   s c a n ?   I f   a n y   a p p s   a r e   r u n n i n g   d u r i n g   a   s y s t e m   c r a s h ,   y o u ' l l   n e e d   t o   f i x   t h e m   m a n u a l l y   b y   r u n n i n g   t h e   \ " E p i c h r o m e   S c a n \ "   a p p ,   i n s t a l l e d   a l o n g s i d e   E p i c h r o m e . " ,   { 
                 w i t h T i t l e :   ' C o n f i r m ' , 
                 w i t h I c o n :   k E p i S c a n I c o n , 
                 b u t t o n s :   [ ' N o ' ,   ' Y e s ' ] , 
                 d e f a u l t B u t t o n :   1 
         } ) . b u t t o n I n d e x   = =   1 ) ; 
 } 
 
 
 / /   - - -   L O G I N   I T E M   F U N C T I O N S   - - - 
 
 O b j C . i m p o r t ( ' S e r v i c e M a n a g e m e n t ' ) ; 
 c o n s t   k L o g i n S c a n I D   =   ' o r g . e p i c h r o m e . L o g i n ' ; 
 
 / /   L O G I N S C A N G E T S T A T E :   r e t u r n   t r u e   i f   l o g i n   i t e m   i s   i n s t a l l e d   o r   f a l s e   i f   n o t 
 f u n c t i o n   l o g i n S c a n G e t S t a t e ( )   { 
         
         / /   g e t   a l l   s t a r t u p   j o b s 
         l e t   i L o g i n I t e m s   =   $ . S M C o p y A l l J o b D i c t i o n a r i e s ( $ . k S M D o m a i n U s e r L a u n c h d ) ; 
         
         / /   i n   a t   l e a s t   C a t a l i n a   a n d   l a t e r ,   S M C o p y A l l J o b D i c t i o n a r i e s   r e t u r n s   a   R e f 
         i f   ( i L o g i n I t e m s . t o N S )   {   i L o g i n I t e m s   =   i L o g i n I t e m s . t o N S ( ) ;   } 
         
         / /   l o o k   f o r   o u r   l o g i n   s c a n n e r 
         f o r   ( l e t   c u r I t e m   o f   i L o g i n I t e m s . j s )   { 
                 i f   ( c u r I t e m . j s . L a b e l . j s   = =   k L o g i n S c a n I D )   { 
                         r e t u r n   t r u e ; 
                 } 
         } 
         
         / /   i f   w e   g o t   h e r e ,   w e   d i d n ' t   f i n d   i t 
         r e t u r n   f a l s e ; 
 } 
 
 
 / /   L O G I N S C A N S E T S T A T E :   r e t u r n   t r u e   i f   l o g i n   i t e m   i s   i n s t a l l e d   o r   f a l s e   i f   n o t 
 f u n c t i o n   l o g i n S c a n S e t S t a t e ( a N e w S t a t e )   { 
         i f   ( $ . S M L o g i n I t e m S e t E n a b l e d ( $ ( k L o g i n S c a n I D ) ,   a N e w S t a t e ) )   { 
                 
                 / /   s u c c e s s 
                 g E p i L o g i n S c a n E n a b l e d   =   a N e w S t a t e ; 
                 
         }   e l s e   { 
                 
                 / /   e r r o r   a l e r t 
                 / /   $ $ $   D O N ' T   O F F E R   R E P O R T I N G : 
                 / /     +   ' \ n \ n I f   t h e   p r o b l e m   p e r s i s t s ,   p l e a s e   r e p o r t   i t   o n   G i t H u b . ' 
                 l e t   i N e w S t a t e V e r b   =   ( a N e w S t a t e   ?   ' e n a b l e '   :   ' d i s a b l e ' ) ; 
                 i f   ( d i a l o g ( ' U n a b l e   t o   '   +   i N e w S t a t e V e r b   +   '   E p i c h r o m e   l o g i n   i t e m . '   +   ( ( g E p i L o g i n S c a n E n a b l e d   = =   ' u n s e t ' )   ?   '   Y o u   c a n   t r y   a g a i n   t o   '   +   i N e w S t a t e V e r b   +   '   i t   b y   h o l d i n g   d o w n   t h e   O p t i o n   k e y   n e x t   t i m e   y o u   r u n   E p i c h r o m e . '   :   ' ' ) ,   { 
                         w i t h T i t l e :   ' L o g i n   S c a n   N o t   '   +   i N e w S t a t e V e r b . c a p i t a l i z e d ( )   +   ' d ' , 
                         w i t h I c o n :   ' c a u t i o n ' , 
                         d e f a u l t B u t t o n :   1 , 
                         b u t t o n s :   [ ' O K ' ]     / /   ,   ' R e p o r t   E r r o r ' ]     / /   $ $ $   D I S A B L E   R E P O R T I N G 
                 } ) . b u t t o n I n d e x   = =   1 )   { 
                         
                         / /   r e p o r t   e r r o r 
                         / /   $ $ $   D I S A B L E   R E P O R T I N G 
                         / /   r e p o r t E r r o r ( ' E p i c h r o m e   '   +   k V e r s i o n   +   '   u n a b l e   t o   '   +   i N e w S t a t e V e r b   +   '   l o g i n   s c a n ' ) ; 
                 } 
                 g E p i L o g i n S c a n E n a b l e d   =   l o g i n S c a n G e t S t a t e ( ) ; 
         } 
 } 
 
 
 / /   - - -   A P P   I N F O   F U N C T I O N S   - - - 
 
 / /   U P D A T E A P P I N F O :   u p d a t e   a n   a p p   s e t t i n g 
 f u n c t i o n   u p d a t e A p p I n f o ( a I n f o ,   a K e y ,   a V a l u e )   { 
 
         / /   n o r m a l i z e   a K e y 
         i f   ( ! ( a K e y   i n s t a n c e o f   A r r a y ) )   { 
                 a K e y   =   [ a K e y . t o S t r i n g ( ) ] ; 
         } 
 
         / /   m a k e   s u r e   w e   h a v e   n e c e s s a r y   o b j e c t s 
         i f   ( ! a I n f o . a p p I n f o )   {   a I n f o . a p p I n f o   =   { } ;   } 
         i f   ( ! a I n f o . a p p I n f o S t a t u s )   {   a I n f o . a p p I n f o S t a t u s   =   { } ;   } 
 
         / /   l o o p   t h r o u g h   a l l   k e y s 
         f o r   ( l e t   c u r K e y   o f   a K e y )   { 
 
                 l e t   c u r V a l u e   =   a V a l u e ; 
 
                 / /   p a t h   &   v e r s i o n   k e y s   a r e   o n l y   f o r   s u m m a r y 
                 i f   ( ! ( ( c u r K e y   = =   ' p a t h ' )   | |   ( c u r K e y   = =   ' v e r s i o n ' ) ) )   { 
 
                         / /   g e t   d e f a u l t   v a l u e 
                         l e t   c u r D e f a u l t V a l u e ; 
                         i f   ( a I n f o . o l d A p p I n f o )   { 
                                 c u r D e f a u l t V a l u e   =   a I n f o . o l d A p p I n f o [ c u r K e y ] ; 
                         }   e l s e   { 
                                 c u r D e f a u l t V a l u e   =   g A p p I n f o D e f a u l t [ c u r K e y ] ; 
                         } 
 
                         / /   i f   n o   v a l u e   &   n o t   a l r e a d y   a   k e y ,   u s e   d e f a u l t 
                         i f   ( ( c u r V a l u e   = = =   u n d e f i n e d )   & &   ( ! a I n f o . a p p I n f o . h a s O w n P r o p e r t y ( c u r K e y ) ) )   { 
                                 c u r V a l u e   =   c u r D e f a u l t V a l u e ; 
                         } 
 
                         / /   i f   w e   n o w   h a v e   a   v a l u e ,   c o p y   i t   i n t o   a p p I n f o 
                         i f   ( c u r V a l u e   ! = =   u n d e f i n e d )   { 
                                 a I n f o . a p p I n f o [ c u r K e y ]   =   o b j C o p y ( c u r V a l u e ) ; 
                         } 
                 } 
 
                 / /   i n i t i a l i z e   s t a t u s   o b j e c t 
                 l e t   c u r S t a t u s   =   { } ; 
                 a I n f o . a p p I n f o S t a t u s [ c u r K e y ]   =   c u r S t a t u s ; 
 
                 / /   s e t   c h a n g e d   s t a t u s 
                 i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                         i f   ( c u r K e y   = =   ' u r l s ' )   { 
                                 l e t   m y U r l S l i c e   =   ( a I n f o . a p p I n f o . w i n d o w S t y l e   = =   k W i n S t y l e A p p )   ?   1   :   u n d e f i n e d ; 
                                 l e t   m y O l d U r l S l i c e   =   ( a I n f o . o l d A p p I n f o . w i n d o w S t y l e   = =   k W i n S t y l e A p p )   ?   1   :   u n d e f i n e d ; 
                                 c u r S t a t u s . c h a n g e d   =   ! o b j E q u a l s ( a I n f o . a p p I n f o . u r l s . s l i c e ( 0 , m y U r l S l i c e ) , 
                                                                                                 a I n f o . o l d A p p I n f o . u r l s . s l i c e ( 0 , m y O l d U r l S l i c e ) ) ; 
                         }   e l s e   i f   ( c u r K e y   = =   ' p a t h ' )   { 
                                 c u r S t a t u s . c h a n g e d   =   f a l s e ; 
                         }   e l s e   i f   ( c u r K e y   = =   ' v e r s i o n ' )   { 
                                 c u r S t a t u s . c h a n g e d   =   ( a I n f o . a p p I n f o . v e r s i o n   ! =   k V e r s i o n ) ; 
                         }   e l s e   { 
                                 c u r S t a t u s . c h a n g e d   =   ! o b j E q u a l s ( a I n f o . a p p I n f o [ c u r K e y ] ,   a I n f o . o l d A p p I n f o [ c u r K e y ] ) ; 
                         } 
                 }   e l s e   { 
                         c u r S t a t u s . c h a n g e d   =   f a l s e ; 
                 } 
 
 
                 / /   S E T   S U M M A R I E S 
 
                 / /   i n i t i a l i z e   s u m m a r i e s 
                 c u r S t a t u s . s t e p S u m m a r y   =   ' ' ; 
                 c u r S t a t u s . b u i l d S u m m a r y   =   ' ' ; 
 
                 / /   f u n c t i o n   t o   s e l e c t   d o t 
                 f u n c t i o n   d o t ( )   { 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                                 i f   ( c u r S t a t u s . c h a n g e d )   { 
                                         r e t u r n   k D o t C h a n g e d   +   '   ' ; 
                                 }   e l s e   { 
                                         r e t u r n   k D o t C u r r e n t   +   '   ' ; 
                                 } 
                         }   e l s e   i f   ( k A d v a n c e d D e f a u l t s . h a s O w n P r o p e r t y ( c u r K e y ) )   { 
                                 r e t u r n   k D o t A d v a n c e d   +   '   ' ; 
                         }   e l s e   { 
                                 r e t u r n   k D o t S e l e c t e d   +   '   ' ; 
                         } 
                 } 
                 
                 i f   ( c u r K e y   = =   ' p a t h ' )   { 
                         
                         / /   P A T H 
 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E )   { 
 
                                 / /   s e t   b u i l d   s u m m a r y   o n l y   i n   c r e a t e   m o d e 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s . p a t h   +   ' : \ n '   +   k I n d e n t   +   d o t ( )   + 
                                         ( a I n f o . a p p I n f o . f i l e   ?   a I n f o . a p p I n f o . f i l e . p a t h   :   ' [ n o t   y e t   s e t ] ' ) ; 
                         } 
 
                 }   e l s e   i f   ( c u r K e y   = =   ' v e r s i o n ' )   { 
 
                         / /   V E R S I O N 
 
                         i f   ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   & &   c u r S t a t u s . c h a n g e d )   { 
 
                                 / /   s e t   b u i l d   s u m m a r y   o n l y   i f   i n   e d i t   m o d e   &   w e   n e e d   a n   u p d a t e 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s . v e r s i o n   +   ' : \ n '   +   k I n d e n t   + 
                                         k D o t N e e d s U p d a t e   +   '   '   +   a I n f o . a p p I n f o . v e r s i o n ; 
                         } 
 
                 }   e l s e   i f   ( ( c u r K e y   = =   ' d i s p l a y N a m e ' )   | |   ( c u r K e y   = =   ' s h o r t N a m e ' ) )   { 
 
                         / /   D I S P L A Y N A M E   &   S H O R T N A M E 
 
                         / /   s t e p   s u m m a r y   - -   s h o w   o l d   v a l u e   o n l y 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                                 i f   ( c u r S t a t u s . c h a n g e d )   { 
                                         c u r S t a t u s . s t e p S u m m a r y   =   ' \ n \ n '   +   k I n d e n t   +   k D o t C h a n g e d   +   '   W a s :   '   +   a I n f o . o l d A p p I n f o [ c u r K e y ] ; 
                                 }   e l s e   { 
                                         c u r S t a t u s . s t e p S u m m a r y   =   ' \ n \ n '   +   k I n d e n t   +   k D o t C u r r e n t   +   '   [ n o t   e d i t e d ] ' ; 
                                 } 
                         } 
 
                         / /   s e t   b u i l d   s u m m a r y 
                         c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s [ c u r K e y ]   +   ' : \ n '   +   k I n d e n t   +   d o t ( )   +   a I n f o . a p p I n f o [ c u r K e y ] ; 
 
                 }   e l s e   i f   ( c u r K e y   = =   ' i d ' )   { 
 
                         / /   I D   - -   A D V A N C E D 
                         
                         / /   s e t   i s C u s t o m I D 
                         a I n f o . s t e p I n f o . i s C u s t o m I D   =   ( a I n f o . a p p I n f o . i d   ! =   a I n f o . s t e p I n f o . a u t o I D ) ; 
                         
                         / /   s t e p   s u m m a r y 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                                 c u r S t a t u s . s t e p S u m m a r y   =   ' \ n \ n '   +   k I n d e n t   +   d o t ( )   +   a I n f o . a p p I n f o . i d ; 
                                 i f   ( c u r S t a t u s . c h a n g e d )   { 
                                         c u r S t a t u s . s t e p S u m m a r y   + =   '     |     W a s :   '   +   ( a I n f o . o l d A p p I n f o . i d   ?   a I n f o . o l d A p p I n f o . i d   :   ' [ n o   I D ] ' ) ; 
                                 } 
                         } 
                         
                         / /   b u i l d   s u m m a r y 
                         i f   ( a I n f o . s t e p I n f o . i s C u s t o m I D   | | 
                                 ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   & &   c u r S t a t u s . c h a n g e d ) )   { 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s . i d   +   ' : \ n '   + 
                                         k I n d e n t   +   d o t ( )   +   a I n f o . a p p I n f o . i d   +   ' \ n '   + 
                                         k I n d e n t   +   d o t ( )   +   ' ~ / L i b r a r y / A p p l i c a t i o n   S u p p o r t / E p i c h r o m e / A p p s / '   +   a I n f o . a p p I n f o . i d ; 
                         }   e l s e   { 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   n u l l ; 
                         } 
 
                 }   e l s e   i f   ( c u r K e y   = =   ' u r l s ' )   { 
                         
                         / /   U R L S 
                         
                         i f   ( a I n f o . a p p I n f o . w i n d o w S t y l e   = =   k W i n S t y l e A p p )   { 
                                 
                                 / /   s e t   s t e p   s u m m a r y   f o r   a p p - s t y l e   U R L   o n l y 
                                 i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
 
                                         / /   d i s p l a y   o l d   v a l u e   i n   s t e p   s u m m a r y 
                                         i f   ( c u r S t a t u s . c h a n g e d )   { 
                                                 c u r S t a t u s . s t e p S u m m a r y   =   ' \ n \ n '   +   k I n d e n t   +   k D o t C h a n g e d   +   '   W a s :   ' ; 
                                                 i f   ( a I n f o . o l d A p p I n f o . w i n d o w S t y l e   = =   k W i n S t y l e A p p )   { 
                                                         / /   a p p   U R L   s u m m a r y 
                                                         c u r S t a t u s . s t e p S u m m a r y   + =   a I n f o . o l d A p p I n f o . u r l s [ 0 ] ; 
                                                 }   e l s e   { 
                                                         / /   b r o w s e r   t a b   s u m m a r y 
                                                         i f   ( a I n f o . o l d A p p I n f o . u r l s . l e n g t h   = =   0 )   { 
                                                                 c u r S t a t u s . s t e p S u m m a r y   + =   '   [ n o n e ] ' ; 
                                                         }   e l s e   { 
                                                                 l e t   m y U r l P r e f i x   =   ' \ n '   +   k I n d e n t   +   k I n d e n t   +   k D o t U n s e l e c t e d   +   '   ' ; 
                                                                 c u r S t a t u s . s t e p S u m m a r y   + =   m y U r l P r e f i x   +   a I n f o . o l d A p p I n f o . u r l s . j o i n ( m y U r l P r e f i x ) ; 
                                                         } 
                                                 } 
                                         }   e l s e   { 
                                                 l e t   m y U r l N a m e   =   ( a I n f o . a p p I n f o . w i n d o w S t y l e   = =   k W i n S t y l e A p p )   ?   ' U R L '   :   ' U R L s ' ; 
                                                 c u r S t a t u s . s t e p S u m m a r y   =   ' \ n \ n '   +   k I n d e n t   +   k D o t C u r r e n t   +   '   [ '   +   m y U r l N a m e   +   '   n o t   e d i t e d ] ' ; 
                                         } 
                                 } 
                         } 
 
                         / /   s e t   b u i l d   s u m m a r y 
                         i f   ( a I n f o . a p p I n f o . w i n d o w S t y l e   = =   k W i n S t y l e A p p )   { 
                                 / /   a p p - s t y l e   U R L   s u m m a r y 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s . u r l s [ 0 ]   +   ' : \ n '   +   k I n d e n t   +   d o t ( )   +   a I n f o . a p p I n f o . u r l s [ 0 ] ; 
                         }   e l s e   { 
                                 / /   b r o w s e r - s t y l e   U R L   s u m m a r y 
                                 l e t   m y U r l P r e f i x   =   k I n d e n t   +   d o t ( ) ; 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s . u r l s [ 1 ]   +   ' : \ n '   +   m y U r l P r e f i x ; 
                                 i f   ( a I n f o . a p p I n f o . u r l s . l e n g t h   = =   0 )   { 
                                         c u r S t a t u s . b u i l d S u m m a r y   + =   ' [ n o n e ] ' ; 
                                 }   e l s e   { 
                                         c u r S t a t u s . b u i l d S u m m a r y   + =   a I n f o . a p p I n f o . u r l s . j o i n ( ' \ n '   +   m y U r l P r e f i x ) ; 
                                 } 
                         } 
                         
                 }   e l s e   i f   ( c u r K e y   = =   ' i c o n ' )   { 
 
                         / /   I C O N 
 
                         / /   s e t   c o m m o n   s u m m a r y 
                         l e t   c u r S u m m a r y ; 
                         i f   ( a I n f o . a p p I n f o . i c o n   = =   k I c o n D E F A U L T )   { 
                                 c u r S u m m a r y   =   d o t ( )   +   ' [ d e f a u l t ] ' ; 
                         }   e l s e   i f   ( a I n f o . a p p I n f o . i c o n   = =   k I c o n C U S T O M )   { 
                                 c u r S u m m a r y   =   d o t ( )   +   ' [ e x i s t i n g   c u s t o m ] ' ; 
                         }   e l s e   i f   ( a I n f o . a p p I n f o . i c o n   = =   k I c o n A U T O )   { 
                                 c u r S u m m a r y   =   d o t ( )   +   ' [ e x i s t i n g   a u t o ] ' ; 
                         }   e l s e   { 
                                 c u r S u m m a r y   =   d o t ( )   + 
                                         ( a I n f o . a p p I n f o . i c o n . a u t o I c o n U r l   ?   ' [ '   +   a I n f o . a p p I n f o . i c o n . a u t o I c o n U r l   +   ' ] '   :   a I n f o . a p p I n f o . i c o n . n a m e ) ; 
                         } 
                         
                         / /   s e t   s t e p   s u m m a r y 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
 
                                 c u r S t a t u s . s t e p S u m m a r y   =   ' \ n \ n '   +   k I n d e n t   +   c u r S u m m a r y ; 
 
                                 / /   d i s p l a y   o l d   v a l u e 
                                 i f   ( c u r S t a t u s . c h a n g e d )   { 
                                         c u r S t a t u s . s t e p S u m m a r y   + =   '     |     W a s :   '   + 
                                                 ( ( a I n f o . o l d A p p I n f o . i c o n   = =   k I c o n D E F A U L T )   ? 
                                                         ' [ d e f a u l t ] '   : 
                                                         ( ( a I n f o . o l d A p p I n f o . i c o n   = =   k I c o n C U S T O M )   ? 
                                                                 ' [ e x i s t i n g   c u s t o m ] '   : 
                                                                 ' [ e x i s t i n g   a u t o ] ' ) ) ; 
                                 } 
                         } 
 
                         / /   s e t   b u i l d   s u m m a r y 
                         c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s . i c o n   +   ' : \ n '   +   k I n d e n t   +   c u r S u m m a r y ; 
 
                 }   e l s e   i f   ( c u r K e y   = =   ' e n g i n e ' )   { 
 
                         / /   E N G I N E 
                         
                         / /   s e t   c o m m o n   s u m m a r y 
                         l e t   c u r S u m m a r y   =   d o t ( )   +   a I n f o . a p p I n f o . e n g i n e . b u t t o n ; 
 
                         / /   n o   s t e p   s u m m a r y   n e e d e d ,   b u t   w e ' l l   s e t   o n e   f o r   s a f e t y 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                                 c u r S t a t u s . s t e p S u m m a r y   =   ' \ n \ n '   +   k I n d e n t   +   c u r S u m m a r y ; 
                         } 
 
                         / /   s e t   b u i l d   s u m m a r y 
                         c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s . e n g i n e   +   ' : \ n '   +   k I n d e n t   +   c u r S u m m a r y ; 
 
                 }   e l s e   i f   ( c u r K e y   = =   ' u p d a t e A c t i o n ' )   { 
                         
                         / /   U P D A T E A C T I O N   - -   A D V A N C E D 
                         
                         / /   n e e d   t w o   s t e p   s u m m a r i e s   f o r   t h e   t w o   l e v e l s   o f   d i a l o g s 
                         c u r S t a t u s . s t e p S u m m a r y   =   [ ' ' ,   ' ' ] ; 
                         
                         / /   s t e p   s u m m a r y   - -   s h o w   o l d   v a l u e   o n l y 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                                 
                                 / /   c r e a t e   b a s i c   s u m m a r y   f o r   s e c o n d - l e v e l   d i a l o g 
                                 c u r S t a t u s . s t e p S u m m a r y [ 1 ]   =   ' \ n \ n '   +   k I n d e n t   +   d o t ( )   + 
                                         k U p d a t e A c t i o n s [ a I n f o . a p p I n f o . u p d a t e A c t i o n ] . b u t t o n ; 
                                 
                                 i f   ( c u r S t a t u s . c h a n g e d )   { 
                                         l e t   c u r W a s S u m m a r y   =   ' W a s :   '   + 
                                                 k U p d a t e A c t i o n s [ a I n f o . o l d A p p I n f o . u p d a t e A c t i o n ] . b u t t o n ; 
                                         c u r S t a t u s . s t e p S u m m a r y [ 0 ]   =   ' \ n \ n '   +   k I n d e n t   +   k D o t C h a n g e d   +   c u r W a s S u m m a r y ; 
                                         c u r S t a t u s . s t e p S u m m a r y [ 1 ]   + =   '     |     '   +   c u r W a s S u m m a r y ; 
                                 } 
                         } 
                         
                         / /   s e t   b u i l d   s u m m a r y 
                         i f   ( ( a I n f o . a p p I n f o . u p d a t e A c t i o n   ! =   k A d v a n c e d D e f a u l t s . u p d a t e A c t i o n )   | | 
                                 ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   & &   c u r S t a t u s . c h a n g e d ) )   { 
                                 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s [ c u r K e y ]   +   ' : \ n '   +   k I n d e n t   +   d o t ( )   + 
                                         k U p d a t e A c t i o n s [ a I n f o . a p p I n f o . u p d a t e A c t i o n ] . b u t t o n ; 
                         }   e l s e   { 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   n u l l ; 
                         } 
                         
                 }   e l s e   { 
 
                         / /   A L L   O T H E R   K E Y S 
 
                         / /   s e t   c o m m o n   s u m m a r y 
                         l e t   c u r S u m m a r y   =   d o t ( )   + 
                                 ( ( t y p e o f   a I n f o . a p p I n f o [ c u r K e y ]   = =   ' b o o l e a n ' )   ? 
                                         ( a I n f o . a p p I n f o [ c u r K e y ]   ?   ' Y e s '   :   ' N o ' )   : 
                                         a I n f o . a p p I n f o [ c u r K e y ] ) ; 
                         
                         / /   s e t   s t e p   s u m m a r y 
                         i f   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   { 
                                 c u r S t a t u s . s t e p S u m m a r y   =   ' \ n \ n '   +   k I n d e n t   +   c u r S u m m a r y ; 
                         } 
 
                         / /   s e t   b u i l d   s u m m a r y 
                         i f   ( ( ! k A d v a n c e d D e f a u l t s . h a s O w n P r o p e r t y ( c u r K e y ) )   | | 
                                 ( k A d v a n c e d D e f a u l t s [ c u r K e y ]   ! =   a I n f o . a p p I n f o [ c u r K e y ] )   | | 
                                 ( ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n E D I T )   & &   c u r S t a t u s . c h a n g e d ) )   { 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   k A p p I n f o K e y s [ c u r K e y ]   +   ' : \ n '   +   k I n d e n t   +   c u r S u m m a r y ; 
                         }   e l s e   { 
                                 c u r S t a t u s . b u i l d S u m m a r y   =   n u l l ; 
                         } 
                 } 
         } 
 } 
 
 
 / /   G E T P A T H I N F O :   b r e a k   d o w n   a   g e n e r i c   p a t h 
 f u n c t i o n   g e t P a t h I n f o ( a P a t h )   { 
 
         l e t   m y R e s u l t   =   { } ; 
 
         / /   t h e   i n p u t   p a t h   m u s t   b e   f u l l y - q u a l i f i e d 
         l e t   m y M a t c h   =   a P a t h . m a t c h ( ' ^ ( ( / ( [ ^ / ] + / + ) * [ ^ / ] + ) / + ) ( [ ^ / ] + ) / * $ ' ) ; 
 
         i f   ( m y M a t c h )   { 
 
                 m y R e s u l t   =   { 
 
                         / /   p a t h :   f u l l   p a t h   i n c l u d i n g   i t e m 
                         p a t h :   a P a t h , 
 
                         / /   d i r :   d i r e c t o r y   c o n t a i n i n g   i t e m 
                         d i r :   m y M a t c h [ 2 ] , 
 
                         / /   n a m e :   n a m e   o f   i t e m   ( i n c l u d i n g   a n y   e x t e n s i o n ) 
                         n a m e :   m y M a t c h [ 4 ] 
                 } ; 
 
         }   e l s e   { 
                 m y R e s u l t   =   n u l l ; 
         } 
 
         r e t u r n   m y R e s u l t ; 
 } 
 
 
 / /   G E T A P P P A T H I N F O :   b r e a k   d o w n   a n   a p p   p a t h   &   f i l l   i n   e x t r a   i n f o 
 f u n c t i o n   g e t A p p P a t h I n f o ( a P a t h ,   a A p p I n f o = t r u e )   { 
 
         / /   g e t   b a s i c   p a t h   i n f o 
         l e t   m y R e s u l t ; 
         i f   ( a P a t h   i n s t a n c e o f   O b j e c t )   { 
                 m y R e s u l t   =   ( ( a P a t h . p a t h   & &   a P a t h . n a m e )   ?   a P a t h   :   n u l l ) ; 
         }   e l s e   { 
                 m y R e s u l t   =   g e t P a t h I n f o ( a P a t h ) ; 
         } 
         i f   ( ! m y R e s u l t )   {   r e t u r n   n u l l ;   } 
 
         / /   s e t   u p   a p p - s p e c i f i c   i n f o 
         l e t   m y M a t c h   =   m y R e s u l t . n a m e . m a t c h ( / ^ ( . + ) \ . a p p $ / i ) ; 
         i f   ( m y M a t c h )   { 
                 m y R e s u l t . b a s e   =   m y M a t c h [ 1 ] ; 
                 m y R e s u l t . e x t A d d e d   =   f a l s e ; 
         }   e l s e   { 
                 m y R e s u l t . b a s e   =   m y R e s u l t . n a m e ; 
                 m y R e s u l t . e x t A d d e d   =   t r u e ; 
         } 
 
         / /   i f   w e ' v e   b e e n   p a s s e d   a n   a p p I n f o   o b j e c t ,   f i l l   i t   i n 
         i f   ( a A p p I n f o   i n s t a n c e o f   O b j e c t )   { 
 
                 / /   s e t   a p p I n f o   f i l e 
                 a A p p I n f o . f i l e   =   m y R e s u l t ; 
 
                 / /   C R E A T E   D I S P L A Y   N A M E   &   D E F A U L T   S H O R T N A M E 
 
                 / /   s e t   d i s p l a y   n a m e   f r o m   f i l e   b a s e n a m e 
                 a A p p I n f o . d i s p l a y N a m e   =   a A p p I n f o . f i l e . b a s e ; 
 
                 / /   s t a r t   s h o r t   n a m e   w i t h   d i s p l a y   n a m e 
                 l e t   m y S h o r t N a m e T e m p ; 
                 a A p p I n f o . s h o r t N a m e   =   a A p p I n f o . d i s p l a y N a m e ; 
 
                 / /   t o o   l o n g   - -   r e m o v e   a l l   n o n - a l p h a n u m e r i c s 
                 i f   ( a A p p I n f o . s h o r t N a m e . l e n g t h   >   1 6 )   { 
                         m y S h o r t N a m e T e m p   =   a A p p I n f o . s h o r t N a m e . r e p l a c e ( / [ ^ 0 - 9 a - z ] / g i ,   ' ' ) ; 
                         i f   ( m y S h o r t N a m e T e m p . l e n g t h   >   0 )   { 
                                 / /   s t i l l   s o m e   n a m e   l e f t ,   s o   w e ' l l   u s e   i t 
                                 a A p p I n f o . s h o r t N a m e   =   m y S h o r t N a m e T e m p ; 
                         } 
                 } 
 
                 / /   s t i l l   t o o   l o n g   - -   r e m o v e   a l l   l o w e r c a s e   v o w e l s 
                 i f   ( a A p p I n f o . s h o r t N a m e . l e n g t h   >   1 6 )   { 
                         m y S h o r t N a m e T e m p   =   a A p p I n f o . s h o r t N a m e . r e p l a c e ( / [ a e i o u ] / g ,   ' ' ) ; 
                         i f   ( m y S h o r t N a m e T e m p . l e n g t h   >   0 )   { 
                                 / /   s t i l l   s o m e   n a m e   l e f t ,   s o   w e ' l l   u s e   i t 
                                 a A p p I n f o . s h o r t N a m e   =   m y S h o r t N a m e T e m p ; 
                         } 
                 } 
 
                 / /   s t i l l   s t i l l   t o o   l o n g   - -   t r u n c a t e 
                 i f   ( a A p p I n f o . s h o r t N a m e . l e n g t h   >   1 6 )   { 
                         a A p p I n f o . s h o r t N a m e   =   a A p p I n f o . s h o r t N a m e . s l i c e ( 0 ,   1 6 ) ; 
                 } 
 
                 / /   c a n o n i c a l i z e   a p p   n a m e   &   p a t h 
                 a A p p I n f o . f i l e . n a m e   =   a A p p I n f o . f i l e . b a s e   +   ' . a p p ' ; 
                 a A p p I n f o . f i l e . p a t h   =   a A p p I n f o . f i l e . d i r   +   ' / '   +   a A p p I n f o . f i l e . n a m e ; 
         } 
 
         r e t u r n   m y R e s u l t ; 
 } 
 
 
 / /   E N G I N E N A M E :   g e n e r a t e   e n g i n e   n a m e   f r o m   I D 
 f u n c t i o n   e n g i n e N a m e ( a E n g i n e ,   a C a p T y p e = t r u e )   { 
         l e t   m y R e s u l t ; 
         i f   ( a E n g i n e . t y p e   = =   ' i n t e r n a l ' )   { 
                 m y R e s u l t   =   ' B u i l t - I n ' ; 
         }   e l s e   { 
                 m y R e s u l t   =   ' E x t e r n a l ' ; 
         } 
         i f   ( ! a C a p T y p e )   {   m y R e s u l t   =   m y R e s u l t . t o L o w e r C a s e ( ) ;   } 
 
         i f   ( k B r o w s e r I n f o [ a E n g i n e . i d ] )   { 
                 m y R e s u l t   + =   '   ( '   +   k B r o w s e r I n f o [ a E n g i n e . i d ] . s h o r t N a m e   +   ' ) ' ; 
         } 
         r e t u r n   m y R e s u l t ; 
 } 
 
 
 / /   I C O N P R E V I E W :   c r e a t e   o r   r e t r i e v e   a   p r e v i e w   i c o n   f o r   t h e   c u r r e n t   s e t t i n g s 
 / /                             o p t i o n a l l y   f i n d   t h e   i c o n   s o u r c e   g i v e n   a   U R L 
 f u n c t i o n   i c o n P r e v i e w ( a I c o n ,   a S o u r c e S i z e A r r a y = n u l l ,   a S e a r c h U r l = n u l l )   { 
         
         / /   g e t   s o u r c e   p a t h   f o r   t h i s   i c o n 
         l e t   i I c o n S o u r c e ; 
 
         / /   s h e l l   p a r a m e t e r s 
         l e t   i S h e l l V a r s   =   [ 
                 ' e p i A c t i o n = i c o n ' , 
                 ' e p i I c o n P r e v i e w P a t h = '   +   g C o r e I n f o . p r e v i e w I c o n , 
                 ' e p i I c o n C r o p = '   +   ( g I c o n S e t t i n g s . c r o p   ?   ' 1 '   :   ' ' ) , 
                 ' e p i I c o n C o m p S i z e = '   +   ( g I c o n S e t t i n g s . c o m p B i g S u r   ?   g I c o n S e t t i n g s . c o m p S i z e   :   ' ' ) , 
                 ' e p i I c o n C o m p B G = '   +   ( g I c o n S e t t i n g s . c o m p B i g S u r   ?   g I c o n S e t t i n g s . c o m p B G   :   ' ' ) 
         ] ; 
         
         / /   s h e l l   a c t i o n 
         i f   ( a S e a r c h U r l )   { 
                 
                 / /   s e t   i c o n   s o u r c e 
                 i I c o n S o u r c e   =   a S e a r c h U r l ; 
                 
                 i f   ( a S e a r c h U r l   ! =   i c o n P r e v i e w . s o u r c e )   { 
                         
                         / /   n e w   U R L ,   s o   a d d   a u t o i c o n   p a r a m e t e r s 
                         i S h e l l V a r s . p u s h ( 
                                 ' e p i A u t o I c o n U R L = '   +   a S e a r c h U r l , 
                                 ' e p i A u t o I c o n T e m p D i r = '   +   g C o r e I n f o . a u t o I c o n T e m p D i r 
                         ) ; 
                 } 
                 
                 / /   p a t h   t o   a u t o i c o n   s o u r c e   i m a g e 
                 i S h e l l V a r s . p u s h ( ' e p i I c o n S o u r c e = '   +   g C o r e I n f o . a u t o I c o n S o u r c e ) ; 
                 
         }   e l s e   { 
                 / /   s e t   i c o n   s o u r c e 
                 i f   ( a I c o n . a u t o I c o n U r l )   { 
                         i I c o n S o u r c e   =   a I c o n . a u t o I c o n U r l ; 
                 }   e l s e   { 
                         i I c o n S o u r c e   =   a I c o n . p a t h ; 
                 } 
                 
                 / /   s e t   p r e v i e w - o n l y   a c t i o n 
                 i S h e l l V a r s . p u s h ( ' e p i I c o n S o u r c e = '   +   g e t I c o n S o u r c e P a t h ( a I c o n ) ) ; 
         } 
         
         i f   ( ( ! i c o n P r e v i e w . p a t h )   | |   ( i c o n P r e v i e w . s o u r c e   ! = =   i I c o n S o u r c e )   | | 
                 ( ! o b j E q u a l s ( i c o n P r e v i e w . s e t t i n g s ,   g I c o n S e t t i n g s ) ) )   { 
                 
                 / /   r u n   s c r i p t 
                 l e t   i S o u r c e S i z e ,   i E r r ; 
                 t r y   { 
                         i S o u r c e S i z e   =   s h e l l . a p p l y ( n u l l ,   i S h e l l V a r s ) ; 
                 }   c a t c h ( i E r r )   { 
                         k A p p . a c t i v a t e ( ) ;     / /   b r i n g   a p p   b a c k   t o   t h e   f r o n t 
                         t h r o w   i E r r ; 
                 } 
                 k A p p . a c t i v a t e ( ) ;     / /   b r i n g   a p p   b a c k   t o   t h e   f r o n t 
                 
                 / /   c a c h e   a l l   i n f o 
                 i c o n P r e v i e w . p a t h   =   P a t h ( g C o r e I n f o . p r e v i e w I c o n ) ; 
                 i c o n P r e v i e w . s o u r c e   =   i I c o n S o u r c e ; 
                 i c o n P r e v i e w . s e t t i n g s   =   o b j C o p y ( g I c o n S e t t i n g s ) ; 
 
                 i f   ( i S o u r c e S i z e )   { 
                         i S o u r c e S i z e   =   J S O N . p a r s e ( i S o u r c e S i z e ) ; 
                 }   e l s e   { 
                         i S o u r c e S i z e   =   [ 0 , 0 ] ; 
                 } 
                 i c o n P r e v i e w . s o u r c e W i d t h   =   i S o u r c e S i z e [ 0 ] ; 
                 i c o n P r e v i e w . s o u r c e H e i g h t   =   i S o u r c e S i z e [ 1 ] ; 
         } 
         
         / /   r e t u r n   s o u r c e   s i z e   i f   r e q u e s t e d 
         i f   ( a S o u r c e S i z e A r r a y )   { 
                 a S o u r c e S i z e A r r a y [ 0 ]   =   i c o n P r e v i e w . s o u r c e W i d t h ; 
                 a S o u r c e S i z e A r r a y [ 1 ]   =   i c o n P r e v i e w . s o u r c e H e i g h t ; 
         } 
         
         r e t u r n   i c o n P r e v i e w . p a t h ; 
 } 
 
 
 / /   G E T I C O N T Y P E :   r e t u r n   t h e   t y p e   o f   a n   a p p ' s   i c o n ,   w h e t h e r   f u l l y   s e t   o r   c o d e 
 f u n c t i o n   g e t I c o n T y p e ( a I c o n )   { 
         i f   ( a I c o n   i n s t a n c e o f   O b j e c t )   { 
                 i f   ( a I c o n . h a s O w n P r o p e r t y ( ' a u t o I c o n U r l ' ) )   { 
                         r e t u r n   k I c o n A U T O ; 
                 }   e l s e   { 
                         r e t u r n   k I c o n C U S T O M ; 
                 } 
         } 
         
         r e t u r n   a I c o n ; 
 } 
 
 
 / /   G E T I C O N S O U R C E P A T H :   r e t u r n   t h e   p a t h   t o   t h e   s o u r c e   i m a g e   f o r   a n   i c o n ,   i f   f o u n d 
 f u n c t i o n   g e t I c o n S o u r c e P a t h ( a I c o n )   { 
         i f   ( a I c o n . a u t o I c o n U r l )   { 
                 r e t u r n   g C o r e I n f o . a u t o I c o n S o u r c e ; 
         }   e l s e   { 
                 r e t u r n   a I c o n . p a t h ; 
         } 
 } 
 
 
 / /   - - -   A P P   I D   F U N C T I O N S   - - - 
 
 / /   A P P I D I S U N I Q U E :   c h e c k   i f   a n   a p p   I D   i s   u n i q u e   o n   t h e   s y s t e m 
 f u n c t i o n   a p p I D I s U n i q u e ( a I D )   { 
         
         l e t   i A p p F o u n d ; 
         
         / /   f i r s t   s e e   i f   t h e r e ' s   a l r e a d y   a n   a p p   w i t h   t h i s   I D 
         i A p p F o u n d   =   a p p I D A p p E x i s t s ( a I D ) ; 
         
         i f   ( i A p p F o u n d   = = =   t r u e )   { 
                 / /   a p p   f o u n d ,   s o   t h i s   I D   i s   n o t   u n i q u e 
                 r e t u r n   f a l s e ; 
         }   e l s e   { 
                 
                 / /   n o   e x i s t i n g   a p p   f o u n d ,   o r   e r r o r   w h i l e   c h e c k i n g ,   s o   f a l l   b a c k   t o   l o o k i n g   f o r   a   d a t a   d i r 
                 l e t   i D a t a D i r F o u n d   =   a p p I D D a t a D i r E x i s t s ( a I D ) ; 
                 
                 i f   ( i D a t a D i r F o u n d   = = =   t r u e )   { 
                         / /   I D   f o u n d   i n   d a t a   d i r ,   s o   I D   i s   n o t   u n i q u e 
                         r e t u r n   f a l s e ; 
                 }   e l s e   i f   ( i A p p F o u n d   i n s t a n c e o f   E r r o r )   { 
                         / /   r e t u r n   e r r o r   f r o m   o r i g i n a l   a p p   s e a r c h 
                         r e t u r n   i A p p F o u n d ; 
                 }   e l s e   i f   ( i D a t a D i r F o u n d   i n s t a n c e o f   E r r o r )   { 
                         / /   r e t u r n   e r r o r   f r o m   d a t a   d i r e c t o r y   s e a r c h 
                         r e t u r n   i D a t a D i r F o u n d ; 
                 }   e l s e   { 
                         / /   i f   w e   g o t   h e r e ,   I D   w a s   n o t   f o u n d   i n   e i t h e r   a n   a p p   o r   a   d a t a   d i r e c t o r y 
                         r e t u r n   t r u e ; 
                 } 
         } 
 } 
 
 
 / /   A P P I D A P P E X I S T S :   c h e c k   i f   a n   a p p   e x i s t s   w i t h   a   g i v e n   I D   ( c a c h e   r e s u l t ) 
 f u n c t i o n   a p p I D A p p E x i s t s ( a I D )   { 
         
         l e t   i E r r ; 
         
         t r y   { 
                 / /   s e e   i f   t h e r e ' s   a l r e a d y   a n   a p p   w i t h   t h i s   I D 
                 r e t u r n   ( f i n d A p p B y I D ( k B u n d l e I D B a s e   +   a I D ) . l e n g t h   >   0 ) ; 
         }   c a t c h   ( i E r r )   { 
                 / /   e r r o r   t r y i n g   t o   f i n d   t h e   a p p   I D 
                 r e t u r n   i E r r ; 
         } 
 } 
 
 
 / /   A P P I D D A T A D I R E X I S T S :   c h e c k   i f   a   d a t a   d i r e c t o r y   e x i s t s   f o r   a   g i v e n   I D 
 g A p p D a t a I d s   =   n u l l ; 
 f u n c t i o n   a p p I D D a t a D i r E x i s t s ( a I D )   { 
         
         / /   b u i l d   l i s t   o f   d a t a   d i r e c t o r i e s   i f   n o t   a l r e a d y   d o n e 
         i f   ( g A p p D a t a I d s   = = =   n u l l )   { 
                 
                 l e t   i A p p D a t a D i r E r r ; 
                 
                 t r y   { 
                         / /   s e t   u p   v a r i a b l e s 
                         g A p p D a t a I d s   =   [ ] ; 
                         l e t   i F i l e M a n a g e r   =   $ . N S F i l e M a n a g e r . d e f a u l t M a n a g e r ; 
                         l e t   i A p p D a t a D i r   =   $ ( g C o r e I n f o . a p p D a t a P a t h ) ; 
                         l e t   i A p p s D i r C o n t e n t s   =   i F i l e M a n a g e r . c o n t e n t s O f D i r e c t o r y A t P a t h E r r o r ( i A p p D a t a D i r ,   n u l l ) . j s ; 
                         l e t   i I s D i r   =   R e f ( ) ; 
                         
                         / /   b u i l d   l i s t   o f   a p p   d a t a   d i r e c t o r y   I D s 
                         f o r   ( l e t   c u r I t e m   o f   i A p p s D i r C o n t e n t s )   { 
                                 i F i l e M a n a g e r . f i l e E x i s t s A t P a t h I s D i r e c t o r y ( i A p p D a t a D i r . s t r i n g B y A p p e n d i n g P a t h C o m p o n e n t ( c u r I t e m ) ,   i I s D i r ) ; 
                                 i f   ( i I s D i r [ 0 ] )   { 
                                         g A p p D a t a I d s . p u s h ( c u r I t e m . j s . t o L o w e r C a s e ( ) ) ; 
                                 } 
                         } 
                 }   c a t c h ( i A p p D a t a D i r E r r )   { 
                         / /   s t o r e   t h e   e r r o r 
                         g A p p D a t a I d s   =   i A p p D a t a D i r E r r ; 
                 } 
         } 
         
         / /   i f   w e   g o t   a n   e r r o r   s e a r c h i n g   t h e   a p p   d a t a   d i r ,   j u s t   r e t u r n   t h a t 
         i f   ( g A p p D a t a I d s   i n s t a n c e o f   E r r o r )   {   r e t u r n   g A p p D a t a I d s ;   } 
         
         / /   r e t u r n   t r u e   i f   I D   f o u n d   i n   l i s t   o f   d a t a   d i r e c t o r i e s ,   f a l s e   i f   n o t 
         r e t u r n   ( g A p p D a t a I d s . i n c l u d e s ( a I D . t o L o w e r C a s e ( ) ) ) ; 
 } 
 
 
 / /   C R E A T E A P P I D :   c r e a t e   a   u n i q u e   a p p   I D   b a s e d   o n   s h o r t   n a m e 
 f u n c t i o n   c r e a t e A p p I D ( a I n f o )   { 
 
         l e t   m y R e s u l t ; 
 
         i f   ( a I n f o . s t e p I n f o . a u t o I D )   { 
 
                 / /   w e   a l r e a d y   c r e a t e d   a n   a u t o   I D ,   s o   j u s t   u s e   t h a t 
                 m y R e s u l t   =   a I n f o . s t e p I n f o . a u t o I D ; 
 
         }   e l s e   { 
 
                 / /   f i r s t   a t t e m p t :   u s e   t h e   s h o r t   n a m e   w i t h   i l l e g a l   c h a r a c t e r s   r e m o v e d 
                 m y R e s u l t   =   a I n f o . a p p I n f o . s h o r t N a m e . r e p l a c e ( k A p p I D I l l e g a l C h a r s R e , ' ' ) ; 
                 l e t   m y B a s e ,   m y I s U n i q u e ; 
 
                 / /   i f   t o o   m a n y   c h a r a c t e r s   t r i m m e d   a w a y ,   s t a r t   w i t h   a   g e n e r i c   I D 
                 i f   ( ( m y R e s u l t . l e n g t h   <   ( a I n f o . a p p I n f o . s h o r t N a m e . l e n g t h   /   2 ) )   | | 
                 ( m y R e s u l t . l e n g t h   <   k A p p I D M i n L e n g t h ) )   { 
                         m y B a s e   =   ' E p i A p p ' ; 
                         m y I s U n i q u e   =   f a l s e ; 
                 }   e l s e   { 
 
                         / /   t r i m   I D   d o w n   t o   m a x   l e n g t h 
                         m y R e s u l t   =   m y R e s u l t . s l i c e ( 0 ,   k A p p I D M a x L e n g t h ) ; 
 
                         / /   c h e c k   f o r   a n y   a p p s   t h a t   a l r e a d y   h a v e   t h i s   I D 
                         m y I s U n i q u e   =   a p p I D I s U n i q u e ( m y R e s u l t ) ; 
 
                         / /   i f   I D   c h e c k s   f a i l i n g ,   w e ' l l   t a c k   o n   t h e   f i r s t   r a n d o m   e n d i n g   w e   t r y 
                         i f   ( m y I s U n i q u e   i n s t a n c e o f   E r r o r )   {   m y I s U n i q u e   =   f a l s e ;   } 
 
                         / /   i f   n e c e s s a r y ,   t r i m   I D   a g a i n   t o   a c c o m m o d a t e   3 - d i g i t   r a n d o m   e n d i n g 
                         i f   ( ! m y I s U n i q u e )   {   m y B a s e   =   m y R e s u l t . s l i c e ( 0 ,   k A p p I D M a x L e n g t h   -   3 ) ;   } 
                 } 
 
                 / /   i f   I D   i s   n o t   u n i q u e ,   t r y   t o   u n i q u i f y   i t 
                 / /   ( i f   m y I s U n i q u e   i s   a n   E r r o r   o b j e c t ,   t h a t   w i l l   e v a l u a t e   t o   t r u e   &   e n d   l o o p ) 
                 w h i l e   ( ! m y I s U n i q u e )   { 
 
                         / /   a d d   a   r a n d o m   3 - d i g i t   e x t e n s i o n   t o   t h e   b a s e 
                         m y R e s u l t   =   m y B a s e   + 
                         M a t h . m i n ( M a t h . f l o o r ( M a t h . r a n d o m ( )   *   1 0 0 0 ) ,   9 9 9 ) . t o S t r i n g ( ) . p a d S t a r t ( 3 , ' 0 ' ) ; 
 
                         m y I s U n i q u e   =   a p p I D I s U n i q u e ( m y R e s u l t ) ; 
                 } 
 
                 / /   u p d a t e   s t e p   i n f o   a b o u t   t h i s   I D 
                 a I n f o . s t e p I n f o . a u t o I D   =   m y R e s u l t ; 
                 a I n f o . s t e p I n f o . a u t o I D E r r o r   =   ( ( m y I s U n i q u e   i n s t a n c e o f   E r r o r )   ?   m y I s U n i q u e   :   f a l s e ) ; 
         } 
 
         / /   u p d a t e   a p p   i n f o   w i t h   n e w   I D 
         u p d a t e A p p I n f o ( a I n f o ,   ' i d ' ,   m y R e s u l t ) ; 
 
 } 
 
 
 / /   - - -   D I A L O G   F U N C T I O N S   - - - 
 
 / /   D I A L O G :   s h o w   a   d i a l o g   &   p r o c e s s   t h e   r e s u l t 
 f u n c t i o n   d i a l o g ( a M e s s a g e ,   a D l g O p t i o n s = { } ,   a B u t t o n M a p = n u l l )   { 
 
         l e t   m y R e s u l t ,   m y E r r ; 
         
         i f   ( t y p e o f ( a M e s s a g e )   ! =   ' s t r i n g ' )   {   a M e s s a g e   =   J S O N . s t r i n g i f y ( a M e s s a g e ,   n u l l ,   3 ) ;   } 
 
         t r y   { 
 
                 / /   d i s p l a y   d i a l o g 
                 m y R e s u l t   =   k A p p . d i s p l a y D i a l o g ( a M e s s a g e ,   a D l g O p t i o n s ) ; 
 
                 / /   a d d   u s e f u l   i n f o   i f   d i a l o g   n o t   c a n c e l e d 
                 m y R e s u l t . c a n c e l e d   =   f a l s e ; 
                 i f   ( a D l g O p t i o n s . b u t t o n s )   { 
                         m y R e s u l t . b u t t o n I n d e x   =   a D l g O p t i o n s . b u t t o n s . i n d e x O f ( m y R e s u l t . b u t t o n R e t u r n e d ) ; 
                 }   e l s e   { 
                         m y R e s u l t . b u t t o n I n d e x   =   1 ; 
                 } 
                 
         }   c a t c h ( m y E r r )   { 
                 i f   ( m y E r r . e r r o r N u m b e r   = =   - 1 2 8 )   { 
 
                         / /   c a n c e l   b u t t o n   - -   c r e a t e   f a u x   o b j e c t 
                         i f   ( a D l g O p t i o n s . b u t t o n s   & &   a D l g O p t i o n s . c a n c e l B u t t o n )   { 
                                 m y R e s u l t   =   { 
                                         b u t t o n R e t u r n e d :   a D l g O p t i o n s . b u t t o n s [ a D l g O p t i o n s . c a n c e l B u t t o n   -   1 ] , 
                                         b u t t o n I n d e x :   a D l g O p t i o n s . c a n c e l B u t t o n   -   1 , 
                                         c a n c e l e d :   t r u e 
                                 } ; 
                         }   e l s e   { 
                                 m y R e s u l t   =   { 
                                         b u t t o n R e t u r n e d :   ' C a n c e l ' , 
                                         b u t t o n I n d e x :   0 , 
                                         c a n c e l e d :   t r u e 
                                 } ; 
                         } 
                 }   e l s e   { 
                         t h r o w   m y E r r ; 
                 } 
         } 
         
         / /   h a n d l e   a n y   b u t t o n   m a p 
         i f   ( a B u t t o n M a p )   { 
                 
                 / /   g e t   t h e   m a p   f o r   t h e   c h o s e n   b u t t o n 
                 l e t   c u r B u t t o n   =   a B u t t o n M a p [ m y R e s u l t . b u t t o n R e t u r n e d ] ; 
                 i f   ( c u r B u t t o n   = = =   u n d e f i n e d )   { 
                         c u r B u t t o n   =   {   n a m e :   m y R e s u l t . b u t t o n R e t u r n e d ,   v a l u e :   m y R e s u l t . b u t t o n R e t u r n e d   } ; 
                 } 
                 
                 / /   a d d   b u t t o n   n a m e   &   v a l u e   t o   r e s u l t 
                 m y R e s u l t . b u t t o n N a m e   =   c u r B u t t o n . n a m e ; 
                 m y R e s u l t . b u t t o n V a l u e   =   c u r B u t t o n . v a l u e ; 
         } 
         
         / /   r e t u r n   o b j e c t 
         r e t u r n   m y R e s u l t ; 
 } 
 
 
 / /   F I L E D I A L O G :   s h o w   a   f i l e   s e l e c t i o n   d i a l o g   &   p r o c e s s   t h e   r e s u l t 
 f u n c t i o n   f i l e D i a l o g ( a T y p e ,   a D i r O b j ,   a D i r K e y ,   a O p t i o n s = { } )   { 
 
         l e t   m y R e s u l t ,   m y E r r ; 
 
         / /   a d d   d e f a u l t   l o c a t i o n   i f   w e   h a v e   o n e 
         i f   ( a D i r O b j [ a D i r K e y ] )   { 
                 a O p t i o n s   =   o b j C o p y ( a O p t i o n s ) ; 
                 a O p t i o n s . d e f a u l t L o c a t i o n   =   a D i r O b j [ a D i r K e y ] ; 
         } 
 
         / /   s h o w   f i l e   s e l e c t i o n   d i a l o g 
         w h i l e   ( t r u e )   { 
                 t r y   { 
                         
                         / /   s h o w   t h e   c h o s e n   t y p e   o f   d i a l o g 
                         i f   ( ( t y p e o f ( a T y p e )   = =   ' s t r i n g ' )   & &   ( a T y p e . t o L o w e r C a s e ( )   = =   ' s a v e ' ) )   { 
                                 m y R e s u l t   =   k A p p . c h o o s e F i l e N a m e ( a O p t i o n s ) . t o S t r i n g ( ) ; 
                         }   e l s e   { 
                                 m y R e s u l t   =   k A p p . c h o o s e F i l e ( a O p t i o n s ) ; 
                         } 
 
                         / /   i f   w e   g o t   h e r e ,   w e   g o t   a   p a t h 
 
                         / /   p r o c e s s   r e s u l t 
                         l e t   m y F i l e D i r ; 
                         i f   ( m y R e s u l t   i n s t a n c e o f   A r r a y )   { 
                                 i f   ( m y R e s u l t . l e n g t h   >   0 )   { 
                                         m y F i l e D i r   =   g e t P a t h I n f o ( m y R e s u l t [ 0 ] . t o S t r i n g ( ) ) ; 
                                 }   e l s e   { 
                                         m y F i l e D i r   =   n u l l ; 
                                 } 
                         }   e l s e   { 
                                 / /   r e t u r n   p a t h   i n f o 
                                 m y R e s u l t   =   g e t P a t h I n f o ( m y R e s u l t . t o S t r i n g ( ) ) ; 
                                 m y F i l e D i r   =   m y R e s u l t ; 
                         } 
 
                         / /   t r y   t o   s e t   t h i s   d i r e c t o r y   a s   d e f a u l t   f o r   n e x t   t i m e 
                         i f   ( m y F i l e D i r   & &   m y F i l e D i r . d i r )   { 
                                 a D i r O b j [ a D i r K e y ]   =   m y F i l e D i r . d i r ; 
                         } 
 
                         / /   a n d   r e t u r n 
                         r e t u r n   m y R e s u l t ; 
 
                 }   c a t c h ( m y E r r )   { 
 
                         i f   ( m y E r r . e r r o r N u m b e r   = =   - 1 7 0 0 )   { 
 
                                 / /   b a d   d e f a u l t L o c a t i o n ,   s o   t r y   a g a i n   w i t h   n o n e 
                                 a D i r O b j [ a D i r K e y ]   =   n u l l ; 
                                 d e l e t e   a O p t i o n s . d e f a u l t L o c a t i o n ; 
                                 c o n t i n u e ; 
 
                         }   e l s e   i f   ( m y E r r . e r r o r N u m b e r   = =   - 1 2 8 )   { 
 
                                 / /   c a n c e l e d :   r e t u r n   n u l l 
                                 r e t u r n   n u l l ; 
 
                         }   e l s e   { 
 
                                 / /   u n k n o w n   e r r o r 
                                 t h r o w   m y E r r ; 
                         } 
                 } 
         } 
 } 
 
 
 / /   C O N F I R M Q U I T :   c o n f i r m   q u i t 
 f u n c t i o n   c o n f i r m Q u i t ( a M e s s a g e = ' ' )   { 
         
         l e t   m y P r e f i x   =   ( a M e s s a g e   ?   a M e s s a g e   +   '   '   :   ' ' ) ; 
         
         / /   c o n f i r m   q u i t 
         r e t u r n   ! d i a l o g ( m y P r e f i x   +   ' A r e   y o u   s u r e   y o u   w a n t   t o   q u i t ? ' ,   { 
                 w i t h T i t l e :   " C o n f i r m " , 
                 w i t h I c o n :   k E p i c h r o m e I c o n , 
                 b u t t o n s :   [ " N o " ,   " Y e s " ] , 
                 d e f a u l t B u t t o n :   2 , 
                 c a n c e l B u t t o n :   1 
         } ) . c a n c e l e d ; 
 } 
 
 
 / /   S T E P D I A L O G :   s h o w   a   s t a n d a r d   s t e p   d i a l o g 
 f u n c t i o n   s t e p D i a l o g ( a I n f o ,   a M e s s a g e ,   a D l g O p t i o n s )   { 
 
         l e t   m y B u t t o n M a p   =   n u l l ; 
         l e t   m y E r r ; 
         
         / /   c o p y   d i a l o g   o p t i o n s   o b j e c t   ( i n c l u d i n g   i c o n   p a t h   i f   a n y ) 
         l e t   m y D l g O p t i o n s   =   o b j C o p y ( a D l g O p t i o n s ) ; 
         i f   ( a D l g O p t i o n s . w i t h I c o n   i n s t a n c e o f   P a t h )   { 
                 m y D l g O p t i o n s . w i t h I c o n   =   P a t h ( a D l g O p t i o n s . w i t h I c o n . t o S t r i n g ( ) ) ; 
         } 
         
         / /   i f   a D l g O p t i o n s   i s   a   b u t t o n   m a p ,   b u i l d   o p t i o n s   f r o m   t h a t 
         i f   ( m y D l g O p t i o n s . h a s O w n P r o p e r t y ( ' k e y ' ) )   { 
                 
                 / /   e n s u r e   w e   h a v e   a   b u t t o n - t o - v a l u e   m a p 
                 m y B u t t o n M a p   =   d i a l o g B u t t o n M a p ( ( m y D l g O p t i o n s . m a p O b j e c t   ?   m y D l g O p t i o n s . m a p O b j e c t   :   a I n f o ) , 
                         m y D l g O p t i o n s . k e y ,   m y D l g O p t i o n s . b u t t o n s ) ; 
                         
                 / /   u p d a t e   d i a l o g   o p t i o n s   w i t h   b u t t o n   m a p   i n f o 
                 m y D l g O p t i o n s . b u t t o n s   =   O b j e c t . k e y s ( m y B u t t o n M a p . m a p ) ; 
                 i f   ( ! m y D l g O p t i o n s . h a s O w n P r o p e r t y ( ' d e f a u l t B u t t o n ' ) )   { 
                         m y D l g O p t i o n s . d e f a u l t B u t t o n   =   m y B u t t o n M a p . d e f a u l t B u t t o n ; 
                 } 
                 d e l e t e   m y D l g O p t i o n s . k e y ; 
                 
                 / /   w e ' r e   d o n e   w i t h   e v e r y t h i n g   e x c e p t   t h e   m a p 
                 m y B u t t o n M a p   =   m y B u t t o n M a p . m a p ; 
         } 
 
         / /   f i l l   i n   b o i l e r p l a t e 
         i f   ( ! m y D l g O p t i o n s . w i t h T i t l e )   {   m y D l g O p t i o n s . w i t h T i t l e   =   a I n f o . s t e p I n f o . d l g T i t l e ;   } 
         i f   ( ! m y D l g O p t i o n s . w i t h I c o n )   {   m y D l g O p t i o n s . w i t h I c o n   =   a I n f o . s t e p I n f o . d l g I c o n ;   } 
         
         / /   a d d   b a c k   b u t t o n 
         m y D l g O p t i o n s . b u t t o n s . p u s h ( a I n f o . s t e p I n f o . b a c k B u t t o n ) ; 
         i f   ( ! m y D l g O p t i o n s . d e f a u l t B u t t o n )   {   m y D l g O p t i o n s . d e f a u l t B u t t o n   =   1 ;   } 
         i f   ( ! m y D l g O p t i o n s . c a n c e l B u t t o n )   {   m y D l g O p t i o n s . c a n c e l B u t t o n   =   m y D l g O p t i o n s . b u t t o n s . l e n g t h ;   } 
         
         r e t u r n   d i a l o g ( a M e s s a g e ,   m y D l g O p t i o n s ,   m y B u t t o n M a p ) ; 
 } 
 
 
 / /   D I A L O G B U T T O N M A P :   s e t   u p   a   b u t t o n   m a p   f o r   a   s t e p   d i a l o g 
 f u n c t i o n   d i a l o g B u t t o n M a p ( a I n f o ,   a K e y ,   a B u t t o n I n f o )   { 
         
         / /   e n s u r e   w e   h a v e   a   b u t t o n - t o - v a l u e   m a p 
         i f   ( a B u t t o n I n f o   i n s t a n c e o f   A r r a y )   { 
                 / /   t u r n   b u t t o n   a r r a y   i n t o   a   o n e - t o - o n e   d i c t 
                 a B u t t o n I n f o   =   a B u t t o n I n f o . r e d u c e ( f u n c t i o n   ( o b j , v a l )   {   o b j [ v a l ]   =   v a l ;   r e t u r n   o b j ;   } ,   { } ) ; 
         } 
         
         / /   i s   t h i s   b u t t o n   m a p   f o r   a p p   i n f o ? 
         l e t   i I s A p p M a p   =   a I n f o . h a s O w n P r o p e r t y ( ' a p p I n f o ' ) ; 
         
         / /   b u i l d   b u t t o n   m a p 
         l e t   i K e y V a l u e   =   ( i I s A p p M a p   ?   a I n f o . a p p I n f o [ a K e y ]   :   a I n f o [ a K e y ] ) ; 
         l e t   c u r B u t t o n N u m   =   1 ; 
         l e t   m y B u t t o n M a p   =   { } ; 
         l e t   m y D e f a u l t B u t t o n   =   1 ; 
         f o r   ( l e t   c u r B u t t o n N a m e   i n   a B u t t o n I n f o )   { 
                 
                 / /   d e f a u l t   b u t t o n   t i t l e 
                 l e t   c u r B u t t o n T i t l e   =   c u r B u t t o n N a m e ; 
                 l e t   c u r B u t t o n V a l u e L i s t   =   a B u t t o n I n f o [ c u r B u t t o n N a m e ] ; 
                 
                 / /   c o n v e r t   s i n g l e   m a t c h   v a l u e   t o   a n   a r r a y 
                 i f   ( ( !   ( c u r B u t t o n V a l u e L i s t   i n s t a n c e o f   A r r a y ) )   | |   i K e y V a l u e   i n s t a n c e o f   A r r a y )   { 
                         c u r B u t t o n V a l u e L i s t   =   [ c u r B u t t o n V a l u e L i s t ] ; 
                 } 
                 
                 / /   p u t   a   d o t   o n   t h e   s e l e c t e d   b u t t o n 
                 i f   ( c u r B u t t o n V a l u e L i s t . r e d u c e ( ( a c c ,   v a l )   = >   ( a c c   | |   o b j E q u a l s ( v a l ,   i K e y V a l u e ) ) ,   f a l s e ) )   { 
                         l e t   c u r D o t   =   ( ( ( ! i I s A p p M a p )   | |   ( a I n f o . s t e p I n f o . a c t i o n   = =   k A c t i o n C R E A T E ) )   ? 
                                                                                         ( ( k A d v a n c e d D e f a u l t s . h a s O w n P r o p e r t y ( a K e y )   & & 
                                                                                                 ( i K e y V a l u e   ! =   k A d v a n c e d D e f a u l t s [ a K e y ] ) )   ? 
                                                                                                 k D o t A d v a n c e d   :   k D o t S e l e c t e d )   : 
                                                         ( ( o b j E q u a l s ( i K e y V a l u e ,   a I n f o . o l d A p p I n f o [ a K e y ] ) )   ? 
                                                         k D o t C u r r e n t   :   k D o t C h a n g e d ) ) ; 
                         c u r B u t t o n T i t l e   =   c u r D o t   +   '   '   +   c u r B u t t o n N a m e ; 
                         m y D e f a u l t B u t t o n   =   c u r B u t t o n N u m ; 
                 } 
                 
                 / /   a d d   t h i s   b u t t o n   t o   b u t t o n   m a p 
                 m y B u t t o n M a p [ c u r B u t t o n T i t l e ]   =   {   n a m e :   c u r B u t t o n N a m e ,   v a l u e :   a B u t t o n I n f o [ c u r B u t t o n N a m e ]   } ; 
                 
                 / /   i n c r e m e n t   b u t t o n   n u m b e r 
                 c u r B u t t o n N u m + + ; 
         } 
         
         r e t u r n   { 
                 m a p :   m y B u t t o n M a p , 
                 d e f a u l t B u t t o n :   m y D e f a u l t B u t t o n 
         } ; 
 } 
 
 
 / /   S E T D L G I C O N :   s e t   t h e   d i a l o g   i c o n   u s i n g   a p p   i n f o ,   d e f a u l t i n g   t o   E p i c h r o m e   i c o n 
 f u n c t i o n   s e t D l g I c o n ( a A p p I n f o )   { 
 
         i f   ( a A p p I n f o . i c o n P a t h )   { 
                 l e t   m y C u s t o m I c o n   =   P a t h ( a A p p I n f o . f i l e . p a t h   +   ' / '   +   a A p p I n f o . i c o n P a t h ) ; 
                 i f   ( k F i n d e r . e x i s t s ( m y C u s t o m I c o n ) )   { 
                         r e t u r n   m y C u s t o m I c o n ; 
                 } 
         } 
 
         / /   i f   w e   g o t   h e r e ,   c u s t o m   i c o n   n o t   f o u n d 
         i f   ( k F i n d e r . e x i s t s ( k E p i A p p I c o n ) )   { 
                 r e t u r n   k E p i A p p I c o n ; 
         }   e l s e   { 
                 / /   f a l l b a c k   d e f a u l t 
                 r e t u r n   ' n o t e ' ; 
         } 
 } 
 
 
 / /   I N D E N T :   i n d e n t   a   s t r i n g 
 f u n c t i o n   i n d e n t ( a S t r ,   a I n d e n t )   { 
         / /   r e g e x   t h a t   i n c l u d e s   e m p t y   l i n e s :   / ^ / g m 
 
         i f   ( t y p e o f   a I n d e n t   ! =   ' n u m b e r ' )   {   a I n d e n t   =   1 ;   } 
         i f   ( a I n d e n t   <   0 )   {   r e t u r n   a S t r ;   } 
 
         / /   i n d e n t   s t r i n g 
         r e t u r n   a S t r . r e p l a c e ( / ^ ( ? ! \ s * $ ) / g m ,   k I n d e n t . r e p e a t ( a I n d e n t ) ) ; 
 } 
 
 
 / /   - - -   L O G G I N G   F U N C T I O N S   - - - 
 
 / /   E R R L O G   - -   l o g   a n   e r r o r   m e s s a g e 
 f u n c t i o n   e r r l o g ( a M s g ,   a T y p e = ' E R R O R ' )   { 
         i f   ( g C o r e I n f o   & &   g C o r e I n f o . l o g F i l e )   { 
                 s h e l l ( 
                         ' e p i A c t i o n = l o g ' , 
                         ' e p i L o g T y p e = '   +   a T y p e , 
                         ' e p i L o g M s g = '   +   a M s g ) ; 
         } 
 } 
 
 
 / /   D E B U G L O G   - -   l o g   a   d e b u g g i n g   m e s s a g e 
 f u n c t i o n   d e b u g l o g ( a M s g )   { 
         e r r l o g ( a M s g ,   ' D E B U G ' ) ; 
 } 
 
 
 / /   E R R I S R E P O R T A B L E   - -   d e t e r m i n e   i f   a n   e r r o r   s t a r t s   w i t h   R E P O R T |   &   s t r i p   o f f 
 f u n c t i o n   e r r I s R e p o r t a b l e ( a M s g )   { 
         i f   ( a M s g . s t a r t s W i t h ( ' R E P O R T | ' ) )   { 
                 r e t u r n   [ t r u e ,   a M s g . s l i c e ( 7 ) ] ; 
         }   e l s e   { 
                 r e t u r n   [ f a l s e ,   a M s g ] ; 
         } 
 } 
 
 
 / /   - - -   S H E L L   I N T E R A C T I O N   F U N C T I O N S   - - - 
 
 / /   S H E L L :   e x e c u t e   e p i c h r o m e . s h   w i t h   a   l i s t   o f   a r g u m e n t s 
 f u n c t i o n   s h e l l ( . . . a A r g s )   { 
         
         / /   i f   l o g g i n g   h a s   b e e n   i n i t i a l i z e d ,   i n c l u d e   l o g g i n g   v a r i a b l e 
         i f   ( g C o r e I n f o . l o g F i l e )   {   a A r g s . u n s h i f t ( ' m y L o g F i l e = '   +   g C o r e I n f o . l o g F i l e ) ;   } 
         
         / /   s t a r t   w i t h   t h e   p a t h   t o   e p i c h r o m e . s h 
         a A r g s . u n s h i f t ( k E p i c h r o m e S c r i p t ) ; 
         
         / /   r u n   t h e   s c r i p t 
         r e t u r n   k A p p . d o S h e l l S c r i p t ( s h e l l Q u o t e . a p p l y ( n u l l ,   a A r g s ) ) ; 
 } 
 
 
 / /   S H E L L Q U O T E :   a s s e m b l e   a   l i s t   o f   a r g u m e n t s   i n t o   a   s h e l l   s t r i n g 
 f u n c t i o n   s h e l l Q u o t e ( . . . a A r g s )   { 
         l e t   r e s u l t   =   [ ] ; 
         f o r   ( l e t   s   o f   a A r g s )   { 
                 r e s u l t . p u s h ( " ' "   +   s . r e p l a c e ( / ' / g ,   " ' \ \ ' ' " )   +   " ' " ) ; 
         } 
         r e t u r n   r e s u l t . j o i n ( '   ' ) ; 
 } 
 
 
 / /   - - -   U T I L I T Y   F U N C T I O N S   - - - 
 
 / /   O B J C O P Y :   d e e p   c o p y   o b j e c t 
 f u n c t i o n   o b j C o p y ( a O b j )   { 
 
     i f   ( a O b j   ! = =   u n d e f i n e d )   { 
         r e t u r n   J S O N . p a r s e ( J S O N . s t r i n g i f y ( a O b j ) ) ; 
     } 
 } 
 
 
 / /   O B J E Q U A L S :   d e e p - c o m p a r e   s i m p l e   o b j e c t s   ( i n c l u d i n g   a r r a y s ) 
 f u n c t i o n   o b j E q u a l s ( a O b j 1 ,   a O b j 2 )   { 
 
 	 / /   i d e n t i c a l   o b j e c t s 
 	 i f   (   a O b j 1   = = =   a O b j 2   )   r e t u r n   t r u e ; 
 
         / /   i f   n o t   s t r i c t l y   e q u a l ,   b o t h   m u s t   b e   o b j e c t s 
         i f   ( ! ( ( a O b j 1   i n s t a n c e o f   O b j e c t )   & &   ( a O b j 2   i n s t a n c e o f   O b j e c t ) ) )   {   r e t u r n   f a l s e ;   } 
 
         / /   t h e y   m u s t   h a v e   t h e   e x a c t   s a m e   p r o t o t y p e   c h a i n ,   t h e   c l o s e s t   w e   c a n   d o   i s 
         / /   t e s t   t h e r e   c o n s t r u c t o r 
         i f   (   a O b j 1 . c o n s t r u c t o r   ! = =   a O b j 2 . c o n s t r u c t o r   )   {   r e t u r n   f a l s e ;   } 
 
         f o r   (   l e t   c u r P r o p   i n   a O b j 1   )   { 
                 i f   ( ! a O b j 1 . h a s O w n P r o p e r t y ( c u r P r o p )   )   {   c o n t i n u e ;   } 
 
                 i f   ( ! a O b j 2 . h a s O w n P r o p e r t y ( c u r P r o p )   )   {   r e t u r n   f a l s e ;   } 
 
                 i f   ( a O b j 1 [ c u r P r o p ]   = = =   a O b j 2 [ c u r P r o p ]   )   {   c o n t i n u e ;   } 
 
                 i f   ( a O b j 1 [ c u r P r o p ]   i n s t a n c e o f   O b j e c t   )   {   r e t u r n   f a l s e ;   } 
 
                 i f   ( ! o b j E q u a l s ( a O b j 1 [ c u r P r o p ] ,   a O b j 2 [ c u r P r o p ] ) )   {   r e t u r n   f a l s e ;   } 
         } 
 
 	 / /   i f   a O b j 2   h a s   a n y   p r o p e r t i e s   a O b j 1   d o e s   n o t   h a v e ,   t h e y ' r e   n o t   e q u a l 
         f o r   ( l e t   c u r P r o p   i n   a O b j 2 )   { 
 	         i f   ( a O b j 2 . h a s O w n P r o p e r t y ( c u r P r o p )   & &   ! a O b j 1 . h a s O w n P r o p e r t y ( c u r P r o p ) )   { 
 	 	         r e t u r n   f a l s e ; 
 	 	 } 
 	 } 
 
         r e t u r n   t r u e ; 
 } 
 
 
 / /   C A P I T A L I Z E D :   e x t e n s i o n   f o r   S t r i n g   o b j e c t   t o   r e t u r n   c a p i t a l i z e d   v e r s i o n 
 S t r i n g . p r o t o t y p e . c a p i t a l i z e d   =   f u n c t i o n   ( )   { 
     r e t u r n   t h i s . c h a r A t ( 0 ) . t o U p p e r C a s e ( )   +   t h i s . s l i c e ( 1 ) ; 
 } 
 
 / / 
 / / 
 / /     l i b a p p . j s :   J X A / O b j - C   b r i d g e   c a l l s   t o   r e g i s t e r ,   f i n d   &   l a u n c h   a p p s 
 / / 
 / /     C o p y r i g h t   ( C )   2 0 2 1     D a v i d   M a r m o r 
 / / 
 / /     h t t p s : / / g i t h u b . c o m / d m a r m o r / e p i c h r o m e 
 / / 
 / /     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y 
 / /     i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y 
 / /     t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r 
 / /     ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n . 
 / / 
 / /     T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l , 
 / /     b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f 
 / /     M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e 
 / /     G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s . 
 / / 
 / /     Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e 
 / /     a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . 
 
 
 / /   O B J E C T I V E - C   S E T U P 
 
 O b j C . i m p o r t ( ' C o r e S e r v i c e s ' ) ; 
 O b j C . i m p o r t ( ' A p p K i t ' ) ; 
 O b j C . b i n d F u n c t i o n ( ' C F M a k e C o l l e c t a b l e ' ,   [   ' i d ' ,   [   ' v o i d   * '   ]   ] ) ; 
 R e f . p r o t o t y p e . t o N S   =   f u n c t i o n ( )   {   r e t u r n   $ . C F M a k e C o l l e c t a b l e ( t h i s ) ;   } 
 
 
 / /   F U N C T I O N S 
 
 / /   F I N D A P P B Y I D :   u s e   l a u n c h   s e r v i c e s   t o   f i n d   a n   a p p   b y   i t s   b u n d l e   I D 
 f u n c t i o n   f i n d A p p B y I D ( a I D )   { 
 
         / /   l o c a l   s t a t e 
         l e t   m y R e s u l t   =   [ ] ; 
         l e t   m y E r r   =   R e f ( ) ; 
 
         / /   c h e c k   l a u n c h   s e r v i c e s   f o r   I D 
         l e t   m y L S R e s u l t   =   $ . L S C o p y A p p l i c a t i o n U R L s F o r B u n d l e I d e n t i f i e r ( $ ( a I D ) ,   m y E r r ) ; 
         m y E r r   =   m y E r r [ 0 ] . t o N S ( ) ; 
 
         / /   c h e c k   f o r   e r r o r s 
         i f   ( m y E r r . c o d e )   { 
                 i f   ( m y E r r . c o d e   ! =   $ . k L S A p p l i c a t i o n N o t F o u n d E r r )   { 
 
                         / /   e r r o r 
                         l e t   m y E r r I D ,   m y I g n o r e ; 
                         t r y   {   m y E r r I D   =   ' " '   +   a I D . t o S t r i n g ( )   +   ' " ' ;   }   c a t c h ( m y I g n o r e )   {   m y E r r I D   =   ' < u n k n o w n > ' ;   } 
                         t h r o w   E r r o r ( ' U n a b l e   t o   s e a r c h   f o r   a p p s   w i t h   I D   '   +   m y E r r I D   +   ' :   '   +   m y E r r . l o c a l i z e d D e s c r i p t i o n . j s ) ; 
                         r e t u r n   n u l l ;   / /   n o t   r e a c h e d 
                 } 
         }   e l s e   { 
 
                 / /   u n w r a p   a p p   l i s t 
                 m y L S R e s u l t   =   m y L S R e s u l t . t o N S ( ) . j s ; 
 
                 / /   c r e a t e   a r r a y   o f   o n l y   a p p   p a t h s 
                 f o r   ( l e t   c u r A p p   o f   m y L S R e s u l t )   { 
                         m y R e s u l t . p u s h ( c u r A p p . p a t h . j s ) ; 
                 } 
         } 
 
         r e t u r n   m y R e s u l t ; 
 } 
 
 
 / /   R E G I S T E R A P P :   r e g i s t e r   a n   a p p   w i t h   l a u n c h   s e r v i c e s 
 f u n c t i o n   r e g i s t e r A p p ( a P a t h )   { 
 
         l e t   m y U r l   =   $ . N S U R L . f i l e U R L W i t h P a t h ( $ ( a P a t h ) ) ; 
 
         l e t   m y R e s u l t   =   $ . L S R e g i s t e r U R L ( m y U r l ,   t r u e ) ; 
 
         i f   ( m y R e s u l t   ! =   0 )   { 
                 t h r o w   E r r o r ( ' E r r o r   c o d e   '   +   m y R e s u l t . t o S t r i n g ( ) ) ; 
         } 
 } 
 
 
 / /   L A U N C H A P P :   l a u n c h   a n   a p p   w i t h   a r g u m e n t s   &   U R L s 
 f u n c t i o n   l a u n c h A p p ( a S p e c ,   a A r g s = [ ] ,   a U r l s = [ ] ,   a O p t i o n s = { } )   { 
         
         / /   p r e p a r e   a r g s   f o r   O b j - C 
         a A r g s   =   a A r g s . m a p ( x   = >   $ ( x ) ) ; 
         
         / /   p r e p a r e   U R L s   f o r   O b j - C 
         i f   ( a U r l s . l e n g t h   >   0 )   { 
                 a U r l s   =   $ ( a U r l s . m a p ( x   = >   $ . N S U R L . U R L W i t h S t r i n g ( $ ( x ) ) ) ) ; 
         }   e l s e   { 
                 a U r l s   =   f a l s e ; 
         } 
         
         / /   i f   w e   g o t   a n   I D ,   l a u n c h   t h e   f i r s t   a p p   w i t h   t h a t   I D 
         i f   ( a O p t i o n s . s p e c I s I D )   { 
                 l e t   m y A p p L i s t   =   f i n d A p p B y I D ( a S p e c ) ; 
                 i f   ( m y A p p L i s t . l e n g t h   <   1 )   { 
                         t h r o w   E r r o r ( ' N o   a p p s   f o u n d   w i t h   I D   '   +   a S p e c ) ; 
                 } 
                 a S p e c   =   m y A p p L i s t [ 0 ] ; 
                 
         } 
         
         / /   i f   w e ' r e   s u p p o s e d   t o   r e g i s t e r   t h e   a p p   f i r s t ,   d o   t h a t   n o w 
         i f   ( a O p t i o n s . r e g i s t e r F i r s t )   { 
                 r e g i s t e r A p p ( a S p e c ) ; 
         } 
         
         / /   s e t   n u m b e r   o f   a t t e m p t s   b e f o r e   t h r o w i n g   a n   e r r o r 
         l e t   i M a x A t t e m p t s   =   2 ; 
         i f   ( a O p t i o n s . m a x A t t e m p t s )   { 
                 i M a x A t t e m p t s   =   a O p t i o n s . m a x A t t e m p t s ; 
         } 
         
         / /   p r e p a r e   a p p   s p e c   f o r   O b j - C 
         a S p e c   =   $ . N S U R L . f i l e U R L W i t h P a t h ( $ ( a S p e c ) ) ; 
         
         / /   n a m e   o f   a p p   f o r   e r r o r s 
         l e t   m y A p p N a m e   =   a S p e c . p a t h C o m p o n e n t s . j s ; 
         m y A p p N a m e   =   ( ( m y A p p N a m e . l e n g t h   >   0 )   ?   ' " '   +   m y A p p N a m e [ m y A p p N a m e . l e n g t h   -   1 ] . j s   +   ' " '   :   ' < u n k n o w n   a p p > ' ) ; 
         
         / /   l a u n c h   i n f o 
         l e t   m y L a u n c h E r r   =   u n d e f i n e d ,   m y A p p   =   u n d e f i n e d ; 
         l e t   m y C o n f i g ; 
         
         / /   d e t e r m i n e   w h e t h e r   t o   u s e   o p e n A p p l i c a t i o n A t U R L   o r   l a u n c h A p p l i c a t i o n A t U R L 
         l e t   m y O S V e r s i o n   =   k A p p . s y s t e m I n f o ( ) . s y s t e m V e r s i o n . s p l i t ( ' . ' ) . m a p ( x   = >   p a r s e I n t ( x ) ) ; 
         l e t   m y U s e O p e n   =   ( ( m y O S V e r s i o n . l e n g t h   > =   2 )   & & 
                 ( ( m y O S V e r s i o n [ 0 ]   >   1 0 )   | |   ( m y O S V e r s i o n [ 0 ]   = =   1 0 )   & &   ( m y O S V e r s i o n [ 1 ]   > =   1 5 ) ) ) ; 
         
         i f   ( m y U s e O p e n )   { 
 
                 / /   s e t u p   f o r   1 0 . 1 5 + 
 
                 / /   s e t   u p   o p e n   c o n f i g u r a t i o n 
                 m y C o n f i g   =   $ . N S W o r k s p a c e O p e n C o n f i g u r a t i o n . c o n f i g u r a t i o n ; 
                 
                 / /   m a k e   s u r e   e n g i n e   d o e s n ' t   f a i l   t o   l a u n c h   d u e   t o   a n o t h e r   s i m i l a r   e n g i n e 
                 m y C o n f i g . a l l o w s R u n n i n g A p p l i c a t i o n S u b s t i t u t i o n   =   f a l s e ; 
                 
                 / /   a d d   a n y   a r g s 
                 i f   ( a A r g s . l e n g t h   >   0 )   {   m y C o n f i g . a r g u m e n t s   =   $ ( a A r g s ) ;   } 
 
                 / /   l a u n c h 
                 f u n c t i o n   m y C o m p l e t i o n H a n d l e r ( a A p p ,   a E r r )   { 
                         m y A p p   =   a A p p ; 
                         m y L a u n c h E r r   =   a E r r ; 
                 } 
         }   e l s e   { 
                 
                 / /   s e t u p   f o r   1 0 . 1 4 - 
 
                 / /   s e t   u p   l a u n c h   c o n f i g u r a t i o n 
                 l e t   m y C o n f i g K e y s   =   [ ] ,   m y C o n f i g V a l u e s   =   [ ] ; 
                 i f   ( a A r g s . l e n g t h   >   0 )   { 
                         m y C o n f i g K e y s . p u s h ( $ . N S W o r k s p a c e L a u n c h C o n f i g u r a t i o n A r g u m e n t s ) ; 
                         m y C o n f i g V a l u e s . p u s h ( $ ( a A r g s ) ) ; 
                 } 
                 
                 m y C o n f i g   =   $ . N S M u t a b l e D i c t i o n a r y . d i c t i o n a r y W i t h O b j e c t s F o r K e y s ( 
                         $ ( m y C o n f i g V a l u e s ) ,   $ ( m y C o n f i g K e y s ) 
                 ) ; 
         } 
         
         / /   t r y   t o   l a u n c h   u p   t o   i M a x A t t e m p t s   t i m e s 
         l e t   c u r A t t e m p t   =   1 ; 
         w h i l e   ( t r u e )   { 
                 
                 l e t   i E r r ; 
                 t r y   { 
                         
                         i f   ( m y U s e O p e n )   { 
                                 
                                 / /   l a u n c h   f o r   1 0 . 1 5 + 
                                 
                                 i f   ( a U r l s )   { 
                                         $ . N S W o r k s p a c e . s h a r e d W o r k s p a c e . o p e n U R L s W i t h A p p l i c a t i o n A t U R L C o n f i g u r a t i o n C o m p l e t i o n H a n d l e r ( 
                                                 a U r l s ,   a S p e c ,   m y C o n f i g ,   m y C o m p l e t i o n H a n d l e r 
                                         ) ; 
                                 }   e l s e   { 
                                         $ . N S W o r k s p a c e . s h a r e d W o r k s p a c e . o p e n A p p l i c a t i o n A t U R L C o n f i g u r a t i o n C o m p l e t i o n H a n d l e r ( 
                                                 a S p e c ,   m y C o n f i g ,   m y C o m p l e t i o n H a n d l e r 
                                         ) ; 
                                 } 
                                 
                                 / /   w a i t   f o r   c o m p l e t i o n   h a n d l e r 
                                 l e t   m y W a i t   =   0 . 0 ; 
                                 w h i l e   ( ( m y L a u n c h E r r   = = =   u n d e f i n e d )   & &   ( m y W a i t   <   1 5 . 0 ) )   { 
                                         d e l a y ( 0 . 1 ) ; 
                                         m y W a i t   + =   0 . 1 ; 
                                 } 
                                 i f   ( m y L a u n c h E r r   = = =   u n d e f i n e d )   { 
                                         / /   d o n ' t   r e a t t e m p t   l a u n c h   a f t e r   t h i s 
                                         i M a x A t t e m p t s   =   c u r A t t e m p t ; 
                                         t h r o w   E r r o r ( ' T i m e d   o u t   w a i t i n g   f o r   '   +   m y A p p N a m e   +   '   t o   o p e n . ' ) ; 
                                 } 
                         }   e l s e   { 
                                 
                                 / /   l a u n c h   f o r   1 0 . 1 4 - 
                                 
                                 / /   l a u n c h   ( e r r o r   a r g   c a u s e s   a   c r a s h ,   s o   i g n o r e   i t ) 
                                 i f   ( a U r l s )   { 
                                         m y A p p   =   $ . N S W o r k s p a c e . s h a r e d W o r k s p a c e . o p e n U R L s W i t h A p p l i c a t i o n A t U R L O p t i o n s C o n f i g u r a t i o n E r r o r ( 
                                                 a U r l s ,   a S p e c ,   $ . N S W o r k s p a c e L a u n c h D e f a u l t   |   $ . N S W o r k s p a c e L a u n c h N e w I n s t a n c e ,   m y C o n f i g ,   n u l l 
                                         ) ; 
                                 }   e l s e   { 
                                         m y A p p   =   $ . N S W o r k s p a c e . s h a r e d W o r k s p a c e . l a u n c h A p p l i c a t i o n A t U R L O p t i o n s C o n f i g u r a t i o n E r r o r ( 
                                                 a S p e c ,   $ . N S W o r k s p a c e L a u n c h D e f a u l t   |   $ . N S W o r k s p a c e L a u n c h N e w I n s t a n c e ,   m y C o n f i g ,   n u l l 
                                         ) ; 
                                 } 
                                 
                                 / /   c r e a t e   g e n e r i c   e r r o r 
                                 i f   ( ! m y A p p . j s )   { 
                                         t h r o w   E r r o r ( ' T h e   a p p l i c a t i o n   '   +   m y A p p N a m e   +   '   c o u l d   n o t   b e   l a u n c h e d . ' ) ; 
                                 } 
                         } 
                         
                         / /   t h r o w   a n y   e r r o r   e n c o u n t e r e d 
                         i f   ( m y L a u n c h E r r   & &   m y L a u n c h E r r . j s )   { 
                                 t h r o w   E r r o r ( m y L a u n c h E r r . l o c a l i z e d D e s c r i p t i o n . j s ) ; 
                         } 
                 }   c a t c h   ( i E r r )   { 
                         
                         / /   t h i s   w a s   o u r   f i n a l   a t t e m p t ,   s o   t h r o w   t h e   e r r o r 
                         i f   ( c u r A t t e m p t   = =   i M a x A t t e m p t s )   { 
                                 i f   ( c u r A t t e m p t   >   1 )   { 
                                         i E r r . m e s s a g e   =   ' A f t e r   '   +   c u r A t t e m p t . t o S t r i n g ( )   +   '   a t t e m p t s :   '   +   i E r r . m e s s a g e ; 
                                 } 
                                 t h r o w   i E r r ; 
                         } 
                         
                         / /   i n c r e m e n t   a t t e m p t   c o u n t e r   a n d   t r y   a g a i n 
                         c u r A t t e m p t + + ; 
                         c o n t i n u e ; 
                 } 
                 
                 / /   i f   w e   g o t   h e r e ,   w e   l a u n c h e d   s u c c e s s f u l l y 
                 r e t u r n   m y A p p ; 
         } 
 } 
 / / 
 / / 
 / /     r e p o r t e r r . j s :   F u n c t i o n   f o r   r e p o r t i n g   e r r o r s   t o   G i t H u b . 
 / / 
 / /     C o p y r i g h t   ( C )   2 0 2 1     D a v i d   M a r m o r 
 / / 
 / /     h t t p s : / / g i t h u b . c o m / d m a r m o r / e p i c h r o m e 
 / / 
 / /     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y 
 / /     i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y 
 / /     t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r 
 / /     ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n . 
 / / 
 / /     T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l , 
 / /     b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f 
 / /     M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e 
 / /     G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s . 
 / / 
 / /     Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e 
 / /     a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . 
 
 c o n s t   k M a x U r l L e n g t h   =   8 1 9 6 ; 
 
 c o n s t   k U r l S t a r t   =   ' h t t p s : / / g i t h u b . c o m / d m a r m o r / e p i c h r o m e / i s s u e s / n e w ? t i t l e = ' ; 
 c o n s t   k U r l B o d y T a g   =   ' & b o d y = ' ; 
 c o n s t   k B o d y S t a r t   =   ' < ! - -  !9�   P l e a s e   p r o v i d e   a s   m u c h   d e t a i l   a s   y o u   c a n   a b o u t   h o w   t h i s   e r r o r   o c c u r r e d .   - - > ' ; 
 c o n s t   k L o g P r e   =   " \ n \ n < ! - -  !9�   B e l o w   i s   a   l o g   o f   t h i s   r u n   o f   E p i c h r o m e ,   w h i c h   m a y   b e   h e l p f u l   i n   d i a g n o s i n g   t h i s   e r r o r .   P l e a s e   r e d a c t   a n y   p a t h s   o r   i n f o r m a t i o n   y o u ' r e   n o t   c o m f o r t a b l e   s h a r i n g   b e f o r e   p o s t i n g   t h i s   i s s u e .   - - > \ n \ n ` ` ` \ n " ; 
 c o n s t   k L o g P o s t   =   ' \ n ` ` ` ' ; 
 
 
 / /   R E P O R T E R R O R :   r e p o r t   a n   e r r o r   t o   G i t H u b 
 f u n c t i o n   r e p o r t E r r o r ( a T i t l e ,   a L o g F i l e = u n d e f i n e d )   { 
         
         l e t   i R e s u l t ,   i E r r ; 
         
         / /   g e t   d e f a u l t   l o g   f i l e   i f   w e   h a v e   o n e 
         i f   ( ( a L o g F i l e   = = =   u n d e f i n e d )   & &   ( t y p e o f   g C o r e I n f o   ! = =   ' u n d e f i n e d ' )   & & 
                         g C o r e I n f o   & &   g C o r e I n f o . l o g F i l e )   { 
                 a L o g F i l e   =   g C o r e I n f o . l o g F i l e ; 
         } 
         
         / /   a t t e m p t   t o   r e a d   i n   l o g 
         l e t   i L o g   =   ' ' ; 
         i f   ( a L o g F i l e )   { 
                 a L o g F i l e   =   P a t h ( a L o g F i l e ) ; 
                 t r y   { 
                         i L o g   =   k A p p . r e a d ( a L o g F i l e ) ; 
                 }   c a t c h ( i E r r )   { }   / /   f a i l   s i l e n t l y 
         } 
         
         / /   o p e n   a   G i t H u b   i s s u e   p a g e   p o p u l a t e d   w i t h   o u r   e r r o r   i n f o 
         l e t   i U r l S t a r t   =   k U r l S t a r t   +   e n c o d e U R I C o m p o n e n t ( a T i t l e )   +   k U r l B o d y T a g   +   e n c o d e U R I C o m p o n e n t ( k B o d y S t a r t ) ; 
         l e t   i U r l   =   i U r l S t a r t ; 
         
         t r y   { 
                 
                 / /   a d d   i n   l o g 
                 i f   ( i L o g )   { 
                         
                         / /   e n s u r e   U R L   i s n ' t   t o o   l o n g   w i t h   l o g 
                         i U r l   + =   e n c o d e U R I C o m p o n e n t ( k L o g P r e   +   i L o g   +   k L o g P o s t ) ; 
                         
                         / /   i f   t o o   l o n g ,   a s k   f o r   i t   t o   b e   m a n u a l l y   p a s t e d   i n 
                         i f   ( i U r l . l e n g t h   > =   k M a x U r l L e n g t h )   { 
                                 i U r l   =   i U r l S t a r t   +   e n c o d e U R I C o m p o n e n t ( " \ n \ n < ! - -  'W   T h e   l o g   f i l e   i s   t o o   l o n g   t o   i n c l u d e   i n   t h e   U R L .   T o   h e l p   d i a g n o s e   t h i s   e r r o r ,   p l e a s e   c o p y   t h e   l o g   a n d   p a s t e   t h e   l o g   i n   t h e   a r e a   b e l o w .   P l e a s e   r e d a c t   a n y   p a t h s   o r   i n f o r m a t i o n   y o u ' r e   n o t   c o m f o r t a b l e   s h a r i n g   b e f o r e   p o s t i n g   t h i s   i s s u e .   - - > \ n \ n ` ` ` \ n [!9�   P A S T E   L O G   H E R E ] \ n ` ` ` " ) ; 
                         } 
                 } 
                 
                 / /   o p e n   U R L 
                 k A p p . o p e n L o c a t i o n ( i U r l ) ; 
                 
         }   c a t c h ( i E r r )   { 
                 l e t   i E r r M s g   =   ' U n a b l e   t o   o p e n   G i t H u b   i s s u e s   p a g e .   ( '   +   i E r r . m e s s a g e   +   ' ) ' ; 
                 i f   ( t y p e o f   d i a l o g   = = =   " f u n c t i o n " )   { 
                         e r r l o g ( i E r r M s g ) ; 
                         d i a l o g ( i E r r M s g   +   '   Y o u   w i l l   n e e d   t o   r e p o r t   t h i s   e r r o r   m a n u a l l y . ' ,   { 
                                 w i t h T i t l e :   ' E r r o r ' , 
                                 w i t h I c o n :   ' s t o p ' , 
                                 b u t t o n s :   [ ' O K ' ] , 
                                 d e f a u l t B u t t o n :   1 
                         } ) ; 
                 }   e l s e   { 
                         i R e s u l t   =   i E r r M s g ; 
                 } 
         } 
         
         i f   ( a L o g F i l e )   { 
                 t r y   { 
                         / /   r e v e a l   l o g   f i l e 
                         k F i n d e r . s e l e c t ( a L o g F i l e ) ; 
                         k F i n d e r . a c t i v a t e ( ) ; 
                 }   c a t c h ( i E r r )   { }   / /   f a i l   s i l e n t l y 
         } 
         
         / /   r e t u r n   a n y   e r r o r   m e s s a g e 
         r e t u r n   i R e s u l t ; 
 } 
 / / 
 / / 
 / /     v c m p . j s :   U t i l i t y   f u n c t i o n   t o   c o m p a r e   E p i c h r o m e   v e r s i o n s . 
 / / 
 / /     C o p y r i g h t   ( C )   2 0 2 1     D a v i d   M a r m o r 
 / / 
 / /     h t t p s : / / g i t h u b . c o m / d m a r m o r / e p i c h r o m e 
 / / 
 / /     T h i s   p r o g r a m   i s   f r e e   s o f t w a r e :   y o u   c a n   r e d i s t r i b u t e   i t   a n d / o r   m o d i f y 
 / /     i t   u n d e r   t h e   t e r m s   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e   a s   p u b l i s h e d   b y 
 / /     t h e   F r e e   S o f t w a r e   F o u n d a t i o n ,   e i t h e r   v e r s i o n   3   o f   t h e   L i c e n s e ,   o r 
 / /     ( a t   y o u r   o p t i o n )   a n y   l a t e r   v e r s i o n . 
 / / 
 / /     T h i s   p r o g r a m   i s   d i s t r i b u t e d   i n   t h e   h o p e   t h a t   i t   w i l l   b e   u s e f u l , 
 / /     b u t   W I T H O U T   A N Y   W A R R A N T Y ;   w i t h o u t   e v e n   t h e   i m p l i e d   w a r r a n t y   o f 
 / /     M E R C H A N T A B I L I T Y   o r   F I T N E S S   F O R   A   P A R T I C U L A R   P U R P O S E .     S e e   t h e 
 / /     G N U   G e n e r a l   P u b l i c   L i c e n s e   f o r   m o r e   d e t a i l s . 
 / / 
 / /     Y o u   s h o u l d   h a v e   r e c e i v e d   a   c o p y   o f   t h e   G N U   G e n e r a l   P u b l i c   L i c e n s e 
 / /     a l o n g   w i t h   t h i s   p r o g r a m .     I f   n o t ,   s e e   < h t t p : / / w w w . g n u . o r g / l i c e n s e s / > . 
 
 
 / /   V C M P :   c o m p a r e   v e r s i o n   n u m b e r s   ( v 1   <   v 2 :   - 1 ,   v 1   = =   v 2 :   0 ,   v 1   >   v 2 :   1 ) 
 / /       a N u m C o m p o n e n t s :   i f   > 0 ,   n u m b e r   o f   c o m p o n e n t s   t o   c o m p a r e ,   s t a r t i n g   w i t h   m a j o r   v e r s i o n   ( 1 . 2 . 3 b 4 [ 5 ] ) 
 f u n c t i o n   v c m p   ( v 1 ,   v 2 ,   a N u m C o m p o n e n t s = 0 )   { 
         
         / /   r e g e x   f o r   p u l l i n g   o u t   v e r s i o n   p a r t s 
         c o n s t   k V e r s i o n R e = ' ^ 0 * ( [ 0 - 9 ] + ) \ \ . 0 * ( [ 0 - 9 ] + ) \ \ . 0 * ( [ 0 - 9 ] + ) ( b 0 * ( [ 0 - 9 ] + ) ) ? ( \ \ [ 0 * ( [ 0 - 9 ] + ) ] ) ? $ ' ; 
         
         / /   n o r m a l i z e   n u m b e r   o f   c o m p o n e n t s   ( i f   < =   0   o r   n o t   a   n u m b e r ,   u s e   a l l ) 
         a N u m C o m p o n e n t s   =   N u m b e r ( a N u m C o m p o n e n t s ) ; 
         i f   ( ! ( a N u m C o m p o n e n t s   >   0 ) )   { 
                 a N u m C o m p o n e n t s   =   1 0 ;     / /   s a f e l y   p a s t   a l l   c o m p o n e n t s 
         } 
         
         / /   a r r a y   f o r   c o m p a r a b l e   v e r s i o n   i n t e g e r s 
         v a r   v S t r   =   [ ] ; 
 
         / /   m u n g e   v e r s i o n   n u m b e r s   i n t o   c o m p a r a b l e   i n t e g e r s 
         f o r   ( l e t   c u r V   o f   [   v 1 ,   v 2   ] )   { 
                 
                 / /   c o n f o r m   c u r r e n t   v e r s i o n   n u m b e r 
                 i f   ( t y p e o f   c u r V   = =   ' n u m b e r ' )   { 
                         c u r V   =   c u r V . t o S t r i n g ( ) ; 
                 }   e l s e   i f   ( t y p e o f   c u r V   ! =   ' s t r i n g ' )   { 
                         c u r V   =   ' ' ; 
                 } 
                 
                 l e t   v m a j ,   v m i n ,   v b u g ,   v b e t a ,   v b u i l d ; 
 
                 c o n s t   c u r M a t c h   =   c u r V . m a t c h ( k V e r s i o n R e ) ; 
 
                 i f   ( c u r M a t c h )   { 
 
                         / /   e x t r a c t   v e r s i o n   n u m b e r   p a r t s 
                         v m a j       =   p a r s e I n t ( c u r M a t c h [ 1 ] ) ; 
                         v m i n       =   ( a N u m C o m p o n e n t s   > =   2 )   ?   p a r s e I n t ( c u r M a t c h [ 2 ] )   :   0 ; 
                         v b u g       =   ( a N u m C o m p o n e n t s   > =   3 )   ?   p a r s e I n t ( c u r M a t c h [ 3 ] )   :   0 ; 
                         v b e t a     =   ( a N u m C o m p o n e n t s   > =   4 )   ?   ( c u r M a t c h [ 5 ]   ?   p a r s e I n t ( c u r M a t c h [ 5 ] )   :   1 0 0 0 )   :   0 ; 
                         v b u i l d   =   ( a N u m C o m p o n e n t s   > =   5 )   ?   ( c u r M a t c h [ 7 ]   ?   p a r s e I n t ( c u r M a t c h [ 7 ] )   :   1 0 0 0 0 )   :   0 ; 
                 }   e l s e   { 
 
                         / /   u n a b l e   t o   p a r s e   v e r s i o n   n u m b e r 
                         c o n s o l e . l o g ( ' U n a b l e   t o   p a r s e   v e r s i o n   " '   +   c u r V   +   ' " ' ) ; 
                         v m a j   =   v m i n   =   v b u g   =   v b e t a   =   v b u i l d   =   0 ; 
                 } 
 
                 / /   a d d   t o   a r r a y 
                 v S t r . p u s h ( v m a j . t o S t r i n g ( ) . p a d S t a r t ( 3 , ' 0 ' ) + ' . ' + 
                                     v m i n . t o S t r i n g ( ) . p a d S t a r t ( 3 , ' 0 ' ) + ' . ' + 
                                     v b u g . t o S t r i n g ( ) . p a d S t a r t ( 3 , ' 0 ' ) + ' . ' + 
                                     v b e t a . t o S t r i n g ( ) . p a d S t a r t ( 4 , ' 0 ' ) + ' . ' + 
                                     v b u i l d . t o S t r i n g ( ) . p a d S t a r t ( 5 , ' 0 ' ) ) ; 
         } 
 
         / /   c o m p a r e   v e r s i o n   s t r i n g s 
         i f   ( v S t r [ 0 ]   <   v S t r [ 1 ] )   { 
                 r e t u r n   - 1 ; 
         }   e l s e   i f   ( v S t r [ 0 ]   >   v S t r [ 1 ] )   { 
                 r e t u r n   1 ; 
         }   e l s e   { 
                 r e t u r n   0 ; 
         } 
 } 
                                   F"jscr  ��ޭ