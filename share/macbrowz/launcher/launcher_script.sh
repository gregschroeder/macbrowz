#!/bin/bash
# macbrowz browser wrapper
#   check for and apply updates
#   launch actual browser with proper arguments
#   adjust launch if already running
#
# This is the 'script' in the Platypus browser wrapper
#

one_time_start_urls=("$@")

myself="$0"
original_argv=("$0" "$@")
curdir=$(dirname "$0")

config_file="${curdir}/macbrowz.conf"
app_name=overridden_by_config
refresh_script=overridden_by_config

#####################################
#
# set variables, load defaults from config
#
#####################################
init() {
  # manually grab script_lib_path for bootstrap
  # could just source the config file and ignore the security hole, but..
  if [ ! -f "${config_file}" ]; then
    echo "Config file not found: ${config_file}" 2>&1
    exit 1
  else
    script_lib_path=$(awk -F= '/^script_lib_path=/{printf("%s", $2)}' "${config_file}" |
      sed -e 's/"//g' -e 's/  *$//' 2> /dev/null)
    if [[ -n "${script_lib_path}" &&
          -d "${script_lib_path}" &&
          -f "${script_lib_path}/notify.sh" &&
          -f "${script_lib_path}/config.sh" ]]; then
      . "${script_lib_path}/notify.sh"
      . "${script_lib_path}/config.sh"
    elif [ -z "${script_lib_path}" ]; then
      echo "Cannot find script_lib_path from ${config_file}" 2>&1
      exit 2
    else
      echo "Unable to load utility scripts in ${script_lib_path} from ${config_file}" 2>&1
      exit 3
    fi
  fi

  # set variables from config
  configLoad "${config_file}" \
    refresh_script \
    launcher_script \
    app_name \
    browser_engine_name \
    profile_path \
    start_urls \
    user_agent \
    window_style
}

#####################################
#
# send desktop notification (using mechanism from Platypus)
#
#####################################
notify() {
  echo "NOTIFICATION:$*"
}

#####################################
#
# return true if SSB app is already running
#
#####################################
isRunning() {
  pgrep -fq "/${app_name}.app/Contents/MacOS/${browser_engine_name:-Brave Browser}"
}

#####################################
#
# return true if this script appears out of date
#
#####################################
scriptOutOfDate() {
  # shellcheck disable=SC2154
  local script="${launcher_script}"

  # fallback to default install location if script not found
  [ -x "${script}" ] || script="/usr/local/share/macbrowz/launcher/launcher.app/Contents/Resources/script"

  [ "${script}" -nt "${myself}" ]
}

#####################################
#
# refresh app as needed
# - normally just the inner browser, since it changes frequently
# - on request, can be the whole app
#
#####################################
refreshApp() {
  # prevent possible recursion - no-op if called by our own relauncher
  if [ -n "${MACBROWZ_RELAUNCH}" ]; then
    notify "Update complete, launching browser"
    return 0
  fi

  local script="${refresh_script}"
  local script_name=${refresh_script##*/}

  # fallback to default install location if script not found
  [ -x "${script}" ] || script="/usr/local/bin/${script_name:-macbrowz}"

  if [ -x "${script}" ]; then
    if scriptOutOfDate; then
      if isRunning; then
        notify "Launch script out of date but application already running"
      else
        notify "Launch script out of date, launching updater"

        # call the refresh script with our config info, and have it do a full app update
        # do this in a separate script so we can exit and get restarted as the updated version.
        local quoted_argv
        printf -v quoted_argv "\"%s\" " "${original_argv[@]}"
        local tmpdir="${TMPDIR}/mbscript.$$"
        mkdir -p "${tmpdir}"
        local updater="${tmpdir}/updater"
        cat << EOF > "${updater}"
#!/usr/bin/env bash
"${script}" -U -c "${config_file}" "${app_name}"
MACBROWZ_RELAUNCH=true ${quoted_argv}
EOF
        chmod +x "${updater}"
        "${updater}" &
        disown $!
        exit 0
      fi
    elif ! isRunning; then
      # call the refresh script with our config info
      "${script}" -u -c "${config_file}" "${app_name}"
    fi
  else
    abort "Refresh script \"${refresh_script}\" not found"
  fi
}

#####################################
#
# launch inner app browser, considering already running state
#
#####################################
launchBrowser() {
  local browser_args=(
    '--args'
    '--no-sandbox'
    '--no-default-browser-check'
    "--user-agent=${user_agent}"
    '--test-type'
    "--user-data-dir=${profile_path}"
    )

  # if already running, call it with optional URL to open if any given
  if isRunning; then
    if [ -z "${one_time_start_urls[*]}" ]; then
      open -a "${PWD}/${app_name}.app"
    else
      open -n -a "${PWD}/${app_name}.app" "${browser_args[@]}" --new-window "${one_time_start_urls[@]}"
    fi
    return
  fi

  # not already running, will launch

  # NB: first-run case where Preferences file does not yet exist is not handled here,
  # because the Preferences file with the flag isn't created until first-run.
  # This is solved in the common practical case by starting with a template.

  # update preferences
  local default_prefs="${profile_path}/Default/Preferences"
  if [ -f "${default_prefs}" ]; then
    sed -i '' -e "s/\"has_seen_welcome_page\":false/\"has_seen_welcome_page\":true/g" "${default_prefs}"
  fi

  # only provide start page if not configured to start with certain pages or from previous state
  # restore_on_startup values:
  #   1 = Continue where you left off
  #   4 = Open a specific page or set of pages
  #   5 = Open the New Tab page
  local secure_prefs="${profile_path}/Default/Secure Preferences"
  if grep -qs "\"restore_on_startup\":1" "${secure_prefs}"; then
    start_args=""
  elif grep -qs "\"restore_on_startup\"" && ! grep -qa "\"restore_on_startup\":5"; then
    start_args=""
  else
    if [ -n "${one_time_start_urls}" ]; then
      start_args=("${one_time_start_urls[@]}")
    else
      start_args=("${start_urls[@]}")
    fi
  fi

  # start with all the initialization flags
  if [ -n "${window_style}" ]; then
    open -n -a "${PWD}/${app_name}.app" "${browser_args[@]}" --app="${start_args[0]}"
  else
    open -n -a "${PWD}/${app_name}.app" "${browser_args[@]}" "${start_args[@]}"
  fi
}

#####################################
#
#  M A I N
#
#####################################

main() {
  init
  refreshApp
  launchBrowser
}

# Execute main() if this is run in standalone mode (i.e. not in a unit test)
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  cd "${curdir}" || exit 1
  main "$@"
fi
