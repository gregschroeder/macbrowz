#!/bin/bash

# try and source dependencies, or assume caller has already included them
lib_dir="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
[ -f "${lib_dir}/notify.sh" ] && . "${lib_dir}/notify.sh"
[ -f "${lib_dir}/util.sh" ] && . "${lib_dir}/util.sh"

#####################################
#
# generate .icns file from given icon PNG file
#   src can be file path or URI
#
#####################################
function generateIcns() {
  local icon_uri="$1"
  local icns_file="$2"

  local tmpdir="${TMPDIR}/libicon.$$"
  mkdir -p "${tmpdir}"

  local tmp_icon_source
  tmp_icon_source="${tmpdir}/$(basename "${icon_uri}")"

  # copy icon from source location into local temp
  case "${icon_uri}" in
    http*)
      curl -q -s "${icon_uri}" -o "${tmp_icon_source}"
      ;;
    *)
      # assume file
      # shellcheck disable=SC2001
      icon_uri_file=$(echo "${icon_uri}" | sed -e 's~^file://*~/~')
      [ -e "${icon_uri_file}" ] || abort "icon file not found: ${icon_uri_file}"
      cp -f "${icon_uri_file}" "${tmp_icon_source}"
      icon_uri=$(absPath "${icon_uri_file}")
      ;;
  esac
  [ -s "${tmp_icon_source}" ] || abort "icon file invalid or not found: ${icon_uri}"

  # trusting file suffix, convert to PNG
  local tmp_png_source="${tmpdir}/icon.png"
  local already_icns
  case "${tmp_icon_source}" in
    *.icns) already_icns=true ;;
    *.png) mv -f "${tmp_icon_source}" "${tmp_png_source}" ;;
    *) sips -s format png "${tmp_icon_source}" -o "${tmp_png_source}" > /dev/null;;
  esac
  file "${tmp_png_source}" | grep -qE "PNG image" || abort "unable to convert icon file to PNG: ${icon_uri_file}"

  # convert to .icns file
  if [ -n "${already_icns}" ]; then
    cp -f "${tmp_icon_source}" "${icns_file}"
  else
    # make folder to contain icon sizes
    local iconset_dir="${tmpdir}/Icon.iconset"
    mkdir -p "${iconset_dir}"

    local tmpsips="${tmpdir}/sips.out"
    (
      # create PNGs of various sizes, all called icon_*
      # use built-in sips as not to require additional runtime dependencies like imagemagick
      sips -z 16 16 "${tmp_png_source}" --out "${iconset_dir}/icon_16x16.png" &&
        sips -z 32 32 "${tmp_png_source}" --out "${iconset_dir}/icon_16x16@2x.png" &&
        sips -z 32 32 "${tmp_png_source}" --out "${iconset_dir}/icon_32x32.png" &&
        sips -z 64 64 "${tmp_png_source}" --out "${iconset_dir}/icon_32x32@2x.png" &&
        sips -z 128 128 "${tmp_png_source}" --out "${iconset_dir}/icon_128x128.png" &&
        sips -z 256 256 "${tmp_png_source}" --out "${iconset_dir}/icon_128x128@2x.png" &&
        sips -z 256 256 "${tmp_png_source}" --out "${iconset_dir}/icon_256x256.png" &&
        sips -z 512 512 "${tmp_png_source}" --out "${iconset_dir}/icon_256x256@2x.png" &&
        sips -z 512 512 "${tmp_png_source}" --out "${iconset_dir}/icon_512x512.png" &&
        cp "${tmp_png_source}" "${iconset_dir}/icon_512x512@2x.png"
    ) > "${tmpsips}" 2>&1 || abort "error converting icon: $(cat "${tmpsips}")"

    # convert to .icns file
    iconutil -c icns "${iconset_dir}" -o "${icns_file}"

    # validate and cleanup
    file "${icns_file}" | grep -qE "Mac OS X icon" || abort "unable to create .icns file: ${icon_uri_file}"
    rm -rf "${iconset_dir}"
  fi

  rm -rf "${tmpdir}"
}
