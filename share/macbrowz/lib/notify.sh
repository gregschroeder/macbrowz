#!/bin/bash

#####################################
#
# display error message as dialog
#
#####################################
function errorDialog() {
  local msg="$1"
  local title="$2"

  osascript << EOF
display dialog "${msg}" buttons {"OK"} default button 1 with icon caution with title "${title}"
return -- ignore
EOF
}

#####################################
#
# display error message and exit
#
#####################################
function abort() {
  local msg=("$@")

  cat 1>&2 << EOF
${msg[@]}
EOF
  exit 2
}

#####################################
#
# internal print tracing message
# - emit to TRACE_OUTPUT if set, else stderr
#   TRACE_OUTPUT can be given as an array or array string (ENV variable values are always strings)
#   in bash array syntax, eg., TRACE_OUTPUT=(/dev/stderr /tmp/logfile) command ...
#
#####################################
function _print_tracer() {
  local message="$*"

  local outputs=( /dev/stderr )
  if [ -n "${TRACE_OUTPUT}" ]; then
    if [[ "$(declare -p TRACE_OUTPUT)" =~ "declare -a" ]]; then
      outputs=( "${TRACE_OUTPUT[@]}" )
    else
      case "${TRACE_OUTPUT}" in
        # convert to array, handling array-syntax string "( item1 item2 )"
        (*\)) typeset -a "outputs=${TRACE_OUTPUT}";;
        *) outputs=( "${TRACE_OUTPUT}" );;
      esac
    fi
  fi

  for output in "${outputs[@]}"; do
    local color1_on color2_on color_off
    case "${output}" in
      # colorize outputs that appear to be destined for a terminal
      /dev/stdout|/dev/stderr|/dev/tty)
        color1_on="${TRACE_COLOR1:-\033[0;34m}"
        color2_on="${TRACE_COLOR2:-\033[0;33m}"
        color_off="\033[0m"
        ;;
    esac

    # assume callstack has trace_*() and then this function, so skip up a frame and use its context
    read -r caller_lineno caller_func caller_file < <(caller 1)
    local called_by
    called_by="$(basename "${caller_file}"):${caller_lineno} ${caller_func}()"

    printf "${color1_on}%s %s>${color_off} ${color2_on}%s${color_off}\n" \
      "$(date +"%Y%m%d.%H%M%S")" \
      "${called_by}" \
      "${message}" \
      >> "${output}"
  done
}


#####################################
#
# return the level number for the given level (or value of TRACE_LEVEL)
# levels are: [FATAL]=0 [ERROR]=1 [WARN]=2 [INFO]=3 [DEBUG]=4 [VERBOSE]=5
#
#####################################
function trace_level_num() {
  local level=
  level=$( echo "${TRACE_LEVEL}" | tr '[:lower:]' '[:upper:]' )
  case "${level}" in
    FATAL|0) echo 0;;
    ERROR|1) echo 1;;
    WARN|2) echo 2;;
    INFO|3) echo 3;;
    DEBUG|4) echo 4;;
    VERBOSE|5) echo 5;;
    *) echo -1;;
  esac
}

#####################################
#
# increment (or decrement) the tracing level
# set TRACE_LEVEL accordingly (as numeric, regardless of former type)
#
#####################################
function trace_level_increment() {
  local offset=${1:-1}

  local level
  level=$(trace_level_num)
  level=$((level+offset))

  [[ ${level} -lt -1 ]] && level=-1
  [[ ${level} -gt 5 ]] && level=5

  export TRACE_LEVEL=${level}
}

#####################################
#
# create tracing functions for every level
# - trace_level_LEVEL() - return true if that level is active
#   useful for enclosing expensive blocks that should only execute while tracing
# - trace_LEVEL() - emit the message if level if active
#
#####################################
function _trace_create_funcs() {
  local level
  local i=0

  for level in fatal error warn info debug verbose; do
    # shellcheck disable=SC1090
    cat << EOF
function trace_level_${level}() {
  [[ \$(trace_level_num) -ge $i ]]
}
function trace_${level}() {
  trace_level_${level} && _print_tracer "\$@"
  true
}
EOF
    ((i++))
  done
}
eval "$(_trace_create_funcs)"
unset _trace_create_funcs
