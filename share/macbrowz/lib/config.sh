#!/bin/bash
# read/write shell variables from/to config file
#

# configLoad [options] config_file [var...]
# load variable(s) from config file, or entire config file if no variables given
# options:
#   -p|--prefix <prefix> : prefix to prepend to loaded variable name
#
function configLoad() {
  local prefix=
  while [ $# -gt 0 ]; do
    case "$1" in
      -p | --prefix)
        shift
        prefix="$1"
        ;;
      *) break ;;
    esac
    shift
  done

  local config_file="$1"
  shift
  local vars=()
  local line
  while IFS='' read -r line; do
    vars+=("${line}")
  done < <(printf -- "%s\n" "$@" | sort -u)

  [ -f "${config_file}" ] || return 1

  local tmpfile=/tmp/configLoad.$$
  if [ "${#vars}" -eq 0 ]; then
    cp -f "${config_file}" "${tmpfile}"
  else
    true > "${tmpfile}"
    for var in "${vars[@]}"; do
      grep "^${var}=" "${config_file}" >> "${tmpfile}"
    done
  fi

  # read one line at a time and load all variables
  while read -r line; do
    # skip comments and lines that are not assignments, special case arrays
    case "${line}" in
      \#*) continue ;;
      *=\(*)
        eval "$(printf -- "%s" "${prefix}${line}")"
        continue
        ;;
      *=*)
        # do assignment
        eval "$(echo "${prefix}${line}" | awk -F= '{printf("%s=\"%s\"", $1, $2)}' | sed 's/""/"/g')"
        ;;
    esac
  done < <(cat "${tmpfile}")

  rm -f "${tmpfile}"
}

# save variable(s) to config file
function configSave() {
  local config_file="$1"
  shift
  local vars=()
  local line
  while IFS='' read -r line; do
    vars+=("${line}")
  done < <(printf -- "%s\n" "$@" | sort -u)

  # create file (and dir) if needed
  if [ ! -f "${config_file}" ]; then
    local config_file_dir
    config_file_dir="$(dirname "${config_file}")"
    mkdir -p "${config_file_dir}"
    true > "${config_file}"
  fi

  local var
  for var in "${vars[@]}"; do
    # handle array values
    local array_name="${var}"
    local array_ref="${array_name}[@]"
    local array=("${!array_ref}")

    # quote all values - for arrays, quote each individually and wrap in parens
    if [ "${#array[@]}" -gt 1 ]; then
      local quoted_values
      printf -v quoted_values "\"%s\" " "${array[@]}"
      printf -v val "( %s)" "${quoted_values}"
    else
      val="\"${array[0]}\""
    fi

    # replace or append value in file
    if grep -q "^${var}=" "${config_file}"; then
      sed -i "" -e "/^${var}=.*/c\\
${var}=${val}" "${config_file}"
    else
      echo "${var}=${val}" >> "${config_file}"
    fi
  done
}
