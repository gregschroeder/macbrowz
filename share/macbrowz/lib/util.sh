#!/bin/bash
# shell utility functions
#

function absPath() {
  local p="$1"

  # get full absolute path
  # if the path/file does not exist, relative paths will remain
  local abs
  case "${p}" in
    /*) abs="$(printf -- "%s" "${p}")" ;;
    *)
      local dir
      if [ -d "${p}" ]; then
        abs="$(cd "${p}" > /dev/null 2>&1 && pwd)"
      else
        dir="$(cd "$(dirname "${p}")" > /dev/null 2>&1 && pwd)"
        abs="$(printf -- "%s/%s" "${dir}" "${p}")"
      fi
      ;;
  esac

  # remove remaining relative subpaths
  echo "${abs}" | sed -e 's|/\./|/|g' \
      -e ':a' \
      -e 's|\.\./\.\./|../..../|g' \
      -e 's|^[^/]*/\.\.\/||' \
      -e 't a' \
      -e 's|/[^/]*/\.\.\/|/|' \
      -e 't a' \
      -e 's|\.\.\.\./|../|g' \
      -e 't a'
}

# get property value from given .plist file
function getPlistProp() {
  local plist="$1"
  local prop="$2"

  plutil -extract "${prop}" xml1 -o - "${plist}" | sed -n 's/.*<string>\(.*\)<\/string>.*/\1/p'
}
