load '../helpers/setup'
load '../helpers/release_func'

@test "release: update changelog, missing changelog file" {
  loadScript
  changelog_file="${BATS_TEST_TMPDIR}/notexists.txt"

  run updateChangelog
  assert_failure
  assert_output -p "missing changelog file"
}

@test "release: update changelog, insert new entry" {
  loadScript

  MACBROWZ_VERSION=1.2.3
  mb_changeset_added=( "some notes" "more changes" )

  changelog_file="${BATS_TEST_TMPDIR}/changelog.md"
  cat << EOF > "${changelog_file}"
# Macbrowz Change Log
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.1] - 2021-10-05
### Added
- Stuff we said we did the first time

## [0.1.0] - 2021-10-01
### Changed
- Initial release
EOF

  run updateChangelog
  assert_success

  cat << EOF > "${changelog_file}.expected"
# Macbrowz Change Log
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.2.3] - $(date +%Y-%m-%d)
### Added
- some notes
- more changes

## [0.1.1] - 2021-10-05
### Added
- Stuff we said we did the first time

## [0.1.0] - 2021-10-01
### Changed
- Initial release
EOF

  assert cmp -s "${changelog_file}" "${changelog_file}.expected" || \
    diff "${changelog_file}" "${changelog_file}.expected"
}

@test "release: update changelog, amend mode should replace entry" {
  loadScript

  MACBROWZ_VERSION=1.2.3
  mb_changeset_added=( "some notes" "more changes" )

  changelog_file="${BATS_TEST_TMPDIR}/changelog.md"
  cat << EOF > "${changelog_file}"
# Macbrowz Change Log
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.2.3] - 2021-10-05
### Added
- Stuff we said we did the first time

## [0.1.0] - 2021-10-01
### Changed
- Initial release
EOF

  run updateChangelog
  assert_success

  cat << EOF > "${changelog_file}.expected"
# Macbrowz Change Log
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.2.3] - $(date +%Y-%m-%d)
### Added
- some notes
- more changes

## [0.1.0] - 2021-10-01
### Changed
- Initial release
EOF

  assert cmp -s "${changelog_file}" "${changelog_file}.expected"
}
