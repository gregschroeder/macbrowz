load '../helpers/setup'
load '../helpers/release_func'

#######################################
#
# missing and general invalid arguments
#
#######################################

@test "release: cli unknown flag" {
  callFunc parseCommandLine --fail
  assert_failure
  assert_output -e '^unknown flag: --fail'
  assert_output -p 'usage:'
}

@test "release: cli missing flag value" {
  callFunc parseCommandLine -t
  assert_failure
  assert_output -e '^missing value for -t'
  assert_output -p 'usage:'
}

#######################################
#
# default usage
#
#######################################

@test "release: cli default usage" {
  local flag

  loadScript
  token=

  parseCommandLine
  assert_success
  assert_equal "${token}" "${file_access_token}"
}

@test "release: cli missing token file" {
  local flag

  loadScript

  token_file="${BATS_TEST_TMPDIR}/notexists.txt"
  token=

  run parseCommandLine
  assert_failure
  assert_output -p "file not found"
}

@test "release: cli missing release information file" {
  local flag

  loadScript

  release_info_file="${BATS_TEST_TMPDIR}/notexists.txt"

  run parseCommandLine
  assert_failure
  assert_output -p "missing release information"
}

#######################################
#
# -t | --token
#
#######################################

@test "release: cli set access token" {
  local flag
  local access_token='asdlkjf@!#$%^'

  loadScript

  for flag in -t --token; do
    token=
    parseCommandLine "${flag}" "${access_token}"
    assert_success
    assert_equal "${token}" "${access_token}"
  done
}

#######################################
#
# -v | --verbose
#
#######################################

@test "release: cli verbose, tracing level from unset to INFO" {
  local flag

  loadScript

  for flag in -v --verbose; do
    TRACE_LEVEL=
    parseCommandLine "${flag}"
    assert_success
    assert_equal "${TRACE_LEVEL}" 3
  done
}

@test "release: cli verbose, tracing level from FATAL to INFO" {
  local flag

  loadScript

  for flag in -v --verbose; do
    TRACE_LEVEL=1
    parseCommandLine "${flag}"
    assert_success
    assert_equal "${TRACE_LEVEL}" 3
  done
}

@test "release: cli verbose, tracing level from set to INFO to DEBUG" {
  local flag

  loadScript

  for flag in -v --verbose; do
    TRACE_LEVEL=1
    parseCommandLine "${flag}"
    assert_success
    assert_equal "${TRACE_LEVEL}" 3
  done
}

@test "release: cli verbose, next tracing level from unset" {
  loadScript

  TRACE_LEVEL=
  parseCommandLine -v -v
  assert_success
  assert_equal "${TRACE_LEVEL}" 4
}

@test "release: cli verbose, next tracing level with vv from unset" {
  loadScript

  TRACE_LEVEL=
  parseCommandLine -vv
  assert_success
  assert_equal "${TRACE_LEVEL}" 4
}

@test "release: cli verbose, highest tracing level from unset" {
  loadScript

  TRACE_LEVEL=
  parseCommandLine -v -v -v
  assert_success
  assert_equal "${TRACE_LEVEL}" 5
}

@test "release: cli verbose, highest tracing level with vvv" {
  loadScript

  TRACE_LEVEL=
  parseCommandLine -vvv
  assert_success
  assert_equal "${TRACE_LEVEL}" 5
}
