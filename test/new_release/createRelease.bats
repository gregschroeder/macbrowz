load '../helpers/setup'
load '../helpers/release_func'

curl() {
  cat << EOF
RESULT: ${curl_result}
EOF
}

@test "release: create release, gets confirmation" {
  loadScript

  MACBROWZ_VERSION=1.2.3
  mb_changeset_text="## [1.2.3] - 2021-10-20
### Changed
- make some changes"

  curl_result="HTTP/2 201
{\"name\":\"Version ${MACBROWZ_VERSION}\",\"tag_name\":\"v${MACBROWZ_VERSION}\",\"description\":\"### Changed\n- make some changes\"}"

  run createRelease
  assert_success
}

@test "release: create release, gets 400" {
  loadScript

  MACBROWZ_VERSION=1.2.3
  mb_changeset_text="## [1.2.3] - 2021-10-20
### Changed
- make some changes"

  curl_result="HTTP/2 400"

  run createRelease
  assert_failure
  assert_output "Create release API failed RESULT: HTTP/2 400"
}
