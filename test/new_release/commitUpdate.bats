load '../helpers/setup'
load '../helpers/release_func'
load '../helpers/stubs'

setup() {
  createStubs \
    _gitListLog \
    _gitDiffChangelog \
    _gitRevertChanges \
    _gitCommitChanges \
    _gitTagRelease
}

@test "release: commit update, diff" {
  loadScript
  loadStubs

  run commitUpdate << 'EOF'
1
4
EOF
  assert_failure
  assert_output -p "CMD: _gitDiffChangelog"
  assert_output -p "changes made but not committed, process aborted"
}

@test "release: commit update, list log" {
  loadScript
  loadStubs

  run commitUpdate << 'EOF'
2
4
EOF
  assert_failure
  assert_output -p "CMD: _gitListLog"
  assert_output -p "changes made but not committed, process aborted"
}

@test "release: commit update, abort" {
  loadScript
  loadStubs

  run commitUpdate <<< 5

  assert_failure
  assert_output -p "changes reverted, process aborted"
}

@test "release: commit update, diff, commit" {
  loadScript
  loadStubs

  run commitUpdate << 'EOF'
1
3
EOF
  assert_success
  assert_output -p "CMD: _gitDiffChangelog"
  assert_output -p "CMD: _gitCommitChanges"
}
