load '../helpers/setup'
load '../helpers/release_func'

@test "release: reset next release" {
  loadScript
  MACBROWZ_VERSION=1.2.3
  macbrowz_script="${BATS_TEST_TMPDIR}/mb"
  touch "${macbrowz_script}"

  run resetNextRelease
  assert_success

  local expected_file="${release_info_file}.expected"
  cat << EOF > "${expected_file}"
# comment
MACBROWZ_VERSION=1.2.4

# comment
mb_changeset_added=()

# another comment
mb_changeset_changed=()

# comments should be ignored
mb_changeset_deprecated=()
mb_changeset_fixed=()
mb_changeset_removed=()

# comment
mb_changeset_security=()
EOF

  assert cmp -s "${release_info_file}" "${release_info_file}.expected" || \
    diff "${release_info_file}" "${release_info_file}.expected"
}
