load '../helpers/setup'
load '../helpers/release_func'

git() {
  echo "git called with $*"
}

@test "release: list log" {
  loadScript

  run listLog
  assert_success

  local expected="git called with log git called with describe --tags --abbrev=0..HEAD"
  assert_output "${expected}"
}
