load '../helpers/setup'
load '../helpers/release_func'
load '../helpers/stubs'

setup() {
  createStubs \
    _gitTagRelease
}

@test "release: tag release" {
  loadScript
  loadStubs

  run tagRelease
  assert_success
  local expected="_gitTagRelease"
  assert_equal "$(commandsRun)" "${expected}"
}
