load '../helpers/setup'
load '../helpers/release_func'

git() {
  local line
  for line in "${git_lines[@]}"; do
    echo "${line}"
  done
}

@test "release: assert all committed, none checked out" {
  loadScript

  git_lines=()
  run assertAllCommitted
  assert_success
}

@test "release: assert all committed, ok files checked out" {
  loadScript

  git_lines=( 'RELEASE_INFO' 'CHANGELOG.md' )
  run assertAllCommitted
  assert_success
}

@test "release: assert all committed, not ok files checked out" {
  loadScript

  git_lines=( 'macbrowz' )
  run assertAllCommitted
  assert_failure
  assert_output "unexpected files still uncommitted: macbrowz"
}

@test "release: assert all committed, ok and not ok files checked out" {
  loadScript

  git_lines=( 'RELEASE_INFO' 'macbrowz' )
  run assertAllCommitted
  assert_failure
  assert_output "unexpected files still uncommitted: macbrowz"
}
