load '../helpers/setup'
load '../helpers/release_func'

@test "release: load next release info" {
  loadScript

  loadNextReleaseInfo
  assert_success

  assert_equal "${MACBROWZ_VERSION}" "1.2.3"
  local changes=( "some notes" "more changes" )
  assert_equal "${mb_changeset_added}" "${changes[@]}"
}

@test "release: load next release info, file missing" {
  loadScript
  release_info_file=

  run loadNextReleaseInfo
  assert_failure
  assert_output -p "unable to get release information"
}

@test "release: load next release info, release format borked" {
  loadScript

  local borked_file="${release_info_file}.borked"
  sed -e 's/1.2.3/v1.2beta/' "${release_info_file}" > "${borked_file}"
  release_info_file="${borked_file}"

  run loadNextReleaseInfo
  assert_failure
  assert_output -p "invalid format MACBROWZ_VERSION"
}
