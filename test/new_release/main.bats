load '../helpers/setup'
load '../helpers/release_func'
load '../helpers/stubs'

setup() {
  createStubs \
    parseCommandLine \
    listLog \
    assertAllCommitted \
    loadNextReleaseInfo \
    updateChangelog \
    resetNextRelease \
    commitUpdate \
    tagRelease \
    createRelease
}

@test "release: main, default" {
  loadScript
  loadStubs

  run main
  assert_success

  local expected="parseCommandLine"
  expected="${expected}; assertAllCommitted"
  expected="${expected}; loadNextReleaseInfo"
  expected="${expected}; updateChangelog"
  expected="${expected}; resetNextRelease"
  expected="${expected}; commitUpdate"
  expected="${expected}; tagRelease"
  expected="${expected}; createRelease"
  assert_equal "$(commandsRun)" "${expected}"
}

@test "release: main, list change log" {
  loadScript
  loadStubs

  list_log_mode=true

  run main --list
  assert_success

  local expected="parseCommandLine --list"
  expected="${expected}; listLog"
  assert_equal "$(commandsRun)" "${expected}"
}
