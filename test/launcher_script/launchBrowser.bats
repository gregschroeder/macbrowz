load '../helpers/setup'

setup() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local launchdir="$( cd "${curdir}/../../share/macbrowz/launcher" && pwd )"

  local stubs_file="${BATS_TEST_TMPDIR}/stubs.bash"
  cat << 'EOF' >> "${stubs_file}"
function open() {
  echo open dummy called with "$@"
}
EOF

  . "${launchdir}/launcher_script.sh"
  . "${stubs_file}"

  profile_path="${BATS_TEST_TMPDIR}/profile"
  app_name=TestApp
  user_agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome Safari/537.36"
  browser_args=('--args' '--no-sandbox' '--no-default-browser-check' "--user-agent=${user_agent}" '--test-type' "--user-data-dir=${profile_path}")
}

# fake pgrep
pgrep() {
  [ -n "${is_running}" ]
}

@test "launcher: launchBrowser if already running no url" {
  one_time_start_urls=()
  is_running=true

  run launchBrowser
  assert_success
  assert_output "open dummy called with -a ${PWD}/${app_name}.app"
}

@test "launcher: launchBrowser if already running, with url" {
  one_time_start_urls=( "https://www.zombo.com" )
  is_running=true

  run launchBrowser
  assert_success

  assert_output "open dummy called with -n -a ${PWD}/${app_name}.app ${browser_args[*]} --new-window ${one_time_start_urls[*]}"
}

@test "launcher: launchBrowser if not already running with url given" {
  one_time_start_urls=( "https://www.zombo.com" )
  start_urls=( "https://www.yahoo.com" "https://www.google.com" )
  is_running=

  run launchBrowser
  assert_success
  assert_output "open dummy called with -n -a ${PWD}/${app_name}.app ${browser_args[*]} ${one_time_start_urls[*]}"
}

@test "launcher: launchBrowser if not already running with no url given" {
  one_time_start_urls=()
  start_urls=( "https://www.yahoo.com" "https://www.google.com" )
  is_running=

  run launchBrowser
  assert_success
  assert_output "open dummy called with -n -a ${PWD}/${app_name}.app ${browser_args[*]} ${start_urls[*]}"
}

@test "launcher: launchBrowser if not already running with url given, window style" {
  start_urls=( "https://www.yahoo.com" )
  is_running=
  window_style=true

  run launchBrowser
  assert_success
  assert_output "open dummy called with -n -a ${PWD}/${app_name}.app ${browser_args[*]} --app=${start_urls[0]}"
}
