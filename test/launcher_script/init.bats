load '../helpers/setup'

setup() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local launchdir="$( cd "${curdir}/../../share/macbrowz/launcher" && pwd )"
  libdir="$( cd "${launchdir}/../lib" && pwd )"

  . "${launchdir}/launcher_script.sh"
}

@test "launcher: init, config file does not exist" {
  config_file="notexists"

  run init
  assert_failure
  assert_output "Config file not found: ${config_file}"
}

@test "launcher: init, script_lib_path var does not exist" {
  config_file="${BATS_TEST_TMPDIR}/config.conf"
  touch "${config_file}"

  run init
  assert_failure
  assert_output "Cannot find script_lib_path from ${config_file}"
}

@test "launcher: init, script_lib_path does not exist" {
  config_file="${BATS_TEST_TMPDIR}/config.conf"
  local bogus="/does/not/exist"
  echo "script_lib_path=\"${bogus}\"" > "${config_file}"

  run init
  assert_failure
  assert_output "Unable to load utility scripts in ${bogus} from ${config_file}"
}

@test "launcher: init, script_lib_path exists but missing libs" {
  config_file="${BATS_TEST_TMPDIR}/config.conf"
  local bogus="${BATS_TEST_TMPDIR}/empty"
  mkdir -p "${bogus}"
  echo "script_lib_path=\"${bogus}\"" > "${config_file}"

  run init
  assert_failure
  assert_output "Unable to load utility scripts in ${bogus} from ${config_file}"
}

@test "launcher: init, proper script_lib_path" {
  config_file="${BATS_TEST_TMPDIR}/config.conf"
  echo "script_lib_path=\"${libdir}\"" > "${config_file}"
  run init
  assert_success
}
