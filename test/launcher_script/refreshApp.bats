load '../helpers/setup'

setup() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local launchdir="$( cd "${curdir}/../../share/macbrowz/launcher" && pwd )"

  local stubs_file="${BATS_TEST_TMPDIR}/stubs.bash"
  cat << 'EOF' >> "${stubs_file}"
function isRunning() {
  return ${is_running}
}

function scriptOutOfDate() {
  return ${out_of_date}
}

function notify() {
  echo NOTIFY called with "$*"
}
EOF

  . "${launchdir}/launcher_script.sh"
  . "${stubs_file}"

  # create a dummy refresh script with testable output
  refresh_script="${BATS_TEST_TMPDIR}/macbrowz"
  touch "${refresh_script}"
  chmod +x "${refresh_script}"

  cat << 'EOF' > "${refresh_script}"
#!/bin/bash
echo refresh dummy script called with "$@"
EOF

  config_file="${BATS_TEST_TMPDIR}/config.conf"
  app_name=MyApp
}

@test "launcher: refreshApp, script not out of date, not running" {
  out_of_date=1
  is_running=1

  run refreshApp
  assert_success

  local expected="refresh dummy script called with -u -c ${config_file} ${app_name}"
  assert_output "${expected}"
}

@test "launcher: refreshApp, script not out of date, running" {
  out_of_date=1
  is_running=0

  run refreshApp
  assert_success

  local expected=""
  assert_output "${expected}"
}

@test "launcher: refreshApp, script out of date, not running" {
  out_of_date=0
  is_running=1
  original_argv=( "${refresh_script}" calls myself with args )

  run refreshApp
  assert_success

  # should call a temp script that calls the refresh script and then myself again
  # here, we are reusing the 'refresh script' for both, and just expecting different output
  local expected="NOTIFY called with Launch script out of date, launching updater"
  expected="${expected}
refresh dummy script called with -U -c ${config_file} ${app_name}"
  expected="${expected}
refresh dummy script called with ${original_argv[@]:1}"
  assert_output "${expected}"
}

@test "launcher: refreshApp, script out of date, running" {
  out_of_date=0
  is_running=0

  run refreshApp
  assert_success

  local expected="NOTIFY called with Launch script out of date but application already running"
  assert_output "${expected}"
}

@test "launcher: refreshApp, no-op on relaunch" {
  MACBROWZ_RELAUNCH=true

  run refreshApp
  assert_success
  assert_output "NOTIFY called with Update complete, launching browser"
}

