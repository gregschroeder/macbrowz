load '../helpers/setup'

setup() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local launchdir="$( cd "${curdir}/../../share/macbrowz/launcher" && pwd )"

  . "${launchdir}/launcher_script.sh"
}

@test "launcher: scriptOutOfDate false" {
  launcher_script="${BATS_TEST_TMPDIR}/primary.sh"
  myself="${BATS_TEST_TMPDIR}/myself.sh"

  touch -t 201103111700 "${launcher_script}"
  chmod +x "${launcher_script}"
  touch "${myself}"
  chmod +x "${myself}"

  run scriptOutOfDate
  assert_failure
}

@test "launcher: scriptOutOfDate true" {
  launcher_script="${BATS_TEST_TMPDIR}/primary.sh"
  myself="${BATS_TEST_TMPDIR}/myself.sh"

  touch -t 201103111700 "${myself}"
  chmod +x "${myself}"
  touch "${launcher_script}"
  chmod +x "${launcher_script}"

  run scriptOutOfDate
  assert_success
}
