load '../helpers/setup'

setup() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local launchdir="$( cd "${curdir}/../../share/macbrowz/launcher" && pwd )"

  . "${launchdir}/launcher_script.sh"
  app_name=MyApp
}

# fake pgrep
pgrep() {
  [ -n "${is_running}" ]
}

@test "launcher: isRunning false" {
  is_running=true
  run isRunning
  assert_success
}

@test "launcher: isRunning true" {
  is_running=
  run isRunning
  assert_failure
}
