load '../helpers/setup'
load '../helpers/stubs'

setup() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local launchdir="$( cd "${curdir}/../../share/macbrowz/launcher" && pwd )"

  createStubs \
    init \
    refreshApp \
    launchBrowser

  . "${launchdir}/launcher_script.sh"
  loadStubs
}

@test "launcher: launcher_script main" {
  run main
  assert_success

  local expected="init"
  expected="${expected}; refreshApp"
  expected="${expected}; launchBrowser"
  assert_equal "$(commandsRun)" "${expected}"
}
