load '../helpers/setup'
load '../helpers/macbrowz_func'


#######################################
#
# missing and general invalid arguments
#
#######################################

@test "cli: unknown flag" {
  callFunc parseCommandLine --fail
  assert_failure
  assert_output -e '^unknown flag: --fail'
  assert_output -p 'usage:'
}

@test "cli: missing flag value" {
  callFunc parseCommandLine -b
  assert_failure
  assert_output -e '^missing value for -b'
  assert_output -p 'usage:'
}

@test "cli: missing flag value with positional arg" {
  callFunc parseCommandLine -b app_name
  assert_failure
  assert_output -e '^missing app name'
  assert_output -p 'usage:'
}

@test "cli: missing flag value with another flag" {
  callFunc parseCommandLine -b -e new_name app_name
  assert_failure
  assert_output -e '^missing value for -b'
  assert_output -p 'usage:'
}

@test "cli: missing required argument" {
  callFunc parseCommandLine
  assert_failure
  assert_output -e '^missing app name'
  assert_output -p 'usage:'
}

@test "cli: invalid config_path" {
  local invalid_path="/path/does/not/exist.conf"
  callFunc parseCommandLine -c "${invalid_path}" app_name
  assert_failure
  assert_output -e "^unable to load config from ${invalid_path}"
  assert_output -p 'usage:'
}


#######################################
#
# basic usage
#
#######################################

# shellcheck disable=SC2154
@test "cli: app name and defaults" {
  local app=MyAppName
  loadScript
  parseCommandLine "${app}"
  assert_success
  assert_equal "${app_name}" "${app}"
  assert_equal "${bundle_name}" "${app}"
  assert_equal "${browser_short_name}" 'Brave'
  assert_equal "${browser_engine_name}" 'Brave Browser'
  assert_equal "${browser_framework}" 'Brave Browser Framework.framework'
  assert_equal "${browser_engine_path}" "/Applications/${browser_engine_name}.app"
  assert_equal "${browser_framework_path}" "${browser_engine_path}/Contents/Frameworks/${browser_framework}"
  assert_equal "${app_support_path}" "${BATS_TEST_TMPDIR}/Library/Application Support/Macbrowz/Apps/${app}"
  assert_equal "${profile_path}" "${app_support_path}/Profile"
  assert_equal "${ssb_path}" "${BATS_TEST_TMPDIR}/Applications/Macbrowz/Apps"
  assert_equal "${outer_app_path}" "${BATS_TEST_TMPDIR}/Applications/Macbrowz/Apps/${app}.app"
  assert_equal "${inner_app_path}" "${outer_app_path}/Contents/Resources/${app}.app"
  assert_equal "${refresh_script}" "$(macbrowzScriptPath)"
}

@test "cli: minimal usage" {
  local app=MyAppName
  loadScript
  parseCommandLine "${app}"
  assert_success
  assert_equal "${app_name}" "${app}"
  assert_equal "${bundle_name}" "${app}"
  assert_equal "${browser_short_name}" 'Brave'
  assert_equal "${browser_engine_name}" 'Brave Browser'
  assert_equal "${browser_framework}" 'Brave Browser Framework.framework'
  assert_equal "${browser_engine_path}" "/Applications/${browser_engine_name}.app"
  assert_equal "${browser_framework_path}" "${browser_engine_path}/Contents/Frameworks/${browser_framework}"
  assert_equal "${app_support_path}" "${BATS_TEST_TMPDIR}/Library/Application Support/Macbrowz/Apps/${app}"
  assert_equal "${profile_path}" "${app_support_path}/Profile"
  assert_equal "${ssb_path}" "${BATS_TEST_TMPDIR}/Applications/Macbrowz/Apps"
  assert_equal "${outer_app_path}" "${BATS_TEST_TMPDIR}/Applications/Macbrowz/Apps/${app}.app"
  assert_equal "${inner_app_path}" "${outer_app_path}/Contents/Resources/${app}.app"
  assert_equal "${refresh_script}" "$(macbrowzScriptPath)"
}


#######################################
#
# -b | --browser
#
#######################################

@test "cli: set browser to Brave" {
  local flag
  local name

  loadScript
  for flag in -b --browser; do
    for name in brave Brave; do
      browser_short_name=
      browser_engine_name=
      browser_framework=
      browser_engine_path=
      browser_framework_path=

      parseCommandLine "${flag}" "${name}" app_name
      assert_success
      assert_equal "${browser_short_name}" 'Brave'
      assert_equal "${browser_engine_name}" 'Brave Browser'
      assert_equal "${browser_framework}" 'Brave Browser Framework.framework'
      assert_equal "${browser_engine_path}" "/Applications/${browser_engine_name}.app"
      assert_equal "${browser_framework_path}" "${browser_engine_path}/Contents/Frameworks/${browser_framework}"
    done
  done
}

@test "cli: set browser to Chrome" {
  local flag
  local name

  loadScript
  for flag in -b --browser; do
    for name in chrome Chrome; do
      browser_short_name=
      browser_engine_name=
      browser_framework=
      browser_engine_path=
      browser_framework_path=

      parseCommandLine "${flag}" "${name}" app_name
      assert_success
      assert_equal "${browser_short_name}" 'Chrome'
      assert_equal "${browser_engine_name}" 'Google Chrome'
      assert_equal "${browser_framework}" 'Google Chrome Framework.framework'
      assert_equal "${browser_engine_path}" "/Applications/${browser_engine_name}.app"
      assert_equal "${browser_framework_path}" "${browser_engine_path}/Contents/Frameworks/${browser_framework}"
    done
  done
}

@test "cli: set browser to invalid" {
  local flag

  for flag in -b --browser; do
    callFunc parseCommandLine "${flag}" IE app_name
    assert_output -e "^unknown browser: IE"
    assert_output -p 'usage:'
  done
}

#######################################
#
# -i | --icon
#
#######################################

@test "cli: set icon" {
  local flag

  loadScript

  local icon_path="${BATS_TEST_TMPDIR}/myicon.png"

  for flag in -i --icon; do
    parseCommandLine "${flag}" "${icon_path}" app_name
    assert_success
    assert_equal "${icon_uri}" "${icon_path}"
  done
}

#######################################
#
# -T | --template
#
#######################################

@test "cli: template with edit should fail" {
  local flag

  loadScript

  local template="basetmpl"
  local template_path="${ssb_path}/app_name.app"
  mkdir -p "${template_path}"

  for flag in -T --template; do
    callFunc parseCommandLine "${flag}" "${template}" -e "${app_name}_new" app_name
    assert_failure
    assert_output -p "templates are only for new app creation"
  done
}

@test "cli: template must exist" {
  local flag

  loadScript

  local template="basetmpl"

  for flag in -T --template; do
    callFunc parseCommandLine "${flag}" "${template}" app_name
    assert_failure
    assert_output -p "unable to find template app"
  done
}

@test "cli: template support must exist" {
  local flag

  loadScript

  local template="basetmpl"
  local template_path="${ssb_path}/${template}.zip"
  mkdir -p "${ssb_path}"
  touch "${template_path}"

  for flag in -T --template; do
    callFunc parseCommandLine "${flag}" "${template}" app_name
    assert_failure
    assert_output -p "unable to find template app"
  done
}

@test "cli: template should use edit mode" {
  local flag

  loadScript

  local template="basetmpl"
  local template_path="${ssb_path}/${template}.zip"
  mkdir -p "${ssb_path}"
  touch "${template_path}"
  local template_support_path="${app_support_root_path}/${template}.zip"
  mkdir -p "${app_support_root_path}"
  touch "${template_support_path}"

  parseCommandLine -T "${template}" app_name
  assert_success
  assert_equal "${edit_mode}" true
  assert_equal "${new_app_name}" app_name
  assert_equal "${app_name}" "${template}"
}

#######################################
#
# --zipTemplate
#
#######################################

@test "cli: zipTemplate requires template name" {
  local flag

  loadScript

  run parseCommandLine --zipTemplate app_name
  assert_failure
  assert_output -p "zip or unzip template requires template name"
}

#######################################
#
# --unzipTemplate
#
#######################################

@test "cli: unzipTemplate requires template name" {
  local flag

  loadScript

  run parseCommandLine --unzipTemplate app_name
  assert_failure
  assert_output -p "zip or unzip template requires template name"
}

#######################################
#
# -c | --config
#
#######################################

@test "cli: set config path to missing file" {
  local flag

  loadScript

  local config_path="${BATS_TEST_TMPDIR}/config.conf"

  for flag in -c --config; do
    run parseCommandLine "${flag}" "${config_path}" app_name
    assert_failure
    assert_output -p "unable to load config"
  done
}

@test "cli: set config path to file" {
  local flag

  loadScript

  local config_path="${BATS_TEST_TMPDIR}/config.conf"
  echo "testvar=\"testvalue\"" > "${config_path}"

  for flag in -c --config; do
    parseCommandLine "${flag}" "${config_path}" app_name
    assert_success
    assert_equal "${testvar}" "testvalue"
  done
}

#######################################
#
# -u | --updateBrowser
#
#######################################

@test "cli: update browser" {
  local flag

  loadScript
  mkdir -p "${ssb_path}/app_name.app"

  for flag in -u --updateBrowser; do
    update_browser=
    parseCommandLine "${flag}" app_name
    assert_success
    assert_equal "${update_browser}" "true"
  done
}

#######################################
#
# -U | --update
#
#######################################

@test "cli: update ssb" {
  local flag

  loadScript
  mkdir -p "${ssb_path}/app_name.app"

  for flag in -U --update; do
    update_mode=
    parseCommandLine "${flag}" app_name
    assert_success
    assert_equal "${update_mode}" "true"
  done
}

#######################################
#
# -l | --launch
#
#######################################

@test "cli: launch SSB" {
  local flag

  loadScript

  for flag in -l --launch; do
    launch_ssb=
    parseCommandLine "${flag}" app_name
    assert_success
    assert_equal "${launch_ssb}" "true"
  done
}

#######################################
#
# -v | --verbose
#
#######################################

@test "cli: verbose, tracing level from unset to INFO" {
  local flag

  loadScript

  for flag in -v --verbose; do
    TRACE_LEVEL=
    parseCommandLine "${flag}" app_name
    assert_success
    assert_equal "${TRACE_LEVEL}" 3
  done
}

@test "cli: verbose, tracing level from FATAL to INFO" {
  local flag

  loadScript

  for flag in -v --verbose; do
    TRACE_LEVEL=1
    parseCommandLine "${flag}" app_name
    assert_success
    assert_equal "${TRACE_LEVEL}" 3
  done
}

@test "cli: verbose, tracing level from set to INFO to DEBUG" {
  local flag

  loadScript

  for flag in -v --verbose; do
    TRACE_LEVEL=1
    parseCommandLine "${flag}" app_name
    assert_success
    assert_equal "${TRACE_LEVEL}" 3
  done
}

@test "cli: verbose, next tracing level from unset" {
  loadScript

  TRACE_LEVEL=
  parseCommandLine -v -v app_name
  assert_success
  assert_equal "${TRACE_LEVEL}" 4
}

@test "cli: verbose, next tracing level with vv from unset" {
  loadScript

  TRACE_LEVEL=
  parseCommandLine -vv app_name
  assert_success
  assert_equal "${TRACE_LEVEL}" 4
}

@test "cli: verbose, highest tracing level from unset" {
  loadScript

  TRACE_LEVEL=
  parseCommandLine -v -v -v app_name
  assert_success
  assert_equal "${TRACE_LEVEL}" 5
}

@test "cli: verbose, highest tracing level with vvv" {
  loadScript

  TRACE_LEVEL=
  parseCommandLine -vvv app_name
  assert_success
  assert_equal "${TRACE_LEVEL}" 5
}

#######################################
#
# --remove
#
#######################################

@test "cli: remove SSB" {
  loadScript

  mkdir -p "${ssb_path}/app_name.app"
  remove_ssb=

  parseCommandLine --remove app_name
  assert_success
  assert_equal "${remove_ssb}" "true"
}

#######################################
#
# --list
#
#######################################

@test "cli: list SSBs" {
  local flag

  loadScript

  for flag in -L --list; do
    list_apps=
    parseCommandLine ${flag}
    assert_success
    assert_equal "${list_apps}" "true"
  done
}

#######################################
#
# --window
#
#######################################

@test "cli: window style" {
  local flag

  loadScript

  for flag in -w --window; do
    window_style=
    parseCommandLine ${flag} app_name
    assert_success
    assert_equal "${window_style}" "true"
  done
}

@test "cli: window style with multiple URLs" {
  loadScript

  run parseCommandLine -w app_name https://google.com https://yahoo.com
  assert_failure
  assert_output "window style implies single URL"
}
