load '../helpers/setup'
load '../helpers/macbrowz_func'

setup() {
  loadScript

  app_name=MyApp
  is_running=
}

# fake pgrep
pgrep() {
  [ -n "${is_running}" ]
}

@test "assertAppIsNotRunning is not running" {
  run assertAppIsNotRunning
  assert_success
}

@test "assertAppIsNotRunning is running" {
  is_running=true

  run assertAppIsNotRunning
  assert_failure
  assert_output "App ${app_name} is running"
}
