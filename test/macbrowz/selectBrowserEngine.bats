load '../helpers/setup'
load '../helpers/macbrowz_func'

# most of selectBrowserEngine is tested in parseCommandLine

@test "selectBrowserEngine to Chrome with prefix" {
  loadScript

  # save current values
  local prev_browser_short_name="${browser_short_name}"
  local prev_browser_engine_name="${browser_engine_name}"
  local prev_browser_framework="${browser_framework}"
  local prev_browser_engine_path="${browser_engine_path}"
  local prev_browser_framework_path="${browser_framework_path}"

  # set browser values into prefixed variables
  selectBrowserEngine -p orig_ Chrome
  assert_success

  # see that our prefixed variables were populated
  assert_equal "${orig_browser_short_name}" 'Chrome'
  assert_equal "${orig_browser_engine_name}" 'Google Chrome'
  assert_equal "${orig_browser_framework}" 'Google Chrome Framework.framework'
  assert_equal "${orig_browser_engine_path}" "/Applications/${orig_browser_engine_name}.app"
  assert_equal "${orig_browser_framework_path}" "${orig_browser_engine_path}/Contents/Frameworks/${orig_browser_framework}"

  # see that unprefixed remain unchanged
  assert_equal "${prev_browser_short_name}" "${browser_short_name}"
  assert_equal "${prev_browser_engine_name}" "${browser_engine_name}"
  assert_equal "${prev_browser_framework}" "${browser_framework}"
  assert_equal "${prev_browser_engine_path}" "${browser_engine_path}"
  assert_equal "${prev_browser_framework_path}" "${browser_framework_path}"
}
