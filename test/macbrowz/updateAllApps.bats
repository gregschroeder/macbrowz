load '../helpers/setup'
load '../helpers/macbrowz_func'
load '../helpers/stubs'

setup() {
  createStubs \
    main

  cat << 'EOF' >> "${stubs_file}"
function listApps() {
  cat << EOLIST
/Applications/Macbrowz/Apps/app1
/Applications/Macbrowz/Apps/app2
/Applications/Macbrowz/Apps/app3
EOLIST
}
EOF

  loadScript
  loadStubs
}

#######################################
#
# update all apps
#
#######################################

@test "updateAllApps" {
  run updateAllApps
  assert_success

  assert_line --index 0 'Updating app1'
  assert_line --index 1 'CMD: main --update app1'
  assert_line --index 2 'Updating app2'
  assert_line --index 3 'CMD: main --update app2'
  assert_line --index 4 'Updating app3'
  assert_line --index 5 'CMD: main --update app3'
}
