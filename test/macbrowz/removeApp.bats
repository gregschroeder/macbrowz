load '../helpers/setup'
load '../helpers/macbrowz_func'
load '../helpers/stubs'

setup() {
  createStubs \
    assertAppExists \
    assertAppIsNotRunning

  loadScript
  loadStubs

  # set variables and create pseudo locations
  outer_app_path="${BATS_TEST_TMPDIR}/removeApp/outer"
  mkdir -p "${outer_app_path}"
  outer_app_file="${outer_app_path}/test"
  touch "${outer_app_file}"

  app_support_path="${BATS_TEST_TMPDIR}/removeApp/appSupportPath"
  mkdir -p "${app_support_path}"
  app_support_file="${app_support_path}/test"
  touch "${app_support_file}"
}

#######################################
#
# remove SSB
#
#######################################

@test "removeApp not confirmed" {
  run removeApp <<< no
  assert_failure
  assert_output -e 'Are you sure'
  assert_output -e 'aborting'

  local expected="assertAppExists"
  expected="${expected}; assertAppIsNotRunning"
  assert_equal "$(commandsRun)" "${expected}"

  # no removal
  assert_file_exist "${outer_app_file}"
  assert_file_exist "${app_support_file}"
}

@test "removeApp confirmed" {
  run removeApp <<< yes
  assert_success
  assert_output -e 'Are you sure'

  local expected="assertAppExists"
  expected="${expected}; assertAppIsNotRunning"
  assert_equal "$(commandsRun)" "${expected}"

  # removal
  assert_file_not_exist "${outer_app_file}"
  assert_file_not_exist "${app_support_file}"
}

@test "removeApp forced" {
  force=true
  run removeApp
  assert_success

  local expected="assertAppExists"
  expected="${expected}; assertAppIsNotRunning"
  assert_equal "$(commandsRun)" "${expected}"

  # removal
  assert_file_not_exist "${outer_app_file}"
  assert_file_not_exist "${app_support_file}"
}


@test "removeApp not found" {
  app_name="missingapp"
  outer_app_path="${BATS_TEST_TMPDIR}/removeApp/no_outer"
  app_support_path="${BATS_TEST_TMPDIR}/removeApp/no_appSupportPath"

  run removeApp <<< no
  assert_failure

  local expected="assertAppExists"
  expected="${expected}; assertAppIsNotRunning"
  assert_equal "$(commandsRun)" "${expected}"
}
