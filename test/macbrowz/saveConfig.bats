load '../helpers/setup'
load '../helpers/macbrowz_func'
load '../helpers/stubs'

setup() {
  createStubs \
    writeConfig
}

#######################################
#
# save config
#
#######################################

@test "saveConfig without local config" {
  loadScript
  loadStubs

  # set variables
  outer_app_path="${BATS_TEST_TMPDIR}/outer"

  run saveConfig
  assert_success

  local expected="writeConfig ${outer_app_path}/Contents/Resources/macbrowz.conf"
  assert_equal "$(commandsRun)" "${expected}"
}

@test "saveConfig with local config" {
  loadScript
  loadStubs

  # set variables
  outer_app_path="${BATS_TEST_TMPDIR}/outer"
  config_path="${BATS_TEST_TMPDIR}/config.conf"

  run saveConfig
  assert_success

  local expected="writeConfig ${config_path}"
  expected="${expected}; writeConfig ${outer_app_path}/Contents/Resources/macbrowz.conf"
  assert_equal "$(commandsRun)" "${expected}"
}
