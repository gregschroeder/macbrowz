load '../helpers/setup'
load '../helpers/macbrowz_func'

setup() {
  loadScript

  # override script variables
  app_name="TestApp"
  bundle_name="TestBund"
  outer_app_path="${BATS_TEST_TMPDIR}/outer"
  inner_app_path="${outer_app_path}/Contents/Resources/${app_name}.app"
}

setupFakeBrowser() {
  # point global variable to fake root
  browser_engine_name="FakeBrowser"
  browser_engine_path="${BATS_TEST_TMPDIR}/browser/Applications/${browser_engine_name}.app"
  browser_framework="${browser_engine_name} Framework.framework"

  # start contents tree
  local fake_contents_dir="${browser_engine_path}/Contents"
  mkdir -p "${fake_contents_dir}"
  cp -f "${BATS_TEST_DIRNAME}/../fixtures/Brave/Contents/Info.plist" "${fake_contents_dir}/"
  fake_pkginfo="${fake_contents_dir}/PkgInfo"
  echo "fake_pkginfo" > "${fake_pkginfo}"

  # add dummy signature
  local fake_codesig_dir="${browser_engine_path}/Contents/_CodeSignature"
  mkdir -p "${fake_codesig_dir}"
  fake_code_resource="${fake_codesig_dir}/CodeResources"
  echo 'fake_code_resource' > "${fake_code_resource}"

  # add dummy MacOS
  local fake_macos_dir="${browser_engine_path}/Contents/MacOS"
  mkdir -p "${fake_macos_dir}"
  fake_browser="${fake_macos_dir}/${browser_engine_name}"
  echo "fake_browser" > "${fake_browser}"

  # add dummy Frameworks
  local fake_framework_dir="${browser_engine_path}/Contents/Frameworks/${browser_framework}"
  mkdir -p "${fake_framework_dir}"
  fake_framework="${fake_framework_dir}/fakeframework"
  echo "fake_framework" > "${fake_framework}"

  # add dummy resources
  local fake_locale_dir="${browser_engine_path}/Contents/Resources/en.lproj"
  mkdir -p "${fake_locale_dir}"
  fake_resource="${fake_locale_dir}/InfoPList.strings"
  echo "# fake" > "${fake_resource}"

  # fake icon
  icns_file="${BATS_TEST_TMPDIR}/app.icns"
  touch "${icns_file}"
}

@test "createInnerApp aborts if inner_app_path unset" {
  unset inner_app_path

  run createInnerApp
  assert_failure
  assert_output -p "undefined inner_app_path"
}

@test "createInnerApp sets up browser engine wrapper" {
  setupFakeBrowser

  # create inner app engine wrapper
  run createInnerApp
  assert_success

  # make sure wrapper subdirectories exist as directories
  assert [ -d "${inner_app_path}/Contents" ]
  assert [ -d "${inner_app_path}/Contents/MacOS" ]
  assert [ -d "${inner_app_path}/Contents/Frameworks" ]
  assert [ -d "${inner_app_path}/Contents/Resources" ]

  # check symlinks to browser engine bits
  assert [ -L "${inner_app_path}/Contents/PkgInfo" ]
  assert [ "${inner_app_path}/Contents/PkgInfo" -ef "${fake_pkginfo}" ]

  assert [ -L "${inner_app_path}/Contents/_CodeSignature" ]
  assert [ "${inner_app_path}/Contents/_CodeSignature/CodeResources" -ef "${fake_code_resource}" ]

  local fake_framework_name="$(basename "${fake_framework}")"
  assert [ -L "${inner_app_path}/Contents/Frameworks/${browser_framework}" ]
  assert [ "${inner_app_path}/Contents/Frameworks/${browser_framework}/${fake_framework_name}" -ef "${fake_framework}" ]

  # check that resources are directory, not symlinked, with copies
  assert [ -d "${inner_app_path}/Contents/Resources" ]
  assert [ ! "${inner_app_path}/Contents/Resources/en.lproj/InfoPList.strings" -ef "${fake_resource}" ]
  assert cmp "${inner_app_path}/Contents/Resources/en.lproj/InfoPList.strings" "${fake_resource}"
  local icon_file_path="${inner_app_path}/Contents/Resources/app.icns"
  assert_file_exist "${icon_file_path}"

  # make sure plist file is there
  local plist="${inner_app_path}/Contents/Info.plist"
  assert_file_exist "${plist}"

  # and that its properties got updated
  assert_equal "$( getPlistProp "${plist}" CFBundleIconFile )" "app.icns"
  assert_equal "$( getPlistProp "${plist}" CFBundleName )" "${bundle_name}"
  assert_equal "$( getPlistProp "${plist}" CFBundleDisplayName )" "${app_name}"

  # and unwanted properties removed
  assert_equal "$( getPlistProp "${plist}" CFBundleURLTypes )" ""

  # won't quarantine in test tmp folder, so can't really test,
  # could only verify that xattr() is called, which we know
}
