load '../helpers/setup'
load '../helpers/macbrowz_func'
load '../helpers/stubs'

setup() {
  createStubs

  cat << 'EOF' >> "${stubs_file}"
function mdfind() {
  cat << EOLIST
"$@"
app1
app2
app3
EOLIST
}
EOF

  loadScript
  loadStubs
}

#######################################
#
# list apps
#
#######################################

@test "listApps" {
  run listApps
  assert_success
  assert_line --index 0 -e 'kMDItemCFBundleIdentifier.*macbrowz'
  assert_line --index 1 'app1'
  assert_line --index 2 'app2'
  assert_line --index 3 'app3'
}
