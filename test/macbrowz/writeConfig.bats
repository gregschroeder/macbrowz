load '../helpers/setup'
load '../helpers/macbrowz_func'

checkVariable() {
  local file="$1"; shift
  local var="$1"; shift
  local val="$1"; shift
  local extra="$@"

  if [ -n "${extra}" ]; then
    local separator='" "'
    local ary=( "${val}" ${extra[@]} )
    local arystr="$( printf "${separator}%s" "${ary[@]}" )"
    arystr="${arystr:${#separator}}"
    local pattern="^${var}=\\( \"${arystr}\" \\)"
    grep -q "${pattern}" "${file}"
  else
    grep -q "^${var}=\"${val}\"" "${file}"
  fi
}

@test "writeConfig to file" {
  local config_file="${BATS_TEST_TMPDIR}/config.txt"
  assert_file_not_exist "${config_file}"

  local app="TestApp"
  local sites=( "https://mysite.com" "https://yoursite.com" )
  loadScript
  parseCommandLine -w "${app}" "${sites[0]}"

  writeConfig "${config_file}"
  assert_success
  assert_file_exist "${config_file}"

  assert checkVariable "${config_file}" MACBROWZ_VERSION "..*"
  assert checkVariable "${config_file}" app_name "${app}"
  assert checkVariable "${config_file}" app_support_path "${BATS_TEST_TMPDIR}/Library/Application Support/Macbrowz/Apps/${app}"
  assert checkVariable "${config_file}" browser_engine_name "Brave Browser"
  assert checkVariable "${config_file}" browser_engine_path "/Applications/Brave Browser.app"
  assert checkVariable "${config_file}" browser_framework "Brave Browser Framework.framework"
  assert checkVariable "${config_file}" browser_framework "Brave Browser Framework.framework"
  assert checkVariable "${config_file}" browser_short_name "Brave"
  assert checkVariable "${config_file}" bundle_name "${app}"
  assert checkVariable "${config_file}" icon_uri ""
  assert checkVariable "${config_file}" last_modified ".*"
  assert checkVariable "${config_file}" last_modified_sec "[0-9][0-9]*"
  assert checkVariable "${config_file}" profile_path "${BATS_TEST_TMPDIR}/Library/Application Support/Macbrowz/Apps/${app}/Profile"
  assert checkVariable "${config_file}" refresh_script ".*"
  assert checkVariable "${config_file}" script_lib_path ".*/share/macbrowz/lib"
  assert checkVariable "${config_file}" ssb_path "${BATS_TEST_TMPDIR}/Applications/Macbrowz/Apps"
  assert checkVariable "${config_file}" start_urls "${sites[0]}"
  assert checkVariable "${config_file}" window_style "true"
}
