load '../helpers/setup'
load '../helpers/macbrowz_func'

setup() {
  loadScript
}

ditto() {
  echo "ditto called $*"
}

#######################################
#
# zip template app
#
#######################################

@test "zipTemplateApp" {
  template_name=mytemplate
  mkdir -p "${ssb_path}/${template_name}.app"
  mkdir -p "${app_support_root_path}/${template_name}"

  run zipTemplateApp
  assert_success
  assert_line --index 0 'ditto called -c -k --sequesterRsrc --keepParent mytemplate.app mytemplate.zip'
  assert_line --index 1 'ditto called -c -k --sequesterRsrc --keepParent mytemplate mytemplate.zip'
}
