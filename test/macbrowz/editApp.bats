load '../helpers/setup'
load '../helpers/macbrowz_func'
load '../helpers/stubs'

setup() {
  createStubs \
    assertAppExists \
    assertAppIsNotRunning \
    configLoad \
    generateIcns \
    createOuterApp \
    createInnerApp \
    saveConfig \
    unzip
}

#######################################
#
# edit app
#
#######################################

@test "editApp, no changes" {
  loadScript
  loadStubs

  # set variables
  app_name=MyApp
  new_app_name=MyApp
  ssb_path="${BATS_TEST_TMPDIR}/apps"
  outer_app_path="${ssb_path}/${app_name}.app"
  config_file="${outer_app_path}/Contents/Resources/macbrowz.conf"
  inner_app_path="${outer_app_path}/inner/${app_name}.app"
  orig_app_support_path="${BATS_TEST_TMPDIR}/appsupport/${app_name}"
  app_support_path="${orig_app_support_path}"
  orig_profile_path="${app_support_path}/Profile"
  profile_path="${orig_profile_path}"
  orig_icon_uri="${BATS_TEST_TMPDIR}/icon.png"

  mkdir -p "$(dirname "${config_file}")"
  touch "${config_file}"
  touch "${outer_app_path}/Contents/Resources/app.icns"

  run editApp
  assert_success

  local expected="assertAppExists"
  expected="${expected}; assertAppIsNotRunning"
  expected="${expected}; configLoad -p orig_ ${config_file}"
  expected="${expected}; createOuterApp"
  expected="${expected}; createInnerApp"
  expected="${expected}; saveConfig"
  assert_equal "$(commandsRun)" "${expected}"
}

# shellcheck disable=SC2154
@test "editApp, new icon" {
  loadScript
  loadStubs

  # set variables
  app_name=MyApp
  new_app_name=MyApp
  ssb_path="${BATS_TEST_TMPDIR}/apps"
  outer_app_path="${ssb_path}/${app_name}.app"
  config_file="${outer_app_path}/Contents/Resources/macbrowz.conf"
  inner_app_path="${outer_app_path}/inner/${app_name}.app"
  app_support_path="${BATS_TEST_TMPDIR}/appsupport/${app_name}"
  orig_profile_path="${app_support_path}/Profile"
  profile_path="${orig_profile_path}"
  orig_icon_uri="${BATS_TEST_TMPDIR}/orig_icon.png"
  icon_uri="${BATS_TEST_TMPDIR}/icon.png"
  icns_file="${mb_tmpdir}/app.icns"

  mkdir -p "$(dirname "${config_file}")"
  touch "${config_file}"

  run editApp
  assert_success

  local expected="assertAppExists"
  expected="${expected}; assertAppIsNotRunning"
  expected="${expected}; configLoad -p orig_ ${config_file}"
  expected="${expected}; generateIcns ${icon_uri} ${icns_file}"
  expected="${expected}; createOuterApp"
  expected="${expected}; createInnerApp"
  expected="${expected}; saveConfig"
  assert_equal "$(commandsRun)" "${expected}"
}

# shellcheck disable=SC2154
@test "editApp, new app name" {
  loadScript
  loadStubs

  # set variables
  app_name=MyApp
  new_app_name=MyNewApp
  ssb_path="${BATS_TEST_TMPDIR}/apps"
  outer_app_path="${ssb_path}/${app_name}.app"
  config_file="${outer_app_path}/Contents/Resources/macbrowz.conf"
  inner_app_path="${outer_app_path}/inner/${app_name}.app"
  app_support_root_path="${BATS_TEST_TMPDIR}/appsupport"
  orig_app_support_path="${app_support_root_path}/${app_name}"
  app_support_path="${orig_app_support_path}"
  orig_profile_path="${app_support_path}/Profile"
  profile_path="${app_support_path}/Profile"
  orig_icon_uri="${BATS_TEST_TMPDIR}/icon.png"

  mkdir -p "$(dirname "${config_file}")"
  touch "${config_file}"

  # destination variables
  local updated_outer_app_path="${ssb_path}/${new_app_name}.app"
  local updated_app_support_path="${app_support_root_path}/${new_app_name}"
  local updated_profile_path="${updated_app_support_path}/Profile"

  # create orig locations so we can track that they move
  mkdir -p "${outer_app_path}"
  mkdir -p "${inner_app_path}"
  mkdir -p "${profile_path}"

  local orig_outer_app_file="${outer_app_path}/test"
  local orig_profile_file="${profile_path}/test"
  local updated_outer_app_file="${updated_outer_app_path}/test"
  local updated_profile_file="${updated_profile_path}/test"

  local checkVal=${RANDOM}
  echo "outer ${checkVal}" > "${orig_outer_app_file}"
  echo "profile ${checkVal}" > "${orig_profile_file}"
  touch "${outer_app_path}/Contents/Resources/app.icns"

  # make sure the new ones are not there
  assert_file_not_exist "${updated_outer_app_file}"
  assert_file_not_exist "${updated_profile_file}"

  run editApp
  assert_success

  local expected="assertAppExists"
  expected="${expected}; assertAppIsNotRunning"
  expected="${expected}; configLoad -p orig_ ${config_file}"
  expected="${expected}; createOuterApp"
  expected="${expected}; createInnerApp"
  expected="${expected}; saveConfig"
  assert_equal "$(commandsRun)" "${expected}"

  # verify that the old got moved to the new
  assert_file_not_exist "${inner_app_path}"
  assert_file_not_exist "${outer_app_file}"
  assert_file_not_exist "${outer_app_path}"
  assert_file_not_exist "${profile_file}"
  assert_file_not_exist "${profile_path}"

  assert_file_exist "${updated_outer_app_file}"
  assert_equal "$(cat "${updated_outer_app_file}")" "outer ${checkVal}"

  assert_file_exist "${updated_profile_file}"
  assert_equal "$(cat "${updated_profile_file}")" "profile ${checkVal}"
}

@test "editApp, preserve start_urls" {
  loadScript
  loadStubs

  # set variables
  app_name=MyApp
  new_app_name=MyApp
  ssb_path="${BATS_TEST_TMPDIR}/apps"
  outer_app_path="${ssb_path}/${app_name}.app"
  config_file="${outer_app_path}/Contents/Resources/macbrowz.conf"
  inner_app_path="${outer_app_path}/inner/${app_name}.app"
  orig_app_support_path="${BATS_TEST_TMPDIR}/appsupport/${app_name}"
  app_support_path="${orig_app_support_path}"
  orig_profile_path="${app_support_path}/Profile"
  profile_path="${orig_profile_path}"
  orig_icon_uri="${BATS_TEST_TMPDIR}/icon.png"
  start_urls=
  orig_start_urls=('https://google.com' 'https://yahoo.com')

  mkdir -p "$(dirname "${config_file}")"
  touch "${config_file}"
  touch "${outer_app_path}/Contents/Resources/app.icns"

  editApp
  assert_success

  assert_equal "${start_urls[*]}" "${orig_start_urls[*]}"
}

# shellcheck disable=SC2034
@test "editApp, start with template" {
  loadScript
  loadStubs

  # set variables
  app_name=MyApp
  new_app_name=MyApp
  ssb_path="${BATS_TEST_TMPDIR}/apps"
  outer_app_path="${ssb_path}/${app_name}.app"
  config_file="${outer_app_path}/Contents/Resources/macbrowz.conf"
  inner_app_path="${outer_app_path}/inner/${app_name}.app"
  orig_app_support_path="${BATS_TEST_TMPDIR}/appsupport/${app_name}"
  app_support_path="${orig_app_support_path}"
  orig_profile_path="${app_support_path}/Profile"
  profile_path="${orig_profile_path}"
  orig_icon_uri="${BATS_TEST_TMPDIR}/icon.png"

  template_name="basetmpl"
  local template_path="${ssb_path}/${template_name}.zip"
  mkdir -p "${ssb_path}"
  touch "${template_path}"
  # shellcheck disable=SC2031
  local template_support_path="${app_support_root_path}/${template_name}.zip"
  # shellcheck disable=SC2031
  mkdir -p "${app_support_root_path}"
  touch "${template_support_path}"

  mkdir -p "$(dirname "${config_file}")"
  touch "${config_file}"
  touch "${outer_app_path}/Contents/Resources/app.icns"

  run editApp
  assert_success

  local expected="unzip -qq ${template_name}.zip" # first, for app
  expected="${expected}; unzip -qq ${template_name}.zip" # second, for support dir
  expected="${expected}; assertAppExists"
  expected="${expected}; assertAppIsNotRunning"
  expected="${expected}; configLoad -p orig_ ${config_file}"
  expected="${expected}; createOuterApp"
  expected="${expected}; createInnerApp"
  expected="${expected}; saveConfig"
  assert_equal "$(commandsRun)" "${expected}"
}
