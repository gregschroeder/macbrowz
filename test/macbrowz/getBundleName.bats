load '../helpers/setup'
load '../helpers/macbrowz_func'

@test "getBundleName same if short" {
  callFunc getBundleName shortname
  assert_success
  assert_output -p 'shortname'
}

@test "getBundleName truncated with hash if long" {
  callFunc getBundleName aratherlongname
  assert_success
  assert_output -p 'aratherlonc2'
}
