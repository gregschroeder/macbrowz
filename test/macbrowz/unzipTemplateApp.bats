load '../helpers/setup'
load '../helpers/macbrowz_func'

setup() {
  loadScript
}

unzip() {
  echo "unzip called $*"
}

#######################################
#
# unzip template app
#
#######################################

@test "unzipTemplateApp" {
  template_name=mytemplate
  mkdir -p "${ssb_path}"
  touch "${ssb_path}/${template_name}.zip"
  mkdir -p "${app_support_root_path}"
  touch "${app_support_root_path}/${template_name}.zip"

  run unzipTemplateApp
  assert_success
  assert_line --index 0 'unzip called -qq mytemplate.zip'
  assert_line --index 1 'unzip called -qq mytemplate.zip'
}
