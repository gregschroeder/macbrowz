load '../helpers/setup'
load '../helpers/macbrowz_func'
load '../helpers/stubs'

setup() {
  createStubs \
    assertAppExists \
    generateIcns \
    createOuterApp \
    createInnerApp \
    saveConfig
}

#######################################
#
# create app
#
#######################################

@test "createApp" {
  loadScript
  loadStubs

  # set variables
  icon_uri="${BATS_TEST_TMPDIR}/icon.png"
  icns_file="${mb_tmpdir}/app.icns"

  run createApp
  assert_success

  local expected="assertAppExists not"
  expected="${expected}; generateIcns ${icon_uri} ${icns_file}"
  expected="${expected}; createOuterApp"
  expected="${expected}; createInnerApp"
  expected="${expected}; saveConfig"
  assert_equal "$(commandsRun)" "${expected}"
}
