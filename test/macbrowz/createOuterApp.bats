load '../helpers/setup'
load '../helpers/macbrowz_func'

setup() {
  loadScript

  # override script variables
  app_name="TestApp"
  bundle_name="TestBund"
  outer_app_path="${BATS_TEST_TMPDIR}/outer"

  # fake icon
  icns_file="${BATS_TEST_TMPDIR}/app.icns"
  touch "${icns_file}"
}

@test "createOuterApp clones launcher and updates properties" {
  assert_file_not_exist "${outer_app_path}/Contents"

  run createOuterApp
  assert_success

  # make sure plist file is there
  local plist="${outer_app_path}/Contents/Info.plist"
  assert_file_exist "${plist}"

  # and that its properties got updated
  assert_equal "$( getPlistProp "${plist}" CFBundleIconFile )" "app.icns"
  assert_equal "$( getPlistProp "${plist}" CFBundleName )" "${bundle_name}"
  assert_equal "$( getPlistProp "${plist}" CFBundleDisplayName )" "${app_name}"
  assert_equal "$( getPlistProp "${plist}" CFBundleIdentifier )" "${macbrowz_namespace}.${bundle_name}"

  # that the icon file is there
  local icon_file_path="${outer_app_path}/Contents/Resources/app.icns"
  assert_file_exist "${icon_file_path}"

  # won't quarantine in test tmp folder, so can't really test,
  # could only verify that xattr() is called, which we know
}
