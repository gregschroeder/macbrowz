load '../helpers/setup'
load '../helpers/macbrowz_func'
load '../helpers/stubs'

setup() {
  createStubs \
    parseCommandLine \
    generateIcns \
    createOuterApp \
    createInnerApp \
    writeConfig \
    saveConfig \
    editApp \
    createApp \
    removeApp \
    listApps \
    updateAllApps \
    printVersion \
    zipTemplateApp \
    unzipTemplateApp \
    open

  loadScript
  loadStubs
}

#######################################
#
# print version
#
#######################################

@test "main - print version" {
  # set variables, since parseCommandLine does not really run
  version_mode=true

  run main --version
  assert_success

  local expected="parseCommandLine --version"
  expected="${expected}; printVersion"
  assert_equal "$(commandsRun)" "${expected}"
}


#######################################
#
# remove SSB
#
#######################################

@test "main - remove ssb" {
  # set variables, since parseCommandLine does not really run
  remove_ssb=true

  run main --remove myapp <<< yes
  assert_success

  local expected="parseCommandLine --remove myapp"
  expected="${expected}; removeApp"
  assert_equal "$(commandsRun)" "${expected}"
}

#######################################
#
# update browser
#
#######################################

@test "main - update browser" {
  # set variables, since parseCommandLine does not really run
  outer_app_path="${BATS_TEST_TMPDIR}/outer"
  browser_engine_path="${BATS_TEST_TMPDIR}"
  icon_uri="${BATS_TEST_TMPDIR}/icon.png"
  icns_file="${mb_tmpdir}/app.icns"
  update_browser=true

  # create folders and names of considered files
  local config_dir="${outer_app_path}/Contents/Resources"
  mkdir -p "${config_dir}"
  local config_file="${config_dir}/macbrowz.conf"
  touch "${config_file}"

  local sig_dir="${browser_engine_path}/Contents"
  mkdir -p "${sig_dir}"
  local sig_file="${sig_dir}/_CodeSignature"
  touch "${sig_file}"

  run main -u myapp
  assert_success

  local expected="parseCommandLine -u myapp"
  expected="${expected}; createInnerApp"
  assert_equal "$(commandsRun)" "${expected}"
}

#######################################
#
# edit SSB
#
#######################################

@test "main - edit ssb" {
  # set variables, since parseCommandLine does not really run
  edit_mode=true

  run main --edit newapp app
  assert_success

  local expected="parseCommandLine --edit newapp app"
  expected="${expected}; editApp"
  assert_equal "$(commandsRun)" "${expected}"
}

#######################################
#
# update SSB
#
#######################################

@test "main - update ssb" {
  # set variables, since parseCommandLine does not really run
  update_mode=true

  run main --update app
  assert_success

  local expected="parseCommandLine --update app"
  expected="${expected}; editApp"
  assert_equal "$(commandsRun)" "${expected}"
}

#######################################
#
# zip template
#
#######################################

@test "main - zip template" {
  # set variables, since parseCommandLine does not really run
  zip_template=true
  template_name=mytemplate

  run main -t "${template_name}" --zipTemplate
  assert_success

  local expected="parseCommandLine -t ${template_name} --zipTemplate"
  expected="${expected}; zipTemplateApp"
  assert_equal "$(commandsRun)" "${expected}"
}

#######################################
#
# unzip template
#
#######################################

@test "main - unzip template" {
  # set variables, since parseCommandLine does not really run
  unzip_template=true
  template_name=mytemplate

  run main -t "${template_name}" --unzipTemplate
  assert_success

  local expected="parseCommandLine -t ${template_name} --unzipTemplate"
  expected="${expected}; unzipTemplateApp"
  expected="${expected}; open -a ${ssb_path}/${template_name}.app"
  assert_equal "$(commandsRun)" "${expected}"
}

#######################################
#
# list all SSBs
#
#######################################

@test "main - list all ssb apps" {
  # set variables, since parseCommandLine does not really run
  list_apps=true

  run main --list
  assert_success

  local expected="parseCommandLine --list"
  expected="${expected}; listApps"
  assert_equal "$(commandsRun)" "${expected}"
}

#######################################
#
# update all SSBs
#
#######################################

@test "main - update all ssb apps" {
  # set variables, since parseCommandLine does not really run
  update_all_mode=true

  run main --updateAll app
  assert_success

  local expected="parseCommandLine --updateAll app"
  expected="${expected}; updateAllApps"
  assert_equal "$(commandsRun)" "${expected}"
}

#######################################
#
# default creation
#
#######################################

@test "main - create ssb" {
  run main app
  assert_success

  local expected="parseCommandLine app"
  expected="${expected}; createApp"
  assert_equal "$(commandsRun)" "${expected}"
}

@test "main - create and launch" {
  # set variables, since parseCommandLine does not really run
  outer_app_path="${BATS_TEST_TMPDIR}/outer.app"
  launch_ssb=true

  run main -l app
  assert_success

  local expected="parseCommandLine -l app"
  expected="${expected}; createApp"
  expected="${expected}; open -a ${outer_app_path}"
  assert_equal "$(commandsRun)" "${expected}"
}
