load '../helpers/setup'
load '../helpers/macbrowz_func'

setup() {
  loadScript

  app_name=MyApp
  outer_app_path="${BATS_TEST_TMPDIR}/${app_name}.app"
  app_support_path="${BATS_TEST_TMPDIR}/support/${app_name}"
}

@test "assertAppExists exists" {
  mkdir -p "${outer_app_path}"
  run assertAppExists
  assert_success

  mkdir -p "${app_support_path}"
  run assertAppExists
  assert_success

  rm -rf "${outer_app_path}"
  run assertAppExists
  assert_success
}

@test "assertAppExists not exists" {
  run assertAppExists
  assert_failure
  assert_output "App ${app_name} not found"
}
