load '../helpers/setup'
load '../helpers/stubs'

setup() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local libdir="$( cd "${curdir}/../../share/macbrowz/lib" && pwd )"

  createStubs \
    osascript

  . "${libdir}/notify.sh"
  loadStubs
}


#######################################
#
# errorDialog
#
#######################################

@test "lib: errorDialog" {
  run errorDialog "my message" "the title"
  assert_success

  # pretty hard to really test functionality

  local expected="osascript"
  assert_equal "$(commandsRun)" "${expected}"
}


#######################################
#
# abort
#
#######################################

@test "lib: abort" {
  run abort "my message" "in parts"
  assert_failure
  assert_output "my message in parts"
}

#######################################
#
# tracing - trace_level_num
#
#######################################

@test "lib: trace_level_num INFO" {
  TRACE_LEVEL=INFO
  run trace_level_num
  assert_success
  assert_output "3"
}

@test "lib: trace_level_num 3" {
  TRACE_LEVEL=3
  run trace_level_num
  assert_success
  assert_output "3"
}

@test "lib: trace_level_num VERBOSE" {
  TRACE_LEVEL=VERBOSE
  run trace_level_num
  assert_success
  assert_output "5"
}

@test "lib: trace_level_num unset" {
  TRACE_LEVEL=
  run trace_level_num
  assert_success
  assert_output -1
}

#######################################
#
# tracing - trace_level_increment
#
#######################################

@test "lib: trace_level_increment INFO + 1" {
  TRACE_LEVEL=INFO
  trace_level_increment
  assert_success
  assert_equal "${TRACE_LEVEL}" 4
}

@test "lib: trace_level_increment 3 + 1" {
  TRACE_LEVEL=3
  trace_level_increment
  assert_success
  assert_equal "${TRACE_LEVEL}" 4
}

@test "lib: trace_level_increment INFO + 2" {
  TRACE_LEVEL=INFO
  trace_level_increment 2
  assert_success
  assert_equal "${TRACE_LEVEL}" 5
}

#######################################
#
# tracing - trace_level_info
#
#######################################

@test "lib: trace_level_info enabled - equal" {
  TRACE_LEVEL=INFO
  run trace_level_info
  assert_success
}

@test "lib: trace_level_info enabled - greater" {
  TRACE_LEVEL=DEBUG
  run trace_level_info
  assert_success
}

@test "lib: trace_level_info disabled - less" {
  TRACE_LEVEL=WARN
  run trace_level_info
  assert_failure
}

@test "lib: trace_level_info disabled - unset" {
  TRACE_LEVEL=
  run trace_level_info
  assert_failure
}

#######################################
#
# tracing - trace_info
#
#######################################

@test "lib: trace_info prints to stderr for INFO" {
  TRACE_LEVEL=INFO
  run trace_info "bingo"
  assert_success
  assert_output -p "bingo"
}

@test "lib: trace_info prints to stderr for DEBUG" {
  TRACE_LEVEL=DEBUG
  run trace_info "bingo"
  assert_success
  assert_output -p "bingo"
}

@test "lib: trace_info does not print to stderr for WARN" {
  TRACE_LEVEL=WARN
  run trace_info "bingo"
  assert_success
  assert_output ""
}

@test "lib: trace_info prints to file1, file2 for INFO" {
  local file1="${BATS_TEST_TMPDIR}/trace1.txt"
  local file2="${BATS_TEST_TMPDIR}/trace2.txt"
  #export TRACE_OUTPUT=( "${file1}" "${file2}" )
  TRACE_LEVEL=INFO

  TRACE_OUTPUT=("${file1}" "${file2}") run trace_info "bingo"
  assert_success
  assert_output ""

  local output1=$(cat "${file1}")
  [[ "${output1}" =~ 'bingo' ]] || fail "output mismatch in file1: ${output1}"

  local output2=$(cat "${file2}")
  [[ "${output2}" =~ 'bingo' ]] || fail "output mismatch in file2: ${output2}"
}

@test "lib: trace_info prints to file1, file2 for INFO with TRACE_OUTPUT array" {
  local file1="${BATS_TEST_TMPDIR}/trace1.txt"
  local file2="${BATS_TEST_TMPDIR}/trace2.txt"
  export TRACE_OUTPUT=( "${file1}" "${file2}" )
  TRACE_LEVEL=INFO

  run trace_info "bingo"
  assert_success
  assert_output ""

  local output1=$(cat "${file1}")
  [[ "${output1}" =~ 'bingo' ]] || fail "output mismatch in file1: ${output1}"

  local output2=$(cat "${file2}")
  [[ "${output2}" =~ 'bingo' ]] || fail "output mismatch in file2: ${output2}"
}
