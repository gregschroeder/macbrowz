load '../helpers/setup'

setup() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local libdir="$( cd "${curdir}/../../share/macbrowz/lib" && pwd )"

  . "${libdir}/util.sh"
}

#######################################
#
# absPath
#
#######################################

@test "lib: absPath from absolute file" {
  local abs_path="/already/abs/path/file"
  local expected="${abs_path}"

  local abs="$(absPath "${abs_path}")"
  assert_equal "${abs}" "${expected}"
}

@test "lib: absPath from absolute with relative bits" {
  local abs_path="/already/abs/down/../up/brother/../sister/file"
  local expected="/already/abs/up/sister/file"

  local abs="$(absPath "${abs_path}")"
  assert_equal "${abs}" "${expected}"
}

@test "lib: absPath from relative file" {
  local root_dir="${BATS_TEST_TMPDIR}/root"
  local my_dir="${root_dir}/foo"
  local dest_dir="${root_dir}/bar"

  mkdir -p "${my_dir}"
  mkdir -p "${dest_dir}"
  touch "${dest_dir}/file"

  local rel_path="../bar/file"
  local expected="$(echo "${dest_dir}/file" | sed 's~//~/~g')"

  cd "${my_dir}"
  local abs="$(absPath "${rel_path}")"
  assert_equal "${abs}" "${expected}"
}

@test "lib: absPath from relative dir" {
  local root_dir="${BATS_TEST_TMPDIR}/root"
  local my_dir="${root_dir}/foo"
  mkdir -p "${my_dir}"

  local rel_path="./foo"
  local expected="$(echo "${my_dir}" | sed 's~//~/~g')"

  cd "${root_dir}"
  local abs="$(absPath "${rel_path}")"
  assert_equal "${abs}" "${expected}"
}
