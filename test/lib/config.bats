load '../helpers/setup'

setup() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local libdir="$( cd "${curdir}/../../share/macbrowz/lib" && pwd )"

  . "${libdir}/config.sh"

  local fixtures_dir="$( cd "${curdir}/../../test/fixtures" && pwd )"
  config_fixture="${fixtures_dir}/config.conf"
}

#######################################
#
# configLoad
#
#######################################

@test "lib: configLoad file does not exist" {
  local config_file="notexists"

  run configLoad "${config_file}"
  assert_failure
}

@test "lib: configLoad file without prefix" {
  local config_file="${config_fixture}"

  assert_equal "${var_name}" ""
  assert_equal "${var_name_spaces}" ""
  assert_equal "${array_var}" ""

  configLoad "${config_file}"
  assert_success

  assert_equal "${var_name}" "var"
  assert_equal "${var_name_spaces}" "my var"
  assert_equal "${#array_var[@]}" 2
  assert_equal "${array_var[0]}" "val1"
  assert_equal "${array_var[1]}" "val2"
}

@test "lib: configLoad file with prefix" {
  local config_file="${config_fixture}"

  assert_equal "${var_name}" ""
  assert_equal "${var_name_spaces}" ""
  assert_equal "${array_var}" ""
  assert_equal "${pre_var_name}" ""
  assert_equal "${pre_var_name_spaces}" ""
  assert_equal "${pre_array_var}" ""

  configLoad -p pre_ "${config_file}"
  assert_success

  assert_equal "${var_name}" ""
  assert_equal "${var_name_spaces}" ""
  assert_equal "${array_var}" ""
  assert_equal "${pre_var_name}" "var"
  assert_equal "${pre_var_name_spaces}" "my var"
  assert_equal "${#pre_array_var[@]}" 2
  assert_equal "${pre_array_var[0]}" "val1"
  assert_equal "${pre_array_var[1]}" "val2"
}

#######################################
#
# configSave
#
#######################################

@test "lib: configSave file does not exist" {
  local config_file="${BATS_TEST_TMPDIR}/path/does/not/exist/config.txt"

  local var_name="var"
  local var_name_spaces="my var"
  local array_var=( "val1" "val2" )

  local vars=( array_var var_name var_name_spaces )
  run configSave "${config_file}" "${vars[@]}"
  assert_success

  assert_file_exist "${config_file}"
  assert cmp "${config_file}" "${config_fixture}"
}

@test "lib: configSave replace and add value" {
  local config_file="${BATS_TEST_TMPDIR}/config.txt"
  cp -f "${config_fixture}" "${config_file}"

  local var_name="https://translate.google.com/?sl=en&tl=de&text=erweiterung&op=translate"
  local new_var="added"

  local vars=( var_name new_var )
  run configSave "${config_file}" "${vars[@]}"
  assert_success

  local expected_file="${BATS_TEST_TMPDIR}/expected.txt"
  cat << EOF > "${expected_file}"
array_var=( "val1" "val2" )
var_name="https://translate.google.com/?sl=en&tl=de&text=erweiterung&op=translate"
var_name_spaces="my var"
new_var="added"
EOF
  assert cmp "${config_file}" "${expected_file}"
}

