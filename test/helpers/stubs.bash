# create a file with stub functions for the given commands
# source the file in variable ${stubs_file} to load
function createStubs() {
  local commands=( "$@" )

  stubs_file="${BATS_TEST_TMPDIR}/stubs.bash"
  cat << 'EOF' > "${stubs_file}"
logCommand() {
  echo "CMD: $@"
}
EOF

  # add stubs for called functions
  local cmd
  for cmd in "${commands[@]}"; do
    cat << EOF >> "${stubs_file}"
function ${cmd}() {
  logCommand "${cmd} \$@"
}
EOF
  done
}

function loadStubs() {
  . "${stubs_file}"
}

# parse stub log output to produce list of commands called
commandsRun() {
  sed -n -e 's/^CMD: //p' -e 's/  *$//' <<< "${output}" | \
    awk '{printf "%s; ", $0}' | \
    sed -e 's/ ;/;/g' -e 's/  *$//' -e 's/;$//'
}
