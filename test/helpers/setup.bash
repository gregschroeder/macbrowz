# load support libs (relative to higher level, not this file location)
module_root="$(dirname "${BATS_ROOT}")"
load "${module_root}/bats-support/load.bash"
load "${module_root}/bats-assert/load.bash"
load "${module_root}/bats-file/load.bash"
