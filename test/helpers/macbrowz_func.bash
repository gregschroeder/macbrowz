#!/bin/bash
# wrapper to load the macbrowz script and then execute the command line,
# allowing its functions to be called by tests

function macbrowzScriptPath() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local bindir="$( cd "${curdir}/../../bin" && pwd )"
  echo "${bindir}/macbrowz"
}

# load the script into our space
# NB: if script has side effects like calling abort() -> exit(), this can bork the test framework
function loadScript() {
  . "$(macbrowzScriptPath)"

  mb_tmpdir="${BATS_TEST_TMPDIR}"

  # change global directories to test temp
  ssb_path="${BATS_TEST_TMPDIR}/Applications/Macbrowz/Apps"
  app_support_root_path="${BATS_TEST_TMPDIR}/Library/Application Support/Macbrowz/Apps"
}

# load script and call function from it
# this uses a separate space by using 'run'
# so side effects are insulated, but one cannot read updated variable values
# for that case, load the script and directly execute the function
function callFunc() {
  loadScript
  run "$@"
}
