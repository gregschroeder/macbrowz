#!/bin/bash
# wrapper to load the macbrowz script and then execute the command line,
# allowing its functions to be called by tests

function releaseScriptPath() {
  local curdir="$( cd "$( dirname "${BATS_TEST_FILENAME}" )" >/dev/null 2>&1 && pwd )"
  local supportdir="$( cd "${curdir}/../../support" && pwd )"
  echo "${supportdir}/new_release.sh"
}

# load the script into our space
# NB: if script has side effects like calling abort() -> exit(), this can bork the test framework
function loadScript() {
  . "$(releaseScriptPath)"

  rel_tmpdir="${BATS_TEST_TMPDIR}"

  # default next-release file
  release_info_file="${BATS_TEST_TMPDIR}/RELEASE_INFO"
  cat << EOF > "${release_info_file}"
# comment
MACBROWZ_VERSION=1.2.3

# comment
mb_changeset_added=( "some notes" "more changes" )

# another comment
mb_changeset_changed=()

# comments should be ignored
mb_changeset_deprecated=()
mb_changeset_fixed=()
mb_changeset_removed=()

# comment
mb_changeset_security=()
EOF

  changelog_file="${BATS_TEST_TMPDIR}/changelog.md"
  echo '# changes' > "${changelog_file}"

  # default access token
  token_file="${BATS_TEST_TMPDIR}/token.txt"
  file_access_token="abc123"
  echo "${file_access_token}" > "${token_file}"
}

# load script and call function from it
# this uses a separate space by using 'run'
# so side effects are insulated, but one cannot read updated variable values
# for that case, load the script and directly execute the function
function callFunc() {
  loadScript
  run "$@"
}
