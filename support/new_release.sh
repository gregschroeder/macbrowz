#!/bin/bash
# update release documents and create a release
#
# uses information in ./RELEASE_INFO, which should be (manually) edited before calling this
#

current_dir="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)"
macbrowz_root=${current_dir%/*}
macbrowz_share="${macbrowz_root}/share/macbrowz"
script_lib_path="${macbrowz_share}/lib"
token_file="${macbrowz_root}/support/.gitlab-release-access-token"
release_info_file="${macbrowz_root}/support/RELEASE_INFO"
changelog_file="${macbrowz_root}/CHANGELOG.md"
macbrowz_script="${macbrowz_root}/bin/macbrowz"
releases_url="https://gitlab.com/api/v4/projects/27455148/releases"

rel_tmpdir="${TMPDIR:-/tmp}/mbrel.$$.tmp"

# load utility libraries
. "${script_lib_path}/notify.sh"

rel_cleanup() {
  rm -rf "${rel_tmpdir}"
}

usage() {
  local msg=("$@")

  local usage_msg
  usage_msg=$(
    cat << EOF
${msg[@]}

usage: new_release.sh [options]
  -t|--token <api_access_token> : API token to use

 Options:
  -l|--list : list Git log changes since last tag
  -v|--verbose : enable tracing, increments TRACE_LEVEL for each occurrence
EOF
  )

  abort "${usage_msg}"
}

parseCommandLine() {
  # expand any combined flags into individual arguments for subsequent parsing
  # eg, -abc => -a -b -c
  local expanded_args=()
  while [ $# -gt 0 ]; do
    case "$1" in
      -[A-Za-z][A-Za-z]*)
        local first_flag="${1:1:1}"
        local remaining_flags="${1:2}"
        while [ -n "${first_flag}" ]; do
          expanded_args+=("-${first_flag}")
          first_flag="${remaining_flags:0:1}"
          remaining_flags="${remaining_flags:1}"
        done
        ;;
      *) expanded_args+=("$1") ;;
    esac
    shift
  done
  set -- "${expanded_args[@]}"

  while [ $# -gt 0 ]; do
    case "$1" in
      -t | --token)
        [[ $# -le 1 || "$2" =~ ^- ]] && usage "missing value for $1"
        shift
        token="$1"
        ;;
      -l | --list)
        list_log_mode=true
        ;;
      -v | --verbose)
        # enable tracing to at least INFO level
        if [[ -z "${TRACE_LEVEL}" || $(trace_level_num) -lt 3 ]]; then
          export TRACE_LEVEL=3
        else
          trace_level_increment 1
        fi
        ;;
      --dev)
        # for internal development
        dev_mode=true
        ;;
      -*) usage "unknown flag: $1" ;;
      *) break ;;
    esac
    shift
  done

  # API access token
  if [ -z "${token}" ]; then
    if [ -s "${token_file}" ]; then
      token="$(cat "${token_file}")"
    else
      abort "no token given and token file not found: ${token_file}"
    fi
  fi

  # check that primary input file exists
  [ ! -s "${release_info_file}" ] && abort "missing release information in ${release_info_file}"

  true
}

_gitListLog() {
  git log "$(git describe --tags --abbrev=0)..HEAD"
}

_gitDiffChangelog() {
  git diff "${changelog_file}"
}

_gitRevertChanges() {
  trace_debug "reverting changes to ${release_info_file}, ${changelog_file}, ${macbrowz_script}"
  local file
  for file in "${release_info_file}" "${changelog_file}" ${macbrowz_script}; do
    git checkout -- "${file}"
  done
}

_gitCommitChanges() {
  git commit -a -m "new_release.sh: bump to version ${MACBROWZ_VERSION}" && git push
}

_gitTagRelease() {
  local tag="v${MACBROWZ_VERSION}"
  git tag -a "${tag}" -m "release ${tag}"
  git push origin "${tag}"
}

listLog() {
  _gitListLog
}

assertAllCommitted() {
  # files that are ok to change as part of a new release commit,
  # which should otherwise be standalone and separate from other changes
  local ok_pattern="RELEASE_INFO|CHANGELOG.md|README.md"
  local staged
  staged="$(git status -s | grep -v "${ok_pattern}")"

  # abort if there are any unexpected files checked out
  if [ -n "${staged}" ]; then
    if [ -n "${dev_mode}" ]; then
      echo "unexpected files still uncommitted: ${staged}"
    else
      abort "unexpected files still uncommitted: ${staged}"
    fi
  fi
  true
}

loadNextReleaseInfo() {
  trace_info "loading next release information"

  MACBROWZ_VERSION=
  mb_changeset_added=()
  mb_changeset_changed=()
  mb_changeset_deprecated=()
  mb_changeset_fixed=()
  mb_changeset_removed=()
  mb_changeset_security=()

  if [ -s "${release_info_file}" ]; then
    # shellcheck source=support/RELEASE_INFO
    . "${release_info_file}"
  else
    abort "unable to get release information from ${release_info_file}"
  fi

  trace_info "MACBROWZ_VERSION=${MACBROWZ_VERSION}
added=${mb_changeset_added[*]}
changed=${mb_changeset_changed[*]}
deprecated=${mb_changeset_deprecated[*]}
fixed=${mb_changeset_fixed[*]}
removed=${mb_changeset_removed[*]}
security=${mb_changeset_security[*]}"

  # validate, although shouldn't have been manually edited
  [ -z "${MACBROWZ_VERSION}" ] && abort "missing MACBROWZ_VERSION"
  [[ ! "${MACBROWZ_VERSION}" =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]] && abort "invalid format MACBROWZ_VERSION: ${MACBROWZ_VERSION}"

  true
}

_emitChangeset() {
  local label="$1"
  shift
  local entries=("$@")

  # section header
  if [ ${#entries[@]} -gt 0 ]; then
    cat << EOF
### ${label}
EOF
  fi

  # section items
  local entry
  for entry in "${entries[@]}"; do
    cat << EOF
- ${entry}
EOF
  done
}

updateChangelog() {
  trace_info "updating changelog"

  # check that Changelog file exists
  [ ! -s "${changelog_file}" ] && abort "missing changelog file ${changelog_file}"

  # find insertion point, save before and after chunks
  local upper=
  local lower=
  local amend_mode
  grep -q '^## \['"${MACBROWZ_VERSION}"'\]' "${changelog_file}" && amend_mode=true

  local start_line
  local next_start_line

  if [ -n "${amend_mode}" ]; then
    # existing entry to replace, so get chunks before and after, leaving it out
    start_line=$(grep -n '^## \['"${MACBROWZ_VERSION}"'\]' "${changelog_file}" | head -n 1 | awk -F: '{print $1}')
    local scan_start_line
    scan_start_line=$((start_line + 1))
    next_start_line=$(tail -n +"${scan_start_line}" "${changelog_file}" | grep -n '^## \[' | head -n 1 | awk -F: '{print $1}')
    next_start_line=$((scan_start_line + next_start_line - 1))
    ((start_line--))
  else
    # find split point
    start_line=$(grep -n '^## \[' "${changelog_file}" | head -n 1 | awk -F: '{print $1}')
    next_start_line=${start_line}
    ((start_line--))
  fi

  # read chunks before and after split points
  trace_debug "changelog split points: ${start_line},${next_start_line} amend=${amend_mode:-false}"
  read -r -d '' upper < <(sed -n "1,${start_line}p" "${changelog_file}")
  trace_debug "changelog upper:
${upper}"
  [ -n "${next_start_line}" ] && read -r -d '' lower < <(sed -n "${next_start_line}"',$p' "${changelog_file}")
  trace_debug "changelog lower:
${lower}"

  # create new changelog
  cat << EOF > "${changelog_file}"
${upper}

EOF

  # emit changeset text
  mkdir -p "${rel_tmpdir}"
  local mb_changeset_file="${rel_tmpdir}/changeset.txt"
  {
    cat << EOF
## [${MACBROWZ_VERSION}] - $(date +%Y-%m-%d)
EOF

    _emitChangeset Added "${mb_changeset_added[@]}"
    _emitChangeset Changed "${mb_changeset_changed[@]}"
    _emitChangeset Deprecated "${mb_changeset_deprecated[@]}"
    _emitChangeset Fixed "${mb_changeset_fixed[@]}"
    _emitChangeset Removed "${mb_changeset_removed[@]}"
    _emitChangeset Security "${mb_changeset_security[@]}"
  } >> "${mb_changeset_file}"

  # save as variable and concat to changelog
  read -r -d '' mb_changeset_text < <(cat "${mb_changeset_file}")
  cat "${mb_changeset_file}" >> "${changelog_file}"

  # emit previous changelog entries
  cat << EOF >> "${changelog_file}"

${lower}
EOF
}

resetNextRelease() {
  trace_info "incrementing version for next release and resetting values"

  # update main script
  sed -i '' -e 's/^\(MACBROWZ_VERSION=\).*/\1'"${MACBROWZ_VERSION}"'/' "${macbrowz_script}"

  # parse
  local major minor patch
  IFS=. read -r major minor patch <<< "${MACBROWZ_VERSION}"
  trace_debug "current major: ${major}, minor: ${minor}, patch: ${patch}"

  # bump
  ((patch++))

  trace_debug "updated major: ${major}, minor: ${minor}, patch: ${patch}"
  local next_version="${major}.${minor}.${patch}"

  # reset next-release file
  sed -i '' -e 's/^\(MACBROWZ_VERSION=\).*/\1'"${next_version}"'/' "${release_info_file}"
  local changeset
  for changeset in added changed deprecated fixed removed security; do
    # empty out arrays
    sed -i '' -e "/^mb_changeset_${changeset}/{"'
:loop
s/(.*)/()/
t
N
b loop
}' "${release_info_file}"
  done
}

commitUpdate() {
  trace_info "committing file changes"

  PS3="Select the operation: "
  local cmd_changelog="CHANGELOG.md changes"
  local cmd_review="REVIEW commits since last release"
  local cmd_commit="APPROVE changes and commit release ${MACBROWZ_VERSION}"
  local cmd_quit="QUIT but leave changed files"
  local cmd_abort="REVERT changes and abort"
  local select_options=("${cmd_changelog}" "${cmd_review}" "${cmd_commit}" "${cmd_quit}" "${cmd_abort}")

  COLUMNS=1
  select opt in "${select_options[@]}"; do
    case "${opt}" in
      "${cmd_changelog}")
        _gitDiffChangelog
        ;;
      "${cmd_review}")
        _gitListLog
        ;;
      "${cmd_commit}")
        break
        ;;
      "${cmd_quit}")
        abort "changes made but not committed, process aborted"
        ;;
      "${cmd_abort}")
        _gitRevertChanges
        abort "changes reverted, process aborted"
        ;;
      *)
        echo "Invalid selection ${REPLY}"
        ;;
    esac
  done

  trace_info "changes approved, committing changes"
  _gitCommitChanges
}

tagRelease() {
  trace_info "tagging release"
  _gitTagRelease
}

createRelease() {
  trace_info "creating release"

  # write API payload into file
  local datafile="${rel_tmpdir}/reldata.txt"
  local description
  description="$(echo "${mb_changeset_text}" | sed -e 1d -e '$!s/$/\\n/' -e 's/"/\\"/g' | tr -d '\n')"
  cat << EOF > "${datafile}"
{
  "name": "Version ${MACBROWZ_VERSION}",
  "tag_name": "v${MACBROWZ_VERSION}",
  "description": "${description}"
}
EOF

  # assemble API call command
  local curl_command=(
    'curl'
    '-i'
    '-s'
    '--header'
    'Content-Type: application/json'
    '--header'
    "PRIVATE-TOKEN: ${token}"
    '--data'
    "@${datafile}"
    '--request'
    'POST'
    "${releases_url}"
  )

  # execute call
  trace_debug "calling create release API: ${curl_command[*]}"
  local result
  result="$("${curl_command[@]}" 2>&1)"
  if [[ ! "${result}" =~ HTTP/2\ 20[01] || \
        ! "${result}" =~ \"tag_name\":\"v${MACBROWZ_VERSION}\" ]]; then
    abort "Create release API failed ${result}"
  fi
}

main() {
  parseCommandLine "${@}"

  if [ -n "${list_log_mode}" ]; then
    listLog
  else
    assertAllCommitted
    loadNextReleaseInfo
    updateChangelog
    resetNextRelease
    commitUpdate
    tagRelease
    createRelease
  fi
}

# Execute main() if this is run in standalone mode (i.e. not in a unit test)
if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  # trap borks bats tests, so do not enable during testing
  trap rel_cleanup EXIT
  main "$@"
fi
