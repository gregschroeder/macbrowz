# Macbrowz [Change Log](https://changelog.md/)
This project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.19] - 2022-09-27
### Added
- add mk_ssb script
- add ssbmanage script
- add upgrade_ssb_brave script
- add enumerate
- add open_ssb

## [0.1.18] - 2022-07-29
### Added
- add support for Microsoft Edge

## [0.1.17] - 2021-12-31

## [0.1.16] - 2021-12-31
### Fixed
- explicitly add URL scheme handlers, hoping to register properly as browser and also get Mission Control/Spaces to behave (no luck there yet)

## [0.1.15] - 2021-12-31
### Fixed
- use user-agent without Chrome version to avoid Google login blocking

## [0.1.14] - 2021-12-29
### Fixed
- handle chars in configSave

## [0.1.13] - 2021-12-23
### Added
- add --zipTemplate and --unzipTemplate commands to help manage template archives

## [0.1.12] - 2021-12-23
### Fixed
- improve updater flow, existence checks for edit modes

## [0.1.11] - 2021-12-23
### Fixed
- update script path in launcher

## [0.1.10] - 2021-12-21
### Fixed
- escape "doublequotes" in release API payload

## [0.1.9] - 2021-12-21
### Added
- add --template support for starting with a base template app
- add --force flag for --remove
- add --updateAll flag to update all Macbrowz SSB apps that are found by --list
### Changed
- only refresh application on launch if not already running
### Fixed
- inner app should not open another tab with start page if "where last left off" set
- do not follow symlinks into main engine installation for xattr
- do not allow multiple standalone flags
- avoid cp errors if icons identical
- regenerate from default icon if no other alternative found
- fix unit test from running git commands
- fix icon and bundle name issues on edit

## [0.1.8] - 2021-10-22
### Fixed
- preserve inner app icon on browser engine update

## [0.1.7] - 2021-10-20
### Fixed
- more version handling fixes

## [0.1.6] - 2021-10-20
### Changed
- version handling updated for brew install

## [0.1.5] - 2021-10-20
### Added
- add --version

## [0.1.4] - 2021-10-20

## [0.1.3] - 2021-10-20
### Added
- add make install/uninstall for brew support

## [0.1.2] - 2021-10-20
### Added
- Automated release creation

## [0.1.1] - 2021-10-19
### Added
- Manage versioning, release notes/CHANGELOG generation mechanism
- Add scripted release creation

## [0.1.0] - 2021-10-18
### Changed
- Initial release
