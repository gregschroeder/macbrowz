# Macbrowz

## Overview

Macbrowz lets you create web-based Mac applications as single-site browsers 
based on Google Chrome or Brave Browser.

## Important Notes

- See [CHANGELOG.md](./CHANGELOG.md) for recent changes.

- Don't click the "Update Now" button from the base browser in your apps. 
It will update to match the level of the installed corresponding browser on launch.

- It's a good idea to back up your Macbrowz apps (default in ```/Applications/Macbrowz/Apps```) 
and their app data (default in ```~/Library/Application Support/Macbrowz/Apps/<AppID>```).


## Technical Information & Limitations

- Apps built with Macbrowz are self-updating with browser updates.
- Currently, there is no extension or other mechanism to intercept links and force them to redirect to a particular app.

## Requirements

- Requires the open source [Brave Browser](https://github.com/brave/brave-browser "Brave Browser") 
or [Google Chrome](https://www.google.com/chrome "chrome").

- To (re)build the inner launcher application requires [Platypus](https://github.com/sveinbjornt/Platypus "Platypus") by [sveinbjornt](https://github.com/sveinbjornt "sveinbjornt").

## Installation

- Clone or download and unpack a [release](https://gitlab.com/gregschroeder/macbrowz/-/releases) package

`make install`

- Override installation root location with `prefix` variable

`make prefix=/tmp/example install`

## Acknowledgements

- Macbrowz was inspired by [Epichrome](https://github.com/dmarmor/epichrome "epichrome") by [David Marmor](https://github.com/dmarmor "dmarmor").
- Built using [![JetBrains](https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg)](https://www.jetbrains.com/?from=macbrowz)
